%%
%%  **************************************
%%  ***   DUNGEON  MONKEY  UNLIMITED   ***
%%  ***         WEAPONS  FILE          ***
%%  **************************************
%%

Sword
	name <Shortsword>
	desc <This weapon does 1-6 points of slashing damage.>
	Damage 1d6
	ELEMENT_Slashing
	AvatarFrame 14
	mass 3

Sword
	name <Longsword>
	desc <This weapon does 1-8 points of slashing damage.>
	Damage 1d8
	ELEMENT_Slashing
	AvatarFrame 0
	mass 8

Sword
	name <Broadsword>
	desc <This weapon does 1-8 points of slashing damage.>
	Damage 1d8
	ELEMENT_Slashing
	AvatarFrame 18
	mass 9

Sword
	name <Bastard Sword>
	desc <This weapon does 1-10 points of slashing damage.>
	Damage 1d10
	ELEMENT_Slashing
	AvatarFrame 12
	mass 11

Sword
	name <Scimitar>
	desc <This weapon does 1-8 points of slashing damage.>
	Damage 1d8
	ELEMENT_Slashing
	AvatarFrame 17
	mass 7
	GPFudge 5

Axe
	name <Hand Axe>
	desc <This weapon does 1-6 points of slashing damage.>
	Damage d6
	ELEMENT_Slashing
	AvatarFrame 11
	mass 10
	GPFudge -5

Axe
	name <War Axe>
	desc <This weapon does 1-8 points of slashing damage.>
	Damage d8
	ELEMENT_Slashing
	AvatarFrame 1
	mass 14
	GPFudge -5

Mace
	name <Flanged Mace>
	desc <This weapon does 1-6 points of crushing damage.>
	Damage d6
	ELEMENT_Crushing
	AvatarFrame 2
	mass 8

Mace
	name <Morning Star>
	desc <This weapon does 1-8 points of crushing damage.>
	Damage d8
	ELEMENT_Crushing
	AvatarFrame 13
	mass 11

Mace
	name <Club>
	desc <This weapon does 1-6 points of crushing damage.>
	Damage d6
	ELEMENT_Crushing
	AvatarFrame 8
	mass 6

Mace
	name <Light Hammer>
	desc <This weapon does 1-4 points of crushing damage.>
	Damage d4
	ELEMENT_Crushing
	AvatarFrame 10
	mass 4

Mace
	name <Warhammer>
	desc <This weapon does 1-8 points of crushing damage.>
	Damage d8
	ELEMENT_Crushing
	AvatarFrame 9
	mass 9

Dagger
	name <Dagger>
	desc <This weapon does 1-4 points of slashing damage.>
	Damage d4
	ELEMENT_Piercing
	AvatarFrame 3
	mass 2

Dagger
	name <Sickle>
	desc <This weapon does 1-6 points of slashing damage.>
	Damage d6
	ELEMENT_Slashing
	AvatarFrame 16
	mass 2
	GPFudge 5

Staff
	name <Staff>
	desc <This weapon does 1-4 points of crushing damage.>
	Damage d4
	ELEMENT_Crushing
	AvatarFrame 15
	mass 4

Staff
	name <Quarterstaff>
	desc <This two-handed weapon does 1-4 points of crushing damage and gives a +5% bonus to defense.>
	Damage d4
	ELEMENT_Crushing
	TwoHanded
	AvatarFrame 4
	PhysDef 5
	mass 6

Staff
	name <Iron Staff>
	desc <This two-handed weapon does 1-8 points of crushing damage and gives a +5% bonus to defense.>
	Damage d8
	ELEMENT_Crushing
	TwoHanded
	AvatarFrame 21
	PhysDef 5
	mass 20

Polearm
	name <Spear>
	desc <This weapon does 1-8 points of piercing damage.>
	Damage d8
	ELEMENT_Piercing
	AvatarFrame 5
	mass 10

Polearm
	name <Trident>
	desc <This weapon does 1-10 points of piercing damage.>
	Damage d10
	ELEMENT_Piercing
	AvatarFrame 23
	mass 11

Polearm
	name <Pike>
	desc <This two-handed weapon does 1-10 points of piercing damage.>
	Damage d10
	ELEMENT_Piercing
	AvatarFrame 22
	mass 13
	GPFudge -5
	TwoHanded

Polearm
	name <Halberd>
	desc <This two-handed weapon does 2-12 points of slashing damage.>
	Damage 2d6
	ELEMENT_Slashing
	AvatarFrame 20
	mass 17
	TwoHanded

Bow
	name <Shortbow>
	desc <This weapon does 1-6 points of piercing damage up to a range of 8 tiles.>
	Damage d6
	ELEMENT_Piercing
	WeaponRange 8
	AvatarFrame 6
	mass 6

Bow
	name <Longbow>
	desc <This weapon does 1-8 points of piercing damage up to a range of 8 tiles.>
	Damage d8
	ELEMENT_Piercing
	WeaponRange 8
	AvatarFrame 19
	mass 7

Sling
	name <Sling>
	desc <This weapon does 1-4 points of crushing damage up to a range of 6 tiles.>
	Damage d4
	ELEMENT_Crushing
	WeaponRange 6
	AvatarFrame 7
	mass 2

%%
%%  **********************
%%  ***   AMMUNITION   ***
%%  **********************
%%

Arrow
	name <Arrows>
	desc <The ammunition for bows.>
	Quantity 20
	NoAvatar

Bullet
	name <Bullets>
	desc <The ammunition for slings.>
	Quantity 20
	NoAvatar






