%%
%%  **************************************
%%  ***   DUNGEON  MONKEY  UNLIMITED   ***
%%  ***         SHIELD  FILE           ***
%%  **************************************
%%
%% Shields provide up to 15 points of protection.
%%

Shield
	name <Buckler>
	desc <>
	AvatarFrame 0
	PhysDef 5
	mass 7

Shield
	name <Eternia Shield>
	desc <>
	AvatarFrame 3
	PhysDef 10
	mass 25

Shield
	name <Kite Shield>
	desc <>
	AvatarFrame 10
	PhysDef 10
	mass 20

Shield
	name <Tower Shield>
	desc <>
	AvatarFrame 28
	PhysDef 15
	mass 40

