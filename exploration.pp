unit exploration;
{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}


interface

uses sdlgfx,gamebook,gears,sdlmap;

Function ExplorationMode( GB: GameBoardPtr ): Integer;

implementation

uses 	uiconfig,hotmaps,equipment,gearparser,effects,frontend,combat,
	joescript,texutil,sdlmenus,monsters,services;

var
	ERDP_GB: GameBoardPtr;

Procedure ExplorationRedraw;
	{ Do a redraw for some menu or whatnot. }
	{ Make sure to assign ERDP_GB beforehand, otherwise bad things will happen. }
begin
	BasicRedraw( ERDP_GB );
end;


Function MoveToDestination( GB: GameBoardPtr; N , X1 , Y1: Integer ): Boolean;
	{ N = Party Slot }
	{ X1,Y1 = Destination }
	{ Attempt to move towards point X1,Y1. If the point has been reached }
	{ or is impossible to reach, return TRUE. Otherwise return FALSE. }
	Function ALaterModel( Target: GearPtr ): Boolean;
		{ If TARGET is a member of the party, and later in }
		{ marching order than N, then N can move through him. }
		{ This function checks to see if that's the case. }
	var
		Found: Boolean;
		N2: Integer;
	begin
		{ If N is at the end, TARGET can't come first. }
		if N >= ( Num_Party_Members ) then begin
			ALaterModel := False;
		end else begin
			{ Assume FALSE, then search the rest. }
			Found := False;
			for N2 := ( N + 1 ) to Num_Party_Members do begin
				if G_Party[ N2 ] = Target then Found := True;
			end;
			ALaterModel := Found;
		end;
	end;
var
	MoveD,Best,D,X0,Y0,X,Y: Integer;
	target: GearPtr;
begin
	X0 := NAttValue( G_Party[n]^.NA , NAG_Location , NAS_X );
	Y0 := NAttValue( G_Party[n]^.NA , NAG_Location , NAS_Y );

	if ( X1 = X0 ) and ( Y1 = Y0 ) then begin
		MoveToDestination := True;
	end else if not OnTheMap( GB , X1 , Y1 ) then begin
		MoveToDestination := True;
	end else begin
		CreatePointHotMap( GB , X1 , Y1 , False );

		{ See if there's a good direction to move in. }
		MoveD := 0;
		Best := HotMap[ X0 , Y0 ];
		for D := 1 to 9 do begin
			X := X0 + VecDir[ D , 1 ];
			Y := Y0 + VecDir[ D , 2 ];
			if OnTheMap( GB , X , Y ) and ( HotMap[ X , Y ] < Best ) and ( D <> 5 ) then begin
				target := FindModelXY( GB , X , Y );
				if ( target = Nil ) then begin
					MoveD := D;
					Best := HotMap[ X , Y ];
				end else if ( X = X1 ) and ( Y = Y1 ) then begin
					MoveD := D;
					Best := 0;
				end else if ALaterModel( target ) then begin
					MoveD := D;
					Best := HotMap[ X , Y ];
				end;
			end;
		end;

		{ Next it's time to move the model. If there's a model in the way, }
		{ check to see if it's a later model or the destination tile. }
		if ( MoveD >= 1 ) and ( MoveD <= 9 ) then begin
			target := FindModelXY( GB , X0 + VecDir[ MoveD , 1 ] , Y0 + VecDir[ MoveD , 2 ] );
			if TileBlocksMovement( GB , X0 + VecDir[ MoveD , 1 ] , Y0 + VecDir[ MoveD , 2 ] ) then begin
				BumpTile( GB , G_Party[ n ] , X0 + VecDir[ MoveD , 1 ] , Y0 + VecDir[ MoveD , 2 ] );
				MoveToDestination := True;

			end else if ( Target = Nil ) or ALaterModel( target ) then begin
				if target <> Nil then begin
					SetNAtt( Target^.NA , NAG_Location , NAS_X , X0 );
					SetNAtt( Target^.NA , NAG_Location , NAS_Y , Y0 );
				end;

				AddNAtt( G_Party[n]^.NA , NAG_Location , NAS_X , VecDir[ MoveD , 1 ] );
				AddNAtt( G_Party[n]^.NA , NAG_Location , NAS_Y , VecDir[ MoveD , 2 ] );
				UpdatePCPosition( GB , G_Party[n] );
				MoveToDestination := False;
			end else begin
				BumpAction( GB, G_Party[n], Target );
				MoveToDestination := True;
			end;
		end else begin
			MoveToDestination := True;
		end;
	end;
end;

Function MoveParty( GB: GameBoardPtr; DX , DY: Integer ): Boolean;
	{ Move the party to the destination. }
	{ DX,DY refers to the destination, not delta values. I'm just copying the code }
	{ from MonkeyTactics and want to blame myself for bad naming... }
var
	T,tt,X0,Y0: Integer;
	MP,First: Boolean;
	PC: GearPtr;
begin
	PC := FirstActivePC( GB );
	if ( DX = NAttValue( PC^.NA , NAG_Location , NAS_X ) ) and ( DY = NAttValue( PC^.NA , NAG_Location , NAS_Y ) ) then begin
		MoveParty := True;
	end else if not OnTheMap( GB , DX , DY ) then begin
		MoveParty := True;
	end else begin
		First := True;
		for t := 1 to Num_Party_Members do begin
			if ( G_Party[t] <> Nil ) and OnTheMap( GB , G_Party[t] ) then begin
				X0 := NAttValue( G_Party[t]^.NA , NAG_Location , NAS_X );
				Y0 := NAttValue( G_Party[t]^.NA , NAG_Location , NAS_Y );
				if First then begin
					MP := MoveToDestination( GB , T , DX , DY );
					First := False;
				end else begin
					{ Models other than the first will move multiple times }
					{ every turn in order to "catch up" with the leader. }
					tt := 3;
					while ( TT > 0 ) and not MoveToDestination( GB , T , DX , DY ) do dec( TT );
				end;
				DX := X0;
				DY := Y0;
			end;
		end;
		MoveParty := MP;
	end;
end;

Procedure UpdateMonsters( GB: GameBoardPtr );
	{ Go through all of the monsters on the board. See if any of them have a LOS to }
	{ the first PC. If they do, roll to see if they become activated. }
var
	M: GearPtr;
	X,Y: Integer;
	aa: effect_stencil;	{ Activation Area }
begin
	M := FirstActivePC( GB );
	if M <> Nil then begin
		X := NAttValue( M^.NA , NAG_Location , NAS_X );
		Y := NAttValue( M^.NA , NAG_Location , NAS_Y );

		{ ASSERT: X,Y is on the map. }
		Plot_Circular_Area( GB , aa , X , Y , 5 );
		M := GB^.Contents;
		while M <> Nil do begin
			if ( M^.G = GG_Model ) and ( M^.S = GS_EnemyTeam ) and ( M^.V = GV_Inactive ) then begin
				X := NAttValue( M^.NA , NAG_Location , NAS_X );
				Y := NAttValue( M^.NA , NAG_Location , NAS_Y );
				if OnTheMap( GB , X , Y ) and aa[ X , Y ] then begin
					ActivateModel( GB , M );
				end;
			end;
			M := M^.Next;
		end;
	end;
end;

Procedure DebuggingConsole( GB: GameBoardPtr );
	{ Allow the player to manually enter a script to execute. }
var
	event: String;
begin
	ERDP_GB := GB;
	event := GetStringFromUser( 'WARNING: HACK IN PROGRESS' , @ExplorationRedraw );

	if event <> '' then begin
		InvokeEvent( GB , event , GB^.Scene , event );
	end;
end;

Procedure SaveCampaign( Camp: CampaignPtr );
	{ Save the current campaign. }
var
	F: Text;
	Name: String;
begin
	Name := Save_Game_Directory + 'rpg_' + GearName( Camp^.Source ) + '.txt';
	Assign( F , Name );
	Rewrite( F );
	WriteCampaign( Camp , F );
	Close( F );
end;

Procedure PickupItem( GB: GameBoardPtr; X,Y: Integer );
	{ There's apparently something on the map right here. }
	{ Attempt to pick it up and give it to the least encumbered PC. }
var
	Item,I: GearPtr;
	msg: String;
	N,T: Integer;
begin
	{ Attempt to locate the item. }
	Item := Nil;
	I := GB^.Contents;
	while ( Item = Nil ) and ( I <> Nil ) do begin
		if ( I^.G = GG_Item ) and ( X = NAttValue( I^.NA , NAG_Location , NAS_X ) ) and ( Y = NAttValue( I^.NA , NAG_Location , NAS_Y ) ) then Item := I;
		I := I^.Next;
	end;

	{ If there's an item here, attempt to pick it up. }
	if Item <> Nil then begin
		{ Locate the least encumbered PC. }
		N := 0;
		for t := 1 to Num_Party_Members do begin
			if ( G_Party[ t ] <> Nil ) and IsAlright( G_Party[ t ] ) and CanTakeMoreItems( G_Party[ T ] ) then begin
				if N = 0 then N := T
				else if EncumberanceLevel( G_Party[ T ] ) < EncumberanceLevel( G_Party[ N ] ) then N := T;
			end;
		end;
		if N <> 0 then begin
			DelinkGear( GB^.Contents , Item );
			InsertInvCom( G_Party[ N ] , Item );
			msg := ReplaceHash( MsgString( '#got#' ) , GearName( G_Party[ N ] ) );
			msg := ReplaceHash( msg , GearName( Item ) );
			Alert( GB , msg );
		end else begin
			Alert( GB , MsgString( 'PartyNoEncumberance' ) );
		end;
	end;
end;

Function PCUnderMouse( GB: GameBoardPtr ): Integer;
	{ Return the party slot of the PC currently being pointed at. If there }
	{ is no PC beneath the mouse, return 0. }
var
	PC,t: Integer;
begin
	if not OnTheMap( GB , Tile_X , Tile_Y ) then Exit( 0 );
	PC := 0;
	for t := 1 to Num_Party_Members do begin
		if ( G_Party[ t ] <> Nil ) and ( NAttValue( G_Party[t]^.NA , NAG_Location , NAS_X ) = tile_x ) and ( NAttValue( G_Party[t]^.NA , NAG_Location , NAS_Y ) = tile_y ) then begin
			PC := T;
		end;
	end;
	PCUnderMouse := PC;
end;


Procedure AmbushParty( GB: GameBoardPtr; MReq: String; Lvl, Strength: Integer );
	{ Construct an ambush. Place it right on top of the party. Allow }
	{ hilarity to ensue. }
var
	Monsters,M,PC: GearPtr;
	P: Point;
	Tries: Integer;
begin
	{ Build the encounter. }
	Monsters := BuildEncounter( MReq + ' ' + MonsterContext( GB^.Scene ) , Lvl , Strength );
	if Monsters = Nil then Alert( GB , 'ERROR: No monsters generated for ' + BStr( Lvl ) + '/' + Bstr( Strength ) );

	{ Deploy it near the party. }
	Tries := 0;
	while ( Monsters <> Nil ) and ( Tries < 200 ) do begin
		PC := RandomPC( GB );
		if PC <> Nil then begin
			P := FindPointNearModel( GB , PC );
			if ( P.X <> NAttValue( PC^.NA , NAG_Location , NAS_X ) ) or ( P.Y <> NAttValue( PC^.NA , NAG_Location , NAS_Y ) ) then begin
				M := Monsters;
				DelinkGear( Monsters , M );
				AppendGear( GB^.Contents , M );
				SetNAtt( M^.NA , NAG_Location , NAS_X , P.X );
				SetNAtt( M^.NA , NAG_Location , NAS_Y , P.Y );
			end;
		end;
		Inc( Tries );
	end;

	{ If we have monsters left over, dispose of them. }
	if Monsters <> Nil then begin
		DisposeGear( Monsters );
	end;
end;

Procedure AttemptFieldRest( GB: GameBoardPtr );
	{ The PCs need to rest. This might go well, or it might attract }
	{ monsters. In either case sleeping in the field doesn't restore }
	{ luck damage. }
var
	N,Luck: Integer;
	Original_Luck_Damage: Array [1..Num_Party_Members] of Integer;
begin
	{ Step One: Make the Luck roll to avoid monsters. }
	Luck := 1 + NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_FieldCampBonus );
	for N := 1 to Num_Party_Members do if IsAlright( G_Party[ N ] ) then Luck := Luck + CStat( G_Party[N] , Stat_Luck ) - NAttValue( G_Party[ N ]^.NA , NAG_Damage , NAS_LuckDmg );

	Alert( GB , MsgString( 'FIELDCAMP_Start' ) );

	{ Step Two: Depending on the roll, either rest or deploy a bunch }
	{ of active monsters. }
	if Random( 100 ) < Luck then begin
		{ Record the original Luck Damage. }
		for N := 1 to Num_Party_Members do if IsAlright( G_Party[ N ] ) then Original_Luck_Damage[N] := NAttValue( G_Party[ N ]^.NA , NAG_Damage , NAS_LuckDmg );

		RestTheNight( GB );
		Alert( GB , MsgString( 'FIELDCAMP_Success' ) );

		{ Restore the Luck Damage. }
		for N := 1 to Num_Party_Members do if IsAlright( G_Party[ N ] ) then SetNAtt( G_Party[ N ]^.NA , NAG_Damage , NAS_LuckDmg , Original_Luck_Damage[N] );
	end else begin
		{ Oh, dear. Looks like your luck has run out... time to }
		{ get ambushed. }
		Alert( GB , MsgString( 'FIELDCAMP_Failure' ) );
		AmbushParty( GB , '' , AveragePCLevel , 30 + Random( 50 ) );
		AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_FieldCampBonus , 25 );
	end;
end;

Procedure BrowseHistory( GB: GameBoardPtr );
	{ As the PC advances throughout the campaign, she will likely }
	{ accumulate a number of history messages. This procedure will }
	{ allow those messages to be browsed. }
var
	HList,SA: SAttPtr;
begin
	HList := Nil;
	SA := GB^.Camp^.Source^.SA;
	while SA <> Nil do begin
		if UpCase( Copy( SA^.Info , 1 , 7 ) ) = 'HISTORY' then begin
			StoreSAtt( HList , RetrieveAString( SA^.Info ) );
		end;
		SA := SA^.Next;
	end;

	if HList <> Nil then begin
		MoreText( HList , 1 );
		DisposeSAtt( HList );
	end;
end;


Function OpenExplorationMenu( GB: GameBoardPtr ): Integer;
	{ The PC just clicked the right mouse button. Don't just stand there, }
	{ do something about it!!! }
var
	PC,N,Cmd: Integer;
	RPM: RPGMenuPtr;
	it: GearPtr;
begin
	{ If the mouse is pointing at a party member, cast a spell }
	{ with that member. }
	PC := PCUnderMouse( GB );
	if PC <> 0 then begin
		PCSelectInvocation( GB, G_Party[ PC ] );
		OpenExplorationMenu := CMD_Whatever;
	end else begin
		Cmd := CMD_Whatever;

		{ Not pointing at a PC... create a menu, then do what it says. }
		RPM := OpenPopupMenu;

		AddRPGMenuItem( RPM , MsgString( 'EXM_Focus' ) , 1 );
		if PartyNeedsRest( GB ) then AddRPGMenuItem( RPM , MsgString( 'EXM_Camp' ) , 2 );
		AddRPGMenuItem( RPM , MsgString( 'EXM_History' ) , 5 );
		AddRPGMenuItem( RPM , MsgString( 'EXM_QuitGame' ) , 4 );

		AddRPGMenuItem( RPM , '-----' , -1 );

		for PC := 1 to Num_Party_Members do begin
			if ( G_Party[ PC ] <> Nil ) and IsAlright( G_Party[ PC ] ) then begin
				AddRPGMenuItem( RPM , ReplaceHash( MsgString( 'PC_Cast_Spell' ) , GearName( G_Party[ PC ] ) ) , PC + 1000 );
			end;
		end;
		AddRPGMenuItem( RPM , '-----' , -1 );
		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		N := QueryPopupMenu( GB , RPM );
		DisposeRPGMenu( RPM );

		if N = 1 then begin
			it := FirstActivePC( GB );
			if it <> Nil then begin
				FocusOnTile( GB , NAttValue( it^.NA , NAG_Location , NAS_X ), NAttValue( it^.NA , NAG_Location , NAS_Y ) );
			end;
		end else if N = 2 then begin
			AttemptFieldRest( GB );
			Cmd := CMD_Whatever;
		end else if N = 4 then begin
			Cmd := CMD_Quit;
		end else if N = 5 then begin
			BrowseHistory( GB );
		end else if N > 1000 then begin
			PCSelectInvocation( GB, G_Party[ N - 1000 ] );
		end;

		OpenExplorationMenu := Cmd;
	end;
end;

Function ExplorationMode( GB: GameBoardPtr ): Integer;
	{ The party is exploring, not fighting. }
var
	T,Command,DX,DY,Tries: Integer;
	gdi: Char;
	PC: GearPtr;
begin
	{ Start every exploration with a START trigger. }
	StoreSAtt( G_Triggers , TRIGGER_Start );

	{ Initialize values. }
	Command := CMD_Whatever;
	G_Destination := 0;

	InitGraphicsForScene( GB );

	{ Update the PC's field of view. }
	for t := 1 to Num_Party_Members do if G_Party[t] <> Nil then UpdatePCPosition( GB , G_Party[T] );
	RPGKey;

	{ Initialize the display variables. }
	PC := FirstActivePC( GB );
	if PC <> Nil then begin
		FocusOnTile( GB , NAttValue( PC^.NA , NAG_Location , NAS_X ), NAttValue( PC^.NA , NAG_Location , NAS_Y ) );
	end else begin
		Command := CMD_Quit;
	end;

	repeat
		{ Start each iteration with a refresh and a redraw. }
		ClearUnderlays;
		BasicRedraw( GB );
		DoFlip;

		{ If we've got a command to work on, do that now. }
		{ Otherwise input a new command from the user. }
		if ( Command = 0 ) and not ShouldEnterCombat( GB ) then begin
			{ Wait for a keypress or mousepress. }
			while Command = 0 do begin
				ClearUnderlays;
				PC := FirstActivePC( GB );
				if PC <> Nil then begin
					IndicateTile( GB , NAttValue( PC^.NA , NAG_Location , NAS_X ), NAttValue( PC^.NA , NAG_Location , NAS_Y ) , IT_PlayerModel );
				end;
				if OnTheMap( GB , tile_X , tile_Y ) then IndicateTile( GB , tile_x , tile_y , IT_Cursor );
				BasicRedraw( GB ); 

				{ Show information on the model under the mouse. }
				DoFlip;

				gdi := RPGKey;

				if gdi = KeyMap[ KMC_Quit ].kcode then begin
					Command := CMD_Quit;

				end else if gdi = KeyMap[ KMC_Center ].kcode then begin
					PC := FirstActivePC( GB );
					if PC <> Nil then begin
						FocusOnTile( GB , NAttValue( PC^.NA , NAG_Location , NAS_X ), NAttValue( PC^.NA , NAG_Location , NAS_Y ) );
					end;

				end else if gdi = KeyMap[ KMC_Rest ].kcode then begin
					if PartyNeedsRest( GB ) then begin
						AttemptFieldRest( GB );
						Command := CMD_Whatever;
					end else Alert( GB , MsgString( 'NotSleepy' ) );

				end else if gdi = KeyMap[ KMC_MiniMap ].kcode then begin
					OpenMiniMap( GB );

				end else if gdi = KeyMap[ KMC_History ].kcode then begin
					BrowseHistory( GB );

				end else if ( gdi = '1' ) and ( G_Party[1] <> Nil ) then begin
					InventoryScreen( G_Party[1], GB );

				end else if ( gdi = '2' ) and ( G_Party[2] <> Nil ) then begin
					InventoryScreen( G_Party[2], GB );

				end else if ( gdi = '3' ) and ( G_Party[3] <> Nil ) then begin
					InventoryScreen( G_Party[3], GB );

				end else if ( gdi = '4' ) and ( G_Party[4] <> Nil ) then begin
					InventoryScreen( G_Party[4], GB );

				end else if gdi = 'X' then begin
					SaveCampaign( GB^.Camp );

				end else if gdi = RPK_MouseButton then begin
					{ We've clicked on the map. If we've clicked on the PC's tile, we don't want to }
					{ move... maybe we want to pick up something. }
					if ( Tile_X = NAttValue( PC^.NA , NAG_Location , NAS_X ) ) and ( Tile_Y = NAttValue( PC^.NA , NAG_Location , NAS_Y ) ) then begin
						PickupItem( GB , Tile_X , Tile_Y );
					end else begin
						Command := CMD_Move;
						DX := Tile_X;
						DY := Tile_Y;
						Tries := 300;
					end;
				end else if ( gdi = '#' ) and Wizard_On then begin
					DebuggingConsole( GB );

				end else if gdi = RPK_RightButton then begin
					{ Use one of the PC's powers. Not all powers can necessarily }
					{ be used outside of combat. }
					Command := OpenExplorationMenu( GB );
				end;
			end;

		end else begin
			{ If the PC has a command pending, process it here. }
			if Command = CMD_Move then begin
				if MoveParty( GB , DX , DY ) then Command := 0;
				Dec( Tries );
				if Tries = 0 then Command := 0;

			end else begin
				{ Unknown commands get deleted. }
				Command := 0;
			end;
			AnimDelay;
		end;

		{ After processing the PC's stuff, process the monsters. }
		UpdateMonsters( GB );

		HandleTriggers( GB );

		{ Finally, if the party is within range of hostiles, }
		{ start combat. }
		if ShouldEnterCombat( GB ) then begin
			Command := CombatMode( GB );
		end;

	until Should_Exit_Scene or ( Command = CMD_Quit );

	{ Upon quitting, save the game. }
	if Command = CMD_Quit then SaveCampaign( GB^.Camp );

	ExplorationMode := Command;
end;



end.
