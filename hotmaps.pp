unit hotmaps;
	{ This unit holds the hotmaps used for pathfinding. Maybe someday I'll }
	{ spring for fancy efficient algorthms, but as long as it works I'm }
	{ not complaining. }

{	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}


interface

uses gears,gamebook;


var
	HotMap: Array [ 1..MaxMapWidth , 1..MaxMapWidth ] of Integer;
		{ Contains pathfinding data so monsters can locate the PC. }
		{ Since I'm using mouse control, also so the PC can locate the monsters... }
	HotCost: Array [ 1..MaxMapWidth , 1..MaxMapWidth ] of Integer;
		{ Holds movement costs for hotmap calculations. }
	HotBlock: Array [ 1..MaxMapWidth , 1..MaxMapWidth ] of Boolean;
		{ Holds inaccessable positions for hotmap calculations. }


Procedure ProcessHotMap( GB: GameBoardPtr );
Procedure CreatePointHotMap( GB: GameBoardPtr; X0,Y0: Integer; IncludeModels: Boolean );
Procedure CreateTeamHotMap( GB: GameBoardPtr; M: GearPtr );


implementation

Procedure ProcessHotMap( GB: GameBoardPtr );
	{ This pathfinding map is based on the model suggested by Isaac Kuo in }
	{ rec.games.roguelike.development; if you want more details on how it works }
	{ I'd advise you to head to groups.google.com and try to locate the thread. }
var
	X,Y,DH,DV,DD,DP: Integer;
	Flag: Boolean;
begin
	{ Persecute the floodfill to its fullest extent. }
	flag := True;
	while flag do begin
		flag := False;
		for y := 2 to ( GB^.map_height - 1 ) do begin
			for x := 2 to ( GB^.map_width - 1 ) do begin
				if not ( TileBlocksMovement( GB , X , Y ) or HotBlock[ X , Y ] ) then begin
					dH := 2 + HotMap[ X - 1 , Y ] + HotCost[ X , Y ];
					dV := 2 + HotMap[ X , Y - 1 ] + HotCost[ X , Y ];
					if DV < DH then DH := DV;
					DD := 3 + HotMap[ X - 1 , Y - 1] + HotCost[ X , Y ];
					if DD < DH then DH := DD;
					DP := 3 + HotMap[ X + 1 , Y - 1] + HotCost[ X , Y ];
					if DP < DH then DH := DP;

					if DH < HotMap[ X , Y ] then begin
						HotMap[X , Y ] := DH;
						flag := True;
					end;
				end;
			end;
		end;


		for y := ( GB^.map_height - 1 ) downto 2 do begin
			for x := ( GB^.map_width - 1 ) downto 2 do begin
				if not ( TileBlocksMovement( GB , X , Y ) or HotBlock[ X , Y ] ) then begin
					dH := 2 + HotMap[ X + 1 , Y ] + HotCost[ X , Y ];
					dV := 2 + HotMap[ X , Y + 1 ] + HotCost[ X , Y ];
					if DV < DH then DH := DV;
					DD := 3 + HotMap[ X + 1 , Y + 1] + HotCost[ X , Y ];
					if DD < DH then DH := DD;
					DP := 3 + HotMap[ X - 1 , Y + 1] + HotCost[ X , Y ];
					if DP < DH then DH := DP;

					if DH < HotMap[ X , Y ] then begin
						HotMap[ X , Y ] := DH;
						flag := True;
					end;
				end;
			end;
		end;

	end;
end;

Procedure CreatePointHotMap( GB: GameBoardPtr; X0,Y0: Integer; IncludeModels: Boolean );
	{ Update the HotMap so that models will be drawn to tile X,Y. }
var
	X,Y: Integer;
	M: GearPtr;
begin
	{ Clear the map, then indicate the "hot spot" provided. }
	for x := 1 to GB^.map_width do begin
		for y := 1 to GB^.map_height do begin
			HotMap[ X , Y ] := 9999;
			HotCost[ X , Y ] := 0;
			HotBlock[ X , Y ] := False;
		end;
	end;

	if IncludeModels then begin
		M := GB^.Contents;
		while M <> Nil do begin
			if IsMasterGear( M ) then begin
				X := NAttValue( M^.NA , NAG_Location , NAS_X );
				Y := NAttValue( M^.NA , NAG_Location , NAS_Y );
				if OnTheMap( GB , X , Y ) then HotBlock[ X , Y ] := True;
			end;
			M := M^.Next;
		end;
	end;

	if OnTheMap( GB , X0 , Y0 ) then HotMap[ X0 , Y0 ] := 0;

	ProcessHotMap( GB );
end;

Procedure CreateTeamHotMap( GB: GameBoardPtr; M: GearPtr );
	{ Make a hotmap so that the given model will seek its enemies. }
	Function HotOrNot( M2: GearPtr ): Integer;
		{ Is M2 hot, or merely lukewarm? This function will find out... }
	var
		it: Integer;
	begin
		{ Alliess are less desirable than other sorts of character. }
		if M2^.S = GS_AllyTeam then it := 15 + Random( 5 )
		else it := 10 + Random( 5 );

		{ Return the final value. }
		HotOrNot := it;
	end;
var
	M2: GearPtr;
	X,Y,XX,YY: Integer;
begin
	{ Clear the map, then indicate the "hot spots". }
	for x := 1 to GB^.map_width do begin
		for y := 1 to GB^.map_height do begin
			HotMap[ X , Y ] := 9999;
			HotCost[ X , Y ] := 0;
			HotBlock[ X , Y ] := False;
		end;
	end;

	{ Add the hot spots. The hotness of a given model is based on how desirable a }
	{ target it is. }
	M2 := GB^.Contents;
	while M2 <> Nil do begin
		X := NAttValue( M2^.NA , NAG_Location , NAS_X );
		Y := NAttValue( M2^.NA , NAG_Location , NAS_Y );
		if OnTheMap( GB , X , Y ) then begin
			HotBlock[ X , Y ] := True;
			if ( M2^.G = GG_Model ) and AreEnemies( M , M2 ) and ( NAttValue( M2^.NA , NAG_FightingStat , NAS_Hidden ) = 0 ) then begin
				HotMap[ X , Y ] := HotOrNot( M2 );
				{ Mark the tiles controlled by this model as having a high }
				{ terrain cost. }
				for XX := ( X - 1 ) to ( X + 1 ) do for YY := ( Y - 1 ) to ( Y + 1 ) do begin
					if OnTheMap( GB , XX , YY ) then HotCost[ XX , YY ] := HotCost[ XX , YY ] + 3;
				end;
			end;
		end;
		M2 := M2^.Next;
	end;
	ProcessHotMap( GB );
end;

end.
