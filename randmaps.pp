unit randmaps;
	{ This unit is the map generator. Given a SCENE gear and all of its }
	{ contents, return a usable gameboard. }
{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface


uses gears,gamebook;

Function GenerateMap( Camp: CampaignPtr; Scene: GearPtr ): GameBoardPtr;


implementation

var
	Debug_File: SAttPtr;

Procedure DebugMap( GB: GameBoardPtr; OX,OY: Integer; const Caption: String );
	{ Spit out a debugging map of the gameboard. }
var
	X,Y: Integer;
	msg: String;
begin
	StoreSAtt( Debug_File , '  ' );
	StoreSAtt( Debug_File , Caption );

	for Y := 1 to GB^.Map_Height do begin
		msg := '';
		for X := 1 to GB^.Map_Width do begin
			if ( X = OX ) and ( Y = OY ) then begin
				msg := msg + '@';
			end else if TileWall( GB , X , Y ) = 0 then begin
				msg := msg + '.';
			end else begin
				msg := msg + '#';
			end;
		end;
		StoreSAtt( Debug_File , msg );
	end;
end;


Function RectPointOverlap( X1,Y1,X2,Y2,PX,PY: Integer ): Boolean;
	{ Return TRUE if point PX,PY is located inside the provided }
	{ rectangle, FALSE if it isn't. }
begin
	RectPointOverlap := ( PX >= X1 ) and ( PX <= X2 ) and ( PY >= Y1 ) and ( PY <= Y2 );
end;

Function RectRectOverlap( X1,Y1,W1,H1,X2,Y2,W2,H2: Integer ): Boolean;
	{ Return TRUE if the two rectangles described by X,Y,Width,Height }
	{ overlap, FALSE if they don't. }
var
	OL: Boolean;
	XB,YB: Integer;
begin
	OL := False;

	{ Check all points of the first rectangle against the second. }
	for XB := X1 to (X1 + W1 - 1 ) do begin
		for YB := Y1 to (Y1 + H1 - 1 ) do begin
			Ol := OL or RectPointOverlap( X2 , Y2 , X2 + W2 - 1 , Y2 + H2 - 1 , XB , YB );
		end;
	end;

	RectRectOverlap := OL;
end;

Function CanAlterTile( GB: GameBoardPtr; X,Y: Integer ): Boolean;
	{ Return TRUE if we can alter this tile, or FALSE if we shouldn't. }
begin
	CanAlterTile := not TileVisible( GB , X , Y );
end;





Procedure PlaceRoomContents( GB: GameBoardPtr; MF: GearPtr );
	{ We have some contents to arrange in the room. Make it so. }
	Function LegalWallPoint( X,Y,D: Integer ): Boolean;
		{ This is a legal wall point if: }
		{ - It can be modified. }
		{ - It has walls to either side and a space in front. }
	begin
		LegalWallPoint := CanAlterTile( GB , X , Y ) and ( TileWall( GB , X + AngDir[D,1] , Y + AngDir[D,2] ) = 0 ) and ( TileWall( GB , X + AngDir[ ( D + 2 ) mod 8 , 1 ] , Y + AngDir[ ( D + 2 ) mod 8 , 2 ] ) <> 0 ) and ( TileWall( GB , X + AngDir[ ( D + 6 ) mod 8 , 1 ] , Y + AngDir[ ( D + 6 ) mod 8 , 2 ] ) <> 0 );
	end;
const
	Num_Default_Positions = 5;
	Default_Positions: Array [1..Num_Default_Positions] of Point = (
		( X: 0; Y: 0 ), ( X: -2; Y: -2 ), ( X: 2; Y: 2 ), ( X: -2; Y: 2 ), ( X: 2; Y: -2 )
	);
var
	LegalTiles: effect_stencil;
	X,Y,CX,CY,XT,YT,N,Tries: Integer;
	Thing: GearPtr;
	NotPlaced: Boolean;
begin
	{ Start by determining the legal tiles for stuff. }
	{ Can't place an item if there's a wall or another gear. }
	{ Only the tiles within the room are allowed. }
	for X := 1 to GB^.Map_Width do begin
		for Y := 1 to GB^.Map_Height do begin
			if RectPointOverlap( MF^.Stat[ STAT_MF_XPos ] , MF^.Stat[ STAT_MF_YPos ] , MF^.Stat[ STAT_MF_XPos ] + MF^.Stat[ STAT_MF_Width ] - 1 , MF^.Stat[ STAT_MF_YPos ] + MF^.Stat[ STAT_MF_Height ] - 1 , X , Y ) then begin
				LegalTiles[ X , Y ] := ( TileWall( GB , X , Y ) = 0 ) and CanAlterTile( GB , X , Y );
			end else begin
				LegalTiles[ X , Y ] := False;
			end;
		end;
	end;
	Thing := GB^.Contents;
	while Thing <> Nil do begin
		if OnTheMap( GB , Thing ) then LegalTiles[ NAttValue( Thing^.NA , NAG_Location , NAS_X ) , NAttValue( Thing^.NA , NAG_Location , NAS_Y ) ] := False;
		Thing := Thing^.Next;
	end;

	{ Next, place the contents. }
	N := 1;
	CX := MF^.Stat[ STAT_MF_XPos ] + MF^.Stat[ STAT_MF_Width ] div 2;
	CY := MF^.Stat[ STAT_MF_YPos ] + MF^.Stat[ STAT_MF_Height ] div 2;

	while MF^.InvCom <> Nil do begin
		Thing := MF^.InvCom;
		DelinkGear( MF^.InvCom , Thing );
		AppendGear( GB^.Contents , Thing );

		if ( Thing^.G = GG_Checkpoint ) and ( Thing^.S = GS_WallPoint ) then begin
			{ This point belongs on the wall. }
			{ Only the north and west walls are candidates- can't see the }
			{ south and east walls. }
			NotPlaced := True;
			Tries := 0;
			while NotPlaced and ( Tries < 100 ) do begin
				if Random( 2 ) = 1 then begin
					{ North Wall. }
					X := MF^.Stat[ STAT_MF_XPos ] + 1 + Random( MF^.Stat[ STAT_MF_Width ] - 2 );
					Y := MF^.Stat[ STAT_MF_YPos ] - 1;
					if LegalWallPoint( X , Y , 2 ) then NotPlaced := False;
				end else begin
					{ West Wall. }
					X := MF^.Stat[ STAT_MF_XPos ] - 1;
					Y := MF^.Stat[ STAT_MF_YPos ] + 1 + Random( MF^.Stat[ STAT_MF_Height ] - 2 );
					if LegalWallPoint( X , Y , 0 ) then NotPlaced := False;
				end;
				Inc( Tries );
			end;

		end else begin
			{ See if one of the default positions is legal. }
			NotPlaced := True;
			while NotPlaced and ( N <= Num_Default_Positions ) do begin
				if OnTheMap( GB , CX + Default_Positions[N].X , CY + Default_Positions[N].Y ) and LegalTiles[ CX + Default_Positions[N].X , CY + Default_Positions[N].Y ] then begin
					X := CX + Default_Positions[N].X;
					Y := CY + Default_Positions[N].Y;
					NotPlaced := False;
				end else begin
					Inc( N );
				end;
			end;
			if NotPlaced then begin
				X := MF^.Stat[ STAT_MF_XPos ] + Random( MF^.Stat[ STAT_MF_Width ] );
				Y := MF^.Stat[ STAT_MF_YPos ] + Random( MF^.Stat[ STAT_MF_Height ] );
			end;
		end;

		SetNAtt( Thing^.NA , NAG_Location , NAS_X , X );
		SetNAtt( Thing^.NA , NAG_Location , NAS_Y , Y );
		if ( Thing^.G = GG_Prop ) or ( Thing^.G = GG_CheckPoint ) then begin
			{ Props and checkpoints can't have anything placed within one tile of them- }
			{ otherwise, it might be possible for them to completely cut off passage. }
			for XT := ( X - 1 ) to ( X + 1 ) do for YT := ( Y - 1 ) to ( Y + 1 ) do if OnTheMap( GB , XT , YT ) then begin
				LegalTiles[ XT , YT ] := False;
				SetVisibility( GB , XT , YT , True );
			end;
			{ Checkpoints don't appear directly on the map, but alter the terrain in their position. }
			{ Handle that now. }
			if Thing^.G = GG_Checkpoint then begin
				if ( Thing^.Stat[ STAT_CP_Floor ] > 0 ) and ( Thing^.Stat[ STAT_CP_Floor ] <= Num_Floor ) then SetFloor( GB , X , Y , Thing^.Stat[ STAT_CP_Floor ] );
				if ( Thing^.Stat[ STAT_CP_Wall ] > 0 ) and ( Thing^.Stat[ STAT_CP_Wall ] <= Num_Wall ) then SetWall( GB , X , Y , Thing^.Stat[ STAT_CP_Wall ] );
				if ( Thing^.Stat[ STAT_CP_Decor ] > 0 ) and ( Thing^.Stat[ STAT_CP_Decor ] <= Num_Decor ) then SetDecor( GB , X , Y , Thing^.Stat[ STAT_CP_Decor ] );
			end;
		end else begin
			LegalTiles[ X , Y ] := False;
			SetVisibility( GB , X , Y , True );
		end;

		Inc( N );
	end;
end;

Procedure RenderTheRooms( GB: GameBoardPtr );
	{ Every gameboard gets a bunch of rooms to arrange. For the time being }
	{ we don't need to worry about the gameboard style or anything else; }
	{ just position the rooms so that none of them overlap. }
	{ Note that this must be relatively trivial, since our placement routine }
	{ isn't all that bright. }
var
	MF: GearPtr;
begin
	if ( GB = Nil ) or ( GB^.Scene = Nil ) then Exit;
	MF := GB^.Scene^.SubCom;
	while MF <> Nil do begin
		if MF^.G = GG_MapFeature then begin
			WallFill( GB , MF^.Stat[ STAT_MF_XPos ] , MF^.Stat[ STAT_MF_YPos ] , MF^.Stat[ STAT_MF_XPos ] + MF^.Stat[ STAT_MF_Width ] - 1 , MF^.Stat[ STAT_MF_YPos ] + MF^.Stat[ STAT_MF_Height ] - 1 , 0 );
{			VisibilityFill( GB , MF^.Stat[ STAT_MF_XPos ] + 1 , MF^.Stat[ STAT_MF_YPos ] + 1 , MF^.Stat[ STAT_MF_XPos ] + MF^.Stat[ STAT_MF_Width ] - 3 , MF^.Stat[ STAT_MF_YPos ] + MF^.Stat[ STAT_MF_Height ] - 3 , True );
}
			PlaceRoomContents( GB , MF );
		end;
		MF := MF^.Next;
	end;
end;



Function FloorWontOpenPassage( GB: GameBoardPtr; X , Y: Integer ): Boolean;
	{ Return TRUE if placing a floor here won't open up a passage }
	{ which otherwise would have been blocked. }
begin
	{ If this tile has no wall, return TRUE. }
	if TileWall( GB , X , Y ) = 0 then begin
		FloorWontOpenPassage := True;
	end else begin
		{ Removing this wall will open a passage if there are walls to }
		{ either side, either N-S or E-W. }
		FloorWontOpenPassage := ( ( TileWall( GB , X - 1 , Y ) = 0 ) or ( TileWall( GB , X + 1 , Y ) = 0 ) ) and ( ( TileWall( GB , X , Y - 1 ) = 0 ) or ( TileWall( GB , X , Y + 1 ) = 0 ) );
	end;
end;

Function WallWontBlockPassage( GB: GameBoardPtr; X , Y: Integer ): Boolean;
	{ Return TRUE if placing a wall here won't block movement. }
var
	T,N: Integer;
	WasASpace,IsASpace: Boolean;
begin
	{ If this tile has a wall, return TRUE. }
	if TileWall( GB , X , Y ) <> 0 then begin
		WallWontBlockPassage := True;
	end else begin
		{ Adding a wall will block a passage if there are two or more }
		{ spaces in the eight surrounding tiles which are separated by walls. }

		WasASpace := TileWall( GB , X + AngDir[ 7 , 1 ] , Y + AngDir[ 7 , 2 ] ) = 0;
		N := 0;
		for t := 0 to 7 do begin
			IsASpace := TileWall( GB , X + AngDir[ T , 1 ] , Y + AngDir[ T , 2 ] ) = 0;
			if IsASpace <> WasASpace then begin
				{ We've gone from wall to space or vice versa. }
				WasASpace := IsASpace;
				Inc( N );
			end;
		end;

		WallWontBlockPassage := N <= 2;
	end;
end;

Procedure CarveSomeNoise( GB: GameBoardPtr );
	{ Expand some of the wall-free areas. }
	Procedure CarveThisPoint( X,Y: Integer );
		{ Start carving at this point. Maybe recurse. }
	var
		XT,YT: Integer;
	begin
		for XT := ( X - 2 ) to ( X + 2 ) do begin
			for YT := ( Y - 2 ) to ( Y + 2 ) do begin
				if CanAlterTile( GB , XT , YT ) then SetWall( GB , XT , YT , 0 );
			end;
		end;
	end;
var
	tries,X,Y,Wall: Integer;
begin
	for tries := 1 to ( GB^.Map_Width * GB^.Map_Height div 25 ) do begin
		X := Random( GB^.Map_Width - 7 ) + 4;
		Y := Random( GB^.Map_Height - 7 ) + 4;
		Wall := TileWall( GB , X , Y );
		if Wall = 0 then begin
			CarveThisPoint( X , Y );
		end;
	end;
end;

Procedure MutateMaze( GB: GameBoardPtr; Mutation_Level: Integer );
	{ Not necessarily beautiful, but mutated... }
	{ This will use a cellular automata-type mutator to shake up the }
	{ level outline a bit. }
	Function NumWallsNearby( X , Y: Integer ): Integer;
		{ Return the number of walls in the 3x3 region centered }
		{ on the provided coordinates. }
	var
		XX,YY,N: Integer;
	begin
		N := 0;
		for XX := ( X - 1 ) to ( X + 1 ) do for YY := ( Y - 1 ) to ( Y + 1 ) do begin
			if TileWall( GB , XX , YY ) = WALL_BasicWall then Inc( N );
		end;
		NumWallsNearby := N;
	end;

	Procedure DoMutation;
		{ The workhorse procedure. We may need several iterations }
		{ to get a fully mutated map. }
	const
		MM_DoNothing = 0;
		MM_WallOn = 1;
		MM_WallOff = 2;
	var
		X,Y: Integer;
		temp_array: Array [ 1..MaxMapWidth , 1..MaxMapWidth ] of Byte;
	begin
		{ Clear the temp array. }
		for X := 1 to GB^.Map_Width do for Y := 1 to GB^.Map_Height do temp_array[ x , y ] := MM_DoNothing;

		for X := 2 to ( GB^.Map_Width - 1 ) do begin
			for Y := 2 to ( GB^.Map_Height - 1 ) do begin
				if CanAlterTile( GB , X , Y ) then begin
					if NumWallsNearby( X , Y ) >= 5 then temp_array[ X , Y ] := MM_WallOn
					else temp_array[ X , Y ] := MM_WallOff;
				end;
			end;
		end;

		{ Implement our results. }
		for X := 2 to ( GB^.Map_Width - 1 ) do begin
			for Y := 2 to ( GB^.Map_Height - 1 ) do begin
				if ( temp_array[ X , Y ] = MM_WallOff ) and FloorWontOpenPassage( GB , X , Y ) then SetWall( GB , X , Y , 0 )
				else if ( temp_array[ X , Y ] = MM_WallOn ) and WallWontBlockPassage( GB , X , Y ) then SetWall( GB , X , Y , WALL_BasicWall );
			end;
		end;
	end;
var
	T: Integer;
begin
	for T := 1 to Mutation_Level do DoMutation;
end;

Procedure Layout_SubtleMonkey( GB: GameBoardPtr );
	{ Lay out the maze using the Subtle Monkey algorithm. }
	{ The total map is divided into 4x4 nodes. Place the rooms so that }
	{ they line up with the node edges. Start with a random section of }
	{ tunnel. Then, as each room is rendered, attach it to a random }
	{ node. }
const
	Node_Size = 4;
	Double_Node_Size = 8;
var
	NXMax,NYMax: Integer;	{ The number of horizontal and vertical nodes. }
	Function NodeX( ND: Integer ): Integer;
		{ Convert node coordinate ND to an actual map coordinate. }
	begin
		NodeX := ( ND - 1 ) * Node_Size + 2;
	end;
	Function NodeY( ND: Integer ): Integer;
		{ Convert node coordinate ND to an actual map coordinate. }
	begin
		NodeY := ( ND - 1 ) * Node_Size + 2;
	end;
	Function SelectNodeForRoom( MF: GearPtr ): Boolean;
		{ Attempt to find a decent place to put map feature MF. }
		{ - It should not intersect with any other map feature               }
		{   currently placed.                                                }
		{ - It should be at least one tile from the edge of the container    }
		{   on all sides.                                                    }
		{ - It should be placed in an area that is considered "clear",       }
		{   depending upon the SCheck, STerr values.                         }
		Function RandomNodeForRoom( GB: GameBoardPtr; W,H: Integer ): Point;
			{ Select a placement point within the bounds of the map. }
		var
			P: Point;
		begin
			if MF^.Stat[ STAT_MF_Anchor ] = MFAnchor_East then P.X := NodeX( NXMax + 1 - W div Node_Size )
			else if MF^.Stat[ STAT_MF_Anchor ] = MFAnchor_West then P.X := NodeX( 1 )
			else P.X := NodeX( Random( NXMax + 1 - W div Node_Size ) + 1 );
			if MF^.Stat[ STAT_MF_Anchor ] = MFAnchor_North then P.Y := NodeY( 1 )
			else if MF^.Stat[ STAT_MF_Anchor ] = MFAnchor_South then P.Y := NodeY( NYMax + 1 - H div Node_Size )
			else P.Y := NodeY( Random( NYMax + 1 - H div Node_Size ) + 1 );
			RandomNodeForRoom := P;
		end;
		Function NodeIsGood( GB: GameBoardPtr; X0,Y0,W,H: Integer ): Boolean;
			{ Return TRUE if the specified area is free for adding a new }
			{ map feature, or FALSE otherwise. }
		var
			BadPosition: Boolean;
			MF2: GearPtr;
		begin
			{ Assume it isn't a bad position until shown otherwise. }
			BadPosition := False;

			{ Check One - see if this position intersects with any }
			{ other map feature at this same depth. }
			{ Only those map features which have }
			{ already been placed need be checked. }
			MF2 := GB^.Scene^.SubCom;
			while MF2 <> Nil do begin
				if ( MF2^.G = GG_MapFeature ) and OnTheMap( GB , MF2^.Stat[ STAT_MF_XPos ] , MF2^.Stat[ STAT_MF_YPos ] ) then begin
					BadPosition := BadPosition or RectRectOverlap( X0 - 4 , Y0 - 4 , W + 8 , H + 8 , MF2^.Stat[ STAT_MF_XPos ] , MF2^.Stat[ STAT_MF_YPos ] , MF2^.Stat[ STAT_MF_Width ] , MF2^.Stat[ STAT_MF_Height ] );
				end;

				MF2 := MF2^.Next;
			end;

			{ So, the placement point is good if X,Y isn't a bad position. }
			NodeIsGood := not BadPosition;
		end;

	const
		MaxTries = 10000;
	var
		Tries: Integer;
		P: Point;
	begin
		if not OnTheMap( GB , MF^.Stat[ STAT_MF_XPos ] , MF^.Stat[ STAT_MF_YPos ] ) then begin
			Tries := 0;
			repeat
				P := RandomNodeForRoom( GB , MF^.Stat[ STAT_MF_Width ] , MF^.Stat[ STAT_MF_Height ] );
				Inc( Tries );
			until ( Tries > MaxTries ) or NodeIsGood( GB , P.X , P.Y , MF^.Stat[ STAT_MF_Width ] , MF^.Stat[ STAT_MF_Height ] );

			MF^.Stat[ STAT_MF_XPos ] := P.X;
			MF^.Stat[ STAT_MF_YPos ] := P.Y;

			SelectNodeForRoom := Tries <= MaxTries;
		end;
	end;

	Procedure DrawAnL( NX , NY: Integer );
		{ Draw an L-Shaped corridor on the map, centered on }
		{ node point NX, NY. }
		{ This will be the first structure in the monkeymap. }
	var
		X1,X2,Y1,Y2: Integer;
	begin
		{ Determine X0,X1,Y0,Y1 points. }
		if ( NX > 1 ) and ( ( Random( 2 ) = 1 ) or ( NX = NXMax ) ) then begin
			X1 := NodeX( NX ) - ( Node_Size * ( Random( NX - 1 ) + 1 ) );
			X2 := NodeX( NX );
		end else begin
			X1 := NodeX( NX );
			X2 := NodeX( NX ) + ( Node_Size * ( Random( NXMax - NX ) + 1 ) );
		end;
		if ( NY > 1 ) and ( ( Random( 2 ) = 1 ) or ( NY = NYMax ) ) then begin
			Y1 := NodeY( NY ) - ( Node_Size * ( Random( NY - 1 ) + 1 ) );
			Y2 := NodeY( NY );
		end else begin
			Y1 := NodeY( NY );
			Y2 := NodeY( NY ) + ( Node_Size * ( Random( NYMax - NY ) + 1 ) );
		end;
		{ The X and Y values we have right now refer to the upper left }
		{ corner of the node. What we want, though, is a path drawn from }
		{ one 2x2 center to the other. }
		WallFill( GB , X1 + 1 , NodeY( NY ) + 1 , X2 + 2 , NodeY( NY ) + 2 , 0 );
		WallFill( GB , NodeX( NX ) + 1 , Y1 + 1 , NodeX( NX ) + 2 , Y2 + 2 , 0 );
	end; {DrawAnL}

	Function NodeIsConnected( NX, NY: Integer ): Boolean;
		{ Return TRUE if this node is connected to the monkey maze, or }
		{ FALSE otherwise. }
	begin
		NodeIsConnected := TileWall( GB , NodeX( NX ) + 1 , NodeY( NY ) + 1 ) = 0;
	end;

	Procedure FindConnectedNode( var NX,NY: Integer );
		{ Pick a node that is connected to the maze, and store its }
		{ coordinates in X,Y. }
	var
		TX,TY: Integer;
		N: LongInt;
	begin
		N := 0;
		for TX := 1 to NXMax do begin
			for TY := 1 to NYMax do begin
				if NodeIsConnected( TX , TY ) then Inc( N );
			end;
		end;
		N := Random( N );
		for TX := 1 to NXMax do begin
			for TY := 1 to NYMax do begin
				if NodeIsConnected( TX , TY ) then begin
					Dec( N );
					if N = -1 then begin
						NX := TX;
						NY := TY;
					end;
				end;
			end;
		end;
	end;

	Procedure ConnectToMonkey( MF: GearPtr );
		{ We have a map feature which has ostensibly been placed on the }
		{ map. Locate a random tile which already has a corridor in it }
		{ and connect a random node of this map feature to that. Stop }
		{ going if we encounter a corridor prematurely. }
	var
		ONX,ONY,DNX,DNY,TX,TY: Integer;
		NotYetConnected: Boolean;
	begin
		{ Start by picking a random node to connect to. }
		{ This will be our destination. }
		FindConnectedNode( DNX , DNY );

		{ Select a node within this map feature to connect from. }
		ONX := ( MF^.Stat[ STAT_MF_XPos ] + 2 ) div 4;
		if MF^.Stat[ STAT_MF_Width ] >= Double_Node_Size then ONX := ONX + Random( MF^.Stat[ STAT_MF_Width ] div Node_Size );
		ONY := ( MF^.Stat[ STAT_MF_YPos ] + 2 ) div 4;
		if MF^.Stat[ STAT_MF_Height ] >= Double_Node_Size then ONY := ONY + Random( MF^.Stat[ STAT_MF_Height ] div Node_Size );

		NotYetConnected := True;
		while ( ONX <> DNX ) and NotYetConnected do begin
			if ONX < DNX then TX := ONX + 1
			else TX := ONX - 1;
			if NodeIsConnected( TX , ONY ) then NotYetConnected := False;
			if ONX < TX then WallFill( GB , NodeX( ONX ) + 1 , NodeY( ONY ) + 1 , NodeX( TX ) + 2 , NodeY( ONY ) + 2 , 0)
			else WallFill( GB , NodeX( TX ) + 1 , NodeY( ONY ) + 1 , NodeX( ONX ) + 2 , NodeY( ONY ) + 2 , 0);
			ONX := TX;
		end;
		while ( ONY <> DNY ) and NotYetConnected do begin
			if ONY < DNY then TY := ONY + 1
			else TY := ONY - 1;
			if NodeIsConnected( ONX , TY ) then NotYetConnected := False;
			if ONY < TY then WallFill( GB , NodeX( ONX ) + 1 , NodeY( ONY ) + 1 , NodeX( ONX ) + 2 , NodeY( TY ) + 2 , 0)
			else WallFill( GB , NodeX( ONX ) + 1 , NodeY( TY ) + 1 , NodeX( ONX ) + 2 , NodeY( ONY ) + 2 , 0);
			ONY := TY;
		end;
	end;

var
	MF: GearPtr;
begin
	NXMax := ( GB^.Map_Width - 2 ) div 4;
	NYMax := ( GB^.Map_Height - 2 ) div 4;

	{ Arrange the rooms on the map. }
	{ We're gonna do this in two passes- first, place all the rooms which }
	{ have anchors defined. After that, place everything else. }
	MF := GB^.Scene^.SubCom;
	while MF <> Nil do begin
		if ( MF^.G = GG_MapFeature ) and ( MF^.Stat[ STAT_MF_Anchor ] <> 0 ) then SelectNodeForRoom( MF );
		MF := MF^.Next;
	end;
	MF := GB^.Scene^.SubCom;
	while MF <> Nil do begin
		if ( MF^.G = GG_MapFeature ) and ( MF^.Stat[ STAT_MF_Anchor ] = 0 ) then SelectNodeForRoom( MF );
		MF := MF^.Next;
	end;

	{ Next, we will connect the rooms to one another. Start with a randomly }
	{ placed "L". Then add corridors to each room in turn, making sure that }
	{ each one connects to a spot already on the grid. }
	DrawAnL( Random( NXMax ) + 1 , Random( NYMax ) + 1 );

	MF := GB^.Scene^.SubCom;
	while MF <> Nil do begin
		if MF^.G = GG_MapFeature then ConnectToMonkey( MF );
		MF := MF^.Next;
	end;

end;

Procedure ConvertWallsToWilds( GB: GameBoardPtr );
	{ The map gets drawn initially as a series of basic walls }
	{ laid over the floor. Now, let's convert the walls to rocks }
	{ and trees and water. Just like Canada. }
const
	Num_Fillers = 10;
	Filler_Walls: Array [ 1..Num_Climate , 1..Num_Fillers ] of Integer = (
		{ Forest Climate }
	(	WALL_SpruceTree, WALL_SpruceTree, WALL_SpruceTree, WALL_SpruceTree, WALL_MapleTree,
		WALL_MapleTree, WALL_MapleTree, WALL_MapleTree, WALL_DeadTree, WALL_MapleTree
	),
		{ Arctic Climate }
	(	WALL_IcyPine, WALL_IcyPine, WALL_IcyPine, WALL_IcyPine, WALL_IcyMaple,
		WALL_IcyMaple, WALL_IcyMaple, WALL_SnowDrift, WALL_SnowDrift, WALL_SnowDrift
	),
 		{ Desert Climate }
	(	WALL_SandDune, WALL_SandDune, WALL_SandDune, WALL_SandDune, WALL_SandDune,
		WALL_SandDune, WALL_SandDune, WALL_SandDune, WALL_SandDune, WALL_Cactus
	),
 		{ Jungle Climate }
	(	WALL_PalmTree, WALL_PalmTree, WALL_PalmTree, WALL_PalmTree, WALL_PalmTree,
		WALL_PalmTree, WALL_PalmTree, WALL_PalmTree, WALL_PalmTree, WALL_PalmTree
	),
		{ Grassland Climate }
	(	WALL_TallTree, WALL_TallTree, WALL_TallTree, WALL_Hill, WALL_Hill,
		WALL_Hill, WALL_Hill, WALL_Hill, WALL_Hill, WALL_Hill
	),
		{ Wasteland Climate }
	(	WALL_DeadTree, WALL_DeadTree, WALL_DeadTree, WALL_SandySpikes, WALL_SandySpikes,
		WALL_Boulder, WALL_FallenTree, WALL_SadTree, WALL_SadTree, WALL_SadTree
	),
		{ Swamp Climate }
	(	WALL_Willow, WALL_Willow, WALL_Willow, WALL_Willow, WALL_Willow,
		WALL_Willow, WALL_SadTree, WALL_SadTree, WALL_SadTree, WALL_DeadTree
	)
	);
var
	C,X,Y: Integer;
begin
	{ For now, just convert everything. }
	C := NAttValue( GB^.Scene^.NA , NAG_StoryData , NAS_Climate );
	if ( C < 1 ) or ( C > Num_Climate ) then C := NAV_Forest;
	for X := 1 to GB^.Map_Width do begin
		for Y := 1 to GB^.Map_Height do begin
			if TileWall( GB , X , Y ) = WALL_BasicWall then begin
				SetWall( GB , X , Y , Filler_Walls[ C , Random( Num_Fillers ) + 1 ] );
			end;
		end;
	end;
end;

Procedure PrepMapForDrawing( GB: GameBoardPtr );
	{ We start with a map covered in walls, and carve our passageways out }
	{ from that. }
const
	Default_Floor_Type: Array [1..Num_Climate] of Integer = (
		FLOOR_Grass, FLOOR_Snow, FLOOR_Sand, FLOOR_Jungle, FLOOR_Grass,
		FLOOR_Sand, FLOOR_Swamp
	);
var
	C,Terr: Integer;
begin
	{ We begin with a map filled with obstacles, and then carve out our }
	{ passageways + features. }
	if ( GB^.Scene^.Stat[ STAT_FloorType ] > 0 ) and ( GB^.Scene^.Stat[ STAT_FloorType ] <= Num_Floor ) then begin
		{ If a floor type has been defined, this overrides anything else. }
		Terr := GB^.Scene^.Stat[ STAT_FloorType ];
	end else if GB^.Scene^.V = GV_Wilderness then begin
		{ For wilderness, use the default floor for the climate. }
		C := NAttValue( GB^.Scene^.NA , NAG_StoryData , NAS_Climate );
		if ( C < 1 ) or ( C > Num_Climate ) then C := NAV_Forest;
		Terr := Default_Floor_Type[ C ];
	end else begin
		Terr := FLOOR_Stone;
	end;
	FloorFill( GB , 1 , 1 , GB^.Map_Width , GB^.Map_Height , Terr );
	WallFill( GB , 1 , 1 , GB^.Map_Width , GB^.Map_Height , WALL_BasicWall );
end;

Procedure AddSomePonds( GB: GameBoardPtr );
	{ Attempt to add some ponds to the game board. }
	Function GoodPondSpot( X0,Y0,X1,Y1: Integer ): Boolean;
		{ Return TRUE if this rect is modifiable, or FALSE otherwise. }
	var
		X,Y: Integer;
		CanModify,AtLeastOneSpace: Boolean;
	begin
		CanModify := True;
		AtLeastOneSpace := False;
		for X := X0 to X1 do begin
			for Y := Y0 to Y1 do begin
				if not CanAlterTile( GB , X , Y ) then CanModify := False;
				if TileWall( GB , X , Y ) = 0 then AtLeastOneSpace := True;
			end;
		end;
		GoodPondSpot := CanModify and AtLeastOneSpace;
	end;

const
	Max_Number_Of_Ponds: Array [1..Num_Climate] of Integer = (
		8, 6, 1, 10, 5,
		5, 20
	);

var
	NumPond,X,Y,W,H,Tries: Integer;
begin
	{ Depending on the climate, see how many ponds to potentially generate. }
	X := NAttValue( GB^.Scene^.NA , NAG_StoryData , NAS_Climate );
	if ( X < 1 ) or ( X > Num_Climate ) then X := NAV_Forest;
	NumPond := Random( Max_Number_Of_Ponds[ X ] + 1 );

	while NumPond > 0 do begin
		W := Random( 11 ) + 7;
		H := Random( 11 ) + 7;
		Tries := 0;
		repeat
			X := Random( GB^.Map_Width - W - 1 ) + 2;
			Y := Random( GB^.Map_Height - H - 1 ) + 2;
			Inc( Tries );
		until ( Tries > 30 ) or GoodPondSpot( X , Y , X + W - 1 , Y + H - 1 );

		if GoodPondSpot( X , Y , X + W - 1 , Y + H - 1 ) then begin
			WallFill( GB , X , Y , X + W - 1 , Y + H - 1 , 0 );
			FloorFill( GB , X + 3 , Y + 2 , X + W - 4 , Y + H - 3 , FLOOR_Water );
			FloorFill( GB , X + 2 , Y + 3 , X + 2 , Y + H - 4 , FLOOR_Water );
			FloorFill( GB , X + W - 3 , Y + 3 , X + W - 3 , Y + H - 4 , FLOOR_Water );
			VisibilityFill( GB , X + 1 , Y + 1 , X + W - 2 , Y + H - 2 , True );
		end;

		Dec( NumPond );
	end;
end;


Function GenerateMap( Camp: CampaignPtr; Scene: GearPtr ): GameBoardPtr;
	{ The meat and potatoes of the generator. Here's how it works: }
	{ Start by arranging all the scene contents on the map. Then, }
	{ connect them up with corridors. }
var
	GB: GameBoardPtr;
begin
	{ Step one: Initialize the gameboard. }
	if ( Scene^.Stat[ STAT_MapWidth ] < 102 ) or ( Scene^.Stat[ STAT_MapHeight ] < 102 ) then begin
		Scene^.Stat[ STAT_MapWidth ] := 102;
		Scene^.Stat[ STAT_MapHeight ] := 102;
	end;
	GB := NewGameBoard( Camp , Scene^.S , Scene^.Stat[ STAT_MapWidth ] , Scene^.Stat[ STAT_MapHeight ] );
	GB^.Scene := Scene;

	Debug_File := Nil;

	{ Prepare the map for drawing: Fill it with floor and walls. }
	PrepMapForDrawing( GB );

	{ Next, select placement points for all the rooms. }
	Layout_SubtleMonkey( GB );

	{ Once the rooms have been placed, it's time to start rendering. }
	RenderTheRooms( GB );

	{ If this is a wilderness or cave map, mutate things a bit. }
	if ( Scene^.V = GV_Cave ) or ( Scene^.V = GV_Wilderness ) then begin
		if Scene^.V = GV_Wilderness then AddSomePonds( GB );
		CarveSomeNoise( GB );
		MutateMaze( GB , 5 );

		{ Wilderness scenes get their walls replaced by trees/mountains, }
		{ depending on the climate/setting. }
		if Scene^.V = GV_Wilderness then ConvertWallsToWilds( GB );
	end;

	{ Clear the visibility bits for the map. }
	VisibilityFill( GB , 1 , 1 , GB^.MAP_Width , GB^.MAP_Height , False );

	if Debug_File <> Nil then begin
		SaveStringList( 'debug.txt' , Debug_File );
		DisposeSAtt( Debug_File );
	end;

	GenerateMap := GB;
end;

end.
