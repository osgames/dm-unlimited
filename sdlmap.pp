unit sdlmap;
{
	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}


interface

uses gamebook,sdl,math,gears,texutil,sdlgfx;

const
	IT_Cursor = 0;
	IT_PlayerModel = 1;
	IT_EnemyModel = 2;
	IT_LegalMove = 3;
	IT_Targeting = 4;
	IT_Hidden = 6;

var
	tile_x,tile_y: LongInt;	{ Tile where the mouse pointer is pointing. }
	origin_x,origin_y: Integer;	{ Tile which the camera is pointing at. }

	Counter_Sprite: SensibleSpritePtr;

	Models_On_Gameboard: Model_Map;

	Focused_On_Model: GearPtr;


Function ScreenDirToMapDir( D: Integer ): Integer;
Procedure RenderMap( GB: GameBoardPtr );

Procedure IndicateTile( GB: GameBoardPtr; X , Y , G: Integer );
Procedure DeindicateTile( GB: GameBoardPtr; X , Y: Integer );
Procedure ClearUnderlays;
Procedure ClearOverlays;
Procedure FocusOnTile( GB: GameBoardPtr; X,Y: Integer );

Function ProcessShotAnimation( GB: GameBoardPtr; var AnimList,AnimOb: GearPtr ): Boolean;
Function ProcessPointAnimation( GB: GameBoardPtr; var AnimList,AnimOb: GearPtr ): Boolean;


Procedure GeneratePartyAvatars;
Procedure InitGraphicsForScene( GB: GameBoardPtr );

implementation

uses sdl_ttf,uiconfig,avatar,effects;

type
	Map_Cel_Description = Record
		Sprite: SensibleSpritePtr;
		F: Integer;	{ The frame to be displayed. }
	end;

	Map_Cel_Toggle = Array [ 1..MaxMapWidth, 1..MaxMapWidth ] of Boolean;

const
	Num_Terrain_Layers = 4;		{ Total number of terrain cel layers. }
		TL_Floor = 1;
		TL_Border = 2;
		TL_Wall = 3;
		TL_Decor = 4;

	Num_Model_Layers = 4;		{ Total number of model cel layers. }
		ML_Underlay = 1;
		ML_Item = 2;
		ML_Being = 3;
		ML_Effect = 4;

	Num_Basic_Model_Layers = 3;

	SI_Ally = 3;
	SI_Neutral = 2;
	SI_Enemy = 1;

	HalfTileWidth = 27;
	HalfTileHeight = 13;

	Party_Avatars_Label = 'PARTY_AVATARS';

	TC_Count = 10;

var
	Terrain_Cels: Array [ 1..MaxMapWidth, 1..MaxMapWidth , 1..Num_Terrain_Layers ] of Map_Cel_Description;
	Model_Cels: Array [ 1..MaxMapWidth, 1..MaxMapWidth , 1..Num_Model_Layers ] of Map_Cel_Description;

	Tile_Captions: Array [ 1..MaxMapWidth, 1..MaxMapWidth ] of String[20];
	Tile_Caption_Count: Array [ 1..MaxMapWidth, 1..MaxMapWidth ] of byte;

	Model_Cel_IsOn: Array [1..Num_Model_Layers] of Map_Cel_Toggle;

	Model_Cel_Names: Array [ 1..MaxMapWidth, 1..MaxMapWidth ] of String;

	Current_Backdrop: SensibleSpritePtr;

	Extras_Sprite, Projectile_Sprite, Effect_Sprite: SensibleSpritePtr;
	Terrain_Sprite,Wall_Sprite,Border_Sprite,Basic_Wall_Sprite,Decor_Sprite: SensibleSpritePtr;
	Party_Avatars: SensibleSpritePtr;

Procedure SetTileCaption( GB: GameBoardPtr; X,Y: Integer; msg: String );
	{ Set a caption for this tile. }
begin
	if ( msg <> '' ) and OnTheMap( GB , X , Y ) then begin
		Tile_Captions[ X , Y ] := Copy( msg , 1 , 19 );
		Tile_Caption_Count[ X , Y ] := TC_Count;
	end;
end;


Function ScreenDirToMapDir( D: Integer ): Integer;
	{ Convert the requested screen direction to a map direction. }
begin
	ScreenDirToMapDir := ( D + 7 ) mod 8;
end;

Procedure ClearModelCelLayer( L: Integer );
	{ Clear sprite descriptions from the provided overlay layer. }
begin
	if ( L >= 1 ) and ( L <= Num_Model_Layers ) then FillChar( Model_Cel_IsOn[ L ] , SizeOf( Model_Cel_IsOn[ L ] ) , False );
end;

Procedure SetModelCel( GB: GameBoardPtr; X,Y,L: Integer; SS: SensibleSpritePtr; Frame: Integer );
	{ Add a sprite image safely to the cel array. }
begin
	if not OnTheMap( GB , X , Y ) then Exit;
	if ( L < 1 ) or ( L > Num_Model_Layers ) then Exit;
	Model_Cels[ X , Y , L ].Sprite := SS;
	Model_Cels[ X , Y , L ].F := Frame;
	Model_Cel_IsOn[ L ][ X , Y ] := SS <> Nil;
end;

Procedure ClearModelCel( GB: GameBoardPtr; X,Y,L: Integer );
	{ Remove the overlay image from this tile. }
begin
	if not OnTheMap( GB , X , Y ) then Exit;
	if ( L < 1 ) or ( L > Num_Model_Layers ) then Exit;
	Model_Cel_IsOn[ L ][ X , Y ] := False;
end;


Function SpriteName( GB: GameBoardPtr; M: GearPtr ): String;
	{ Locate the sprite name for this gear. If no sprite name is defined, }
	{ set the default sprite name for the gear type & store it as a string }
	{ attribute so we won't need to do this calculation later. }
const
	Default_Model_Sprite = 'monster_default.png';
var
	it: String;
begin
	it := SAttValue( M^.SA , 'SDL_SPRITE' );
	if it = '' then begin
		it := Default_Model_Sprite;
		SetSAtt( M^.SA , 'SDL_SPRITE <' + it + '>' );
	end;
	SpriteName := it;
end;

Function AlmostSeen( GB: GameBoardPtr; X1 , Y1: Integer ): Boolean;
	{ Tell whether or not to show the edge of visibility symbol here. We'll }
	{ show it if this tile is unseen, and is adjacent to a seen tile that's not a wall. }
var
	IsAlmostSeen: Boolean;
	D,X2,Y2: Integer;
begin
	IsAlmostSeen := False;
	For D := 0 to 7 do begin
		X2 := X1 + AngDir[ D , 1 ];
		Y2 := Y1 + AngDir[ D , 2 ];
		if OnTheMap( GB , X2 , Y2 ) and TileVisible( GB , X2 , Y2 ) then begin
			IsAlmostSeen := True;
			Break;
		end;
	end;
	AlmostSeen := IsAlmostSeen;
end;

Procedure DrawBackdrop;
	{ If Current_Backdrop exists, fill the screen with it. }
var
	X,Y: Integer;
	MyDest: TSDL_Rect;
begin
	if ( Current_Backdrop <> Nil ) and ( Current_Backdrop^.W > 0 ) and ( Current_Backdrop^.H > 0 ) then begin
		for x := 0 to ( ScreenWidth div Current_Backdrop^.W ) do begin
			for y := 0 to ( ScreenHeight div Current_Backdrop^.H ) do begin
				MyDest.X := x * Current_Backdrop^.W;
				MyDest.Y := y * Current_Backdrop^.H;
				DrawSprite( Current_Backdrop , MyDest , 0 );
			end;
		end;
	end;
end;

Function RelativeX( X,Y: Integer ): LongInt;
	{ Return the relative position of tile X,Y. The UpLeft corner }
	{ of tile [1,1] is the origin of our display. }
begin
	RelativeX := ( (X-1) * HalfTileWidth ) - ( (Y-1) * HalfTileWidth );
end;

Function RelativeY( X,Y: Integer ): LongInt;
	{ Return the relative position of tile X,Y. The UpLeft corner }
	{ of tile [1,1] is the origin of our display. }
begin
	RelativeY := ( (Y-1) * HalfTileHeight ) + ( (X-1) * HalfTileHeight );
end;

Function ScreenX( X,Y: Integer ): LongInt;
	{ Return the screen coordinates of map column X. }
begin
	ScreenX := RelativeX( X , Y ) - Origin_X;
end;

Function ScreenY( X,Y: Integer ): Integer;
	{ Return the screen coordinates of map row Y. }
begin
	ScreenY := RelativeY( X , Y ) - Origin_Y;
end;


Procedure CheckOrigin( GB: GameBoardPtr );
	{ Make sure the origin position is legal. }
begin
	if Origin_X < RelativeX( 1 , GB^.Map_Height ) then Origin_X := RelativeX( 1 , GB^.Map_Height )
	else if Origin_X > RelativeX( GB^.Map_Width , 1 ) then Origin_X := RelativeX( GB^.Map_Width , 1 );
	if Origin_Y < RelativeY( 1 , 1 ) then Origin_Y := RelativeY( 1 , 1 )
	else if Origin_Y > RelativeY( GB^.Map_Width , GB^.Map_Height ) then Origin_Y := RelativeY( GB^.Map_Width , GB^.Map_Height );
end;


Procedure ScrollMap( GB: GameBoardPtr );
	{ Asjust the position of the map origin. }
const
	Scroll_Step = 12;
begin
	if Mouse_X < 20 then begin
		Origin_X := Origin_X - Scroll_Step;
		checkorigin( GB );
	END else if Mouse_X > ( screenwidth - 20 ) then begin
		Origin_X := Origin_X + Scroll_Step;
		checkorigin( GB );
	end;
	if ( Mouse_Y < 20 ) then begin
		Origin_Y := Origin_Y - Scroll_Step;
		checkorigin( GB );
	END else if ( Mouse_Y > ( screenheight - 20 ) ) then begin
		Origin_Y := Origin_Y + Scroll_Step;
		checkorigin( GB );
	end;
end;

Procedure QuickModelStatus( Model: GearPtr; X,Y: Integer );
	{ The mouse is pointing at this model. Give us some info. }
const
	bar_width = 40;
var
	MyDest,Box: TSDL_Rect;
	HP,HPD: Integer;
begin
	MyDest.X := ScreenX( X , Y );
	MyDest.Y := ScreenY( X , Y ) - 6;
	MyDest.W := 54;
	MyDest.H := 10;
	QuickCaption( GearName( Model ) , MyDest , StdWhite , Small_Font );

	{ Show the HP-Meter }
	Box.X := MyDest.X + 7;
	Box.W := bar_width;
	Box.H := 3;
	Box.Y := MyDest.Y + TTF_FontLineSkip( small_font ) + 2;
	SDL_FillRect( game_screen , @Box , SDL_MapRGBA( Game_Screen^.Format , 250 , 0 , 0 , 200 ) );
	HP := MaxHP( Model );
	HPD := NAttValue( Model^.NA , NAG_Damage , NAS_HPDmg );
	if HPD > HP then HPD := HP;
	if HP > 0 then begin
		Box.X := Box.X + Box.W - ( HPD * Box.W ) div HP;
		Box.W := MyDest.X + 7 + Box.W - Box.X;
		SDL_FillRect( game_screen , @Box , SDL_MapRGBA( Game_Screen^.Format , 120 , 0 , 0 , 100 ) );
	end;

	{ Show the MP-Meter }
	Box.X := MyDest.X + 7;
	Box.W := bar_width;
	Box.Y := MyDest.Y + TTF_FontLineSkip( small_font ) + 5;
	SDL_FillRect( game_screen , @Box , SDL_MapRGBA( Game_Screen^.Format , 0 , 150 , 250 , 230 ) );
	HP := MaxMP( Model );
	HPD := NAttValue( Model^.NA , NAG_Damage , NAS_MPDmg );
	if HPD > HP then HPD := HP;
	if HP > 0 then begin
		Box.X := Box.X + Box.W - ( HPD * Box.W ) div HP;
		Box.W := MyDest.X + 7 + Box.W - Box.X;
		SDL_FillRect( game_screen , @Box , SDL_MapRGBA( Game_Screen^.Format , 0 , 0 , 120 , 100 ) );
	end;
end;

Function OnTheScreen( X , Y: Integer ): Boolean;
	{ This function returns TRUE if the specified point is visible }
	{ on screen, FALSE if it isn't. }
var
	SX,SY: LongInt;		{ Find Screen X and Screen Y and see if it's in the map area. }
begin
	SX := ScreenX( X , Y );
	SY := ScreenY( X , Y );
	if ( SX >= ( -64 ) ) and ( SX <= ( ScreenWidth ) ) and ( SY >= -64 ) and ( SY <= ( ScreenHeight ) ) then begin
		OnTheScreen := True;
	end else begin
		OnTheScreen := False;
	end;
end;


Procedure RenderMap( GB: GameBoardPtr );
	{ Render the isometric 2D map. }
var
	X,Y,T: Integer;
	M: GearPtr;
	MyDest,TexDest: TSDL_Rect;
begin
	ClrScreen;
	ScrollMap( GB );

	{ Clear the basic cels- the ones that the map renderer has access to. There will be additional }
	{ layers which the map renderer shouldn't touch. }
	for X := 2 to Num_Basic_Model_Layers do ClearModelCelLayer( X );

	{ Clear the model map and the off-map icon. }
	for X := 1 to GB^.Map_Width do begin
		for Y := 1 to GB^.Map_Height do begin
			models_on_gameboard[ X , Y ] := Nil;
			if Names_Above_Heads then model_cel_names[ X , Y ] := '';
		end;
	end;

	{ Next add the characters, mecha, and items to the list. }
	M := GB^.Contents;
	while M <> Nil do begin
		if OnTheMap( GB , M ) and ModelVisible( GB , M ) then begin
			if IsMasterGear( M ) then begin
				X := NAttValue( M^.NA , NAG_Location , NAS_X );
				Y := NAttValue( M^.NA , NAG_Location , NAS_Y );
				Models_On_Gameboard[ X , Y ] := M;

				if NAttValue( M^.NA , NAG_FightingStat , NAS_Hidden ) > 0 then begin
					SetModelCel( 	GB , X , Y , ML_Being , Extras_Sprite , IT_Hidden );
				end else begin
					{ Insert sprite-drawing code here. }
					SetModelCel( 	GB , X , Y , ML_Being ,
							LocateBUSprite( SpriteName( GB , M ) , 54 , 54 ),
							NAttValue( M^.NA , NAG_Appearance , NAS_Frame )
					);
				end;
				if Names_Above_Heads and OnTheMap( GB , X , Y ) then model_cel_names[ X , Y ] := GearName( M );
			end else if M^.G = GG_Item then begin
				X := NAttValue( M^.NA , NAG_Location , NAS_X );
				Y := NAttValue( M^.NA , NAG_Location , NAS_Y );

				{ Insert sprite-drawing code here. }
				SetModelCel( 	GB , X , Y , ML_Item , Extras_Sprite, 5 );
			end;
		end;

		M := M^.Next;
	end;

	{ Go through each tile on the map, displaying terrain and }
	{ other contents. }
	DrawBackdrop;
	TexDest.W := 64;
	TexDest.H := 15;

	{ Initialize the tile_x,tile_y vars to off the map. }
	tile_x := -1;
	tile_y := -1;

	for X := 1 to GB^.map_width do begin
		for Y := 1 to GB^.map_height do begin
			if OnTheScreen( X , Y ) and TileVisible( GB , X , Y ) then begin
				{ Draw the Terrain first. }
				MyDest.X := ScreenX( X , Y );
				MyDest.Y := ScreenY( X , Y );

				{ If the mouse is within the bottom 13 pixels of this box, }
				{ we can determine the tile it's pointing at. }
				if ( Mouse_X >= MyDest.X ) and ( Mouse_X < ( MyDest.X + 54 ) ) and ( Mouse_Y >= ( MyDest.Y + 41 ) ) and ( Mouse_Y < ( MyDest.Y + 54 ) ) then begin
					{ If it's in the lower left triangle, it's one tile south. If it's in the lower right }
					{ triangle, it's one tile east. Otherwise it's right here. }
					if Mouse_Y > ( MyDest.Y + 41 + ( Mouse_X - MyDest.X ) div 2 ) then begin
						tile_x := X;
						tile_y := Y + 1;

					end else if Mouse_Y > ( MyDest.Y + 67 - ( Mouse_X - MyDest.X ) div 2 ) then begin
						tile_x := X + 1;
						tile_y := Y;

					end else begin
						tile_x := X;
						tile_y := Y;
					end;
				end;

				{ Draw the terrain layers. }
				for t := 1 to 2 do begin
					if Terrain_Cels[ X , Y , T ].Sprite <> Nil then begin
						DrawSprite( Terrain_Cels[ X ,Y , T ].Sprite , MyDest , Terrain_Cels[ X ,Y , T ].F );
					end;
				end;
				if Model_Cel_IsOn[ ML_Underlay ][ X , Y ] then begin
					DrawSprite( Model_Cels[ X ,Y , ML_Underlay ].Sprite , MyDest , Model_Cels[ X ,Y , ML_Underlay ].F );
				end;
				for t := 3 to 4 do begin
					if Terrain_Cels[ X , Y , T ].Sprite <> Nil then begin
						DrawSprite( Terrain_Cels[ X ,Y , T ].Sprite , MyDest , Terrain_Cels[ X ,Y , T ].F );
					end;
				end;

				{ Draw the models next. }
				for t := 2 to Num_Model_Layers do begin
					if Model_Cel_IsOn[ T ][ X , Y ] then begin
						DrawSprite( Model_Cels[ X ,Y , T ].Sprite , MyDest , Model_Cels[ X ,Y , T ].F );
					end;
				end;

				{ On top of everything else, the caption. }
				if Tile_Caption_Count[ X , Y ] > 0 then begin
					TexDest.X := ScreenX( X , Y );
					TexDest.Y := ScreenY( X , Y ) - 5;
					QuickCaption( Tile_Captions[ X , Y ] , TexDest , TextColor , Game_Font );
					Dec( Tile_Caption_Count[ X , Y ] );
				end else begin
					{ If this tile is where the mouse is pointing, and there's a }
					{ model here, show its info. }
					if ( tile_x = x ) and ( tile_y = y ) and ( Models_On_Gameboard[ X , Y ] <> Nil ) then begin
						QuickModelStatus( Models_On_Gameboard[ X , Y ] , X, Y );
					end else if Names_Above_Heads and ( Model_Cel_Names[ X , Y ] <> '' ) then begin
						TexDest.X := ScreenX( X , Y );
						TexDest.Y := ScreenY( X , Y ) - 5;
						QuickCaption( Model_Cel_Names[ X , Y ] , TexDest , StdWhite , Small_Font );
					end;
				end;

			end; { if OnTheScreen... }
		end;
	end;
end;


Procedure IndicateTile( GB: GameBoardPtr; X , Y , G: Integer );
	{ Indicate the requested tile. }
begin
	if OnTheMap( GB , X , Y ) then begin
		SetModelCel( GB , X , Y , ML_Underlay , Extras_Sprite , G );
	end;
end;

Procedure DeindicateTile( GB: GameBoardPtr; X , Y: Integer );
	{ Indicate the requested tile. }
begin
	ClearModelCel( GB , X , Y , ML_Effect );
end;

Procedure ClearUnderlays;
	{ Erase all overlays currently on the screen. }
begin
	ClearModelCelLayer( ML_Underlay );
end;

Procedure ClearOverlays;
	{ Erase all overlays currently on the screen. }
begin
	ClearModelCelLayer( ML_Effect );
end;

Procedure AddOverlay( GB: GameBoardPtr; OL_Sprite: SensibleSpritePtr; X , Y , F: Integer );
	{ Add an overlay to the screen. }
begin
	SetModelCel( GB , X , Y , ML_Effect , OL_Sprite , F );
end;

Procedure FocusOnTile( GB: GameBoardPtr; X,Y: Integer );
	{ Move the origin so that the tile X,Y is in the center of the }
	{ display. }
begin
	Origin_X := RelativeX( X , Y ) - ( ScreenWidth div 2 );
	Origin_Y := RelativeY( X , Y ) - ( ScreenHeight div 2 );
	CheckOrigin( GB );
end;

Function ProcessShotAnimation( GB: GameBoardPtr; var AnimList,AnimOb: GearPtr ): Boolean;
	{ Process this shot. Return TRUE if the missile }
	{ is still going, FALSE if it has reached its target. }
var
	P: Point;
begin
	{ Increase the counter, and find the next spot. }
	Inc( AnimOb^.V );
	P := SolveLine( AnimOb^.Stat[ STAT_AOShot_X1 ] , AnimOb^.Stat[ STAT_AOShot_Y1 ] , AnimOb^.Stat[ STAT_AOShot_X2 ] , AnimOb^.Stat[ STAT_AOShot_Y2 ] , AnimOb^.V );

	{ If this is the destination point, then we're done. }
	if ( P.X = AnimOb^.Stat[ STAT_AOShot_X2 ] ) and ( P.Y = AnimOb^.Stat[ STAT_AOShot_Y2 ] ) then begin
		RemoveGear( AnimList , ANimOb );
		P.X := 0;

	{ If this is not the destination point, draw the missile. }
	end else begin
		{Display bullet...}
		AddOverlay( GB , Projectile_Sprite , P.X , P.Y , ( AnimOb^.Stat[ STAT_AOShot_ShotSeq ] - 1 ) * 8 + AnimOb^.Stat[ STAT_AOShot_DirMod ] );
	end;

	ProcessShotAnimation := True;
end;

Function ProcessPointAnimation( GB: GameBoardPtr; var AnimList,AnimOb: GearPtr ): Boolean;
	{ Process this effect. Return TRUE if the blast }
	{ is still going, FALSE if it is over. }
const
	AnimSeq_FirstFrame: Array [1..Num_AnimSeq] of Integer = (
		0, 3, 6, 9, 12,
		15, 18, 21, 24, 27,
		30, 38, 46, 54, 60,
		68, 76, 84, 92, 100,
		106, 114, 122, 130, 138,
		148
	);
	AnimSeq_Length: Array [1..Num_AnimSeq] of Integer = (
		3, 3, 3, 3, 3,
		3, 3, 3, 3, 3,
		8, 8, 8, 6, 8,
		8, 8, 8, 8, 6,
		8, 8, 8, 8, 10,
		10
	);
var
	it: Boolean;
begin
	if AnimOb^.V = 0 then begin
		{ This is the start of something. Set the caption, if one exists. }
		SetTileCaption( GB , AnimOb^.Stat[ STAT_AOPoint_X ] , AnimOb^.Stat[ STAT_AOPoint_Y ] , SAttValue( AnimOb^.SA , 'CAPTION' ) );
	end;
	if AnimOb^.V < AnimSeq_Length[ AnimOb^.Stat[ STAT_AOPoint_AnimSeq ] ] then begin
		AddOverlay( GB , Effect_Sprite , AnimOb^.Stat[ STAT_AOPoint_X ] , AnimOb^.Stat[ STAT_AOPoint_Y ] , AnimSeq_FirstFrame[ AnimOb^.Stat[ STAT_AOPoint_AnimSeq ] ] + AnimOb^.V );

		{ Increment the counter. }
		Inc( AnimOb^.V );

		it := True;
	end else begin

		RemoveGear( AnimList , AnimOb );
		it := False;
	end;

	ProcessPointAnimation := it;
end;

Procedure GeneratePartyAvatars;
	{ Generate avatars for all party members. }
var
	PCA: SensibleSpritePtr;
	T: Integer;
	MyDest: TSDL_Rect;
begin
	{ Start by clearing the sprite sheet. }
	SDL_FillRect( Party_Avatars^.Img , Nil , SDL_MapRGBA( Party_Avatars^.Img^.Format , 255 , 0 , 255 , 0 ) );
	MyDest.Y := 0;
	for T := 1 to Num_Party_Members do begin
		if G_Party[ T ] <> Nil then begin
			PCA := GenerateAvatar( G_Party[ T ] );
			MyDest.X := ( t - 1 ) * 54;
			DrawSprite( PCA , Party_Avatars^.Img , MyDest , 0 );
			RemoveSprite( PCA );
			SetSAtt( G_Party[ T ]^.SA , 'SDL_SPRITE <' + Party_Avatars_Label + '>' );
			SetNAtt( G_Party[ T ]^.NA , NAG_Appearance , NAS_Frame , T - 1 );
		end;
	end;
end;


Procedure InitGraphicsForScene( GB: GameBoardPtr );
	{ Alright, we've got this scene that needs to be displayed, this is what }
	{ we're going to do: }
	{ - Load the required graphics sets. }
	{ - Figure out the sprites to use for each tile. }
	{ This second part is a bit more complicated than you may think. Certain }
	{ tiles (such as walls and shoreline) are affected by the tiles around }
	{ them. Decide on the proper sprite for each one. }
	Function CalculateFloorScore( X, Y, Terr: Integer ): Integer;
		{ See how many edges of this tile are touching the specified }
		{ terrain. Top corner is one point, right 2, bottom 4, and left 8. }
		{ If the tile is touching the specified terrain in a cardinal }
		{ direction it gets the sum of both corners. }
		{ This will return a number between 0 and 15, with 0 and 15 being }
		{ the two degenerate cases. }
	var
		it: Integer;
	begin
		it := 0;
		if ( TileFloor( GB , X - 1 , Y - 1 ) = Terr ) or ( TileFloor( GB , X , Y - 1 ) = Terr ) or ( TileFloor( GB , X - 1 , Y ) = Terr ) then it := it + 1;
		if ( TileFloor( GB , X + 1 , Y - 1 ) = Terr ) or ( TileFloor( GB , X , Y - 1 ) = Terr ) or ( TileFloor( GB , X + 1 , Y ) = Terr ) then it := it + 2;
		if ( TileFloor( GB , X + 1 , Y + 1 ) = Terr ) or ( TileFloor( GB , X , Y + 1 ) = Terr ) or ( TileFloor( GB , X + 1 , Y ) = Terr ) then it := it + 4;
		if ( TileFloor( GB , X - 1 , Y + 1 ) = Terr ) or ( TileFloor( GB , X , Y + 1 ) = Terr ) or ( TileFloor( GB , X - 1 , Y ) = Terr ) then it := it + 8;
		CalculateFloorScore := it;
	end;
	Function IsSystemWall( X, Y: Integer ): Boolean;
		{ Return TRUE if this tile contains a system wall, and FALSE }
		{ if it doesn't. }
	var
		terr: Integer;
	begin
		terr := TileWall( GB , X , Y );
		IsSystemWall := ( terr > 0 ) and ( terr <= 5 );
	end;
	Function TileAllowsVision( X , Y: Integer ): Boolean;
		{ Just a NOT TileBlocksVision call. }
	begin
		TileAllowsVision := not TileBlocksVision( GB , X , Y );
	end;
	Function BasicWallFrame( X , Y: Integer ): Integer;
		{ The basic wall cel is a complex creature. }
		{ Create a score to see where to stick wall extensions: }
		{ 1 for the north tile, 2 for the east, 4 for the south and }
		{ 8 for the west. Apply the score if the tile in question }
		{ contains a system wall which is visible from one of four }
		{ adjacent frames which also touch this tile. }
		{ In the degenerate case that this tile is completely isolated, }
		{ assign a sprite frame of 15. }
	var
		it: Integer;
	begin
		it := 0;
		if IsSystemWall( X , Y - 1 ) and ( TileAllowsVision( X-1 , Y -1 ) or TileAllowsVision( X - 1 , Y ) or TileAllowsVision( X + 1 , Y - 1 ) or TileAllowsVision( X + 1 , Y ) ) then it := it + 1;
		if IsSystemWall( X + 1 , Y ) and ( TileAllowsVision( X+1 , Y -1 ) or TileAllowsVision( X , Y-1 ) or TileAllowsVision( X + 1 , Y + 1 ) or TileAllowsVision( X , Y+1 ) ) then it := it + 2;
		if IsSystemWall( X , Y + 1 ) and ( TileAllowsVision( X-1 , Y +1 ) or TileAllowsVision( X - 1 , Y ) or TileAllowsVision( X + 1 , Y + 1 ) or TileAllowsVision( X + 1 , Y ) ) then it := it + 4;
		if IsSystemWall( X - 1 , Y ) and ( TileAllowsVision( X-1 , Y -1 ) or TileAllowsVision( X , Y-1 ) or TileAllowsVision( X - 1 , Y + 1 ) or TileAllowsVision( X , Y+1 ) ) then it := it + 8;
		if it = 0 then it := 15;
		BasicWallFrame := it - 1;
	end;
	Function BasicBorderFrame( X , Y: Integer ): Integer;
		{ Walls need a border to help them look neat and tidy. Otherwise }
		{ they have little bits of floor poking out all over the place. }
		{ 1 point for top corner, 2 points for right, yadda yadda yadda... }
		{ You know the drill by now. Apply the border if three tiles around }
		{ the corner are basic walls. }
	var
		it: Integer;
	begin
		it := 0;
		if ( IsSystemWall( X - 1 , Y - 1 ) and IsSystemWall( X - 1 , Y ) and IsSystemWall( X , Y - 1 ) ) or not OnTheMap( GB , X - 1 , Y - 1 ) then it := it + 1;
		if ( IsSystemWall( X + 1 , Y - 1 ) and IsSystemWall( X + 1 , Y ) and IsSystemWall( X , Y - 1 ) ) or not OnTheMap( GB , X + 1 , Y - 1 ) then it := it + 2;
		if ( IsSystemWall( X + 1 , Y + 1 ) and IsSystemWall( X + 1 , Y ) and IsSystemWall( X , Y + 1 ) ) or not OnTheMap( GB , X + 1 , Y + 1 ) then it := it + 4;
		if ( IsSystemWall( X - 1 , Y + 1 ) and IsSystemWall( X - 1 , Y ) and IsSystemWall( X , Y + 1 ) ) or not OnTheMap( GB , X - 1 , Y + 1 ) then it := it + 8;
		BasicBorderFrame := it - 1;
	end;
	Function SurroundedByWalls( X , Y: Integer ): Boolean;
		{ Return TRUE if this tile has walls in all four cardinal directions. }
	begin
		SurroundedByWalls := ( TileWall( GB , X - 1 , Y ) <> 0 ) and ( TileWall( GB , X + 1 , Y ) <> 0 ) and ( TileWall( GB , X , Y - 1 ) <> 0 ) and ( TileWall( GB , X , Y + 1 ) <> 0 );
	end;
	Function TouchingAWall( X , Y: Integer ): Boolean;
		{ Return TRUE if this tile has walls in all four cardinal directions. }
	begin
		TouchingAWall := ( TileWall( GB , X - 1 , Y ) <> 0 ) or ( TileWall( GB , X + 1 , Y ) <> 0 ) or ( TileWall( GB , X , Y - 1 ) <> 0 ) or ( TileWall( GB , X , Y + 1 ) <> 0 );
	end;
	Function SpaceToSouth( X , Y: Integer ): Boolean;
		{ Return TRUE if there's no wall directly to the south. }
	begin
		SpaceToSouth := ( TileWall( GB , X , Y + 1 ) = 0 );
	end;
const
	{ The BTF constants tell the contents of the basic_terrain.png file. }
	BTF_Grass = 0;
	BTF_TileFloor = 1;
	BTF_Water = 2;
	BTF_StoneFloor = 3;
	BTF_GravelFloor = 4;
	BTF_SandFloor = 5;
	BTF_GrassWater = 6;
		{ The next 13 tiles are the grass/water boundaries. }
	BTF_IceFloor = 20;
	BTF_SnowFloor = 21;
	BTF_GrassSnow = 22;
		{ The next 13 tiles are the grass/snow boundaries. }
	BTF_GrassSand = 36;
		{ The next 13 tiles are the grass/sand boundaries. }
	BTF_SnowWater = 50;
		{ The next 13 tiles are the snow/water boundaries. }
	BTF_SandWater = 64;
		{ The next 13 tiles are the sand/water boundaries. }
	BTF_CaveFloor = 78;
	BTF_JungleFloor = 79;
	BTF_SwampFloor = 80;
	BTF_SewerFloor = 81;
	BTF_DungeonFloor = 83;

	Floor_Frame: Array [1..Num_Floor] of Integer = (
		BTF_Grass, BTF_TileFloor, BTF_Water, BTF_StoneFloor, BTF_GravelFloor,
		BTF_IceFloor, BTF_SnowFloor, BTF_SandFloor, BTF_CaveFloor, BTF_JungleFloor,
		BTF_SwampFloor, BTF_SewerFloor, BTF_DungeonFloor
	);

	SysWall_Frame: Array [2..5] of Integer = (
		15, 17, 19, 21
	);

	BWF_PalmTree = 1;
	BWF_BerryBush = 3;
	BWF_AppleTree = 4;
	BWF_Cactus = 5;
	BWF_DeadTree = 7;
	BWF_FallenTree = 8;
	BWF_SpruceTree = 9;
	BWF_IcyPine = 10;
	BWF_IcyMaple = 11;
	BWF_MapleTree = 16;
	BWF_SandDune = 17;
	BWF_SnowDrift = 19;
	BWF_TallTree = 0;
	BWF_SadTree = 18;
	BWF_Willow = 6;
	BWF_RockySpikes = 12;
	BWF_SandySpikes = 13;
	BWF_Boulder = 14;
	BWF_IcySpikes = 15;
	BWF_Mushrooms = 20;
	BWF_Bush = 21;
	BWF_Hill = 22;
	BWF_Cave = 23;
	BWF_CampFire = 24;
	Wall_Frame: Array [6..Num_Wall] of Integer = (
		BWF_SpruceTree, BWF_MapleTree, BWF_DeadTree, BWF_FallenTree, BWF_PalmTree,
		BWF_SandDune, BWF_SnowDrift, BWF_IcyPine, BWF_IcyMaple, BWF_Cactus,
		BWF_TallTree, BWF_Bush, BWF_Hill, BWF_SadTree, BWF_Willow,
		BWF_RockySpikes, BWF_SandySpikes , BWF_Boulder, BWF_IcySpikes, BWF_Mushrooms,
		BWF_Cave, BWF_CampFire, BWF_BerryBush, BWF_AppleTree
	);
	IsSystemTree: Array [6..Num_Wall] of Boolean = (
		False, False, False, False, True,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False
	);

	BDF_Skull = 0;
	BDF_Bone = 1;
	BDF_Skeleton = 2;
	BDF_IcePuddle = 3;
	BDF_MudPuddle = 4;
	BDF_Portrait = 5;
	BDF_SunSign = 7;
	BDF_MoonSign = 9;
	BDF_Landscape = 11;
	BDF_Bookshelf = 13;
	BDF_Message = 15;
	BDF_Fireplace = 17;
	Decor_Frame: Array [1..Num_Decor] of Integer = (
		BDF_Skull, BDF_Bone, BDF_Skeleton, BDF_IcePuddle, BDF_MudPuddle,
		BDF_Portrait, BDF_SunSign, BDF_MoonSign, BDF_Landscape, BDF_Bookshelf,
		BDF_Message, BDF_Fireplace
	);
	Is_Wall_Decor: Array [1..Num_Decor] of Boolean = (
		False, False, False, False, False,
		True, True, True, True, True,
		True, True
	);

	Default_Wall_Sprite = 'wall_lightbrick.png';
var
	X,Y,Terr,AdScore: Integer;
	Wall_Sprite_Name: String;
begin
	GeneratePartyAvatars;

	{ Clean the sprite list. }
	CleanSpriteList;

	{ Get rid of the old wall sprite (maybe) and load the new one. }
	if Wall_Sprite <> Nil then RemoveSprite( Wall_Sprite );
	Wall_Sprite_Name := SAttValue( GB^.Scene^.SA , 'SDL_WALLS' );
	if Wall_Sprite_Name <> '' then begin
		Wall_Sprite := LocateSprite( Wall_Sprite_Name , 54 , 54 );
	end else begin
		Wall_Sprite := LocateSprite( Default_Wall_Sprite , 54 , 54 );
	end;

	{ Next, the complicated bit- take the terrain description and convert }
	{ it to sprites + indicies. Because there's no terrain destruction we }
	{ can do this when the level is loaded and then never worry about it }
	{ ever again. }
	for x := 1 to GB^.map_width do begin
		for y := 1 to GB^.map_height do begin
			{ First, deal with the floors. }
			Terr := TileFloor( GB , X , Y );
			{ We have the floor. All tiles MUST have a floor, though }
			{ the wall and decor are optional. Depending on the identity }
			{ of this floor, we may need to do a bit of work on it. }
			Terrain_Cels[ X , Y , TL_Floor ].Sprite := Terrain_Sprite;
			if ( terr = FLOOR_Grass ) or ( terr = FLOOR_Jungle ) or ( terr = FLOOR_Swamp ) then begin
				{ Grass terrain gets a number of borders: if it's }
				{ adjacent to water, snow, or sand we better }
				{ deal with that now. }
				AdScore := CalculateFloorScore( X , Y , FLOOR_Water );
				if ( AdScore > 0 ) and ( AdScore < 15 ) then begin
					Terrain_Cels[ X , Y , TL_Floor ].F := BTF_GrassWater + AdScore - 1;
				end else begin
					AdScore := CalculateFloorScore( X , Y , FLOOR_Snow );
					if ( AdScore > 0 ) and ( AdScore < 15 ) then begin
						Terrain_Cels[ X , Y , TL_Floor ].F := BTF_GrassSnow + AdScore - 1;
					end else begin
						{ With all this nesting, I'm starting to feel like some kind }
						{ of migratory waterfowl. }
						AdScore := CalculateFloorScore( X , Y , FLOOR_Sand );
						if ( AdScore > 0 ) and ( AdScore < 15 ) then begin
							Terrain_Cels[ X , Y , TL_Floor ].F := BTF_GrassSand + AdScore - 1;
						end else begin
							{ Yay, we've checked for all the boundary types! }
							{ Just draw a plain old grass tile, then. }
							Terrain_Cels[ X , Y , TL_Floor ].F := Floor_Frame[ Terr ];
						end;
					end;
				end;

			end else if terr = FLOOR_Snow then begin
				{ Snow gets a boundary with water. }
				AdScore := CalculateFloorScore( X , Y , FLOOR_Water );
				if ( AdScore > 0 ) and ( AdScore < 15 ) then begin
					Terrain_Cels[ X , Y , TL_Floor ].F := BTF_SnowWater + AdScore - 1;
				end else begin
					{ Just draw a plain old snow tile, then. }
					Terrain_Cels[ X , Y , TL_Floor ].F := Floor_Frame[ Terr ];
				end;

			end else if terr = FLOOR_Sand then begin
				{ Sand gets a boundary with water. }
				AdScore := CalculateFloorScore( X , Y , FLOOR_Water );
				if ( AdScore > 0 ) and ( AdScore < 15 ) then begin
					Terrain_Cels[ X , Y , TL_Floor ].F := BTF_SandWater + AdScore - 1;
				end else begin
					{ Just draw a plain old tile, then. }
					Terrain_Cels[ X , Y , TL_Floor ].F := Floor_Frame[ Terr ];
				end;

			end else if terr = FLOOR_Sewer then begin
				{ Sewers get pseudorandomization. }
				if TileWall( GB , X , Y ) <> 0 then Terrain_Cels[ X , Y , TL_Floor ].F := Floor_Frame[ Terr ] + 1
				else if TouchingAWall( X , Y ) then Terrain_Cels[ X , Y , TL_Floor ].F := Floor_Frame[ Terr ]
				else if ( ( ( X mod 7 ) + ( Y mod 11 ) ) mod 3 ) = 1 then Terrain_Cels[ X , Y , TL_Floor ].F := BTF_DungeonFloor
				else Terrain_Cels[ X , Y , TL_Floor ].F := Floor_Frame[ Terr ];

			end else begin
				{ Other floors don't get any special treatment. }
				{ Consult the FLOOR_FRAME array. }
				Terrain_Cels[ X , Y , TL_Floor ].F := Floor_Frame[ Terr ];
			end;

			{ Next, deal with the walls. Unlike floors, there will }
			{ not necessarily be a wall in any given tile. }
			Terr := TileWall( GB , X , Y );

			if Terr = 0 then begin
				{ No wall present- set sprite to Nil. }
				Terrain_Cels[ X , Y , TL_Wall ].Sprite := Nil;
				Terrain_Cels[ X , Y , TL_Border ].Sprite := Nil;

			end else if Terr = WALL_BasicWall then begin
				{ This is a basic wall. Time to get serious. }
				{ We need to determine both the wall frame and }
				{ the border (or lack thereof). }
				Terrain_Cels[ X , Y , TL_Wall ].Sprite := Wall_Sprite;
				Terrain_Cels[ X , Y , TL_Wall ].F := BasicWallFrame( X , Y );

				AdScore := BasicBorderFrame( X , Y );
				if AdScore >= 0 then begin
					Terrain_Cels[ X , Y , TL_Border ].Sprite := Border_Sprite;
					Terrain_Cels[ X , Y , TL_Border ].F := AdScore;
					if AdScore = 14 then begin
						{ This tile is completely blacked out. To }
						{ save effort, remove the wall and floor sprites. }
						Terrain_Cels[ X , Y , TL_Wall ].Sprite := Nil;
						Terrain_Cels[ X , Y , TL_Floor ].Sprite := Nil;
					end;
				end else begin
					Terrain_Cels[ X , Y , TL_Border ].Sprite := Nil;
				end;
			end else if IsSystemWall( X , Y ) then begin
				{ This is not a basic wall, but it is part of the basic }
				{ wall system- this means that its sprite is located on }
				{ the WALL_SPRITE sheet. }
				Terrain_Cels[ X , Y , TL_Wall ].Sprite := Wall_Sprite;
				Terrain_Cels[ X , Y , TL_Wall ].F := SysWall_Frame[ Terr ];
				if SpaceToSouth( X , Y ) then Inc( Terrain_Cels[ X , Y , TL_Wall ].F );

				AdScore := BasicBorderFrame( X , Y );
				if AdScore >= 0 then begin
					Terrain_Cels[ X , Y , TL_Border ].Sprite := Border_Sprite;
					Terrain_Cels[ X , Y , TL_Border ].F := AdScore;
					if AdScore = 14 then begin
						{ This tile is completely blacked out. To }
						{ save effort, remove the wall and floor sprites. }
						Terrain_Cels[ X , Y , TL_Wall ].Sprite := Nil;
						Terrain_Cels[ X , Y , TL_Floor ].Sprite := Nil;
					end;
				end else begin
					Terrain_Cels[ X , Y , TL_Border ].Sprite := Nil;
				end;

			end else begin
				Terrain_Cels[ X , Y , TL_Wall ].Sprite := Basic_Wall_Sprite;
				Terrain_Cels[ X , Y , TL_Wall ].F := Wall_Frame[ Terr ];
				Terrain_Cels[ X , Y , TL_Border ].Sprite := Nil;

				if IsSystemTree[ Terr ] and SurroundedByWalls( X , Y ) then Inc( Terrain_Cels[ X , Y , TL_Wall ].F );

			end;

			{ Finally, deal with the decor. }
			Terr := TileDecor( GB , X , Y );

			if ( Terr < 1 ) or ( Terr > Num_Decor ) then begin
				{ No wall present- set sprite to Nil. }
				Terrain_Cels[ X , Y , TL_Decor ].Sprite := Nil;
			end else if Is_Wall_Decor[ Terr ] then begin
				Terrain_Cels[ X , Y , TL_Decor ].Sprite := Decor_Sprite;
				Terrain_Cels[ X , Y , TL_Decor ].F := Decor_Frame[ Terr ];
				if SpaceToSouth( X , Y ) then Inc( Terrain_Cels[ X , Y , TL_Decor ].F );

			end else begin
				Terrain_Cels[ X , Y , TL_Decor ].Sprite := Decor_Sprite;
				Terrain_Cels[ X , Y , TL_Decor ].F := Decor_Frame[ Terr ];

			end;

			{ While we're at it, clear any tile captions left over from }
			{ a previous map. }
			Tile_Caption_Count[ X , Y ] := 0;

		end; {for Y }
	end; { for X }
end;

initialization
	RPGKey;

	SDL_PumpEvents;
	SDL_GetMouseState( Mouse_X , Mouse_Y );

	Extras_Sprite := LocateSprite( 'basic_extras.png' , 54 , 54 );
	Projectile_Sprite := LocateSprite( 'basic_projectiles.png' , 54 , 54 );
	Effect_Sprite := LocateSprite( 'basic_effects.png' , 54 , 54 );
	Terrain_Sprite := LocateSprite( 'basic_terrain.png' , 54 , 54 );
	Basic_Wall_Sprite := LocateSprite( 'basic_walls.png' , 54 , 54 );
	Decor_Sprite := LocateSprite( 'basic_decor.png' , 54 , 54 );
	Border_Sprite := LocateSprite( 'basic_wallborder.png' , 54 , 54 );
	Counter_Sprite := LocateSprite( 'basic_counters.png' , 10, 16 );
	Current_Backdrop := Nil;
	Wall_Sprite := Nil;
	Party_Avatars := CreateBlankSprite( 54 , 54 , 54 * Num_Party_Members , 54 );
	Party_Avatars^.Name := Party_Avatars_Label;

	tile_x := 1;
	tile_y := 1;

	ClearUnderlays;
	ClearOverlays;
end.
