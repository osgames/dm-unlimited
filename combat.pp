unit combat;
	{ This unit holds the combat loop. It does not hold the effect processor; }
	{ that's another unit's problem. }
{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses sdlgfx,gears,gearparser,gamebook,effects,sdlmap;

var
	In_Combat_Mode: Boolean;

Procedure InvokePower( GB: GameBoardPtr; PC , I: GearPtr; X1,Y1: Integer );

Function ShouldEnterCombat( GB: GameBoardPtr ): Boolean;

Procedure BumpAction( GB: GameBoardPtr; Actor , Target: GearPtr );
Procedure BumpTile( GB: GameBoardPtr; Actor: GearPtr; X,Y: Integer );

Function PCSelectInvocation( GB: GameBoardPtr; PC: GearPtr ): Boolean;

Function CombatMode( GB: GameBoardPtr ): Integer;


implementation

uses uiconfig,hotmaps,frontend,equipment,joescript,monsters,sdlmenus,texutil;

const
	InvoMenu_Stealth = -2;
	InvoMenu_Backpack = -3;
	InvoMenu_TurnUndead = -4;

Procedure EndTurn( PC: GearPtr );
	{ The PC is doing something which should end their turn. Or, }
	{ maybe it's a monster doing it... doesn't matter much. }
begin
	AddNAtt( PC^.NA , NAG_FightingStat , NAS_ActionPointsSpent , ActionPoints( PC ) );
end;


Procedure ClearFightingStats( GB: GameBoardPtr );
	{ Restore the movement points to all models. }
	{ Also remove any combat-only enchantments that may be in effect. }
var
	M,E,E2: GearPtr;
begin
	M := GB^.Contents;
	while M <> Nil do begin
		StripNAtt( M , NAG_FightingStat );
		if M^.G = GG_Model then begin
			{ Only models have enchantments. }
			E := M^.SubCom;
			while E <> Nil do begin
				E2 := E^.Next;
				if E^.G = GG_Enchantment then begin
					{ Monsters lose all enchantments at the end of combat. PCs }
					{ only lose those with short duration. }
					if M^.S <> GS_PCTeam then begin
						if E^.Stat[ STAT_Enchantment_Duration ] <> EDUR_Inherent then RemoveGear( M^.SubCom , E );
					end else if E^.Stat[ STAT_Enchantment_Duration ] = EDUR_CombatOnly then begin
						RemoveGear( M^.SubCom , E );
					end;
				end;
				E := E2;
			end;
		end;
		M := M^.Next;
	end;
end;

Function MakeStealthRoll( GB: GameBoardPtr; PC: GearPtr ): Boolean;
	{ The PC will attempt to hide from the active enemies. }
var
	hi,c: Integer;
	M: GearPtr;
begin
	{ Determine the highest spotting value from all enemies. }
	hi := 0;
	M := GB^.Contents;
	while M <> Nil do begin
		if ( M^.G = GG_Model ) and IsAlright( M ) and AreEnemies( M , PC ) and ( M^.V = GV_Active ) then begin
			C := ComScore( M , CS_Awareness ) + StatBonus( M , STAT_Intelligence );
			if C > hi then hi := C;
		end;
		M := M^.Next;
	end;

	{ Try the skill roll. }
	MakeStealthRoll := ( Random( 100 ) + ComScore( PC , CS_Stealth ) + StatBonus( PC , STAT_Reflexes ) ) > ( 49 + hi );
end;

Procedure InvokePower( GB: GameBoardPtr; PC , I: GearPtr; X1,Y1: Integer );
	{ Activate the listed INVOCATION gear. }
	{ Add to the PC's AP_Used counter. }
	{ Decrease the PC's mana total. }
	{ Call the effect frontend- if the PC has killed himself using this power, }
	{  the body will be removed at the end of that. }
	{ - Initialize the effect_history variables. }
var
	T: effect_stencil;
begin
	{ If the user is silenced and the invocation is a spell, it'll probably fail. }
	if ( I^.S = GS_Spell ) and ( NAttValue( PC^.NA , NAG_FightingStat , NAS_Silenced ) <> 0 ) and ( Random( 4 ) <> 1 ) then begin
		ApplySTCEffectToModel( GB , 'FX_SILENCED' , PC );
		Add_MP_Damage( PC , MPRequired( I ) );
		if In_Combat_Mode then EndTurn( PC )
		else ClearFightingStats( GB );
		Exit;
	end;

	{ Initialize the effect history. }
	if I^.Stat[ STAT_Invocation_Range ] = IRNG_Missile then Add_Initial_Shot( NAttValue( I^.NA , NAG_EffectData , NAS_ShotSequence ) , NAttValue( PC^.NA , NAG_Location , NAS_X ) , NAttValue( PC^.NA , NAG_Location , NAS_Y ) , X1 , Y1 );

	Calc_Invocation_Stencil( GB , T , I , X1 , Y1 );
	EffectFrontEnd( GB , I^.SubCom , PC , T );
	Add_MP_Damage( PC , MPRequired( I ) );
	SetNAtt( PC^.NA , NAG_FightingStat , NAS_Hidden , 0 );
	if In_Combat_Mode then EndTurn( PC )
	else ClearFightingStats( GB );
end;

Function CastIdentify( GB: GameBoardPtr; PC,Spell: GearPtr ): Boolean;
	{ The PC wants to identify an item. Prompt for a choice, then set it }
	{ as identified. }
var
	RPM: RPGMenuPtr;
	T,N: Integer;
	Item,It: GearPtr;
begin
	{ Create the menu. }
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_1CR_Total );
	N := 1;
	for t := 1 to Num_Party_Members do begin
		if G_Party[ t ] <> Nil then begin
			Item := G_Party[ t ]^.InvCom;
			while Item <> Nil do begin
				if ( Item^.G = GG_Item ) and not IsIdentified( Item ) then begin
					AddRPGMenuItem( RPM , GearName( Item ) + ' (' + GearName( G_Party[ t ] ) + ')' , N );
					Inc( N );
				end;
				Item := Item^.Next;
			end;
		end;
	end;
	RPMSortAlpha( RPM );
	AlphaKeyMenu( RPM );
	AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

	{ Query the menu. }
	SetupOneColumnRedraw( GB , MsgString( 'CastIdentify' ) , '' );
	N := SelectMenu( RPM , @OneColumnRedraw );
	DisposeRPGMenu( RPM );

	{ Identify the item. }
	if N > -1 then begin
		It := Nil;

		for t := 1 to Num_Party_Members do begin
			if G_Party[ t ] <> Nil then begin
				Item := G_Party[ t ]^.InvCom;
				while Item <> Nil do begin
					if ( Item^.G = GG_Item ) and not IsIdentified( Item ) then begin
						Dec( N );
						if N = 0 then begin
							It := Item;
							Break;
						end;
					end;
					Item := Item^.Next;
				end;
			end;
		end;

		if It <> Nil then begin
			It^.V := GV_Identified;
			Alert( GB , ReplaceHash( MsgSTring( 'Identify_Results' ) , GearName( It ) ) );
		end else Alert( GB , 'ERROR: CastIdentify cannot find the item you selected!!!' );

		Add_MP_Damage( PC , MPRequired( Spell ) );
		CastIdentify := True;
	end else begin
		CastIdentify := False;
	end;
end;


Function ShouldEnterCombat( GB: GameBoardPtr ): Boolean;
	{ The party should enter combat if there are any active }
	{ monsters within a certain range of the party. }
var
	M: GearPtr;
	NeedToFight: Boolean;
begin
	M := GB^.Contents;
	NeedToFight := False;
	while M <> Nil do begin
		if IsFighting( GB , M ) then begin
			NeedToFight := True;
		end;

		M := M^.Next;
	end;
	ShouldEnterCombat := NeedToFight;
end;

Function WalkTowardsGoal( GB: GameBoardPtr; M: GearPtr; var WalkError: Boolean ): GearPtr;
	{ Walk M towards the hotmap goal; stop when action points all used }
	{ or when destination reached or when movement blocked. }
	{ Cardinal movements cost 2AP; Diagonals cost 3AP. }
	{ If movement stops because another model was encountered, return a pointer to }
	{ that model. }
	{ If movement stops because no movement is possible, set WalkError to TRUE. }
var
	X1,Y1,X,Y,MoveD,Best,D: Integer;
	M2: GearPtr;
begin
	M2 := Nil;

	{ Record the current position of the model being moved. }
	X1 := NAttValue( M^.NA , NAG_Location , NAS_X );
	Y1 := NAttValue( M^.NA , NAG_Location , NAS_Y );

	{ If the current position is the desired position, don't do anything. }
	if HotMap[X1,Y1] = 0 then begin
		Exit( Nil );
	{ If the desired position isn't on the map, don't do anything. }
	end else begin
		{ Keep going until we're there, or until the model is out of action points. }
		while OnTheMap( GB , X1 , Y1 ) and ( HotMap[X1,Y1] <> 0 ) and ( ActionPoints( M ) > NAttValue( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent ) ) do begin
			{ Do the screen redraw. }
			ClearUnderlays;
			if M^.S = GS_PCTeam then begin
				IndicateTile( GB , NAttValue( M^.NA , NAG_Location , NAS_X ), NAttValue( M^.NA , NAG_Location , NAS_Y ) , IT_PlayerModel );
			end else begin
				IndicateTile( GB , NAttValue( M^.NA , NAG_Location , NAS_X ), NAttValue( M^.NA , NAG_Location , NAS_Y ) , IT_EnemyModel );
			end;
			TacticsRedraw( GB , M , 0 );
			DoFlip;

			{ RPGKey isn't just an input-getter; it also serves as a timer. }
			AnimDelay;

			{ Decide what's the best direction to travel in. }
			MoveD := 0;
			Best := HotMap[ X1 , Y1 ];
			{ Check all the directions; whichever one has the lowest score is best. }
			for D := 1 to 9 do begin
				X := X1 + VecDir[ D , 1 ];
				Y := Y1 + VecDir[ D , 2 ];
				if OnTheMap( GB , X , Y ) and ( HotMap[ X , Y ] < Best ) and ( D <> 5 ) then begin
					MoveD := D;
					Best := HotMap[ X , Y ];
				end;
			end;

			{ If a direction was found, go there. }
			if ( MoveD >= 1 ) and ( MoveD <= 9 ) then begin
				X1 := X1 + VecDir[ MoveD , 1 ];
				Y1 := Y1 + VecDir[ MoveD , 2 ];
				if Models_On_Gameboard[ X1 , Y1 ] = Nil then begin
					SetNAtt( M^.NA , NAG_Location , NAS_X , X1 );
					SetNAtt( M^.NA , NAG_Location , NAS_Y , Y1 );

					if M^.S = GS_PCTeam then begin
						UpdatePCPosition( GB , M );
					end;

					{ Add the movement points spent. }
					if ( MoveD mod 2 ) = 1 then AddNAtt( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent , 3 )
					else AddNAtt( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent , 2 );
				end else begin
					M2 := Models_On_Gameboard[ X1 , Y1 ];
					break;
				end;

			{ If no direction was found, break the loop. }
			end else begin
				{ If no move possible, break this loop. }
				{ Also set WalkError. }
				WalkError := True;
				Break;
			end;
		end;
	end;
	{ Return the model that was bumped into, or NIL if nothing was bumped into. }
	WalkTowardsGoal := M2;
end;

Function WalkRandomly( GB: GameBoardPtr; M: GearPtr ): GearPtr;
	{ Walk M in no particular direction; stop when action points all used }
	{ or when destination reached or when movement blocked. }
	{ Cardinal movements cost 2AP; Diagonals cost 3AP. }
	{ If movement stops because another model was encountered, return a pointer to }
	{ that model. }
	{ If movement stops because no movement is possible, set WalkError to TRUE. }
var
	X1,Y1,X,Y,MoveD: Integer;
	M2: GearPtr;
begin
	M2 := Nil;

	{ Record the current position of the model being moved. }
	X1 := NAttValue( M^.NA , NAG_Location , NAS_X );
	Y1 := NAttValue( M^.NA , NAG_Location , NAS_Y );

	{ Keep going until we're there, or until the model is out of action points. }
	while OnTheMap( GB , X1 , Y1 ) and ( ActionPoints( M ) > NAttValue( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent ) ) do begin
		{ Do the screen redraw. }
		ClearUnderlays;
		if M^.S = GS_PCTeam then begin
			IndicateTile( GB , NAttValue( M^.NA , NAG_Location , NAS_X ), NAttValue( M^.NA , NAG_Location , NAS_Y ) , IT_PlayerModel );
		end else begin
			IndicateTile( GB , NAttValue( M^.NA , NAG_Location , NAS_X ), NAttValue( M^.NA , NAG_Location , NAS_Y ) , IT_EnemyModel );
		end;
		TacticsRedraw( GB , M , 0 );
		DoFlip;
		AnimDelay;

		{ Decide on a direction to travel in. }
		MoveD := Random( 8 );
		X := X1 + AngDir[ MoveD , 1 ];
		Y := Y1 + AngDir[ MoveD , 2 ];
		if ( Models_On_Gameboard[ X , Y ] = Nil ) and not TileBlocksMovement( GB , X , Y ) then begin
			SetNAtt( M^.NA , NAG_Location , NAS_X , X );
			SetNAtt( M^.NA , NAG_Location , NAS_Y , Y );
			X1 := X;
			Y1 := Y;

			if M^.S = GS_PCTeam then begin
				UpdatePCPosition( GB , M );
			end;

			{ Add the movement points spent. }
			if ( MoveD mod 2 ) = 1 then AddNAtt( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent , 3 )
			else AddNAtt( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent , 2 );
		end else if Models_On_Gameboard[ X , Y ] <> Nil then begin
			M2 := Models_On_Gameboard[ X , Y ];
			break;
		end else begin
			AddNAtt( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent , 1 );
		end;
	end;
	{ Return the model that was bumped into, or NIL if nothing was bumped into. }
	WalkRandomly := M2;
end;

Function WalkTowardsSpot( GB: GameBoardPtr; M: GearPtr; X2,Y2: Integer ): GearPtr;
	{ Walk towards a certain spot, using the rules of the above procedure. }
var
	WalkError: Boolean;
begin
	if not OnTheMap( GB , X2 , Y2 ) then begin
		Exit( Nil );
	{ If the desired position is an obstacle, don't walk towards it. }
	end else if TileBlocksMovement( GB , X2 , Y2 ) then begin
		Exit( Nil );
	end else begin
		{ Everything seems to be okay. Generate a pathfinding hotmap for the }
		{ requested destination. }
		CreatePointHotMap( GB , X2 , Y2 , True );
		WalkTowardsSpot := WalkTowardsGoal( GB , M , WalkError );
	end;
end;

Function AttackRange( PC: GearPtr ): Integer;
	{ Return the range at which this model can attack. }
var
	Weapon,Ammo: GearPtr;
begin
	{ Step One: Find the weapon. }
	Weapon := FindEquippedItem( PC , NAV_Hand1 );

	{ If the weapon exists and is a bow or sling, check for ammo. }
	if ( Weapon <> Nil ) and ( ( Weapon^.S = GS_Bow ) or ( Weapon^.S = GS_Sling ) ) then begin
		Ammo := FindEquippedItem( PC , NAV_Hand2 );
		if ( Ammo = Nil ) or ( Ammo^.S <> AmmoTypeNeeded( Weapon ) ) then begin
			AttackRange := 0;
		end else begin
			AttackRange := Weapon^.Stat[ STAT_WeaponRange ] + 1;
		end;
	end else if Weapon <> Nil then begin
		AttackRange := Weapon^.Stat[ STAT_WeaponRange ] + 1;
	end else begin
		AttackRange := 0;
	end;
end;

Procedure BasicAttack( GB: GameBoardPtr; PC, Target: GearPtr );
	{ Perform a basic unarmed or weapon attack. }
	Function WeaponShotSeq( Weapon: GearPtr ): Integer;
		{ Return the shot sequence used by this weapon. }
	const
		WSS: Array [1..Num_Item_Types] of Byte = (
			NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet,
			NAV_Shot_Arrow, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Stone,
			NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet,
			NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Bullet,
			NAV_Shot_Bullet, NAV_Shot_Bullet, NAV_Shot_Cannonball
		);
	var
		it: Integer;
	begin
		if Weapon = Nil then begin
			WeaponShotSeq := NAV_Shot_Bullet;
		end else begin
			it := NAttValue( Weapon^.NA , NAG_EffectData , NAS_SHotSequence );
			if ( it < 1 ) or ( it > Num_ShotSeq ) then it := WSS[ Weapon^.S ];
			WeaponShotSeq := it;
		end;
	end;
var
	FX,Weapon,Ammo: GearPtr;
	FX_Stencil: effect_stencil;
	X,Y,X0,Y0,Num_Attacks: Integer;
begin
	{ Step One: Find the weapon. }
	Weapon := FindEquippedItem( PC , NAV_Hand1 );

	{ If the weapon exists and is a bow or sling, check for ammo. }
	if ( Weapon <> Nil ) and ( ( Weapon^.S = GS_Bow ) or ( Weapon^.S = GS_Sling ) ) then begin
		Ammo := FindEquippedItem( PC , NAV_Hand2 );
		if ( Ammo = Nil ) or ( Ammo^.S <> AmmoTypeNeeded( Weapon ) ) then begin
			{ Either we have no ammo or it's the wrong kind. }
			Weapon := Nil;
			Ammo := Nil;
		end;
	end else begin
		Ammo := Nil;
	end;

	{ Create the attack request. }
	FX := CreateAttackEffect( PC, Weapon );

	{ Create the stencil. }
	for X := 1 to GB^.Map_Width do for Y := 1 to GB^.Map_Height do FX_Stencil[X,Y] := False;
	X := NAttValue( Target^.NA , NAG_Location , NAS_X );
	Y := NAttValue( Target^.NA , NAG_Location , NAS_Y );
	FX_Stencil[ X , Y ] := True;

	{ Determine how many attacks the attacker gets. }
	{ Provided that you haven't moved more than one step, you get to make }
	{ a full attack. }
	if NAttValue( PC^.NA , NAG_FightingStat , NAS_ActionPointsSpent ) <= 3 then begin
		{ Number of attacks is calculated using the basic comscore. No fair }
		{ using accuracy-improving items to increase speed. }
		Num_Attacks := ( BaseComScore( PC , CS_PhysicalAttack ) div 20 ) + 1;
	end else Num_Attacks := 1;

	X0 := NAttValue( PC^.NA , NAG_Location , NAS_X );
	Y0 := NAttValue( PC^.NA , NAG_Location , NAS_Y );

	{ Perform the attacks. }
	while ( Num_Attacks > 0 ) and IsAlright( Target ) do begin
		if ( Abs( X0 - X ) > 1 ) or ( Abs( Y0 - Y ) > 1 ) then begin
			{ This is a missile attack. Add the initial shot. }
			Add_Initial_Shot( WeaponShotSeq( Weapon ) , X0 , Y0 , X , Y );
		end;

		{ Process the effect. }
		EffectFrontEnd( GB, FX, PC, FX_Stencil );

		{ If we have ammo, spend one arrow/bullet. }
		if Ammo <> Nil then begin
			SpendOneCharge( PC , Ammo );
			if Ammo = Nil then Num_Attacks := 0;
		end;

		Dec( Num_Attacks );
	end;

	{ Dispose of the effect. }
	DisposeGear( FX );

	{ Attacking automatically reveals the target and will probably reveal }
	{ the attacker as well. }
	SetNAtt( Target^.NA , NAG_FightingStat , NAS_Hidden , 0 );
	if IsAlright( Target ) or not MakeStealthRoll( GB , PC ) then SetNAtt( PC^.NA , NAG_FightingStat , NAS_Hidden , 0 );

	if In_Combat_Mode then EndTurn( PC );
end;


Procedure BumpAction( GB: GameBoardPtr; Actor , Target: GearPtr );
	{ ACTOR has bumped into TARGET. Do something appropriate... probably an attack. }
var
	Trigger: String;
begin
	if AreEnemies( Actor , Target ) then begin
		BasicAttack( GB , Actor , Target );
	end else if Actor^.S = GS_PCTeam then begin
		Trigger := TRIGGER_UseProp;
		TriggerGearScript( GB, Target, Trigger );
	end;
	if In_Combat_Mode then EndTurn( Actor );
end;

Procedure BumpTile( GB: GameBoardPtr; Actor: GearPtr; X,Y: Integer );
	{ ACTOR has bumped into tile X,Y. If there's a checkpoint there, }
	{ activate its script. }
var
	Trigger: String;
	CP: GearPtr;
begin
	CP := FindCheckpointXY( GB , X , Y );
	if ( Actor^.S = GS_PCTeam ) and ( CP <> Nil ) then begin
		Trigger := TRIGGER_UseProp;
		TriggerGearScript( GB, CP, Trigger );
	end;
	if In_Combat_Mode then EndTurn( Actor );
end;


Function AimPower( GB: GameBoardPtr; PC,I: GearPtr ): Boolean;
	{ The player will aim and use a power. Have fun. }
	{ Return TRUE if the power was invoked, or FALSE if it was cancelled. }
var
	X,Y,X0,Y0,TX,TY,IRng: Integer;
	Can_Target_Here,Target_Area: effect_stencil;
	A: Char;
begin
	{ Start by creating an effect stencil which should show where the PC can }
	{ target. Use the raycaster for this, since doing it once is much faster }
	{ than checking line of sight every time the PC moves the mouse. }
	X0 := NAttValue( PC^.NA , NAG_Location , NAS_X );
	Y0 := NAttValue( PC^.NA , NAG_Location , NAS_Y );
	Plot_Circular_Area( GB , Can_Target_Here , X0 , Y0 , 10 );

	{ Initialize some values. }
	TX := 0;

	{ Determine the Invocation Range. This is gonna vary depending on whether }
	{ or not we're in combat. }
	if In_Combat_Mode or ( I^.Stat[ STAT_Invocation_NonCombatRange ] = 0 ) then IRng := I^.Stat[ STAT_Invocation_Range ]
	else IRng := I^.Stat[ STAT_Invocation_NonCombatRange ];

	{ There are a couple of special cases. }
	if IRng = IRNG_Unusable then begin
		Alert( GB , ReplaceHash( MsgString( 'SPELL_MAY_NOT_BE_USED_HERE' ) , GearName ( I ) ) );
		Exit( False );
	end else if IRng = IRNG_Identify then begin
		Exit( CastIdentify( GB , PC , I ) );
	end;

	{ Begin the processing loop. }
	repeat
		{ Prepare the graphics. }
		ClearUnderlays;
		IndicateTile( GB , X0 , Y0 , IT_PlayerModel );

		{ Gather some information about where the mouse is }
		{ pointing, and whether or not it's a good target point. }
		if IRng = IRng_Personal then begin
			TX := X0;
			TY := Y0;
		end else if IRng = IRng_Touch then begin
			if OnTheMap( GB , tile_X , tile_Y ) and ( Abs( X0 - tile_X ) <= 1 ) and ( Abs( Y0 - tile_Y ) <= 1 ) then begin
				TX := tile_X;
				TY := tile_Y;
			end;
		end else if IRng = IRng_Missile then begin
			if OnTheMap( GB , tile_X , tile_Y ) and Can_Target_Here[ tile_X , tile_Y ] then begin
				TX := tile_X;
				TY := tile_Y;
			end;
		end else if IRng = IRng_OnePC then begin
			if OnTheMap( GB , tile_X , tile_Y ) and ( Models_On_Gameboard[ tile_x , tile_y ] <> Nil ) and ( Models_On_Gameboard[ tile_x , tile_y ]^.S = GS_PCTeam ) then begin
				TX := tile_X;
				TY := tile_Y;
			end;
		end;
		if OnTheMap( GB , TX , TY ) then begin
			Calc_Invocation_Stencil( GB , Target_Area , I , TX , TY );
			for X := 1 to GB^.Map_Width do for Y := 1 to GB^.Map_Height do if Target_Area[ X , Y ] then IndicateTile( GB , X , Y , IT_Targeting );
		end;
		BasicRedraw( GB );
		DoFlip;

		A := RPGKey;
		{ Left mouse key escapes from targeting. }
		if A = RPK_RightButton then A := #27;

	until ( A = RPK_MouseButton ) or ( A = #27 );
	ClearUnderlays;

	if OnTheMap( GB , TX , TY ) and ( A <> #27 ) then begin
		InvokePower( GB , PC , I , TX , TY );
		AimPower := True;
	end else begin
		AimPower := False;
	end;
end;

Procedure AttemptStealth( GB: GameBoardPtr; PC: GearPtr );
	{ The PC will attempt to hide. }
begin
	if MakeStealthRoll( GB , PC ) then begin
		{ Yay! Invisibility for you. }
		ApplySTCEffectToModel( GB , 'FX_STEALTH_OK' , PC );
		SetNAtt( PC^.NA , NAG_FightingStat , NAS_Hidden , 2 + Random( 5 ) );
	end else begin
		{ No invisibility for you this time. }
		ApplySTCEffectToModel( GB , 'FX_STEALTH_FAIL' , PC );
	end;
end;

Procedure AttemptTurnUndead( GB: GameBoardPtr; PC: GearPtr );
	{ The PC will attempt to turn undead. That is all. }
var
	T_Invo: GearPtr;
begin
	{ Step One: Load the invocation. }
	T_Invo := LoadNewSTC( 'DEFAULT_TURN_UNDEAD' );

	{ Step Two: Invoke the invocation. }
	if T_Invo <> Nil then begin
		InvokePower( GB , PC , T_Invo , NAttValue( PC^.NA , NAG_Location , NAS_X ) , NAttValue( PC^.NA , NAG_Location , NAS_Y ) );
		DisposeGear( T_Invo );
	end;

	{ Step Three: Add one to the Turning counter. }
	AddNAtt( PC^.NA , NAG_DailyData , NAS_TurnAttempts , 1 );
end;

Procedure BuildInvocationMenu( PC: GearPtr; RPM: RPGMenuPtr );
	{ Add any invocations the PC can make to the menu. The value for each }
	{ menu item is the number of the invocation along the PC's subcom list. }
var
	I: GearPtr;
	N,U: Integer;
begin
	{ Add the spells. }
	I := PC^.SubCom;
	N := 1;
	while I <> Nil do begin
		if ( I^.G = GG_Invocation ) and ( MPRequired( I ) <= CurrentMP( PC ) ) then begin
			AddRPGMenuItem( RPM , GearName( I ) , N , GearDesc( I ) );
		end;
		I := I^.Next;
		Inc( N );
	end;
	RPMSortAlpha( RPM );
	AlphaKeyMenu( RPM );

	{ Add other abilities. }
	AddRPGMenuItem( RPM , '-----' , -1 );
	AddRPGMenuItem( RPM , MsgSTring( 'MENU_BACKPACK' ) , InvoMenu_Backpack );
	N := TurningAttemptsPerDay( PC );
	U := NAttValue( PC^.NA , NAG_DailyData , NAS_TurnAttempts );
	if In_Combat_Mode and ( ( N - U ) > 0 ) then begin
		AddRPGMenuItem( RPM , ReplaceHash( ReplaceHash( MsgSTring( 'MENU_SKILL_TURN' ) , BStr( N - U ) ) , BStr( N ) ) , InvoMenu_TurnUndead );
	end;
	if In_Combat_Mode and ( BaseComScore( PC , CS_Stealth ) > 0 ) and ( NAttValue( PC^.NA , NAG_FightingStat , NAS_Hidden ) = 0 ) then begin
		AddRPGMenuItem( RPM , MsgSTring( 'MENU_SKILL_STEALTH' ) , InvoMenu_Stealth );
	end;
end;

Function PCSelectInvocation( GB: GameBoardPtr; PC: GearPtr ): Boolean;
	{ The PC is about to cast a spell or something. Go for it. }
var
	RPM: RPGMenuPtr;
	N: Integer;
	Invo: GearPtr;
begin
	RPM := OpenPopupMenu;

	{ Add the spells known to the list. }
	BuildInvocationMenu( PC , RPM );

	{ Select an invocation. }
	N := QueryPopupMenu( GB , RPM );
	DisposeRPGMenu( RPM );

	if N > 0 then begin
		Invo := RetrieveGearSib( PC^.SubCom , N );
		PCSelectInvocation := AimPower( GB , PC , Invo );
	end else if N = InvoMenu_Backpack then begin
		InventoryScreen( PC , GB );
		PCSelectInvocation := True;
	end else if N = InvoMenu_Stealth then begin
		AttemptStealth( GB , PC );
		PCSelectInvocation := True;
	end else if N = InvoMenu_TurnUndead then begin
		AttemptTurnUndead( GB , PC );
		PCSelectInvocation := True;
	end else begin
		PCSelectInvocation := False;
	end;
end;

Function CanActNow( M: GearPtr ): Boolean;
	{ Return TRUE if M can act this turn, or FALSE otherwise. }
begin
	CanActNow := IsAlright( M ) and ( NAttValue( M^.NA , NAG_FightingStat , NAS_LoseATurn ) <= 0 );
end;

Function HasActionsRemaining( M: GearPtr ): Boolean;
	{ Return TRUE if this model has actions remaining, or FALSE if not. }
begin
	HasActionsRemaining := ( ActionPoints( M ) > NAttValue( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent ) );
end;

Function RangeArcCheck( GB: GameBoardPtr; Attacker,Target: GearPtr ): Boolean;
	{ Return TRUE if the ATTACKER can attack the TARGET from its current }
	{ location, or FALSE otherwise. }
var
	S: Effect_Stencil;
begin
	Plot_Circular_Area( GB , S , NAttValue( Attacker^.NA , NAG_Location , NAS_X ) , NAttValue( Attacker^.NA , NAG_Location , NAS_Y ) , AttackRange( Attacker ) );
	RangeArcCheck := S[ NAttValue( Target^.NA , NAG_Location , NAS_X ) , NAttValue( Target^.NA , NAG_Location , NAS_Y ) ];
end;

Procedure Perform_Confused_Action( GB: GameBoardPtr; PC: GearPtr );
	{ This model is confused. Walk around randomly; if you touch another }
	{ model, attack it. }
var
	M2: GearPtr;
begin
	M2 := WalkRandomly( GB , PC );
	if ( M2 <> Nil ) and ( M2^.G = GG_Model ) then BasicAttack( GB , PC , M2 );
	EndTurn( PC );
	AddNAtt( PC^.NA , NAG_FightingStat , NAS_Confused , -1 );
end;

Function GetPCInput( GB: GameBoardPtr; PC: GearPtr ): Integer;
	{ Get input from the PC. If the player wants to quit the }
	{ game, return CMD_Quit. }
var
	Command: Integer;
	gdi: Char;
	X,Y,APtoSpend: Integer;
	Target: GearPtr;
begin
	{ Keep processing actions for the current party member as long }
	{ as he's alive, has action points remaining, and hasn't quit. }
	Command := -1;

	if NAttValue( PC^.NA , NAG_FightingStat , NAS_LoseATurn ) > 0 then begin
		AddNAtt( PC^.NA , NAG_FightingStat , NAS_LoseATurn , -1 );
		EndTurn( PC );
	end else if ( NAttValue( PC^.NA , NAG_FightingStat , NAS_Confused ) > 0 ) and ( Random( 3 ) <> 1 ) then begin
		Perform_Confused_Action( GB , PC );
	end;

	{ Now, one of the things we're going to do here is create a hotmap }
	{ centered on the PC's position. That way, we can instantly see how }
	{ many APs it'll take to move to any tile within range. }
	{ Do it once now, and again if the PC moves. }
	CreatePointHotMap( gb , NAttValue( PC^.NA , NAG_Location , NAS_X ) , NAttValue( PC^.NA , NAG_Location , NAS_Y ) , True );

	while CanActNow( PC ) and OnTheMap( GB , PC ) and ( Command <> CMD_Quit ) and HasActionsRemaining( PC ) do begin
		{ Prepare the graphics. }
		ClearUnderlays;
		if Show_Legal_Moves then begin
			APToSpend := ActionPoints( PC ) - NAttValue( PC^.NA , NAG_FightingStat , NAS_ActionPointsSpent );
			for X := 1 to GB^.Map_Width do begin
				for Y := 1 to GB^.Map_Height do begin
					if ( HotMap[ X , Y ] <= APtoSpend ) or ( HotMap[ X , Y ] <= 3 ) then IndicateTile( GB , X , Y , IT_LegalMove );
				end;
			end;
		end;
		IndicateTile( GB , NAttValue( PC^.NA , NAG_Location , NAS_X ), NAttValue( PC^.NA , NAG_Location , NAS_Y ) , IT_PlayerModel );
		if OnTheMap( GB , tile_X , tile_Y ) then begin
			IndicateTile( GB , tile_x , tile_y , IT_Cursor );
			APtoSpend := HotMap[ tile_X , tile_Y ];
		end else APtoSpend := 0;
		TacticsRedraw( GB , PC , APtoSpend );
		DoFlip;

		{ Get input from the user. This will likely return a timer event }
		{ instead of real input. }
		gdi := RPGKey;

		{ Process the input. }
		if gdi = KeyMap[ KMC_Quit ].kcode then begin
			Command := CMD_Quit;

		end else if gdi = KeyMap[ KMC_Center ].kcode then begin
			FocusOnTile( GB , NAttValue( PC^.NA , NAG_Location , NAS_X ), NAttValue( PC^.NA , NAG_Location , NAS_Y ) );


		end else if gdi = ' ' then begin
			EndTurn( PC );

		end else if gdi = RPK_MouseButton then begin
			if ( AttackRange( PC ) > 1 ) and ( Models_On_Gameboard[ tile_X , tile_Y ] <> Nil ) and AreEnemies( PC , Models_On_Gameboard[ tile_X , tile_Y ] ) then begin
				Target := Models_On_Gameboard[ tile_X , tile_Y ];
				if RangeArcCheck( GB , PC , Target ) then begin
					BasicAttack( GB , PC , Target );
					EndTurn( PC );
				end else Alert( GB , ReplaceHash( MsgString( 'TARGET_OUT_OF_RANGE' ) , GearName( Target ) ) );
			end else begin
				Target := WalkTowardsSpot( GB , PC , tile_X , tile_Y );
				if Target <> Nil then begin
					BumpAction( GB , PC , Target );
				end;
				CreatePointHotMap( gb , NAttValue( PC^.NA , NAG_Location , NAS_X ) , NAttValue( PC^.NA , NAG_Location , NAS_Y ) , True );
			end;

		end else if gdi = RPK_RightButton then begin
			if PCSelectInvocation( GB , PC ) then EndTurn( PC );

		end;
	end;

	GetPCInput := Command;
end;


Function GetPartyInput( GB: GameBoardPtr ): Integer;
	{ Get input from the party. If the player wants to quit the }
	{ game, return CMD_Quit. }
var
	PC,Command: Integer;
begin
	PC := 1;
	Command := -1;

	{ Keep processing party actions until every member of the party runs out of }
	{ action points, or until we get a quit command. }
	while ( PC <= Num_Party_Members ) and ( Command <> CMD_Quit ) and ShouldEnterCombat( GB ) do begin
		if G_Party[ PC ] <> Nil then Command := GetPCInput( GB , G_Party[ PC ] );
		HandleTriggers( GB );

		{ Switch to the next member of the party. }
		PC := PC + 1;
	end;

	GetPartyInput := Command;
end;

Function SelectRandomInvocation( M: GearPtr ): GearPtr;
	{ Select a random direct-use invocation known by this model. }
	Function IsUsableInvocation( I: GearPtr ): Boolean;
		{ Return TRUE if I is a usable invocation, or FALSE otherwise. }
	begin
		IsUsableInvocation := ( I^.G = GG_Invocation ) and ( MPRequired( I ) <= CurrentMP( M ) );
	end;
var
	N: Integer;
	I,it: GearPtr;
begin
	{ Step One: Go through the gear structure, counting the invocations. }
	N := 0;
	I := M^.SubCom;
	while I <> Nil do begin
		if IsUsableInvocation( I ) then Inc( N );
		I := I^.Next;
	end;

	if N > 0 then begin
		N := Random( N );
		I := M^.SubCom;
		it := Nil;
		while ( I <> Nil ) and ( it = Nil ) do begin
			if IsUsableInvocation( I ) then begin
				Dec( N );
				if N = -1 then it := I;
			end;
			I := I^.Next;
		end;
	end else begin
		it := Nil;
	end;
	SelectRandomInvocation := it;
end;

Procedure Perform_Monster_Action( GB: GameBoardPtr; M: GearPtr );
	{ Do the actions for this monster. }
var
	I,M2,TM: GearPtr;
	Tries: Integer;
	Can_Target_Here: effect_stencil;
	WalkError: Boolean;
begin
	{ There's a TRIES counter here to make sure the NPC doesn't get itself }
	{ caught in an endless loop, which could happen if it was trapped inside of a }
	{ wall or similar. }
	Tries := 0;

	if NAttValue( M^.NA , NAG_FightingStat , NAS_LoseATurn ) > 0 then begin
		AddNAtt( M^.NA , NAG_FightingStat , NAS_LoseATurn , -1 );
		EndTurn( M );
	end else if ( NAttValue( M^.NA , NAG_FightingStat , NAS_Confused ) > 0 ) and ( Random( 3 ) <> 1 ) then begin
		Perform_Confused_Action( GB , M );
	end;

	WalkError := False;

	while OnTheMap( GB , M ) and CanActNow( M ) and ( ActionPoints( M ) > NAttValue( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent ) ) and ( Tries < 100 ) and ( not Party_Dead ) do begin
		{ Simple way of doing things. Select one of the monster's invocations at }
		{ random. Then, try to apply it to a good target. }
		I := SelectRandomInvocation( M );
		if ( I <> Nil ) and ( Random(5) <> 1 ) then begin
			if I^.Stat[ STAT_Invocation_Range ] = IRng_Personal then begin
				{ It's a personal range power. Let's just use it and worry }
				{ about the appropriateness later. }
				InvokePower( GB , M , I , NAttValue( M^.NA , NAG_Location , NAS_X ) , NAttValue( M^.NA , NAG_Location , NAS_Y ) );

			end else if I^.Stat[ STAT_Invocation_Range ] = IRng_Touch then begin
				CreateTeamHotMap( GB , M );
				M2 := WalkTowardsGoal( GB , M , WalkError );
				if ( M2 <> Nil ) and ( M2^.G = GG_Model ) and AreEnemies( M , M2 ) then InvokePower( GB , M , I , NAttValue( M2^.NA , NAG_Location , NAS_X ) , NAttValue( M2^.NA , NAG_Location , NAS_Y ) );

			end else if I^.Stat[ STAT_Invocation_Range ] = IRng_Missile then begin
				M2 := Nil;
				Plot_Circular_Area( GB , Can_Target_Here , NAttValue( M^.NA , NAG_Location , NAS_X ) , NAttValue( M^.NA , NAG_Location , NAS_Y ) , 10 );
				TM := GB^.Contents;
				while TM <> Nil do begin
					if OnTheMap( GB , TM ) and AreEnemies( M , TM ) then begin
						if Can_Target_Here[ NAttValue( TM^.NA , NAG_Location , NAS_X ) , NAttValue( TM^.NA , NAG_Location , NAS_Y ) ] then begin
							if M2 = Nil then M2 := TM
							else if Random( 3 ) = 1 then M2 := TM;
						end;
					end;
					TM := TM^.Next;
				end;
				if M2 <> Nil then begin
					InvokePower( GB , M , I , NAttValue( M2^.NA , NAG_Location , NAS_X ) , NAttValue( M2^.NA , NAG_Location , NAS_Y ) );
				end else begin
					CreateTeamHotMap( GB , M );
					M2 := WalkTowardsGoal( GB , M , WalkError );
					if ( M2 <> Nil ) and ( M2^.G = GG_Model ) and AreEnemies( M , M2 ) then BumpAction( GB , M , M2 );
				end;
			end;
		end else if WalkError then begin
			{ Can't move towards our goal, apparently... walk randomly! }
			M2 := WalkRandomly( GB , M );
			if ( M2 <> Nil ) and ( M2^.G = GG_Model ) and AreEnemies( M , M2 ) then BumpAction( GB , M , M2 );

		end else begin
			CreateTeamHotMap( GB , M );
			M2 := WalkTowardsGoal( GB , M , WalkError );
			if ( M2 <> Nil ) and ( M2^.G = GG_Model ) and AreEnemies( M , M2 ) then BumpAction( GB , M , M2 );
		end;
		Inc( Tries );
	end;
end;

Procedure GetMonsterInput( GB: GameBoardPtr );
	{ Go through each "monster" on the board and act appropriately. }
var
	M: GearPtr;
begin
	{ Look through everything on the board, for monsters to activate and move. }
	M := GB^.Contents;
	while M <> Nil do begin
		if ( M^.G = GG_Model ) and ( M^.V = GV_Active ) and OnTheMap( GB , M ) and ShouldEnterCombat( GB ) and ( not Should_Exit_Scene ) then begin
			Perform_Monster_Action( GB , M );
		end;
		HandleTriggers( GB );

		M := M^.Next;
	end;
end;


Procedure RestoreMovementPoints( GB: GameBoardPtr );
	{ Restore the movement points to all models. }
var
	M: GearPtr;
begin
	M := GB^.Contents;
	while M <> Nil do begin
		SetNAtt( M^.NA , NAG_FightingStat , NAS_ActionPointsSpent , 0 );
		if NAttValue( M^.NA , NAG_FightingStat , NAS_Hidden ) > 0 then AddNAtt( M^.NA , NAG_FightingStat , NAS_Hidden , -1 );
		M := M^.Next;
	end;
end;

Procedure DoleCombatXP( GB: GameBoardPtr );
	{ Combat is over. Yay! Time to divide up the experience. }
	{ How to tell the amount of XP? All the monsters we killed are }
	{ still on the map, they've just been moved off to one side. }
var
	Total,T,N,GP,Party_Level: LongInt;
	M,M2: GearPtr;
	msg: String;
begin
	{ Calculate the Party_Level. }
	Party_Level := AveragePCLevel;
	if Party_Level < 1 then Party_Level := 1;

	{ Count up the XP, and clean up the dead models while we're at it. }
	Total := 0;
	GP := 0;
	M := GB^.Contents;
	while M <> Nil do begin
		M2 := M^.Next;
		if ( M^.G = GG_Model ) and ( M^.S <> GS_PCTeam ) and ( M^.S <> GS_AllyTeam ) and not IsAlright( M ) then begin
			Total := Total + ExperienceValue( M , Party_Level );
			GP := GP + NAttValue( M^.NA , NAG_MonsterData , NAS_MonsterGP );
			RemoveGear( GB^.Contents , M );
		end else if NAttValue( M^.NA , NAG_FightingStat , NAS_Temporary ) <> 0 then begin
			RemoveGear( GB^.Contents , M );
		end;
		M := M2;
	end;

	{ We now have a total. Find out how many PCs there are and divide }
	{ the number by that. }
	N := 0;
	for t := 1 to Num_Party_Members do if ( G_Party[ t ] <> Nil ) and IsAlright( G_Party[ t ] ) then Inc( N );
	if N > 0 then Total := Total div N;

	{ Finally, give out the award. }
	for t := 1 to Num_Party_Members do if ( G_Party[ t ] <> Nil ) and IsAlright( G_Party[ t ] ) then AddNAtt( G_Party[ t ]^.NA , NAG_CharacterData , NAS_Experience , Total );
	AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , GP );

	if not Party_Dead then begin
		if ( Total > 0 ) and ( GP > 0 ) then begin
			msg := ReplaceHash( ReplaceHash( MsgString( 'COMBAT_XPandGP' ) , BStr( Total ) ) , BStr( GP ) );
			Alert( GB , msg );
		end else if Total > 0 then begin
			msg := ReplaceHash( MsgString( 'COMBAT_XPonly' ) , BStr( Total ) );
			Alert( GB , msg );
		end;
	end;
end;

Procedure DoCombatEnchantments( GB: GameBoardPtr );
	{ Some enchantments trigger an effect once per round. }
var
	M,E: GearPtr;
	T: Effect_Stencil;
	X,Y: Integer;
begin
	M := GB^.Contents;
	while M <> Nil do begin
		X := NAttValue( M^.NA , NAG_Location , NAS_X );
		Y := NAttValue( M^.NA , NAG_Location , NAS_Y );
		if IsAlright( M ) and OnTheMap( GB , X , Y ) then begin
			E := M^.SubCom;
			while E <> Nil do begin
				{ If E is an everyround-type enchantment, and it has }
				{ an effect loaded as its subcom, deal with it here. }
				if ( E^.G = GG_Enchantment ) and ( E^.S = GS_ENCHANT_EveryRound ) and ( E^.SubCom <> Nil ) and ( E^.SubCom^.G = GG_Effect ) then begin
					Clear_Effect_Stencil( GB , T );
					T[X,Y] := True;
					EffectFrontEnd( GB , E^.SubCom , Nil , T );
				end;
				E := E^.Next;
			end;
		end;

		M := M^.Next;
	end;
end;

Function CombatMode( GB: GameBoardPtr ): Integer;
	{ The party is in combat. This changes the way things are handled. }
var
	Command: Integer;
begin
	In_Combat_Mode := True;
	repeat
		{ Get the party input. }
		Command := GetPartyInput( GB );

		{ Get the monster input. }
		if Command <> CMD_Quit then begin
			GetMonsterInput( GB );

			{ Handle the every-round enchantments. }
			DoCombatEnchantments( GB );

			{ Restore everyone's movement points. }
			RestoreMovementPoints( GB );
		end;
	until Should_Exit_Scene or ( COmmand = CMD_Quit ) or ( not ShouldEnterCombat( GB ) );

	{ Combat is over. Dole the experience. }
	if Command <> CMD_Quit then begin
		DoleCombatXP( GB );

		{ After combat, remove all FightingStats from everyone. }
		ClearFightingStats( GB );
	end;
	In_Combat_Mode := False;

	CombatMode := Command;
end;

initialization
	In_Combat_Mode := False;

end.
