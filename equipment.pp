unit equipment;
	{ This unit will handle two things: Shopping and the inventory display. }

{
	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses sdl,sdlgfx,sdlmenus,sdlmap,gears,gamebook,texutil;

Function ItemMinLevel( I: GearPtr ): Integer;

Function GenerateStoreWares( StoreLvl: Integer ): GearPtr;
Procedure GenerateTreasureDrop( Container: GearPtr; DropLvl,Strength: Integer );

Function ItemCanBeEquipped( PC,I: GearPtr ): Boolean;
Function SlotForItem( Item: GearPtr ): Integer;
Procedure EquipItem( PC, Item: GearPtr );

Procedure InventoryScreen( PC: GearPtr; GB: GameBoardPtr );
Procedure OpenShop( var ItemList: GearPtr; GB: GameBoardPtr );

Procedure SpendOneCharge( PC: GearPtr; var Item: GearPtr );
Function AmmoTypeNeeded( Weapon: GearPtr ): Integer;

Procedure PCTradeItems( GB: GameBoardPtr; Target: GearPtr );

implementation

uses uiconfig,gearparser,frontend;


Function ItemCanBeEquipped( PC,I: GearPtr ): Boolean;
	{ Return TRUE if the item can be equipped by the PC, or FALSE otherwise. }
const
	Class_Can_Equip: Array [ 1..Num_Classes , 1..Num_Item_Types ] of Boolean = (
		{ Warrior }
	(	True, True, True, True, True,
		True, True, True, True, True,
		True, True, True, True, True,
		True, True, True, True, True,
		True, True, False
	),
		{ Thief }
	(	False, False, False, True, True,
		True, False, True, False, True,
		True, True, True, False, True,
		False, True, False, True, True,
		True, True, False
	),
		{ Bard }
	(	True, False, True, True, True,
		True, False, True, False, True,
		True, True, True, False, True,
		False, True, False, True, True,
		True, True, False
	),
		{ Priest }
	(	False, False, True, False, True,
		False, False, False, True, True,
		True, True, True, True, True,
		True, True, True, True, True,
		True, True, False
	),
		{ Mage }
	(	False, False, False, True, True,
		False, False, False, False, True,
		True, True, False, False, True,
		False, True, False, True, True,
		True, True, False
	),
		{ Druid }
	(	False, False, False, True, True,
		True, True, True, False, True,
		True, True, False, False, True,
		False, True, False, True, True,
		True, True, False
	),
		{ Animal - MONSTER CLASS }
	(	False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, True
	),
		{ Humanoid - MONSTER CLASS }
	(	False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, True
	),
		{ Dragon - MONSTER CLASS }
	(	False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, True
	),
		{ Defender - MONSTER CLASS }
	(	False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, False, False, False,
		False, False, True
	),
		{ Knight }
	(	True, False, True, False, False,
		False, True, False, True, False,
		False, True, True, True, True,
		True, True, True, True, True,
		True, True, False
	),
		{ Ranger }
	(	True, True, True, True, True,
		True, True, True, True, True,
		True, True, True, False, True,
		False, True, True, True, True,
		True, True, False
	),
		{ Necromancer }
	(	False, False, False, True, True,
		False, False, False, False, True,
		True, True, False, False, True,
		False, True, False, True, True,
		True, True, False
	),
		{ Samurai }
	(	True, True, True, True, True,
		False, True, False, True, False,
		False, True, True, False, True,
		True, True, True, True, True,
		True, True, False
	),
		{ Monk }
	(	False, False, False, True, True,
		True, False, True, False, True,
		True, True, False, False, True,
		False, True, False, True, True,
		True, True, False
	),
		{ Ninja }
	(	True, False, False, True, True,
		True, False, True, False, True,
		True, True, False, False, True,
		False, True, False, True, True,
		True, True, False
	)

	);
var
	C: Integer; { PC's Class. }
begin
	{ Begin with the exceptions. }
	{ Centaurs can't wear shoes. }
	if ( SlotForItem( I ) = NAV_Feet ) and ( NAttValue( PC^.NA , NAG_CharacterData , NAS_Species ) = SPECIES_Centaur ) then Exit( False );

	{ No exceptions... check the class vs item type. }
	C := NAttValue( PC^.NA , NAG_CharacterData , NAS_CurrentClass );
	if ( C > 0 ) and ( C <= Num_Classes ) and ( I^.S >= 1 ) and ( I^.S <= Num_Item_Types ) then begin
		ItemCanBeEquipped := Class_Can_Equip[ C , I^.S ];
	end else begin
		ItemCanBeEquipped := True;
	end;
end;

Function ItemName( PC,I: GearPtr ): String;
	{ Return a name for this item based on its IDENTIFIED status, quantity, }
	{ magical bonus, and whatever else you can think of. }
var
	it: String;
begin
	it := GearName( I );

	if NAttValue( I^.NA , NAG_ItemData , NAS_EquipSlot ) <> 0 then it := '+' + it
	else if ( PC <> Nil ) and not ItemCanBeEquipped( PC , I ) then it := '#' + it;
	if I^.Stat[ STAT_Quantity ] <> 0 then it := it + ' [' + BStr( I^.Stat[ STAT_Quantity ] ) + ']';

	ItemName := it;
end;

Function ItemMinLevel( I: GearPtr ): Integer;
	{ Return the minimum level at which this item should be encountered. }
const
	GP_Per_Level: Array [0..Num_Item_Types] of Integer = (
		20,
		20, 20, 20, 20, 20,	20, 20, 20, 25, 20,
		25, 30, 50, 75, 5,	10, 5, 10, 5, 5,
		5, 10, 20
	);
var
	itype,lvl: Integer;
begin
	{ Determine the type of item we're dealing with. If the value is out }
	{ of range, set it to type 0. }
	itype := I^.S;
	if ( itype < 0 ) or ( itype > Num_Item_Types ) then itype := 0;

	{ The minimum level is the GP value of the item divided by the GP_Per_Level }
	{ appropriate for its type. }
	lvl := ( GPValue( I ) div GP_Per_Level[ itype ] ) + 1;

	{ The level can't exceed 20. }
	if lvl > 20 then lvl := 20;

	ItemMinLevel := lvl;
end;

Function CreateItemShoppingList( MinLvl,MaxLvl: Integer ): NAttPtr;
	{ Create this shopping list. No item can have a MinLevel higher }
	{ than MaxLvl. }
	{ For randomly picking items, we're going to use a special NAtt shopping }
	{ list format. G = The index of the item, S = The type of the item, and }
	{ V = The desirability of the item. }
var
	SL: NAttPtr;
	Item: GearPtr;
	N,ItemLvl: Integer;
begin
	Item := Standard_Equipment_List;
	SL := Nil;
	N := 1;

	while Item <> Nil do begin
		ItemLvl := ItemMinLevel( Item );
		if ItemLvl <= MaxLvl then begin
			{ This is a good one. Let's store it. }
			if ItemLvl < MinLvl then SetNAtt( SL , N , Item^.S , 1 )
			else SetNAtt( SL , N , Item^.S , ItemLvl + 15 );
		end;

		Item := Item^.Next;
		Inc( N );
	end;

	CreateItemShoppingList := SL;
end;

Function RandomItemListingByType( ItemType: Integer; ShoppingList: NAttPtr ): NAttPtr;
	{ We've been handed a shopping list. Select one of the elements from this }
	{ list randomly based on the weight of the V values. }
	{ ItemType is the type of item we're looking for, or 0 for any item. }
var
	N: Integer;
	C,It: NAttPtr;
begin
	{ Error check- no point in working with an empty list. }
	if ShoppingList = Nil then Exit( Nil );

	{ Step one- count the number of matching plots. }
	C := ShoppingList;
	N := 0;
	while C <> Nil do begin
		if ( ItemType = 0 ) or ( C^.S = ItemType ) then N := N + C^.V;
		C := C^.Next;
	end;

	{ Pick one of the matches at random. }
	C := ShoppingList;
	N := Random( N );
	it := Nil;
	while ( C <> Nil ) and ( it = Nil ) do begin
		if ( ItemType = 0 ) or ( C^.S = ItemType ) then begin
			N := N - C^.V;
			if ( N < 0 ) and ( it = Nil ) then it := C;
		end;
		C := C^.Next;
	end;

	{ Return the entry we found. }
	RandomItemListingByType := it;
end;

Function GenerateItemOfType( ItemType: Integer; var ShoppingList: NAttPtr ): GearPtr;
	{ Select an item of the given type. Remove its entry from the }
	{ shopping list, and return a clone of the item from the Standard }
	{ Equipment list. If any of this cannot be done then return Nil. }
var
	SLE: NAttPtr;
	Item: GearPtr;
begin
	{ Error check- no point in working with an empty list. }
	if ShoppingList = Nil then Exit( Nil );

	{ Select a shopping list entry. }
	SLE := RandomItemListingByType( ItemType , ShoppingList );
	if SLE = Nil then Exit( Nil );

	{ Generate the item before deleting SLE. }
	Item := CloneGear( RetrieveGearSib( Standard_Equipment_List , SLE^.G ) );
	RemoveNAtt( ShoppingList , SLE );

	GenerateItemOfType := Item;
end;

Function MakeItemMagic( Item: GearPtr; Lvl: Integer ): Boolean;
	{ Attempt to place a magical effect upon this item. Return TRUE if }
	{ such an effect was added, or FALSE if not. }
var
	ShoppingList: NAttPtr;
	N: Integer;
	FX,Extra: GearPtr;
	msg: String;
begin
	{ Modify Lvl based on the item's min level- low level items get }
	{ better enchantments. }
	Lvl := Lvl - ItemMinLevel( Item ) + 1;

	{ Step One: Locate an appropriate effect from the magical effects }
	{ list. We'll do a simple indexed list with equal weight for each }
	{ thingamajig. }
	ShoppingList := Nil;
	N := 1;
	FX := Standard_Enhancement_List;
	while FX <> Nil do begin
		if ( NAttValue( FX^.NA , NAG_OkFor , Item^.S ) <> 0 ) and ( NAttValue( FX^.NA , NAG_StoryData , NAS_DifficultyLevel ) <= Lvl ) then begin
			SetNAtt( ShoppingList , N , N , N );
		end;
		Inc( N );
		FX := FX^.Next;
	end;

	{ If we found some candidates, apply one of them. }
	if ShoppingList <> Nil then begin
		{ Select one of the items at random. }
		FX := RetrieveGearSib( Standard_Enhancement_List , SelectRandomNAtt( ShoppingList )^.G );
		DisposeNAtt( ShoppingList );

		{ Copy the name and description. }
		msg := ReplaceHash( SAttValue( FX^.SA , 'NAME' ) , GearName( Item ) );
		if msg <> '' then SetSAtt( Item^.SA , 'NAME <' + msg + '>' );
		SetSAtt( Item^.SA , 'DESC <' + SAttValue( FX^.SA , 'DESC' ) + '>' );

		{ Copy the sub and inv coms. }
		Extra := FX^.SubCom;
		while Extra <> Nil do begin
			InsertSubCom( Item , CloneGear( Extra ) );
			Extra := Extra^.Next;
		end;
		Extra := FX^.InvCom;
		while Extra <> Nil do begin
			InsertInvCom( Item , CloneGear( Extra ) );
			Extra := Extra^.Next;
		end;

		{ Sum up the comscores and the GPFudge. }
		AddNAtt( Item^.NA , NAG_ItemData , NAS_GPFudge , NAttValue( FX^.NA , NAG_ItemData , NAS_GPFudge ) );
		for N := 1 to Num_Com_Score do AddNAtt( Item^.NA , NAG_ComScoreMod , N , NAttValue( FX^.NA , NAG_ComScoreMod , N ) );

		MakeItemMagic := True;
	end else begin
		{ No enhancement is possible. Crud. }
		MakeItemMagic := False;
	end;
end;

Function GenerateStoreWares( StoreLvl: Integer ): GearPtr;
	{ Create a list of wares for this store based upon the provided }
	{ store level. In general a store will stock items up to several }
	{ levels above this. }
const
	Guaranteed_Items: Array [ 1..Num_Item_Types ] of Byte = (
		{ This array tells how many items of each type will definitely }
		{ be placed in the store. In the name of fairness, and possibly }
		{ due to my experience in Wizardry Gold as the pantsless ninja, }
		{ at least one item of most types will be present. }
		1, 1, 1, 1, 1,	1, 1, 1, 1, 1,
		1, 1, 1, 1, 1,	1, 1, 1, 1, 1,
		1, 1, 0
	);
var
	ShoppingList: NAttPtr;
	Wares,Item: GearPtr;
	t,tt: Integer;
begin
	{ First, generate the shopping list. }
	ShoppingList := CreateItemShoppingList( StoreLvl, StoreLvl + 3 );

	{ Next, add the guaranteed items. }
	Wares := Nil;
	for t := 1 to Num_Item_Types do begin
		for tt := 1 to Guaranteed_Items[ t ] do begin
			Item := GenerateItemOfType( T , ShoppingList );
			if Item <> Nil then begin
				AppendGear( Wares , Item );
			end;
		end;
	end;

	{ Add some extra items. }
	T := 0;
	while ( NumSiblingGears( Wares ) < 30 ) and ( T < 30 ) do begin
		Item := GenerateItemOfType( 0 , ShoppingList );
		if Item <> Nil then begin
			if ItemMinLevel( Item ) < ( StoreLvl - Random( 10 ) ) then MakeItemMagic( Item , StoreLvl + Random( 5 ) );
			AppendGear( Wares , Item );
		end;
		Inc( T );
	end;

	DisposeNAtt( ShoppingList );
	GenerateStoreWares := Wares;
end;

Procedure GenerateTreasureDrop( Container: GearPtr; DropLvl,Strength: Integer );
	{ Generate a treasure drop and stick it in the container. }
	{ Store any leftover points as gold in the container. }
const
	Num_Premium_Treasure = 20;
	Premium_Treasure: Array [1..Num_Premium_Treasure] of Integer = (
		GS_Sword, GS_Axe, GS_Mace, GS_Dagger, GS_Staff,
		GS_Bow, GS_Polearm, GS_Arrow, GS_Shield, GS_Sling,
		GS_Bullet, GS_LightArmor, GS_HeavyArmor, GS_Helm, GS_Gauntlet,
		GS_LightArmor, GS_HeavyArmor, GS_Sword, GS_Axe, GS_Mace
	);
var
	GP,Tries: LongInt;
	ShoppingList: NAttPtr;
	Item: GearPtr;
begin
	{ First, generate the shopping list. }
	ShoppingList := CreateItemShoppingList( DropLvl , DropLvl + 1 );

	{ Determine how many GP to give out. }
	GP := ( DropLvl + Random( DropLvl + 1 ) ) * Strength + Random( 100 ) - Random( 100 );
	if GP < 100 then GP := 100;

	Tries := 10 - ( DropLvl div 2 ) - Random( 5 );
	if Tries < 0 then Tries := 0;
	while ( GP > 0 ) and ( Tries < 10 ) do begin
		if Random( 3 ) <> 1 then Item := GenerateItemOfType( Premium_Treasure[ Random( Num_Premium_Treasure ) + 1 ] , ShoppingList )
		else Item := GenerateItemOfType( 0 , ShoppingList );
		if Item <> Nil then begin
			if ( ItemMinLevel( Item ) < DropLvl ) and ( Random( 20 ) < DropLvl ) then begin
				MakeItemMagic( Item , DropLvl );
				Item^.V := GV_Unidentified;
			end;
			InsertInvCom( Container , Item );
			GP := GP - GPValue( Item ) div 2;
		end;
		Inc( Tries );
	end;

	DisposeNAtt( ShoppingList );
	SetNAtt( Container^.NA , NAG_CampaignData , NAS_Gold , GP );
end;

Procedure UnequipItem( Item: GearPtr );
	{ Unequip the item. This one is easy. }
begin
	SetNAtt( Item^.NA , NAG_ItemData , NAS_EquipSlot , 0 );
end;

Function SlotForItem( Item: GearPtr ): Integer;
	{ Return the slot which this item fits into. }
const
	ITEM_SLOT: Array [1..Num_Item_Types] of Byte = (
		NAV_Hand1, NAV_Hand1, NAV_Hand1, NAV_Hand1, NAV_Hand1,
		NAV_Hand1, NAV_Hand1, NAV_Hand2, NAV_Hand2, NAV_Hand1,
		NAV_Hand2, NAV_Body, NAV_Body, NAV_Body, NAV_Head,
		NAV_Head, NAV_Arm, NAV_Arm, NAV_Feet, NAV_Feet,
		NAV_Feet, NAV_Back, NAV_Hand1
	);
begin
	if ( Item^.S >= 1 ) and ( Item^.S <= Num_Item_Types ) then begin
		SlotForItem := ITEM_SLOT[ Item^.S ];
	end else begin
		SlotForItem := 0;
	end;
end;

Procedure EquipItem( PC, Item: GearPtr );
	{ Unequip any item currently using this slot, then equip this item. }
var
	I: GearPtr;
	it: Integer;	{ Item Type }
begin
	if ItemCanBeEquipped( PC , Item ) then begin
		{ First, unequip any item currently equipped in the requested slot. }
		it := SlotForItem( Item );
		I := FindEquippedItem( PC , it );

		if I <> Nil then UnequipItem( I );

		{ If a two-handed weapon is being equipped, unequip shield. }
		if ( it = NAV_Hand1 ) and ( Item^.Stat[ STAT_WeaponHands ] <> 0 ) then begin
			I := FindEquippedItem( PC , NAV_Hand2 );
			if I <> Nil then UnequipItem( I );
		end else if it = NAV_Hand2 then begin
			I := FindEquippedItem( PC , NAV_Hand1 );
			if ( I <> Nil ) and ( I^.Stat[ STAT_WeaponHands ] <> 0 ) then UnequipItem( I );
		end;

		{ Finally, set the slot number for this item. }
		SetNAtt( Item^.NA , NAG_ItemData , NAS_EquipSlot , it );
	end;
end;

Procedure TradeItem( GB: GameBoardPtr; PC, Item: GearPtr );
	{ Give this item to another member of the party. }
var
	RPM: RPGMenuPtr;
	T: Integer;
begin
	SetupTwoColumnRedraw( GB , MsgString( 'TRADE_ITEM_CAPTION' ) , PC );
	SetTwoColumnInfoGear( Item );

	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );

	{ Add an option for each character in the party. }
	for t := 1 to Num_Party_Members do begin
		if ( G_Party[ T ] <> Nil ) and ( G_Party[ T ] <> PC ) then begin
			AddRPGMenuItem( RPM , GearName( G_Party[ T ] ) , T );
		end;
	end;
	AlphaKeyMenu( RPM );
	AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

	T := SelectMenu( RPM , @TwoColumnRedraw );
	if T <> -1 then begin
		if CanTakeMoreItems( G_Party[ T ] ) then begin
			DelinkGear( PC^.InvCom , Item );
			InsertInvCom( G_Party[ T ] , Item );
			SetNAtt( Item^.NA , NAG_ItemData , NAS_EquipSlot , 0 );
		end else begin
			Alert( GB , ReplaceHash( MsgString( 'OutOfEncumberance' ) , GearName( G_Party[ T ] ) ) );
		end;
	end;
end;

Procedure DropItem( PC, Item: GearPtr );
	{ Delink the item from the PC and place it in the same tile. }
begin
	DelinkGear( PC^.InvCom , Item );
	AppendGear( PC , Item );
	SetNAtt( Item^.NA , NAG_Location , NAS_X , NAttValue( PC^.NA , NAG_Location , NAS_X ) );
	SetNAtt( Item^.NA , NAG_Location , NAS_Y , NAttValue( PC^.NA , NAG_Location , NAS_Y ) );
end;

Procedure ThisItemWasSelected( GB: GameBoardPtr; PC, Item: GearPtr );
	{ ITEM was selected. Allow the PC to do whatever he wants with it. }
var
	RPM: RPGMenuPtr;
	N: Integer;
begin
	{ Start, as always, with an error check. }
	if Item = Nil then Exit;

	SetupTwoColumnRedraw( GB , ItemName( PC , Item ) , PC );

	{ Options: Equip/Unequip, Transfer, Drop }
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );

	SetTwoColumnInfoGear( Item );

	if NAttValue( Item^.NA , NAG_ItemData , NAS_EquipSlot ) <> 0 then begin
		AddRPGMenuItem( RPM , MsgString( 'TIWS_UNEQUIP' ) , 4 );
	end else if ItemCanBeEquipped( PC , Item ) then begin
		AddRPGMenuItem( RPM , MsgString( 'TIWS_EQUIP' ) , 3 );
	end;
	AddRPGMenuItem( RPM , MsgString( 'TIWS_TRADE' ) , 2 );
	AddRPGMenuItem( RPM , MsgString( 'TIWS_DROP' ) , 1 );
	AddRPGMenuItem( RPM , MsgString( 'TIWS_EXIT' ) , -1 );
	AlphaKeyMenu( RPM );

	N := SelectMenu( RPM , @TwoColumnRedraw );
	DisposeRPGMenu( RPM );

	Case N of
		1:	DropItem( PC , Item );
		2:	TradeItem( GB , PC , Item );
		3:	EquipItem( PC , Item );
		4:	UnequipItem( Item );
	end;
end;

Procedure InventoryScreen( PC: GearPtr; GB: GameBoardPtr );
	{ This does the inventory screen update. }
var
	RPM: RPGMenuPtr;
	I: GearPtr;
	N: Integer;
begin
	{ Error check - there must be a PC here. }
	if ( PC = Nil ) then Exit;

	repeat
		SetupTwoColumnRedraw( GB , ReplaceHash( MsgString( 'INV_CAPTION' ) , GearName( PC ) ) , PC );
		{ Create the menu. }
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		AttachTwoColumnInfo( RPM , PC^.InvCom );

		{ Add all of the PC's items to the menu. }
		I := PC^.InvCom;
		N := 1;
		while I <> Nil do begin
			AddRPGMenuItem( RPM , ItemName( PC , I ) , N , GearDesc( I ) );
			I := I^.Next;
			Inc( N );
		end;

		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );

		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		N := SelectMenu( RPM , @TwoColumnRedraw );

		if N > 0 then begin
			ThisItemWasSelected( GB , PC , RetrieveGearSib( PC^.InvCom , N ) );
		end;

	until N = -1;

	{ Upon leaving the inventory screen, refresh the party avatars. }
	GeneratePartyAvatars;
end;

Procedure BuyItem( GB: GameBoardPtr; PC: GearPtr; var ItemList: GearPtr );
	{ PC wants to buy something. Go to it! }
var
	RPM: RPGMenuPtr;
	N: Integer;
	I: GearPtr;
	msg: String;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
	N := 1;
	I := ItemList;
	while I <> Nil do begin
		AddRPGMenuItem( RPM , ItemName( PC , I ) + '   ...' + BStr( GPValue( I ) ) + 'gp' , N , GearDesc( I ) );
		I := I^.Next;
		Inc( N );
	end;
	RPMSortAlpha( RPM );
	AlphaKeyMenu( RPM );

	msg := MsgSTring( 'SHOP_Buy' );

	repeat
		SetupTwoColumnRedraw( GB , msg , PC );
		AttachTwoColumnInfo( RPM , ItemList );

		N := SelectMenu( RPM , @TwoColumnRedraw );
		if N > -1 then begin
			I := RetrieveGearSib( ItemList , N );
			if ( I <> Nil ) and ( GPValue( I ) <= NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold ) ) then begin
				if CanTakeMoreItems( PC ) then begin
					AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , -GPValue( I ) );
					I := CloneGear( I );
					InsertInvCom( PC , I );
					Msg := ReplaceHash( MsgString( 'SHOP_BUY_CAPTION' ) , GearName( I ) );
				end else begin
					Msg := ReplaceHash( MsgString( 'SHOP_NO_ENCUMBERANCE' ) , GearName( PC ) );
				end;
			end;
		end;

	until N = -1;

	DisposeRPGMenu( RPM );
end;

Function SalePrice( Item: GearPtr ): LongInt;
	{ Return how much the store will pay for this item. }
begin
	if IsIdentified( Item ) then begin
		SalePrice := ( GPValue( Item ) + 1 ) div 2;
	end else begin
		SalePrice := 25;
	end;
end;

Procedure SellItem( GB: GameBoardPtr; PC: GearPtr );
	{ PC wants to sell something. Go to it! }
var
	RPM: RPGMenuPtr;
	I: GearPtr;
	N: Integer;
	msg: String;
begin
	msg := MsgSTring( 'SHOP_Sell' );
	repeat
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		N := 1;
		I := PC^.InvCom;
		while I <> Nil do begin
			AddRPGMenuItem( RPM , ItemName( PC , I ) + ' (' + BStr( SalePrice( I ) ) + 'gp)' , N , GearDesc( I ) );
			I := I^.Next;
			Inc( N );
		end;
		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );
		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		SetupTwoColumnRedraw( GB , msg , PC );
		AttachTwoColumnInfo( RPM , PC^.InvCom );

		N := SelectMenu( RPM , @TwoColumnRedraw );
		DisposeRPGMenu( RPM );

		if N > 0 then begin
			I := RetrieveGearSib( PC^.InvCom , N );
			AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold ,  SalePrice( I ) );
			Msg := ReplaceHash( MsgString( 'SHOP_SELL_CAPTION' ) , GearName( I ) );
			RemoveGear( PC^.InvCom , I );
		end;
	until N = -1;
end;

Procedure IdentifyItem( GB: GameBoardPtr; PC: GearPtr );
	{ PC wants to identify something. Go to it! }
const
	Identify_Price = 50;
var
	RPM: RPGMenuPtr;
	I: GearPtr;
	N: Integer;
	msg: String;
begin
	msg := MsgSTring( 'SHOP_Identify' );
	repeat
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		N := 1;
		I := PC^.InvCom;
		while I <> Nil do begin
			if not IsIdentified( I ) then AddRPGMenuItem( RPM , ItemName( PC , I ) + ' (' + BStr( Identify_Price ) + 'gp)' , N , GearDesc( I ) );
			I := I^.Next;
			Inc( N );
		end;
		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );
		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		SetupTwoColumnRedraw( GB , msg , PC );
		AttachTwoColumnInfo( RPM , PC^.InvCom );

		N := SelectMenu( RPM , @TwoColumnRedraw );
		DisposeRPGMenu( RPM );

		if N > 0 then begin
			if NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold ) >= Identify_Price then begin
				I := RetrieveGearSib( PC^.InvCom , N );
				AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold ,  -Identify_Price );
				I^.V := GV_Identified;
				Msg := ReplaceHash( MsgString( 'SHOP_IDENTIFY_CAPTION' ) , GearName( I ) );
			end else msg := MsgString( 'SHOP_CANT_IDENTIFY' );
		end;
	until N = -1;
end;

Procedure DoShopping( PC: GearPtr; var ItemList: GearPtr; GB: GameBoardPtr );
	{ This is the main shopping window. The options are: Buy things, }
	{ sell things, pool gold, or exit. }
var
	RPM: RPGMenuPtr;
	N: Integer;
begin
	{ Error check - there must be a PC here. }
	if PC = Nil then Exit;

	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightTotal );
	AddRPGMenuItem( RPM , MsgString( 'SHOP_Buy' ) , 1 );
	AddRPGMenuItem( RPM , MsgString( 'SHOP_Sell' ) , 2 );
	AddRPGMenuItem( RPM , MsgString( 'SHOP_Identify' ) , 3 );
	AddRPGMenuItem( RPM , MsgString( 'SHOP_Exit' ) , -1 );
	AlphaKeyMenu( RPM );

	repeat
		SetupTwoColumnRedraw( GB , ReplaceHash( MsgString( 'SHOP_Caption' ) , GearName( PC ) ) , PC );
		N := SelectMenu( RPM , @TwoColumnRedraw );

		Case N of
			1:	BuyItem( GB , PC , ItemList );
			2:	SellItem( GB , PC );
			3:	IdentifyItem( GB , PC );
		end;

	until N = -1;

	DisposeRPGMenu( RPM );
end;

Procedure OpenShop( var ItemList: GearPtr; GB: GameBoardPtr );
	{ Prompt for character, then enter the shop above. }
var
	RPM: RPGMenuPtr;
	N: Integer;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightTotal );
	for N := 1 to Num_Party_Members do if G_Party[ N ] <> Nil then AddRPGMenuItem( RPM , GearName( G_Party[ N ] ) , N );
	AlphaKeyMenu( RPM );
	AddRPGMenuItem( RPM , MsgString( 'SHOP_EXIT' ) , -1 );

	repeat
		SetupTwoColumnRedraw( GB , MsgString( 'SHOP_WhoEnters' ) , Nil );
		N := SelectMenu( RPM , @TwoColumnRedraw );

		if ( N >= 1 ) and ( N <= Num_Party_Members ) then DoShopping( G_Party[ N ] , ItemList , GB );
	until N = -1;

	DisposeRPGMenu( RPM );

	{ Upon leaving the shop screen, refresh the party avatars. }
	GeneratePartyAvatars;
end;

Procedure SpendOneCharge( PC: GearPtr; var Item: GearPtr );
	{ ITEM is about to lose one charge. If it runs out, delete it. }
	{ IMPORTANT NOTE: It may be deleted! I know I mentioned that above, }
	{  but it really bears repeating! And exclamation marks!!! }
begin
	if Item^.Stat[ STAT_Quantity ] > 0 then begin
		Dec( Item^.Stat[ STAT_Quantity ] );
		if Item^.Stat[ STAT_Quantity ] < 1 then begin
			RemoveGear( PC^.InvCom , Item );
		end;
	end;
end;

Function AmmoTypeNeeded( Weapon: GearPtr ): Integer;
	{ Return the ammo type needed by this weapon. }
begin
	if Weapon = Nil then AmmoTypeNeeded := 0
	else if Weapon^.S = GS_Bow then AmmoTypeNeeded := GS_Arrow
	else if Weapon^.S = GS_Sling then AmmoTypeNeeded := GS_Bullet
	else AmmoTypeNeeded := 0;
end;


Procedure PCTradeItems( GB: GameBoardPtr; Target: GearPtr );
	{ The PC will attempt to trade items with TARGET. }
	Procedure SetupMenu( RPM: RPGMenuPtr; Master: GearPtr );
		{ The setup procedure for both menus is the same, so here }
		{ it is. }
	var
		N: Integer;
		LList: GearPtr;
	begin
		N := 1;
		LList := Master^.InvCom;
		while LList <> Nil do begin
			AddRPGMenuItem( RPM , GearName( LList ) , N );
			Inc( N );
			LList := LList^.Next;
		end;
		RPMSortAlpha( RPM );

		{ If the menu is empty, add a message saying so. }
		If RPM^.NumItem < 1 then AddRPGMenuItem( RPM , ReplaceHash( MsgString( 'ContainerNoItems' ) , GearName( Master ) ) , -1 )
		else AlphaKeyMenu( RPM );

		{ Add the menu keys. }
		AddRPGMenuKey(RPM,'/',-2);
		AddRPGMenuKey(RPM, RPK_Left ,-3);
		AddRPGMenuKey(RPM, RPK_Right ,-4);

	end;
	Procedure TransferItem( Source , Item , Destination: GearPtr );
		{ If possible, move ITEM from SOURCE to DESTINATION. }
	begin
		if CanTakeMoreItems( Destination ) then begin
			{ Clear the item's location values. }
			StripNAtt( Item , NAG_Location );
			SetNAtt( Item^.NA , NAG_ItemData , NAS_EquipSlot , 0 );

			DelinkGear( Source^.InvCom , Item );
			InsertInvCom( Destination , Item );
		end;
	end;
var
	item: GearPtr;
	PCMenu,TarMenu: RPGMenuPtr;
	EscMenu,UseTarInv: Boolean;
	PN,N: Integer;
begin
	{ Error check. }
	if ( Target = Nil ) then Exit;

	{ Determine the first PC. }
	PN := FirstAlrightPCIndex( GB );
	if PN = 0 then Exit;

	{ Initialize variables. }
	UseTarInv := True;

	{ Keep going back and forth between the PC and the target until }
	{ the player hits ESCape. }
	EscMenu := False;
	repeat
		{ Create the two menus. }
		PCMenu := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_LeftMenu );
		SetupMenu( PCMenu , G_Party[ PN ] );

		TarMenu := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		SetupMenu( TarMenu , Target );

		{ Determine which of the two menus is going to be our active one, }
		{ based on the UseTarInv variable. }
		if UseTarInv then begin
			SetupTradeItemsRedraw( GB , TarMenu , PCMenu , G_Party[ PN ] , Target , Target^.InvCom );
			N := SelectMenu( TarMenu , @TradeItemsRedraw );
		end else begin
			SetupTradeItemsRedraw( GB , PCMenu , TarMenu , G_Party[ PN ] , Target , G_Party[ PN ]^.InvCom );
			N := SelectMenu( PCMenu , @TradeItemsRedraw );
		end;

		if N > 0 then begin
			{ An item has been selected. Find it, then attempt to swap from }
			{ target to PC or vice versa. }
			if UseTarInv then begin
				Item := RetrieveGearSib( Target^.InvCom , N );
				TransferItem( Target , Item , G_Party[ PN ] );
			end else begin
				Item := RetrieveGearSib( G_Party[ PN ]^.InvCom , N );
				TransferItem( G_Party[ PN ] , Item , Target );
			end;

		end else if N = -1 then begin
			{ An Escape has been recieved. Quit this procedure. }
			EscMenu := True;
		end else if N = -2 then begin
			{ A menu swap has been requested. Change the active menu. }
			UseTarInv := Not UseTarInv;
		end else if N = -3 then begin
			PN := NextAlrightPCIndex( GB , PN , False );
		end else if N = -4 then begin
			PN := NextAlrightPCIndex( GB , PN , True );
		end;

		{ Dispose the two menus. }
		DisposeRPGMenu( PCMenu );
		DisposeRPGMenu( TarMenu );
	until EscMenu;

	{ Upon leaving the trading screen, refresh the party avatars. }
	GeneratePartyAvatars;
end;



end.
