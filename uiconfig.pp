unit uiconfig;
	{ User Interface Configuration for Dungeon Monkey Unlimited. }
{
	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}


interface

uses gears,texutil,sysutils,dos;

type
	KeyMapDesc = Record
		CmdName,CmdDesc: String;
		KCode: Char;
	end;

const
	{ ******************************* }
	{ ***  FILE  NAME  CONSTANTS  *** }
	{ ******************************* }

	OS_Dir_Separator = DirectorySeparator;
	OS_Search_Separator = PathSeparator;
	OS_Current_Directory = '.';

	{ All of the following file names have been checked for }
	{ correct capitalization. Hopefully, everything should run }
	{ fine. }
	Default_File_Ending = '.txt';
	Default_Search_Pattern = '*.txt';
	Save_Game_DirName = 'savegame';
	Save_Game_Directory = Save_Game_DirName + OS_Dir_Separator;

	Data_DirName = 'gamedata';
	Data_Directory = Data_DirName + OS_Dir_Separator;
	Standard_Message_File = Data_Directory + 'messages.txt';

	Doc_DirName = 'doc';
	Doc_Directory = Doc_DirName + OS_Dir_Separator;

	Graphics_DirName = 'image';
	Graphics_Directory = Graphics_Dirname + OS_Dir_Separator;

	STARTUP_OK: Boolean = True;


	RPK_UpRight = #$89;
	RPK_Up = #$88;
	RPK_UpLeft = #$87;
	RPK_Left = #$84;
	RPK_Right = #$86;
	RPK_DownRight = #$81;
	RPK_Down = #$82;
	RPK_DownLeft = #$83;
	RPK_MouseButton = #$90;
	RPK_TimeEvent = #$91;
	RPK_RightButton = #$92;

	FrameDelay: Integer = 50;

	DoFullScreen: Boolean = False;

	Names_Above_Heads: Boolean = False;

	DoAutoSave: Boolean = True;

	NumMappedKeys = 5;
	KeyMap: Array [1..NumMappedKeys] of KeyMapDesc = (
	(	CmdName: 'Quit';
		CmdDesc: 'Quit the game.';
		KCode: 'Q';	),
	(	CmdName: 'Center';
		CmdDesc: 'Center the display on the first active player character.';
		KCode: 'h';	),
	(	CmdName: 'Rest';
		CmdDesc: 'Rest to restore HP and MP.';
		KCode: 'R';	),
	(	CmdName: 'MiniMap';
		CmdDesc: 'View the navigation map.';
		KCode: 'm';	),
	(	CmdName: 'History';
		CmdDesc: 'Review the progress of your adventure.';
		KCode: 'H';	)
	);

	{ *** KEYMAP COMMAND NUMBERS *** }
	KMC_Quit = 1;
	KMC_Center = 2;
	KMC_Rest = 3;
	KMC_MiniMap = 4;
	KMC_History = 5;

	Ersatz_Mouse: Boolean = False;
	Wizard_On: Boolean = False;

	Use_Software_Surface: Boolean = False;

	Show_Legal_Moves: Boolean = False;

	Config_File = 'config.cfg';

var
	Text_Messages: SAttPtr;


Function MsgString( const MsgLabel: String ): String;


implementation

Procedure LoadConfig;
	{ Open the configuration file and set the variables }
	{ as needed. }
var
	F: Text;
	S,CMD,C: String;
	T: Integer;
begin
	{See whether or not there's a configuration file.}
	S := FSearch(Config_File,'.');
	if S <> '' then begin
		{ If we've found a configuration file, }
		{ open it up and start reading. }
		Assign(F,S);
		Reset(F);

		while not Eof(F) do begin
			ReadLn(F,S);
			cmd := ExtractWord(S);
			if (cmd <> '') then begin
				{Check to see if CMD is one of the standard keys.}
				cmd := UpCase(cmd);
				for t := 1 to NumMappedKeys do begin
					if UpCase(KeyMap[t].CmdName) = cmd then begin
						C := ExtractWord(S);
						if Length(C) = 1 then begin
							KeyMap[t].KCode := C[1];
						end;
					end;
				end;

				{ Check to see if CMD is the animation speed throttle. }
				if cmd = 'ANIMSPEED' then begin
					T := ExtractValue( S );
					if T < 0 then T := 0;
					FrameDelay := T;

				end else if cmd = 'FULLSCREEN' then begin
					DoFullScreen := True;


				end else if cmd = 'USE_SOFTWARE_SURFACE' then begin
					Use_Software_Surface := True;

				end else if cmd = 'NAMESON' then begin
					Names_Above_Heads := True;

				end else if cmd = 'SHOW_LEGAL_MOVES' then begin
					Show_Legal_Moves := True;

				end else if cmd = 'ERSATZ_MOUSE' then begin
					Ersatz_Mouse := True;

				end else if cmd = 'WIZARD_ON' then begin
					Wizard_On := True;

				end else if cmd[1] = '#' then begin
					S := '';

				end;
			end;
		end;

		{ Once the EOF has been reached, close the file. }
		Close(F);
	end;

end;

Procedure SaveConfig;
	{ Open the configuration file and record the variables }
	{ as needed. }
var
	F: Text;
	T: Integer;
	Procedure AddBoolean( const OpTag: String; IsOn: Boolean );
		{ Add one of the boolean options to the file. }
	begin
		if IsOn then begin
			writeln( F , OpTag );
		end else begin
			writeln( F , '#' + OpTag );
		end;
	end;
begin
	{ If we've found a configuration file, }
	{ open it up and start reading. }
	Assign( F , Config_File );
	Rewrite( F );

	writeln( F , '#' );
	writeln( F , '# ATTENTION:' );
	writeln( F , '#   Only edit the config file if program is not running.' );
	writeln( F , '#   Configuration overwritten at game exit.' );
	writeln( F , '#' );

	for t := 1 to NumMappedKeys do begin
		WriteLn( F, KeyMap[t].CmdName + ' ' + KeyMap[t].KCode );
	end;

	writeln( F, 'ANIMSPEED ' + BStr( FrameDelay ) );


	AddBoolean( 'FULLSCREEN' , DoFullScreen );

	AddBoolean( 'USE_SOFTWARE_SURFACE' , Use_Software_Surface );

	AddBoolean( 'NAMESON' , Names_Above_Heads );

	AddBoolean( 'ERSATZ_MOUSE' , Ersatz_Mouse );
	AddBoolean( 'WIZARD_ON' , Wizard_On );

	AddBoolean( 'SHOW_LEGAL_MOVES' , Show_Legal_Moves );

	Close(F);
end;


Function MsgString( const MsgLabel: String ): String;
	{ Return the standard message string which has the requested }
	{ label. }
begin
	MsgString := SAttValue( Text_Messages , MsgLabel );
end;

Procedure CheckDirectoryPresent;
	{ Make sure that the default save directory exists. If not, }
	{ create it. }
begin
	if not DirectoryExists( Save_Game_DirName ) then begin
		MkDir( Save_Game_DirName );
	end;

	{ Check to make sure all the other directories can be found. }
	Startup_OK := DirectoryExists( Data_DirName ) and DirectoryExists( Graphics_DirName );
end;


initialization
	{ Make sure we have the required data directories. }
	CheckDirectoryPresent;

	Text_Messages := LoadStringList( Standard_Message_File );
	LoadConfig;

	Randomize;

finalization
	SaveConfig;
	DisposeSAtt( Text_Messages );

end.
