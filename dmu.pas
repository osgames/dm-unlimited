program dmu;
{
	The main program.
}

{
	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

uses	chargen,sdl,sdlgfx,sdlmenus,uiconfig,gears,gamebook,exploration,
	gearparser,monsters,randmaps,randworld,avatar,frontend,equipment;

var
	DMU_SelectPartyMenu: RPGMenuPtr;
	DMU_SelectPartyList,DMU_LastPlayerShown: GearPtr;
	DMU_SelectPartyAvatar: SensibleSpritePtr;

Procedure MainMenuRedraw;
	{ Set up the screen for the main menu. }
begin
	PrettyPictureDisplay;
	InfoBox( ZONE_MainMenu );
end;

Procedure SelectPartyRedraw;
	{ Set up the screen to display the party. }
const
	ZONE_SelectParty_Avatar: TSDL_Rect = ( x:screenwidth * 3 div 4 - 27; y: 193; w: 54; h: 54 );
var
	N: Integer;
	PC: GearPtr;
begin
	PrettyPictureDisplay;
	InfoBox( ZONE_MainMenu );
	N := CurrentMenuItemValue( DMU_SelectPartyMenu );
	if N > 0 then begin
		PC := RetrieveGearSib( DMU_SelectPartyList , N );
		if PC <> DMU_LastPlayerShown then begin
			DMU_LastPlayerShown := PC;
			if DMU_SelectPartyAvatar <> Nil then RemoveSprite( DMU_SelectPartyAvatar );
			DMU_SelectPartyAvatar := GenerateAvatar( PC );
		end;

		DrawCharacterSheet( Nil , PC , False );
		InfoBox( ZONE_SelectParty_Avatar );
		DrawSprite( LocateBUSprite( 'avatar_frame.png' , 54 , 54 ) , ZONE_SelectParty_Avatar , 0 );
		DrawSprite( DMU_SelectPartyAvatar , ZONE_SelectParty_Avatar , 0 );
	end;
end;

Procedure BuildSiblingMenu( RPM: RPGMenuPtr; LList: GearPtr );
	{ Build a menu of these sibling gears. The index numbers will be }
	{ sibling indicies, not the universal gear numbers used in other }
	{ procedures here. }
var
	N: Integer;
begin
	N := 1;
	while LList <> Nil do begin
		AddRPGMenuItem( RPM , GearName( LList ) , N );
		Inc( N );
		LList := LList^.Next;
	end;
end;


Function SelectParty: GearPtr;
	{ Load the party from disk. Return the party as a list of gears, }
	{ in proper marching order. }
	Function LoadAllCharacters: GearPtr;
		{ Load all the characters from disk. }
	var
		FList,FName: SAttPtr;
		F: Text;
		PC,LList: GearPtr;
	begin
		FList := CreateFileList( Save_Game_Directory + 'cha_' + Default_Search_Pattern );
		FName := FList;
		LList := Nil;
		while FName <> Nil do begin
			Assign( F , Save_Game_Directory + FName^.Info );
			Reset( F );
			PC := ReadCGears( F );
			Close( F );

			AppendGear( LList , PC );

			FName := FName^.Next;
		end;
		DisposeSAtt( FList );
		LoadAllCharacters := LList;
	end;
var
	ShoppingList,Party,PC: GearPtr;
	RPM: RPGMenuPtr;
	N: Integer;
begin
	ShoppingList := LoadAllCharacters;
	Party := Nil;

	{ Initialize some of the display variables. These only need to be }
	{ set at the beginning. }
	DMU_LastPlayerShown := Nil;
	DMU_SelectPartyAvatar := Nil;

	while ( ShoppingList <> Nil ) and ( NumSiblingGears( Party ) < Num_Party_Members ) do begin
		{ Create the menu. }
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_MainMenu );
		BuildSiblingMenu( RPM , ShoppingList );
		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );

		{ Initialize those display variables which need to be reset }
		{ for each menu selection. }
		DMU_SelectPartyMenu := RPM;
		DMU_SelectPartyList := ShoppingList;

		N := SelectMenu( RPM , @SelectPartyRedraw );
		DisposeRPGMenu( RPM );

		if N = -1 then begin
			DisposeGear( ShoppingList );
		end else begin
			PC := RetrieveGearSib( ShoppingList , N );
			DelinkGear( ShoppingList , PC );
			AppendGear( Party , PC );
		end;
	end;

	DisposeGear( ShoppingList );
	SelectParty := Party;
end;

Procedure PlacePartyOnMap( GB: GameBoardPtr; ComingFrom: Integer );
	{ Arrange the party on this map. ComingFrom is the SceneID }
	{ of the scene the party is coming from; use 0 when starting a }
	{ campaign on the first map. }
const
	Member_Offset: Array [1..Num_Party_Members,1..2] of SmallInt = (
		(1,0), (-1,0), (0,1), (0,-1)
	);
var
	CPoint,Thing: GearPtr;
	X0,Y0,T: Integer;
begin
	{ Step One: Locate the checkpoint. }
	CPoint := Nil;
	Thing := GB^.Contents;
	while ( Thing <> Nil ) and ( CPoint = Nil ) do begin
		if ( Thing^.G = GG_Checkpoint ) and ( Thing^.S = GS_EntryGrid ) and ( Thing^.Stat[ STAT_Destination ] = ComingFrom ) then CPoint := Thing;
		Thing := Thing^.Next;
	end;

	if CPoint <> Nil then begin
		X0 := NAttValue( CPoint^.NA , NAG_Location , NAS_X );
		Y0 := NAttValue( CPoint^.NA , NAG_Location , NAS_Y );
	end else begin
		X0 := Random( GB^.MAP_WIDTH - 2 ) + 2;
		Y0 := Random( GB^.MAP_HEIGHT - 2 ) + 2;
	end;

	for T := 1 to Num_Party_Members do begin
		if ( G_Party[t] <> Nil ) and IsAlright( G_Party[ t ] ) then begin
			SetNAtt( G_Party[t]^.NA , NAG_Location , NAS_X , X0 + Member_Offset[ T , 1 ] );
			SetNAtt( G_Party[t]^.NA , NAG_Location , NAS_Y , Y0 + Member_Offset[ T , 2 ] );
		end;
	end;
end;

Function PCStartingCash( PC: GearPtr ): Integer;
	{ The PC's starting cash is lessened by the amount of equipment carried. }
var
	total: Integer;
	Item: GearPtr;
begin
	total := 250;
	Item := PC^.InvCom;
	while Item <> Nil do begin
		if Item^.G = GG_Item then total := total - GPValue( Item );
		Item := Item^.Next;
	end;
	if total < 0 then total := 0;
	PCStartingCash := total div 2;
end;

Procedure CampaignPlayer( Camp: CampaignPtr );
	{ Play this campaign. }
	{ I don't really like this setup... I never like the setup of my flow }
	{ control procedure. If something happens to corrupt the value of ActiveGB, }
	{ it will render the campaign unplayable even though it should be trivial }
	{ to figure out where the party is. Admittedly corrupting variables could }
	{ have all kinds of bad effects but this one strikes me as particularly }
	{ unfair. I like the rest of the program... it's like you have a really }
	{ nice stereo, but the CD switching task is handled by trained weasels. }
var
	GBID: LongInt;
	GB,OldGB: GameBoardPtr;
	command,t: Integer;
	Scene: GearPtr;
begin
	{ Determine the active gameboard. If undefined, default to the }
	{ first one in the adventure structure. }
	GBID := NAttValue( Camp^.Source^.NA , NAG_SaveFileData , NAS_CurrentGB );
	if GBID = 0 then GBID := Camp^.boards^.ID;
	GB := FindGameboard( Camp , GBID );
	if GB = Nil then Exit;

	repeat
		command := ExplorationMode( GB );

		if ( G_Destination <> 0 ) and ( command <> CMD_Quit ) and not Party_Dead then begin
			{ Time to move to a new scene. }
			{ Record the ID of the current scene; we'll need it. }
			GBID := GB^.ID;
			OldGB := GB;
			GB := FindGameBoard( Camp , G_Destination );
			if GB = Nil then begin
				Scene := SeekGearByIDTag( Camp^.Source , NAG_StoryData , NAS_UniqueID , G_Destination );
				if ( Scene <> Nil ) and ( Scene^.G = GG_Scene ) then begin
					GB := GenerateMap( Camp , Scene );
				end else begin
					command := CMD_Quit;
				end;
			end;
			if GB <> Nil then begin
				SetNAtt( Camp^.Source^.NA , NAG_SaveFileData , NAS_CurrentGB , G_Destination );
				for t := 1 to Num_Party_Members do begin
					if G_Party[ t ] <> Nil then begin
						DelinkGear( OldGB^.Contents , G_Party[ T ] );
						AppendGear( GB^.Contents , G_Party[ T ] );
					end;
				end;
				PlacePartyOnMap( GB , GBID );
			end;
		end;
	until Party_Dead or ( Command = CMD_Quit );
end;

Procedure LoadCampaign;
	{ Attempt to restore a campaign previously saved to disk. }
var
	RPM: RPGMenuPtr;
	rpgname: String;	{ Campaign Name }
	Camp: CampaignPtr;
	F: Text;		{ A File }
begin
	{ Create a menu listing all the units in the SaveGame directory. }
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_MainMenu );
	BuildFileMenu( RPM , Save_Game_Directory + 'rpg_*.txt' );

	{ If any units are found, allow the player to load one. }
	if RPM^.NumItem > 0 then begin
		RPMSortAlpha( RPM );

		rpgname := SelectFile( RPM , @MainMenuRedraw );

		if rpgname <> '' then begin
			Assign(F, Save_Game_Directory + rpgname );
			reset(F);
			Camp := ReadCampaign(F);
			Close(F);

			CampaignPlayer( Camp );
			DisposeCampaign( Camp );
		end;
	end;

	DisposeRPGMenu( RPM );
end;

Procedure InitCampaign( Camp: CampaignPtr );
	{ A campaign has been generated. Start it going. }
var
	GB: GameBoardPtr;
	Party,PC: GearPtr;
	N: Integer;
	Cash: LongInt;
	name: String;
begin
	Party := SelectParty;
	if Party <> Nil then begin
		{ Create the gameboard. }
		GB := GenerateMap( Camp , Camp^.Source^.SubCom );

		name := GetStringFromUser( MsgString( 'StartCampaign_EnterName' ) , @PrettyPictureDisplay );
		if name = '' then name := 'Bobs Dwarf';

		SetSAtt( Camp^.Source^.SA , 'name <' + name + '>' );

		{ Initialize the party and starting cash. }
		Cash := 100;

		{ Place the party on the map. }
		for N := 1 to Num_Party_Members do G_Party[ N ] := Nil;
		N := 1;

		while Party <> Nil do begin
			PC := Party;
			DelinkGear( Party , PC );
			AppendGear( GB^.Contents, PC );
			Cash := Cash + PCStartingCash( PC );

			G_Party[ N ] := PC;

			Inc( N );
		end;
		PlacePartyOnMap( GB , 0 );

		SetNAtt( Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , Cash );

		SetNAtt( Camp^.Source^.NA , NAG_SaveFileData , NAS_CurrentGB , GB^.ID );
		CampaignPlayer( Camp );

		DisposeCampaign( Camp );
	end;
end;

Procedure StartRandomCampaign;
	{ Adding and testing systems one at a time. }
var
	Camp: CampaignPtr;
begin
	Camp := RandomCampaign( 7 );
	InitCampaign( Camp );
end;

Procedure StartDebug;
	{ Adding and testing systems one at a time. }
var
	Camp: CampaignPtr;
begin
	Camp := DebugCampaign;
	InitCampaign( Camp );
end;


Procedure EquipmentAnalysis;
	{ Check to see the MinLevel of each item in the standard list. }
var
	E: GearPtr;
	Lvl: Integer;
begin
	E := Standard_Equipment_List;
	if e = Nil then writeln( 'WTF' );
	while E <> Nil do begin
		Lvl := ItemMinLevel( E );
		writeln( GearName( E ) , ': ' , Lvl , '  (' , GPValue( E ) , 'gp)' );
		E := E^.Next;
	end;
end;


var
	RPM: RPGMenuPtr;
	N: Integer;

begin
{	EquipmentAnalysis;}

	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_MainMenu );
	AddRPGMenuItem( RPM , MsgString( 'MM_CreateNewPC' ) , 1 );
	AddRPGMenuItem( RPM , MsgString( 'MM_LoadCampaign' ) , 2 );
	AddRPGMenuItem( RPM , MsgString( 'MM_StartCampaign' ) , 3 );

	if Wizard_On then AddRPGMenuItem( RPM , 'Debug Campaign' , 101 );

	AddRPGMenuItem( RPM , MsgString( 'MM_QuitGame' ) , -1 );

	repeat
		N := SelectMenu( RPM , @MainMenuRedraw );

		case N of
			1:	CreateThenSaveNewPC;
			2:	LoadCampaign;
			3:	StartRandomCampaign;
			101:	StartDebug;
		end;
	until N = -1;

	{deallocate all dynamic resources.}
	DisposeRPGMenu( RPM );
end.
