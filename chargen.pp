unit chargen;
{
	The PC generator.
}

{
	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses gears;


Function CreateNewPC: GearPtr;
Procedure CreateThenSaveNewPC;


implementation

uses texutil,sdl,sdlgfx,uiconfig,sdlmenus,gamebook,avatar,gearparser,equipment,frontend;

const
	Right_Column_Width = 200;
	Right_Column_X = ( ScreenWidth * 3 ) div 4 - ( Right_Column_Width div 2 );

	{ The TopBoxTotal includes both the Instructions and the MenuDesc, to be separated by }
	{ a swash border or somesuch. }
	ZONE_CharGen_TopBoxTotal: TSDL_Rect = ( x:Right_Column_X; y: 80; w: Right_Column_Width; h: 170 );
	ZONE_CharGen_Instructions: TSDL_Rect = ( x:Right_Column_X; y: 80; w: Right_Column_Width; h: 40 );
	ZONE_CharGen_MenuDesc: TSDL_Rect = ( x:Right_Column_X; y: 150; w: Right_Column_Width; h: 100 );
	ZONE_CharGen_Avatar: TSDL_Rect = ( x:screenwidth * 3 div 4 - 27; y: 193; w: 54; h: 54 );
	ZONE_CharGen_Menu: TSDL_Rect = ( x:Right_Column_X; y: screenheight div 2 + 10; w: Right_Column_Width; h: screenheight div 2 - 100 );

var
	CG_PC: GearPtr;
	CG_Instructions: String;
	CG_Avatar: SensibleSpritePtr;



Procedure ChargenRedraw;
	{ Redraw the screen for the character generation routines. }
begin
	{ Step One: Display the background graphic. }
	PrettyPictureDisplay;
	InfoBox( ZONE_CharGen_TopBoxTotal );
	InfoBox( ZONE_CharGen_Menu );
	if CG_PC <> Nil then DrawCharacterSheet( Nil , CG_PC , False );
	if CG_Instructions <> '' then CMessage( CG_Instructions , ZONE_CHarGen_Instructions , TextColor );
	if CG_Avatar <> Nil then begin
		DrawSprite( LocateBUSprite( 'avatar_frame.png' , 54 , 54 ) , ZONE_CharGen_Avatar , 0 );
		DrawSprite( CG_Avatar , ZONE_CharGen_Avatar , 0 );
	end;
end;

Function RollStat(n: integer): integer;
	{Roll Nd6; take the three highest values, add them together,}
	{and return the result. N must be in the range of 1 to 10.}
var
	k: array [1..10] of integer;
	t,tt: integer;	{Loop counters.}
	l: integer;	{in theory, the low value.}
	stat: integer;	{The total value rolled}
begin
	{Range check.}
	if n>10 then n := 10;

	{Initialize stat}
	stat := 0;

	{Roll the indicated number of dice.}
	for t := 1 to n do begin
		{Roll the die}
		k[t] := Random(6) + 1;

		{Add it to the total}
		stat := stat + k[t];
	end;

	{If we rolled more dice than we need, go through and eliminate}
	{the low rolls.}
	if n > 3 then for t := 1 to n-3 do begin
		{locate the first nonzero value for l}
		l := 1;

		while k[l] = 0 do Inc(l);

		for tt := 1 to n do begin
			if (k[tt] > 0) and (k[tt] < k[l]) then l := tt
		end;
		stat := stat - k[l];
		k[l] := 0;
	end;

	RollStat := stat;
end;


Function ChooseGender( PC: GearPtr ): Boolean;
	{ Select a gender for the PC. }
	{ Return TRUE if a gender was chosen, or FALSE if selection was cancelled. }
var
	RPM: RPGMenuPtr;
	G: Integer;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_CharGen_Menu );
	CG_Instructions := MsgString( 'CHARGEN_ChooseGender' );
	CG_PC := PC;

	AddRPGMenuItem( RPM , MsgString( 'Gender_1' ) , NAV_Female );
	AddRPGMenuItem( RPM , MsgString( 'Gender_2' ) , NAV_Male );
	AlphaKeyMenu( RPM );

	G := SelectMenu( RPM , @ChargenRedraw );
	if G <> -1 then SetNAtt( PC^.NA , NAG_CharacterData , NAS_Gender , G );

	DisposeRPGMenu( RPM );

	ChooseGender := G <> -1;
end;

Function ChooseSpecies( PC: GearPtr ): Boolean;
	{ Select a species for the PC. }
	{ Return TRUE if one was chosen, or FALSE if selection was cancelled. }
var
	RPM: RPGMenuPtr;
	T: Integer;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_CharGen_Menu );
	AttachMenuDesc( RPM , ZONE_CharGen_MenuDesc );
	CG_Instructions := MsgString( 'CHARGEN_ChooseSpecies' );
	CG_PC := PC;

	for t := 1 to Num_Species do begin
		AddRPGMenuItem( RPM , MsgString( 'SPECIES_' + BStr( T ) ) , T , MsgString( 'SPECIESDESC_' + BStr( T ) ) );
	end;
	AlphaKeyMenu( RPM );

	T := SelectMenu( RPM , @ChargenRedraw );
	if T <> -1 then SetNAtt( PC^.NA , NAG_CharacterData , NAS_Species , T );
	if T = SPECIES_Reptal then SetNAtt( PC^.NA , NAG_Template , NAS_Reptile , 1 );

	DisposeRPGMenu( RPM );

	ChooseSpecies := T <> -1;
end;

Function ChooseStats( PC: GearPtr ): Boolean;
	{ Roll some stats for the PC. }
	{ Return TRUE if stats accepted, or FALSE if selection was cancelled. }
	Function AtLeastOneClass: Boolean;
		{ Return TRUE if the PC can join at least one class, or FALSE }
		{ otherwise. }
	var
		T: Integer;
		FoundOne: Boolean;
	begin
		FoundOne := False;
		for t := 1 to Num_Classes do begin
			if CanJoinClass( PC , T ) then begin
				FoundOne := True;
				Break;
			end;
		end;
		AtLeastOneClass := FoundOne;
	end;
const
	Species_Stat_Mod: Array [1..Num_Species,1..Num_Model_Stats] of smallint = (
	( 	 0,  0,  0,  0,  0,  0 	), { Human }
	( 	 0,  2, -2,  0,  0,  0 	), { Dwarf }
	( 	-1, -1,  1,  1,  0,  0 	), { Elf }
	( 	-2,  0,  0,  0,  2,  0 	), { Gnome }
	( 	 2,  0,  0,  0, -2,  0 	), { Orc }
	( 	-3, -2,  2,  0,  0,  2 	), { Hurthling }
	( 	-1, -1,  0, -1, -1,  3 	), { Fuzzy }
	( 	 4,  3, -2, -4, -3, -2 	), { Reptal }
	( 	 0,  0,  0,  0,  0,  0 	)  { Centaur }
	);
var
	RPM: RPGMenuPtr;
	G,T,PCS: Integer;
begin
	CG_Instructions := MsgString( 'CHARGEN_ChooseStats' );
	CG_PC := PC;

	PCS := NAttValue( PC^.NA , NAG_CharacterData , NAS_Species );

	repeat
		{ Roll the stats. }
		G := 0;
		repeat
			for t := 1 to Num_Model_Stats do begin
				PC^.Stat[ T ] := RollStat( 4 ) + Species_Stat_Mod[ PCS , T ];
				if PC^.Stat[ T ] < 3 then PC^.Stat[ T ] := 3;
			end;
			Inc( G )
		until AtLeastOneClass or ( G > 20 );

		{ Create the menu with a list of legal class choices. }
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_CharGen_Menu );
		AttachMenuDesc( RPM , ZONE_CharGen_MenuDesc );
		for t := 1 to Num_Classes do begin
			if CanJoinClass( PC , T ) then AddRPGMenuItem( RPM , MsgString( 'CLASS_' + BStr( T ) ) , T , MsgString( 'CLASSDESC_' + BStr( T ) ) );
		end;
		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );
		AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseStats_Reroll' ) , -2 );
		SetItemByValue( RPM, -2 );


		G := SelectMenu( RPM , @ChargenRedraw );
		DisposeRPGMenu( RPM );
	until G <> -2;

	if G <> -1 then begin
		{ A starting character gets half a die's worth of HP and MP, }
		{ in addition to the randomly rolled amount. }
		SetNAtt( PC^.NA , NAG_CharacterData , NAS_BaseHP , Class_HP_Die[ G ] div 2 );
		SetNAtt( PC^.NA , NAG_CharacterData , NAS_BaseMP , Class_MP_Die[ G ] div 2 );

		{ Add this class level for all the extra stuff. }
		AddClassLevel( PC , G );
	end;

	ChooseStats := G <> -1;
end;

Function ChooseAvatar( PC: GearPtr ): Boolean;
	{ Edit the PC's avatar. }
	{ Return TRUE once avatar accepted. }
	Function IsLegalStarterItem( I: GearPtr; Slot: Integer ): Boolean;
		{ Return TRUE if this is a legal starter item for the given slot, }
		{ or FALSE otherwise. }
	const
		Slot_Allowance: Array [1..Num_Equipment_Slots] of integer = (
			75, 0, 90, 20, 10,
			10, 10
		);
	begin
		IsLegalStarterItem := ( Slot = SlotForItem( I ) ) and ItemCanBeEquipped( PC , I ) and ( GPValue( I ) <= Slot_Allowance[ SlotForItem( I ) ] );
	end;
	Procedure ChooseItemForSlot( Slot: Integer );
		{ Choose an item to go in this slot. If an item is already }
		{ equipped, dispose of it. }
	var
		RPM: RPGMenuPtr;
		Item: GearPtr;
		N: Integer;
	begin
		{ Begin by creating the menu. }
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_CharGen_Menu );
		Item := Standard_Equipment_List;
		N := 1;

		while Item <> Nil do begin
			if IsLegalStarterItem( Item , Slot ) then begin
				AddRPGMenuItem( RPM , GearName( Item ) , N );
			end;
			Inc( N );
			Item := Item^.Next;
		end;
		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );
		AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_RemoveItem' ) , -2 );
		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		N := SelectMenu( RPM , @ChargenRedraw );
		DisposeRPGMenu( RPM );

		if N > -1 then begin
			{ We want a new item. Get rid of the old item, if }
			{ appropriate, and insert the new one. }
			Item := FindEquippedItem( PC , Slot );
			if Item <> Nil then RemoveGear( PC^.InvCom , Item );
			Item := CloneGear( RetrieveGearSib( Standard_Equipment_List , N ) );
			InsertInvCom( PC , Item );
			EquipItem( PC , Item );
		end else if N = -2 then begin
			{ The player doesn't want an item in this slot. }
			Item := FindEquippedItem( PC , Slot );
			if Item <> Nil then RemoveGear( PC^.InvCom , Item );
		end;
	end;
var
	RPM: RPGMenuPtr;
	Species,Gender,G,T: Integer;
	Item: GearPtr;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_CharGen_Menu );
	CG_Instructions := MsgString( 'CHARGEN_ChooseAvatar' );
	CG_PC := PC;

	Species := NAttValue( PC^.NA , NAG_CharacterData , NAS_Species );
	Gender := NAttValue( PC^.NA , NAG_CharacterData , NAS_Gender );

	AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Skin' ) , 1 );
	SetNAtt( PC^.NA , NAG_Appearance , NAS_Avatar_Skin , Random( Num_Species_Skins[ Species ] ) );

	if Species <> SPECIES_Reptal then begin
		AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Hair' ) , 2 );
		SetNAtt( PC^.NA , NAG_Appearance , NAS_Avatar_Hair , Random( Num_Hair_Styles + 1 ) );

		if ( Species = SPECIES_DWARF ) or ( Gender <> NAV_Female ) then AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Beard' ) , 3 );
	end;

	AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Weapon' ) , 4 );
	AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Armor' ) , 5 );
	AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Hat' ) , 6 );
	if Species <> SPECIES_Centaur then AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Shoes' ) , 7 );
	AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Gloves' ) , 8 );
	AddRPGMenuItem( RPM , MsgString( 'CHARGEN_ChooseAvatar_Cloak' ) , 9 );


	AddRPGMenuItem( RPM , MsgString( 'Done' ) , -1 );
	AlphaKeyMenu( RPM );

	repeat
		CG_Avatar := GenerateAvatar( PC );
		G := SelectMenu( RPM , @ChargenRedraw );

		if G = 1 then begin
			{ Alter skin tone. }
			T := NAttValue( PC^.NA , NAG_Appearance , NAS_Avatar_Skin );
			T := ( T + 1 ) mod Num_Species_Skins[ Species ];
			SetNAtt( PC^.NA , NAG_Appearance , NAS_Avatar_Skin , T );
		end else if G = 2 then begin
			{ Alter hair. }
			T := NAttValue( PC^.NA , NAG_Appearance , NAS_Avatar_Hair );
			T := ( T + 1 ) mod ( Num_Hair_Styles + 1 );
			SetNAtt( PC^.NA , NAG_Appearance , NAS_Avatar_Hair , T );

		end else if G = 3 then begin
			{ Alter beard. }
			T := NAttValue( PC^.NA , NAG_Appearance , NAS_Avatar_Beard );
			T := ( T + 1 ) mod ( Num_Beard_Styles + 1 );
			SetNAtt( PC^.NA , NAG_Appearance , NAS_Avatar_Beard , T );

		end else if G = 4 then begin
			ChooseItemForSlot( NAV_Hand1 );

		end else if G = 5 then begin
			ChooseItemForSlot( NAV_Body );

		end else if G = 6 then begin
			ChooseItemForSlot( NAV_Head );

		end else if G = 7 then begin
			ChooseItemForSlot( NAV_Feet );

		end else if G = 8 then begin
			ChooseItemForSlot( NAV_Arm );

		end else if G = 9 then begin
			ChooseItemForSlot( NAV_Back );

		end;

		RemoveSprite( CG_Avatar );
	until G = -1;

	{ If we chose a bow or a sling as our weapon, give the basic quiver or }
	{ stones as the Hand2 item. }
	Item := FindEquippedItem( PC , NAV_Hand1 );
	if ( Item <> Nil ) then begin
		if ( Item^.S = GS_Bow ) then begin
			Item := LoadNewItem( 'Arrows' );
			InsertInvCom( PC , Item );
			EquipItem( PC , Item );
		end else if ( Item^.S = GS_Sling ) then begin
			Item := LoadNewItem( 'Bullets' );
			InsertInvCom( PC , Item );
			EquipItem( PC , Item );
		end;
	end;

	DisposeRPGMenu( RPM );

	ChooseAvatar := True;
end;

Function CreateNewPC: GearPtr;
	{ Generate a new player character, set its avatar stats and give it }
	{ some starting equipment (clothes, shoes, and a weapon at least). }
	{ This function will return Nil if character generation was cancelled. }
var
	PC: GearPtr;
	KeepGoing: Boolean;
	name: String;
begin
	{ Generate a new PC record. }
	PC := NewGear( Nil );
	PC^.G := GG_Model;
	PC^.S := GS_PCTeam;

	CG_Avatar := Nil;

	{ Step One: Choose Gender. }
	KeepGoing := ChooseGender( PC );

	{ Step Two: Choose Species. }
	if KeepGoing then KeepGoing := ChooseSpecies( PC );

	{ Step Three and Four: Roll Stats and Choose Class. }
	if KeepGoing then KeepGoing := ChooseStats( PC );

	{ Step Five: Edit Avatar. }
	if KeepGoing then KeepGoing := ChooseAvatar( PC );

	{ Step Six: Enter Name. }
	if KeepGoing then begin
		{ Enter a name. }
		CG_Instructions := MsgString( 'CHARGEN_Complete' );
		name := GetStringFromUser( MsgString( 'CHARGEN_ChooseName' ) , @ChargenRedraw );
		if name <> '' then begin
			SetSAtt( PC^.SA , 'name <' + name + '>' );
		end else begin
			KeepGoing := False;
		end;
	end;

	{ At the end, if not KeepGoing then dispose of the PC record. }
	if not KeepGoing then DisposeGear( PC );

	CreateNewPC := PC;
end;

Procedure CreateThenSaveNewPC;
	{ Create a new character, then save it to disk. }
var
	PC: GearPtr;
	F: Text;
begin
	PC := CreateNewPC;
	if PC <> Nil then begin
		Assign( F , Save_Game_Directory + 'cha_' + GearName( PC ) + '.txt' );
		Rewrite( F );
		WriteCGears( F , PC );
		Close( F );
		DisposeGear( PC );
	end;
end;


end.
