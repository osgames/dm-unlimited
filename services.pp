unit services;
	{ This unit will handle services, such as the temples and }
	{ the Adventurer's Guild. }

{
	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}


interface

uses gamebook;

Procedure AdvancementGuild( GB: GameBoardPtr );
Procedure OpenLibrary( GB: GameBoardPtr );

Function PartyNeedsRest( GB: GameBoardPtr ): Boolean;
Procedure RestTheNight( GB: GameBoardPtr );
Procedure OpenInn( GB: GameBoardPtr );

Procedure OpenTemple( GB: GameBoardPtr );


implementation

uses texutil,gears,sdlgfx,sdlmenus,frontend,uiconfig,gearparser;


Procedure DoTraining( GB: GameBoardPtr; PC: Integer );
	{ The provided PC is going up a level. Add one to the level count, add one to }
	{ a random stat, then prompt to either switch classes or gain another level in }
	{ the current class. }
var
	RPM: RPGMenuPtr;
	Stat,N: Integer;
	C: GearPtr;
begin
	SetupTwoColumnRedraw( GB , '' , G_Party[ PC ] );

	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
	AttachMenuDesc( RPM , ZONE_2CR_RightText );

	AddRPGMenuItem( RPM , MsgString( 'KeepJob' ) , -1 , MsgString( 'KeepJobDesc' ) );
	AddRPGMenuItem( RPM , MsgString( 'ChangeJob' ) , 1 , MsgString( 'ChangeJobDesc' ) );

	N := SelectMenu( RPM , @TwoColumnRedraw );
	DisposeRPGMenu( RPM );

	if N = 1 then begin
		{ The player wants to switch class. Prompt for a new one. }
		{ First, unequip all currently equipped items, since they might not }
		{ be compatable with the new class. }
		C := G_Party[PC]^.InvCom;
		while C <> Nil do begin
			SetNAtt( C^.NA , NAG_ItemData , NAS_EquipSlot , 0 );
			C := C^.Next;
		end;

		{ Create the menu. }
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		AttachMenuDesc( RPM , ZONE_2CR_RightText );
		SetupTwoColumnRedraw( GB , ReplaceHash( MsgString( 'DoTraining_NewClass' ) , GearName( G_Party[PC] ) ) , G_Party[ PC ] );

		{ Add the classes. }
		for N := 1 to Num_Classes do begin
			if CanJoinClass( G_Party[PC] , N ) then begin
				AddRPGMenuItem( RPM , MsgString( 'CLASS_' + BStr( N ) ) , N , MsgString( 'CLASSDESC_' + BStr( N ) ) );
			end;
		end;

		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );

		RPM^.Mode := RPMNoCancel;
		N := SelectMenu( RPM , @TwoColumnRedraw );
		DisposeRPGMenu( RPM );

	end else begin
		N := NAttValue( G_Party[PC]^.NA , NAG_CharacterData , NAS_CurrentClass );
	end;

	{ If not changing class, you get +1 to a random stat. }
	if N = NAttValue( G_Party[ PC ]^.NA , NAG_CharacterData , NAS_CurrentClass ) then begin
		Stat := Random( Num_Model_Stats ) + 1;
		Inc( G_Party[ PC ]^.Stat[ Stat ] );
		TwoColumnAlert( ReplaceHash( ReplaceHash( msgString( 'DoTraining_ImproveStat' ) , GearName( G_Party[ PC ] ) ) , MsgString( 'STAT_' + BStr( STAT ) ) ) );
	end;

	AddClassLevel( G_Party[PC] , N );
end;

Procedure AdvancementGuild( GB: GameBoardPtr );
	{ Allow each member of the party to be checked by the guild. If they have enough }
	{ XP for the next level, allow them to either take an advance in their current }
	{ class or switch jobs to something else. }
var
	RPM: RPGMenuPtr;
	N: Integer;
begin
	repeat
		SetupTwoColumnRedraw( GB , MsgString( 'Adavancement_Guild_Title' ) , Nil );

		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		AttachMenuDesc( RPM , ZONE_2CR_RightText );
		for N := 1 to Num_Party_Members do if ( G_Party[ N ] <> Nil ) and ( XPNeededForNextLevel( G_Party[ N ] ) <= NAttValue( G_Party[N]^.NA , NAG_CharacterData , NAS_Experience ) ) then AddRPGMenuItem( RPM , GearName( G_Party[ N ] ) , N );
		AlphaKeyMenu( RPM );
		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		N := SelectMenu( RPM , @TwoColumnRedraw );
		DisposeRPGMenu( RPM );

		if ( N >= 1 ) and ( N <= Num_Party_Members ) then DoTraining( GB , N );
	until N = -1;
end;

Function CanAddSpell( PC, S: GearPtr ): Boolean;
	{ Return TRUE if the PC has enough free Knowledge to add this spell, }
	{ or FALSE if not. }
var
	T: Integer;
	it: Boolean;
begin
	if ( SpellGemsRequired( S , 0 ) <= TotalFreeSpellGems( PC ) ) and ( S^.V <= ( ( TotalLevel( PC ) + 1 ) div 2 )) then begin

		{ If enough general Knowledge exists, assume true until shown false. }
		it := True;
		for t := 1 to num_spell_Colors do begin
			if SpellGemsRequired( S , T ) > FreeSpellGems( PC , T ) then it := False;
		end;
		CanAddSpell := it;
	end else begin
		{ Not enough general Knowledge to prepare this spell. Sorry. }
		CanAddSpell := False;
	end;
end;

Procedure AddSpell( GB: GameBoardPtr; PC: Integer );
	{ The PC will be given a list of potential spells to learn, and then may }
	{ select some to use. }
var
	RPM: RPGMenuPtr;
	S: GearPtr;
	N: Integer;
	msg: String;
begin
	msg := MsgString( 'AddSpell_Caption' );
	repeat
		{ Generate the menu. }
		SetupTwoColumnRedraw( GB , msg , G_Party[ PC ] );
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		AttachTwoColumnInfo( RPM , Standard_Spell_List );

		{ Add all of the spells that can be prepared. }
		S := Standard_Spell_List;
		N := 1;
		while S <> Nil do begin
			if CanAddSpell( G_Party[ PC ] , S ) and ( SeekSibByName( G_Party[PC]^.SubCom , GearName( S ) ) = Nil ) then begin
				AddRPGMenuItem( RPM , GearName( S ) , N , GearDesc( S ) );
			end;
			S := S^.Next;
			Inc( N );
		end;

		{ Sort the list. }
		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );

		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		{ Query the menu, and delete it immediately afterwards. }
		N := SelectMenu( RPM , @TwoColumnRedraw );
		DisposeRPGMenu( RPM );

		if N > -1 then begin
			{ Add the spell to the PC's list, and also }
			{ subtract the points from sk_used and sk_required. }
			S := CloneGear( RetrieveGearSib( Standard_Spell_List , N ) );
			msg := ReplaceHash( MsgString( 'AddSpell_Result' ) , GearName( G_Party[ PC ] ) );
			msg := ReplaceHash( msg , GearName( S ) );
			InsertSubCom( G_Party[ PC ] , S );
		end;

	until N = -1;
end;

Procedure MinusSpell( GB: GameBoardPtr; PC: Integer );
	{ The PC will choose a spell from among those currently known, and }
	{ have it erased from the spell list. This will free up room for }
	{ other spells to be learned. }
var
	N: Integer;
	RPM: RPGMenuPtr;
	Spell: GearPtr;
	msg: String;
begin
	msg := MsgString( 'MinusSpell_Caption' );
	repeat
		{ Generate the menu. }
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
		SetupTwoColumnRedraw( GB , msg , G_Party[ PC ] );
		AttachTwoColumnInfo( RPM , G_Party[ PC ]^.SubCom );

		{ Add all of the spells that the PC knows. }
		N := 1;
		Spell := G_Party[ PC ]^.SubCom;
		while Spell <> Nil do begin
			if ( Spell^.G = GG_Invocation ) and ( Spell^.S = GS_Spell ) then begin
				AddRPGMenuItem( RPM , GearName( Spell ) , N , GearDesc( Spell ) );
			end;
			Inc( N );
			Spell := Spell^.Next;
		end;

		{ Sort the list. }
		RPMSortAlpha( RPM );
		AlphaKeyMenu( RPM );

		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		{ Query the menu, and delete it immediately afterwards. }
		N := SelectMenu( RPM , @TwoColumnRedraw );
		DisposeRPGMenu( RPM );

		if N > -1 then begin
			{ Remove the spell from the PC's list, and also }
			{ give back the points to sk_used and sk_required. }
			Spell := RetrieveGearSib( G_Party[ PC ]^.SubCom , N );
			msg := ReplaceHash( MsgString( 'MinusSpell_Result' ) , GearName( G_Party[ PC ] ) );
			msg := ReplaceHash( msg , GearName( Spell ) );
			RemoveGear( G_Party[ PC ]^.SubCom , Spell );
		end;
	until N = -1;
end;


Procedure LearnSpells( GB: GameBoardPtr; PC: Integer );
	{ PC will now pick the spells for the adventure. }
	{ Available options are to learn a new spell, to discard an existing spell, }
	{ or to exit. }
var
	RPM: RPGMenuPtr;
	N: Integer;
begin
	{ Generate the menu. }
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightMenu );
	AddRPGMenuItem( RPM , MsgString( 'LS_LearnNewSpell' ) , 1 );
	AddRPGMenuItem( RPM , MsgString( 'LS_DiscardSpell' ) , 2 );
	AddRPGMenuItem( RPM , MsgString( 'LS_Exit' ) , -1 );
	AlphaKeyMenu( RPM );

	repeat
		SetupTwoColumnRedraw( GB , ReplaceHash( MsgString( 'LearnSpells' ) , GearName( G_Party[ PC ] ) ) , G_Party[ PC ] );
		N := SelectMenu( RPM , @TwoColumnRedraw );

		case N of
			1: AddSpell( GB , PC );
			2: MinusSpell( GB , PC );
		end;
	until N = -1;

	DisposeRPGMenu( RPM );
end;


Procedure OpenLibrary( GB: GameBoardPtr );
	{ Prompt for character, then learn spells above. }
var
	RPM: RPGMenuPtr;
	N: Integer;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_2CR_RightTotal );
	for N := 1 to Num_Party_Members do if ( G_Party[ N ] <> Nil ) then AddRPGMenuItem( RPM , GearName( G_Party[ N ] ) , N );
	AlphaKeyMenu( RPM );
	AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

	repeat
		SetupTwoColumnRedraw( GB , MsgString( 'LIBRARY_WhoEnters' ) , Nil );
		N := SelectMenu( RPM , @TwoColumnRedraw );

		if ( N >= 1 ) and ( N <= Num_Party_Members ) then LearnSpells( GB , N );
	until N = -1;

	DisposeRPGMenu( RPM );
end;

Function PartyNeedsRest( GB: GameBoardPtr ): Boolean;
	{ Return TRUE if the party needs rest, or FALSE otherwise. }
var
	NeedsRest: Boolean;
	N: Integer;
	E: GearPtr;
begin
	NeedsRest := False;
	for N := 1 to Num_Party_Members do begin
		if ( G_Party[ N ] <> Nil ) and IsAlright( G_Party[ N ] ) then begin
			if ( NAttValue( G_Party[ N ]^.NA , NAG_Damage , NAS_HPDmg ) > 0 ) or ( NAttValue( G_Party[ N ]^.NA , NAG_Damage , NAS_MPDmg ) > 0 ) then NeedsRest := True;
			E := G_Party[ N ]^.SubCom;
			while ( E <> Nil ) and not NeedsRest do begin
				if ( E^.G = GG_Enchantment ) and ( E^.Stat[ STAT_Enchantment_Duration ] = EDUR_OneDay ) then NeedsRest := True;
				E := E^.Next;
			end;

		end;
	end;
	PartyNeedsRest := NeedsRest;
end;

Procedure RestTheNight( GB: GameBoardPtr );
	{ The party is resting for the night. Do everything that requires. }
var
	N: Integer;
	E,E2: GearPtr;
begin
	for N := 1 to Num_Party_Members do begin
		if ( G_Party[ N ] <> Nil ) and IsAlright( G_Party[ N ] ) then begin
			{ Restore HP, MP, and get rid of daily enchantments. }
			StripNAtt( G_Party[ N ] , NAG_Damage );
			StripNAtt( G_Party[ N ] , NAG_DailyData );
			E := G_Party[ N ]^.SubCom;
			while E <> Nil do begin
				E2 := E^.Next;
				if ( E^.G = GG_Enchantment ) and ( E^.Stat[ STAT_Enchantment_Duration ] = EDUR_OneDay ) then begin
					RemoveGear( G_Party[ N ]^.SubCom , E );
				end;
				E := E2;
			end;

		end;
	end;
	{ Resting resets the Field Camp Bonus. }
	SetNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_FieldCampBonus , 0 );
end;

Procedure OpenInn( GB: GameBoardPtr );
	{ This is a pretty easy one. Pay 20gp to recover all HP and MP. }
const
	Inn_Cost = 20;
var
	RPM: RPGMenuPtr;
	N: Integer;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_1CR_Menu );
	if PartyNeedsRest( GB ) then AddRPGMenuItem( RPM , MsgString( 'INN_StayTheNight' ) , 1 );
	AddRPGMenuItem( RPM , MsgString( 'INN_NotRightNow' ) , -1 );
	AlphaKeyMenu( RPM );

	SetupOneColumnRedraw( GB , MsgString( 'INN_Caption' ) , MsgString( 'INN_Text' ) );
	N := SelectMenu( RPM , @OneColumnRedraw );
	DisposeRPGMenu( RPM );

	if N = 1 then begin
		if NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold ) > Inn_Cost then begin
			AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , -Inn_Cost );
		end else begin
			Alert( GB , MsgString( 'INN_NoMoney' ) );
			SetNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , 0 );
		end;
		Alert( GB , MsgString( 'INN_Rest' ) );
		RestTheNight( GB );
	end;
end;

Procedure OpenTemple( GB: GameBoardPtr );
	{ We're off to the temple for a quick resurrection, or some other }
	{ service. }
	Function ResurrectionCost( PC: GearPtr ): LongInt;
		{ Return the cost of bringing this PC back from the dead. }
	begin
		ResurrectionCost := TotalLevel( PC ) * 100;
	end;
	Procedure AttemptResurrection( var msg: String );
		{ Try to resurrect a party member. This will fail if you select }
		{ cancel or if you can't afford it. }
		{ We are passed the text message because we may need to modify }
		{ it here. }
	var
		RPM: RPGMenuPtr;
		N: Integer;
		Cost: LongInt;
		P: Point;
	begin
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_1CR_Menu );
		for N := 1 to Num_Party_Members do if ( G_Party[ N ] <> Nil ) and not IsAlright( G_Party[ N ] ) then AddRPGMenuItem( RPM , GearName( G_Party[ N ] ) + ' (' + BStr( ResurrectionCost( G_Party[ N ] ) ) + 'gp)' , N );
		AlphaKeyMenu( RPM );
		SetupOneColumnRedraw( GB , MsgString( 'TEMPLE_Caption' ) , MsgString( 'TEMPLE_ResText' ) );
		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		N := SelectMenu( RPM , @OneColumnRedraw );
		DisposeRPGMenu( RPM );

		if N > -1 then begin
			Cost := ResurrectionCost( G_Party[ N ] );
			if Cost <= NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold ) then begin
				AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , -Cost );
				StripNAtt( G_Party[ N ] , NAG_Damage );
				P := FindPointNearParty( GB );
				SetNAtt( G_Party[ N ]^.NA , NAG_Location , NAS_X , P.X );
				SetNAtt( G_Party[ N ]^.NA , NAG_Location , NAS_Y , P.Y );
				msg := ReplaceHash( MsgString( 'TEMPLE_OKResurrect' ) , GearName( G_Party[ N ] ) );
			end else begin
				msg := ReplaceHash( MsgString( 'TEMPLE_NoCashToResurrect' ) , GearName( G_Party[ N ] ) );
			end;
		end;
	end;

	Function NeedsRestoration( PC: GearPtr ): Boolean;
		{ Return TRUE if this PC has any points of stat drainage, or }
		{ FALSE otherwise. }
	var
		t: Integer;
		Need: Boolean;
	begin
		Need := False;
		for t := 1 to Num_Model_Stats do if NAttValue( PC^.NA , NAG_StatDrain , T ) > 0 then Need := True;
		NeedsRestoration := Need;
	end;
	Function RestorationCost( PC: GearPtr ): LongInt;
		{ Return the cost of restoring this PC's stats. }
	begin
		RestorationCost := TotalLevel( PC ) * 50;
	end;
	Procedure AttemptRestoration( var msg: String );
		{ Try to restore a party member's stats. This will fail if you }
		{ select cancel or if you can't afford it. }
		{ We are passed the text message because we may need to modify }
		{ it here. }
	var
		RPM: RPGMenuPtr;
		N: Integer;
		Cost: LongInt;
	begin
		RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_1CR_Menu );
		for N := 1 to Num_Party_Members do if ( G_Party[ N ] <> Nil ) and NeedsRestoration( G_Party[ N ] ) then AddRPGMenuItem( RPM , GearName( G_Party[ N ] ) + ' (' + BStr( RestorationCost( G_Party[ N ] ) ) + 'gp)' , N );

		AlphaKeyMenu( RPM );
		SetupOneColumnRedraw( GB , MsgString( 'TEMPLE_Caption' ) , MsgString( 'TEMPLE_StatText' ) );
		AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );

		N := SelectMenu( RPM , @OneColumnRedraw );
		DisposeRPGMenu( RPM );

		if N > -1 then begin
			Cost := RestorationCost( G_Party[ N ] );
			if Cost <= NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold ) then begin
				AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , -Cost );
				StripNAtt( G_Party[ N ] , NAG_StatDrain );
				msg := ReplaceHash( MsgString( 'TEMPLE_OKRestore' ) , GearName( G_Party[ N ] ) );
			end else begin
				msg := ReplaceHash( MsgString( 'TEMPLE_NoCashToRestore' ) , GearName( G_Party[ N ] ) );
			end;
		end;
	end;

var
	RPM: RPGMenuPtr;
	N: Integer;
	Msg: String;
begin
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_1CR_Menu );
	AddRPGMenuItem( RPM , MsgString( 'TEMPLE_Restoration' ) , 2 );
	AddRPGMenuItem( RPM , MsgString( 'TEMPLE_Resurrection' ) , 1 );
	AddRPGMenuItem( RPM , MsgString( 'TEMPLE_Exit' ) , -1 );
	AlphaKeyMenu( RPM );
	Msg := MsgString( 'TEMPLE_Text' );

	repeat
		SetupOneColumnRedraw( GB , MsgString( 'TEMPLE_Caption' ) , msg );
		N := SelectMenu( RPM , @OneColumnRedraw );

		if N = 1 then begin
			{ Resurrection. See if there are any dead party members. }
			AttemptResurrection( msg );
		end else if N = 2 then begin
			AttemptRestoration( msg );
		end;

	until N = -1;
	DisposeRPGMenu( RPM );
end;

end.
