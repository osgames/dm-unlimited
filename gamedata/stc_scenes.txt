%%
%%  *****************************
%%  ***   SCENE  PROTOTYPES   ***
%%  *****************************
%%


WildernessScene
	name <Default Scene>
	desig <SCENE_Default>

CaveScene
	name <Basic Tomb>
	desig <SCENE_Tomb>
	context <TOMB_ LUNAR DOWN_>
	FloorType 13
	SDL_WALLS <wall_darkstone.png>

CaveScene
	name <Bone Tomb>
	desig <SCENE_BoneTomb>
	context <TOMB_ LUNAR DOWN_>
	FloorType 13
	SDL_WALLS <wall_bone.png>

MazeScene
	name <Basic Sewer>
	desig <SCENE_Sewer>
	%% Enter a sewer, divers...
	context <SEWER WATER DOWN_>
	FloorType 12
	SDL_WALLS <wall_darkstone.png>

MazeScene
	name <Labyrinth>
	desig <SCENE_Labyrinth>
	context <CRYPT EARTH DOWN_>
	FloorType 5
	SDL_WALLS <wall_darkstone.png>

CaveScene
	name <Cave>
	desig <SCENE_Cave>
	context <CAVER EARTH DOWN_>
	FloorType 9
	SDL_WALLS <wall_cave.png>

CaveScene
	name <Cave Fort>
	desig <SCENE_CaveFort>
	context <CAVER EARTH FORT_ DOWN_>
	FloorType 5
	SDL_WALLS <wall_lightstone.png>

CaveScene
	name <Icy Cave>
	desig <SCENE_IcyCave>
	context <CAVER WATER ICE__ DOWN_>
	FloorType 6
	SDL_WALLS <wall_ice.png>

MazeScene
	name <Cabin>
	desig <SCENE_Cabin>
	context <CABIN UP___>
	FloorType 4
	SDL_WALLS <wall_logcabin.png>

MazeScene
	name <Castle>
	desig <SCENE_Castle>
	context <FORT_ FIRE_ UP___>
	FloorType 13
	SDL_WALLS <wall_lightbrick.png>

MazeScene
	name <Dark Tower>
	desig <SCENE_DarkTower>
	context <FORT_ LUNAR AIR__ UP___>
	FloorType 2
	SDL_WALLS <wall_darkbrick.png>

MazeScene
	name <Holy Tower>
	desig <SCENE_HolyTower>
	context <FORT_ SOLAR AIR__ UP___>
	FloorType 2
	SDL_WALLS <wall_lightbrick.png>


