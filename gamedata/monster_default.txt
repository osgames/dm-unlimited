%%
%%  ********************
%%  ***   MONSTERS   ***
%%  ********************
%%
%% Unless there's a reason for it, all monsters should have a minimum Strength
%% of 10 and a maximum Luck of 1.
%%
%% If you don't know where to put it, stick it in here.
%%
%% Statline: STR TGH REF INT PIE LUC

	Monster
	name <Gargoyle>
	statline 14 15 9 12 11 1
	Move 8
	Class:Defender 5
	MonsterGP 25
	RESIST:Slashing 40
	RESIST:Piercing 40
	RESIST:Crushing 40
	Template:Earth
	SDL_Sprite <monster_monsters.png>
	Frame 12
	type <EVERY MAZE_ CHAOS RENFA STEAM EARTH AIR__>
	inv
		STC CLAW_2d4
	end

	Monster
	name <Umbull>
	statline 18 16 10 9 11 1
	Move 10
	Class:Beast 5
	Class:Humanoid 5
	MonsterGP 180
	SDL_Sprite <monster_default.png>
	Frame 4
	type <DUNGE CAVE_ DESER CAVER EARTH>
	inv
		STC CLAW_3d6
	end
	sub
		STC MFX_GAZE_CONFUSION
	end


	Monster
	name <Gryphon>
	statline 20 18 12 3 11 1
	Move 12
	Class:Beast 9
	SDL_Sprite <monster_default.png>
	Frame 44
	PhysAtt 5
	PhysDef 5
	type <FORES GRASS SNOWY CAVER NATUR AIR__ BEAST RENFA STEAM>
	inv
		STC CLAW_2d8
	end

	Monster
	name <Kobold>
	statline 10 7 14 12 10 1
	Move 14
	Class:Humanoid 1
	MonsterGP 5
	SDL_Sprite <monster_default.png>
	Frame 5
	type <EVERY DUNGE DRAGO THIEF STEAM EARTH>
	inv
		STC POINTY_d8
	end

	Monster
	name <Kobold Archer>
	statline 10 7 14 12 10 1
	Move 14
	Class:Humanoid 2
	MonsterGP 25
	SDL_Sprite <monster_default.png>
	Frame 10
	type <EVERY DUNGE DRAGO STEAM EARTH>
	inv
		STC SHARP_d4
	end
	sub
		STC MFX_BOW_d6
	end

	Monster
	name <Kobold Fighter>
	statline 10 7 14 12 10 1
	Move 14
	Class:Humanoid 3
	MonsterGP 40
	SDL_Sprite <monster_default.png>
	Frame 6
	PhysDef 10
	type <EVERY DUNGE DRAGO STEAM EARTH FIGHT>
	inv
		STC BLUNT_d8
	end

	Monster
	name <Kobold Priest>
	statline 10 7 14 12 14 1
	Move 14
	Class:Humanoid 4
	MonsterGP 60
	SDL_Sprite <monster_default.png>
	Frame 8
	PhysDef 10
	type <EVERY DUNGE DRAGO STEAM EARTH MAGE_>
	inv
		STC BLUNT_d8
	end
	sub
		StdSpell Silence
		StdSpell Sleep
	end

	Monster
	name <Kobold Champion>
	statline 13 7 15 12 10 1
	Move 14
	Class:Humanoid 6
	MonsterGP 90
	SDL_Sprite <monster_default.png>
	Frame 9
	PhysDef 10
	type <EVERY DUNGE DRAGO STEAM EARTH FIGHT>
	inv
		STC SHARP_d10
	end
	sub
		STC MFX_BOW_d10
	end

	Monster
	name <Kobold Mage>
	statline 10 7 14 14 14 1
	Move 14
	Class:Humanoid 8
	MonsterGP 160
	SDL_Sprite <monster_default.png>
	Frame 7
	PhysDef 10
	type <EVERY DUNGE DRAGO STEAM EARTH MAGE_>
	inv
		STC BLUNT_d8
	end
	sub
		StdSpell Explosion
		StdSpell Acid Spray
	end


	Monster
	name <Corpse Eater>
	statline 13 18 13 1 10 1
	Move 8
	Class:Beast 4
	SDL_Sprite <monster_monsters.png>
	Frame 26
	type <SWAMP DUNGE LUNAR BEAST>
	inv
		STC BITE_d6
		sub
			STC MFX_PARALYSIS_d6
		end
	end

	Monster
	name <Snow Crawler>
	statline 10 14 12 1 10 1
	Move 10
	Class:Beast 4
	Template:Ice
	SDL_Sprite <monster_monsters.png>
	Frame 28
	type <SNOWY ICE__ CAVER BEAST SCIFA SILLY>
	inv
		STC BITE_d6
	end
	sub
		STC MFX_BW_FROST_1d6
	end

	Monster
	name <Winter Wolf>
	statline 15 13 13 1 10 1
	Move 10
	Class:Beast 6
	Template:Ice
	SDL_Sprite <monster_monsters.png>
	Frame 29
	type <SNOWY ICE__ CAVER NATUR BEAST NORSE>
	inv
		STC BITE_d10
	end
	sub
		STC MFX_BW_FROST_3d6
	end


