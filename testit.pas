program testit;

uses gears,gamebook,exploration,chargen,avatar,sdlgfx;

var
	GB: GameBoardPtr;
	PC: GearPtr;
	A: SensibleSpritePtr;

begin
	GB := NewGameBoard( 30 , 30 );
	PC := NewGear( Nil );
	PC^.G := GG_Model;
	PC^.S := GS_PCTeam;
	PC^.Stat[ STAT_Toughness ] := 10;
	AppendGear( GB^.Contents , PC );
	G_Party[ 1 ] := PC;
	SetNAtt( PC^.NA , NAG_Location , NAS_X , 5 );
	SetNAtt( PC^.NA , NAG_Location , NAS_Y , 5 );
	SetSAtt( PC^.SA , 'name <Cow>' );
	SetNAtt( PC^.NA , NAG_Appearance , NAS_Frame , 9 );
	SetNAtt( PC^.NA , NAG_CHaracterData , NAS_BaseHP , 10 );


	PC := CreateNewPC;
	if PC <> Nil then begin
		AppendGear( GB^.Contents , PC );
		G_Party[ 2 ] := PC;
		SetNAtt( PC^.NA , NAG_Location , NAS_X , 4 );
		SetNAtt( PC^.NA , NAG_Location , NAS_Y , 4 );
		A := GenerateAvatar( PC );
		A^.Basically_Unimportant := False;
		SetSAtt( PC^.SA , 'SDL_SPRITE <' + A^.name + '>' );
		SetSAtt( PC^.SA , 'name <Chicken>' );
	end;

	FloorFill( GB , 8 , 8 , 12 , 12 , FLOOR_Sand );
	FloorFill( GB , 9 , 9 , 11 , 11 , FLOOR_Water );

	SetFloor( GB , 16 , 20 , FLOOR_Snow );
	SetWall( GB , 15 , 19 , WALL_BasicWall );
	SetWall( GB , 15 , 18 , WALL_BasicWall );

	FloorFill( GB , 1 , 1 , 6 , 6 , FLOOR_Stone );


	WallFill( GB , 20 , 20 , 23 , 25 , WALL_BasicWall );

	ExplorationMode( GB );

	DisposeGameboard( GB );
end.
