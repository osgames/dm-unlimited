unit effects;
	{ Some things happen and this causes stuff. }

{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}



interface

uses gears,texutil,gamebook;

const
	{ An effect is modeled as a series of gears. Each effect has a type, }
	{ which determines the procedure used to handle it as well as the }
	{ meaning of its stats. Upon activating an event list, first call the }
	{ effect function. If this function returns TRUE, then activate the }
	{ subcom list. If it returns FALSE, activate the invcom list. Keep }
	{ going until you reach the end of the list. }

	{ An effect which gets activated will add its animation sequence to }
	{ the effect history, if it has one. }

	{ The effect results will be reported as a list of animation objects, }
	{ modeled using gears. The G descriptor holds the event order, the S }
	{ descriptor holds the animation type, and the V descriptor is used as }
	{ an internal timer for the display routines. }

	{ The CAPTION string attribute should be displayed along with the }
	{ animation. }

	{ There are two kinds of animation sequence: A shot travels from one }
	{ tile to another, and a point stays in one tile. }
	GS_AOShot = 1;
		STAT_AOShot_X1 = 1;
		STAT_AOShot_Y1 = 2;
		STAT_AOShot_X2 = 3;
		STAT_AOShot_Y2 = 4;
		STAT_AOShot_ShotSeq = 5;
		STAT_AOShot_DirMod = 6;

	GS_AOPoint = 2;
		STAT_AOPoint_X = 1;
		STAT_AOPoint_Y = 2;
		STAT_AOPoint_AnimSeq = 3;

var
	effect_history: GearPtr;	{ Holds effects which then get shown to user. }
	effect_event_order: Integer;	{ Keeps everything from happening at once. }

Procedure Add_HP_Damage( M: GearPtr; HP: Integer );
Procedure Add_MP_Damage( M: GearPtr; MP: Integer );

Procedure HandleEffect( GB: GameBoardPtr; FX,Originator: GearPtr; FX_Stencil: effect_stencil );

Procedure Add_Initial_Shot( ShotSeq,X0,Y0,X1,Y1: Integer );
Function CreateAttackEffect( PC,Weapon: GearPtr ): GearPtr;

implementation

uses gearparser;

Procedure Add_HP_Damage( M: GearPtr; HP: Integer );
	{ Add some damage to model M's total. If this damage kills M, add }
	{ all the needed triggers to the list. }
begin
	AddNAtt( M^.NA , NAG_Damage , NAS_HPDmg , HP );
end;

Procedure Add_MP_Damage( M: GearPtr; MP: Integer );
	{ Add some stamina drain to M's total. If this takes M beyond the }
	{ normal limits of stamina, add that damage to M's HP instead. }
	{ The above procedure should react properly if this kills M. }
var
	MP_Left: Integer;
begin
	MP_Left := MaxMP( M ) - NAttValue( M^.NA , NAG_Damage , NAS_MPDmg );
	if MP > MP_Left then begin
		AddNAtt( M^.NA , NAG_Damage , NAS_MPDmg , MP_Left );
		Add_HP_Damage( M , MP - MP_Left );
	end else begin
		AddNAtt( M^.NA , NAG_Damage , NAS_MPDmg , MP );
	end;
end;

Procedure Add_Shot_Precisely( X1,Y1,X2,Y2,Frame: Integer; Caption: String );
	{ Add a shot animation to the history list. }
	Function DirSpriteOffset( DX, DY: Integer ): Integer;
		{ There are 8 sprites for each projectile type, one for each of }
		{ the eight directions. Determine the direction which best suits }
		{ this vector. }
		{ Sprite 0 is pointing 12 o'clock and they go clockwise from there. }
	var
		Slope: Real;
		tmp: Integer;
	begin
		if DX = 0 then begin
			if DY > 0 then begin
				DirSpriteOffset := 1;
			end else begin
				DirSpriteOffset := 5;
			end;
		end else begin
			Slope := DY/DX;
			if Slope > 4.51 then tmp := 0
			else if Slope > 0.414 then tmp := 1
			else if Slope > -0.414 then tmp := 2
			else if Slope > -4.51 then tmp := 3
			else tmp := 4;
			if DX > 0 then DirSpriteOffset := 5 - tmp
			else DirSpriteOffset := ( 9 - tmp ) mod 8;
		end;
	end;
var
	AnimOb: GearPtr;
begin
	AnimOb := AddGear( Effect_History , Nil );
	AnimOb^.G := EFFECT_Event_Order;
	AnimOb^.S := GS_AOShot;
	AnimOb^.Stat[ STAT_AOShot_X1 ] := X1;
	AnimOb^.Stat[ STAT_AOShot_Y1 ] := Y1;
	AnimOb^.Stat[ STAT_AOShot_X2 ] := X2;
	AnimOb^.Stat[ STAT_AOShot_Y2 ] := Y2;
	if ( Frame < 1 ) or ( Frame > Num_ShotSeq ) then Frame := 1;
	AnimOb^.Stat[ STAT_AOShot_ShotSeq ] := Frame;
	AnimOb^.Stat[ STAT_AOShot_DirMod ] := DirSpriteOffset( X2 - X1 , Y2 - Y1 );
	SetSAtt( AnimOb^.SA , 'CAPTION <' + Caption + '>' );
end;

Procedure Add_Point_Animation( X,Y,AnimSeq: Integer; Caption: String );
	{ Add a shot animation to the history list. }
var
	AnimOb: GearPtr;
begin
	AnimOb := AddGear( Effect_History , Nil );
	AnimOb^.G := EFFECT_Event_Order;
	AnimOb^.S := GS_AOPoint;

	AnimOb^.Stat[ STAT_AOPoint_X ] := X;
	AnimOb^.Stat[ STAT_AOPoint_Y ] := Y;
	if ( AnimSeq < 1 ) or ( AnimSeq > Num_AnimSeq ) then AnimSeq := 1;
	AnimOb^.Stat[ STAT_AOPoint_AnimSeq ] := AnimSeq;

	SetSAtt( AnimOb^.SA , 'CAPTION <' + Caption + '>' );
end;

Function Process_AttackRoll( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ ORIGINATOR will attempt an attack roll against TARGET. Roll the dice; }
	{ return TRUE if Originator wins, or FALSE if Target does. }
	Function d100: Integer;
		{ Roll a d100, but if you get above 95 roll it again. }
	var
		d,t: Integer;
	begin
		t := 0;
		repeat
			d := Random( 100 ) + 1;
			t := t + d;
		until ( d <= 95 ) or ( t > 1000 );
		d100 := t;
	end;
var
	AtRoll,DefSkill,DefRoll,Luck: Integer;
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) then exit( False );

	if Originator <> Nil then begin
		AtRoll := d100 + ComScore( Originator , FX^.Stat[ STAT_AttackRoll_AtSkill ] ) + StatBonus( Originator , FX^.Stat[ STAT_AttackRoll_AtStat ] ) + FX^.Stat[ STAT_AttackRoll_AtBonus ];
	end else begin
		AtRoll := d100 + NAttValue( GB^.Scene^.NA , NAG_StoryData , NAS_DifficultyLevel ) * 5 + FX^.Stat[ STAT_AttackRoll_AtBonus ];
	end;

	if FX^.Stat[ STAT_AttackRoll_DefSkill ] = CS_PhysicalDefense then begin
		DefSkill := BestPhysicalDefense( Target );
	end else begin
		DefSkill := ComScore( Target , FX^.Stat[ STAT_AttackRoll_DefSkill ] );
	end;

	DefRoll := 51 + DefSkill + StatBonus( Target , FX^.Stat[ STAT_AttackRoll_DefStat ] );

	{ If the attack hit, we may still be able to avoid it with a luck save. }
	if DefRoll < AtRoll then begin
		Luck := Random( 100 ) + NAttValue( Target^.NA , NAG_Damage , NAS_LuckDmg );
		if Luck < CStat( Target , STAT_Luck ) then begin
			AddNAtt( Target^.NA , NAG_Damage , NAS_LuckDmg , 1 );
			AtRoll := -100;
		end;
	end;

	Process_AttackRoll := AtRoll > DefRoll;
end;

Function RollDamage( N, D: Integer ): Integer;
	{ Roll N dice of size D and add up the results. }
var
	Total: Integer;
begin
	Total := 0;
	if N < 1 then N := 1;
	if D < 2 then D := 2;
	while N > 0 do begin
		Total := Total + Random( D ) + 1;
		Dec( N );
	end;
	RollDamage := Total;
end;

Function Process_Healing( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ Let's restore some of the damage the above procedure dished out. }
	{ Or, alternatively, restore some magic points... it's all the same }
	{ to this wacky procedure. }
	{ Return TRUE if the target is alive afterwards, or FALSE if it's dead. }
var
	Dmg: Integer;
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) then begin
		SetSAtt( FX^.SA , 'CAPTION <>' );
		exit( False );
	end;

	{ Healing only affects models which are alright. It cannot affect }
	{ those which are fainted or dead. }
	{ The type of damage to be restored is stored in FX^.V. }
	if IsAlright( Target ) then begin
		{ See how many HP to restore. }
		Dmg := RollDamage( FX^.Stat[ STAT_Healing_DieN ], FX^.Stat[ STAT_Healing_DieD ] );
		if ( FX^.Stat[ STAT_Healing_BonusStat ] > 0 ) and ( FX^.Stat[ STAT_Healing_BonusStat ] <= Num_Model_Stats ) and ( Originator <> Nil ) then Dmg := Dmg + DamageBonus( Originator , FX^.Stat[ STAT_Healing_BonusStat ] );
		if Dmg < 1 then Dmg := 1;

		{ Can't heal more damage than the target has taken. }
		if NAttValue( Target^.NA , NAG_Damage , FX^.V ) < Dmg then Dmg := NAttValue( Target^.NA , NAG_Damage , FX^.V );

		if Dmg > 0 then begin
			SetSAtt( FX^.SA , 'CAPTION <' + BStr( Dmg ) + '>' );
			AddNAtt( Target^.NA , NAG_Damage , FX^.V , -DMG );
		end;
	end;

	Process_Healing := IsAlright( Target );
end;

Function Process_HPDamage( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ The model is about to take some physical damage... probably. Return }
	{ TRUE if the target is still alive afterwards, or FALSE if it's dead. }
var
	Dmg,Resist: Integer;
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) then begin
		SetSAtt( FX^.SA , 'CAPTION <>' );
		exit( False );
	end;

	{ Any model targeted by a damaging effect becomes active. }
	if ( Target^.G = GG_Model ) and ( Target^.V = GV_Inactive ) then ActivateModel( GB , Target );

	{ A targeted model also loses Hiding. }
	SetNAtt( Target^.NA , NAG_FightingStat , NAS_Hidden , 0 );

	{ The attack hit. Time to apply damage. }
	Dmg := RollDamage( FX^.Stat[ STAT_HPDamage_DieN ], FX^.Stat[ STAT_HPDamage_DieD ] ) + FX^.Stat[ STAT_HPDamage_DmgBonus ];
	if ( FX^.Stat[ STAT_HPDamage_DmgStat ] > 0 ) and ( FX^.Stat[ STAT_HPDamage_DmgStat ] <= Num_Model_Stats ) and ( Originator <> Nil ) then Dmg := Dmg + DamageBonus( Originator , FX^.Stat[ STAT_HPDamage_DmgStat ] );
	if Dmg < 1 then Dmg := 1;

	{ Modify damage by the model's resistances. }
	if ( FX^.Stat[ STAT_HPDamage_Element ] >= 0 ) and ( FX^.Stat[ STAT_HPDamage_Element ] < Num_Elements ) then begin
		Resist := ComScore( Target , Resist_X_ComScore[ FX^.Stat[ STAT_HPDamage_Element ] ] );
		if Resist <> 0 then Dmg := Dmg * ( 100 - Resist ) div 100;
		if Dmg < 1 then begin
			if Resist < 150 then Dmg := 1
			else Dmg := 0;
		end;
	end;

	SetSAtt( FX^.SA , 'CAPTION <' + BStr( Dmg ) + '>' );
	Add_HP_Damage( Target , DMG );

	{ If this model can mitose, check for that now. }
	if ( NAttValue( Target^.NA , NAG_MonsterData , NAS_CanMitose ) <> 0 ) and ( ( FX^.Stat[ STAT_HPDamage_Element ] = ELEMENT_Slashing ) or ( FX^.Stat[ STAT_HPDamage_Element ] = ELEMENT_Piercing ) ) then begin
		SetNAtt( Target^.NA , NAG_FightingStat , NAS_ShouldMitose , 1 );
	end;

	Process_HPDamage := IsAlright( Target );
end;

Function Process_Summon( GB: GameBoardPtr; FX,Originator,Target: GearPtr; X,Y: Integer ): Boolean;
	{ This effect will attempt to summon some monsters which will fight on }
	{ the same side as the originator. }
const
	Num_Summon_Slots = 6;
var
	T,N: Integer;
	Summon_Options: Array [1..Num_Summon_Slots] of String;
	msg: String;
	mon: GearPtr;
begin
	{ Check for a summoning spot. }


	{ Determine what options we have. }
	N := 0;
	for T := 1 to Num_Summon_Slots do begin
		msg := SAttValue( FX^.SA , SATT_Summon_Option + BStr( T ) );
		if msg <> '' then begin
			Summon_Options[ N + 1 ] := msg;
			Inc( N );
		end;
	end;

	{ If we have options at all, pick one of them at random. }
	if N > 0 then begin
		N := Random( N ) + 1;
		mon := LoadNewMonster( Summon_Options[ N ] );
		if mon <> Nil then begin
			Mon^.V := GV_Active;
			if ( Originator = Nil ) then Mon^.S := GS_EnemyTeam
			else if ( Originator^.S = GS_PCTeam ) then Mon^.S := GS_AllyTeam
			else Mon^.S := Originator^.S;

			SetNAtt( Mon^.NA , NAG_Location , NAS_X , X );
			SetNAtt( Mon^.NA , NAG_Location , NAS_Y , Y );
			SetNAtt( Mon^.NA , NAG_FightingStat , NAS_Temporary , 1 );
			AppendGear( GB^.Contents , Mon );

			Process_Summon := True;
		end else begin
			SetSAtt( FX^.SA , 'CAPTION <ERROR: ' + Summon_Options[ N ] + '/' + BStr( N ) + '>' );
			Process_Summon := False;
		end;
	end else begin
		Process_Summon := False;
	end;
end;

Function Process_Enchant( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ Let's stick an enchantment gear on the target. }
	{ This function always returns TRUE, even if the target doesn't exist. }
var
	E: GearPtr;
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	{ The FX also needs an enchantment as its invcom. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) or ( FX^.InvCom = Nil ) or ( FX^.InvCom^.G <> GG_Enchantment ) then exit( True );

	{ Check to make sure there's no enchantment by this name currently }
	{ attached to the target. If there is, dispose of it to make room }
	{ for this new one. }
	E := SeekSibByName( Target^.SubCom , GearName( FX^.InvCom ) );
	if E <> Nil then RemoveGear( Target^.SubCom , E );

	{ Clone the attachment and slap it on. }
	E := CloneGear( FX^.InvCom );
	InsertSubCom( Target , E );

	Process_Enchant := True;
end;

Function Process_IfTarget( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ Not really an effect, this here is a control structure. }
	{ Returns TRUE if Target meets the criteria, or FALSE otherwise. }
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) then exit( False );

	if FX^.V = GV_IfTarget_IsAlly then Process_IfTarget := ( Originator <> Nil ) and not AreEnemies( Originator , Target )
	else if FX^.V = GV_IfTarget_NAttGreater then Process_IfTarget := NAttValue( Target^.NA , FX^.Stat[ STAT_IfTarget_NAtt_G ] , FX^.Stat[ STAT_IfTarget_NAtt_S ] ) > FX^.Stat[ STAT_IfTarget_NAtt_V ]
	else if FX^.V = GV_IfTarget_NAttNotEqual then Process_IfTarget := NAttValue( Target^.NA , FX^.Stat[ STAT_IfTarget_NAtt_G ] , FX^.Stat[ STAT_IfTarget_NAtt_S ] ) <> FX^.Stat[ STAT_IfTarget_NAtt_V ]
	else if FX^.V = GV_IfTarget_IsAnimal then Process_IfTarget := ModelIsAnimal( Target )
	else if FX^.V = GV_IfTarget_IsLiving then Process_IfTarget := ModelIsLiving( Target )
	else if FX^.V = GV_IfTarget_IsUnholy then Process_IfTarget := ModelIsUnholy( Target )
	else Process_IfTarget := ( Originator <> Nil ) and AreEnemies( Originator , Target );
end;

Function Process_LoseATurn( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ The target is being hit with a LoseATurn effect. }
var
	N: Integer;
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) then exit( False );

	N := FX^.Stat[ STAT_LoseATurn_Duration ];
	if N < 1 then N := 1;

	SetNAtt( Target^.NA , NAG_FightingStat , NAS_LoseATurn , Random( N ) + 1 );

	Process_LoseATurn := True;
end;

Function Process_ChangeNAtt( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ The target is being hit with a LoseATurn effect. }
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) then exit( True );

	if FX^.V = GV_SetNAtt then SetNAtt( Target^.NA , FX^.Stat[ STAT_ChangeNAtt_G ] , FX^.Stat[ STAT_ChangeNAtt_S ] , FX^.Stat[ STAT_ChangeNAtt_V ] )
	else if FX^.V = GV_AddNAtt then AddNAtt( Target^.NA , FX^.Stat[ STAT_ChangeNAtt_G ] , FX^.Stat[ STAT_ChangeNAtt_S ] , FX^.Stat[ STAT_ChangeNAtt_V ] );

	Process_ChangeNAtt := True;
end;

Function Process_Confusion( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ The target is being hit with a Confusion effect. }
var
	N: Integer;
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type with a brain. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) or not ModelHasBrain( Target ) then exit( False );

	N := FX^.Stat[ STAT_Confusion_Duration ];
	if N < 1 then N := 1;

	SetNAtt( Target^.NA , NAG_FightingStat , NAS_Confused , Random( N ) + 1 );

	Process_Confusion := True;
end;

Function Process_Kill( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ Do enough damage to the target to kill it. Return TRUE if everything }
	{ goes well, or FALSE if the target doesn't exist. }
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) then exit( False );

	{ Any model targeted by a damaging effect becomes active. }
	{ Yes, this is even done for an instakill... the monster's buddies have }
	{ to be informed, you see. }
	if ( Target^.V = GV_Inactive ) then ActivateModel( GB , Target );

	{ Give the model enough damage to ensure that it's dead... and then some. }
	Add_HP_Damage( Target , MaxHP( Target ) + 50 );

	Process_Kill := True;
end;

Function Process_PercentRoll( GB: GameBoardPtr; FX,Originator,Target: GearPtr ): Boolean;
	{ Roll d100 against one of the originator's ComScores. }
var
	OLvl,TLvl,TarNum: Integer;
begin
	{ Error check- this procedure needs a model, and it must be a GG_Model type. }
	{  It also needs an originator. }
	if ( Target = Nil ) or ( Target^.G <> GG_Model ) or ( Originator = Nil ) then exit( False );

	TarNum := ComScore( Originator , FX^.Stat[ STAT_PercentRoll_Skill ] ) + StatBonus( Originator , FX^.Stat[ STAT_PercentRoll_Stat ] ) + FX^.Stat[ STAT_PercentRoll_Bonus ];

	OLvl := TotalLevel( Originator );
	TLvl := TotalLevel( Target );
	if TLvl > OLvl then TarNum := TarNum - ( TLvl - OLvl ) * 4;

	Process_PercentRoll := Random( 100 ) < TarNum;
end;


Procedure HandleEffectList( GB: GameBoardPtr; FXList,Originator,Target: GearPtr; X,Y: Integer );
	{ Go through the list and apply each effect to the model. For any effect }
	{ which returns TRUE, also apply the subcom effect list. On the other }
	{ hand, if it returns FALSE apply the invcom effect list. Recursive }
	{ programming is fun and harldy ever leads to heartbreak or violence. }
var
	Success: Boolean;
begin
	while FXList <> Nil do begin
		{ We have an effect to apply. Consult the proper functions, }
		{ then recurse as necessary. }
		case FXList^.S of
			GS_AttackRoll:	Success := Process_AttackRoll( GB , FXList , Originator , Target );
			GS_HPDamage:	Success := Process_HPDamage( GB , FXList , Originator , Target );
			GS_Healing:	Success := Process_Healing( GB , FXList , Originator , Target );
			GS_Enchant:	Success := Process_Enchant( GB , FXList , Originator , Target );
			GS_IfTarget:	Success := Process_IfTarget( GB , FXList , Originator , Target );
			GS_LoseATurn:	Success := Process_LoseATurn( GB , FXList , Originator , Target );
			GS_Summon:	Success := Process_Summon( GB , FXList , Originator , Target , X , Y );
			GS_ChangeNAtt:	Success := Process_ChangeNAtt( GB , FXList , Originator , Target );
			GS_Confusion:	Success := Process_Confusion( GB , FXList , Originator , Target );
			GS_Kill:	Success := Process_Kill( GB , FXList , Originator , Target );
			GS_PercentRoll:	Success := Process_PercentRoll( GB , FXList , Originator , Target );
		else Success := True;
		end;

		{ Store the point animation of this effect, if needed. }
		if NAttValue( FXList^.NA , NAG_EffectData , NAS_AnimSequence ) <> 0 then begin
			Add_Point_Animation( X , Y , NAttValue( FXList^.NA , NAG_EffectData , NAS_AnimSequence ) , SAttValue( FXList^.SA , 'CAPTION' ) );
			Inc( effect_event_order );
		end;

		{ Activate the subcoms or the invcoms, depending on success. }
		if Success then HandleEffectList( GB, FXList^.SubCom, Originator, Target, X, Y )
		else HandleEffectList( GB, FXList^.InvCom, Originator, Target, X, Y );

		FXList := FXList^.Next;
	end;
end;

Procedure ApplyEffectToTile( GB: GameBoardPtr; FXTree, Originator: GearPtr; X, Y: Integer );
	{ The provided FX tree is going to be applied to tile X,Y. }
var
	M: GearPtr;
	EO0: Integer;
begin
	{ Locate the model in this tile. We're assuming that there's only one. }
	M := FindModelXY( GB , X , Y );

	EO0 := effect_event_order;
	HandleEffectList( GB, FXTree, Originator, M, X, Y );

	effect_event_order := EO0;
end;

Procedure HandleEffect( GB: GameBoardPtr; FX,Originator: GearPtr; FX_Stencil: effect_stencil );
	{ PARAMETERS: }
	{   FX is a tree of effects that will be processed in strict order. }
	{   Originator is the model that caused this effect. It may be nil. }
	{     It's needed for calculating hit scores, damage bonuses, and that kind }
	{     of thing. }
	{     If Originator is Nil, the difficulty rating of GB^.Scene will be used }
	{     to determine the skill ranks, etc, of the attack. }
	{   FX_Stencil indicates every tile which is to be affected by the effect. }
	{     Every model standing in the tile gets affected; some effects also }
	{     do things to terrain even if there's no model there. }
	{ PRE-CONDITIONS: }
	{   effect_history has been cleared and effect_event_order initialized. }
	{ WHAT THIS PROCEDURE DOES: }
	{   Invoke the effects, including skill rolls, defenses, and handing out damage. }
	{   Add animation details to event_history. }
	{   Recurse to handle reactive effects. }
	{   Add triggers for models hit or disabled by effects. }
	{   Activate enemy models targeted by effects. }
	{ WHAT THIS PROCEDURE DOESN'T DO: }
	{   Add the initial shot animation. }
	{   Screen output. }
	{   Move disabled models off the map. }
var
	X,Y,EO0: Integer;
begin
	{ Check through every tile in the FX_Stencil, looking for the affected tiles. }
	for X := 1 to GB^.Map_Width do begin
		for Y := 1 to GB^.Map_Height do begin
			if FX_Stencil[ X , Y ] then begin
				{ Process the effects in a set order. }
				{ If an effect returns FALSE, no further effects will affect }
				{ this tile. }
				EO0 := effect_event_order;

				ApplyEffectToTile( GB, FX, Originator, X, Y );

				effect_event_order := EO0;
			end;
		end;
	end;
end;

Procedure Add_Initial_Shot( ShotSeq,X0,Y0,X1,Y1: Integer );
	{ This procedure will add an initial shot animation. }
	{ I is the invocation- this should have a shot sequence defined. }
begin
	if effect_history <> Nil then DisposeGear( effect_history );
	effect_event_order := 0;

	Add_Shot_Precisely( X0 , Y0 , X1 , Y1 , ShotSeq , '' );
	Inc( effect_event_order );
end;


Function CreateAttackEffect( PC,Weapon: GearPtr ): GearPtr;
	{ Create an effect tree detailing this PC's physical attack. }
	Procedure AddAttackModifier( FX , ModProto: GearPtr );
		{ Add a modifer to this attack. The big question is, where to }
		{ stick it? Fortunately we have an attribute for that. }
		{ FX should be the attack roll, and FX^.SubCom should be the }
		{ damage roll. That's all we need. }
	var
		ModFX: GearPtr;
	begin
		{ Clone the Modifier Effect. }
		ModFX := CloneGear( ModProto );

		{ ...and you know where to stick it. }
		case NAttValue( ModFX^.NA , NAG_EffectData , NAS_AttackModSlot ) of
			NAV_AMS_OnHit:		InsertSubCom( FX , ModFX );
			NAV_AMS_OnMiss:		InsertInvCom( FX , ModFX );
			NAV_AMS_OnDamage:	InsertSubCom( FX^.SubCom , ModFX );
			NAV_AMS_OnKill:		InsertInvCom( FX^.SubCom , ModFX );
			else AppendGear( FX , ModFX );
		end;
	end;
const
	Kung_Fu_Dmg: Array [0..20,1..4] of Byte = (
		( 1, 2, 0, 0 ),
		( 1, 2, 0, 0 ),( 1, 3, 0, 0 ),( 1, 4, 0, 0 ),( 1, 6, 0, 0 ),( 1, 8, 0, 0 ),
		( 1, 8, 0, 0 ),( 1,10, 0, 0 ),( 1,10, 0, 0 ),( 2, 6, 0, 0 ),( 2, 6, 0, 0 ),
		( 2, 6, 1, 2 ),( 2, 6, 1, 3 ),( 2, 8, 1, 4 ),( 2, 8, 1, 5 ),( 2, 8, 1, 6 ),
		( 2, 9, 1, 6 ),( 2, 9, 1, 7 ),( 2,10, 1, 7 ),( 2,10, 1, 8 ),( 2,10, 1,10 )
	);
var
	FX,Dmg,E: GearPtr;
	KungFu,CritHit: Integer;
begin
	fx := LoadNewSTC( 'DEFAULT_ATTACK_TEMPLATE' );
	Dmg := FX^.SubCom;
	if Weapon <> Nil then begin
		Dmg^.Stat[ STAT_HPDamage_DieN ] := Weapon^.Stat[ STAT_WeaponN ];
		Dmg^.Stat[ STAT_HPDamage_DieD ] := Weapon^.Stat[ STAT_WeaponD ];
		Dmg^.Stat[ STAT_HPDamage_Element ] := Weapon^.Stat[ STAT_WeaponElement ];

		{ Because of two-handed weapons etc, don't use the standard }
		{ stat-based damage bonus functionality. Instead, calculate }
		{ the damage bonus here. }
		if ( Weapon^.S <> GS_Bow ) and ( Weapon^.S <> GS_Sling ) then begin
			Dmg^.Stat[ STAT_HPDamage_DmgBonus ] := DamageBonus( PC , STAT_Strength );
			{ Two-handed weapons get 50% bonus to damage bonus. }
			if Weapon^.Stat[ STAT_WeaponHands ] <> 0 then Dmg^.Stat[ STAT_HPDamage_DmgBonus ] := ( Dmg^.Stat[ STAT_HPDamage_DmgBonus ] * 3 ) div 2;
		end else begin
			{ Bows and Slings use Reflexes to attack. }
			FX^.Stat[ STAT_AttackRoll_AtStat ] := STAT_Reflexes;
		end;

		{ If this weapon has a magic effect, add that now. }
		{ Maybe it has multiple special effects... better check all subcoms }
		{ just to be sure. }
		E := Weapon^.SubCom;
		while E <> Nil do begin
			if ( E^.G = GG_Effect ) then begin
				AddAttackModifier( FX , E );
			end;
			E := E^.Next;
		end;
	end else begin
		{ Start with the basic stuff. }
		Dmg^.Stat[ STAT_HPDamage_DmgBonus ] := DamageBonus( PC , STAT_Strength );
		Dmg^.Stat[ STAT_HPDamage_Element ] := ELEMENT_Crushing;

		{ Unarmed damage depends on Kung Fu. }
		KungFu := ComScore( PC , CS_KungFu ) div 5;
		if KungFu > 20 then begin
			Dmg^.Stat[ STAT_HPDamage_DmgBonus ] := Dmg^.Stat[ STAT_HPDamage_DmgBonus ] + KungFu - 20;
			KungFu := 20;
		end;

		Dmg^.Stat[ STAT_HPDamage_DieN ] := Kung_Fu_Dmg[ KungFu , 1 ];
		Dmg^.Stat[ STAT_HPDamage_DieD ] := Kung_Fu_Dmg[ KungFu , 2 ];

		{ High level Kung Fu gets some solar damage as well. }
		if Kung_Fu_Dmg[ KungFu , 3 ] > 0 then begin
			E := LoadNewSTC( 'KUNGFU_BONUS_DAMAGE' );
			E^.Stat[ STAT_HPDamage_DieN ] := Kung_Fu_Dmg[ KungFu , 3 ];
			E^.Stat[ STAT_HPDamage_DieD ] := Kung_Fu_Dmg[ KungFu , 4 ];
			InsertSubCom( Dmg , E );
		end;
	end;

	{ If the PC is hidden, get +20% to attack and triple damage. }
	if NAttValue( PC^.NA , NAG_FightingStat , NAS_Hidden ) > 0 then begin
		FX^.Stat[ STAT_AttackRoll_AtBonus ] := FX^.Stat[ STAT_AttackRoll_AtBonus ] + 20;
		Dmg^.Stat[ STAT_HPDamage_DieN ] := Dmg^.Stat[ STAT_HPDamage_DieN ] * 3;
	end;

	{ Add the Critical Hit skill, if known. }
	CritHit := ComScore( PC , CS_CriticalHit );
	if CritHit > 0 then begin
		E := LoadNewSTC( 'CRITICAL_HIT_SKILL' );
		InsertSubCom( Dmg , E );
	end;

	{ Also add any enchantment bonuses the PC may have. }
	E := PC^.SubCom;
	while E <> Nil do begin
		if ( E^.G = GG_Enchantment ) and ( E^.S = GS_ENCHANT_AttackMod ) and ( E^.SubCom <> Nil ) and ( E^.SubCom^.G = GG_Effect ) then begin
			{ This is an attack modifier enchantment. Add the effect. }
			AddAttackModifier( FX , E^.SubCom );
		end;
		E := E^.Next;
	end;

	CreateAttackEffect := FX;
end;

initialization
	effect_history := Nil;

finalization
	if effect_history <> Nil then DisposeGear( effect_history );

end.
