unit grabgear;
	{ This unit has one purpose: To seek gears and stick them in }
	{ ArenaScript's grabbed_gear global variable. }
{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses gears,gamebook;


Function GG_LocateModel( GB: GameBoardPtr; ID: LongInt ): GearPtr;

Function Attempt_Gear_Grab( GB: GameBoardPtr; var Cmd,Event: String; Source: GearPtr ): Boolean;


implementation

uses joescript;


Function GG_LocateModel( GB: GameBoardPtr; ID: LongInt ): GearPtr;
	{ Attempt to locate a model with the provided ID. }
var
	it: GearPtr;
	GB2Search: GameBoardPtr;
begin
	{ Begin with an error check. }
	if ID = 0 then Exit( Nil );

	{ Start by checking to see whether or not it's in the adventure source. }
	it := SeekGearByIDTag( GB^.Camp^.Source , NAG_StoryData , NAS_UniqueID , ID );
	if it = nil then begin
		{ Not in source... start checking the gameboards. }
		GB2Search := GB^.Camp^.Boards;
		while ( GB2Search <> Nil ) and ( it = Nil ) do begin
			it := SeekGearByIDTag( GB2Search^.Contents , NAG_StoryData , NAS_UniqueID , ID );
			GB2Search := GB2Search^.Next;
		end;
	end;
	GG_LocateModel := it;
end;

Function Attempt_Gear_Grab( GB: GameBoardPtr; var Cmd,Event: String; Source: GearPtr ): Boolean;
	{ See whether or not CMD refers to a valid Gear-Grabbing command. }
	{ If CMD is not a gear-grabbing command, the only changes made to }
	{ anything will be that CMD is now uppercase. }
	{ Return TRUE if a gear was grabbed, FALSE otherwise. }
var
	it: Boolean;
	X: LongInt;
begin
	CMD := UpCase( CMD );

	{ Assume this is a gear-grabbing command, for now. }
	it := True;

	if CMD = 'GRABSOURCE' then begin
		Grabbed_Gear := Source;

	end else if CMD = 'GRABMODEL' then begin
		X := ScriptValue( GB , Event , Source );
		Grabbed_Gear := GG_LocateModel( GB , X );

	end else if CMD = 'GRABPC' then begin
		Grabbed_Gear := FirstActivePC( GB );

	end else if CMD = 'GRABADVENTURE' then begin
		Grabbed_Gear := GB^.Camp^.Source;

	end else if CMD = 'GRABSCENE' then begin
		Grabbed_Gear := GB^.Scene;

	end else begin
		{ No command was found matching CMD... return False. }
		it := False;
	end;

	Attempt_Gear_Grab := it;
end;

end.
