%%
%%  *RESOLUTION Content
%%
%%  If the FINAL encounter has been reached, end the story with a victory
%%  message. Otherwise pass the torch to a new EXPOSITION subplot.
%%
%%  Param1: The city for this chapter
%%  Param2: The dungeon for the conclusion
%%

Content
	name <Hurry! Stop Invasion>
	requires <*RESOLUTION P:WAR__ -FINAL>
	SingleUse

	% E1 is the city for this chapter
	% E2 is the dungeon for the conclusion
	% E3 is the wilderness road
	% E4 is the next city
	% E5 is the entrance to E3
	% E6 is the way back to E1
	Element3 <NewScene Wilderness 1 1    0>
	Element4 <NewScene City 3>
	Element5 <Prefab 1>
	Element6 <Prefab 3>

	% G%id%01 = Have opened pathway

	.%id%_GoInit <if= G%id%01 0 G= %id%01 1 Print %id%01 History %id%02>
	Msg%id%01 <Your enemy's next target is likely to be Twilingate. That's where you decide to head next.>
	Msg%id%02 <You decide to go protect Twilingate.>

	SubPlot1 <*COMPLICATION 4>

	inv
		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_East
		inv
			STC RoadSign
			Destination 3
			mm_desc <To Twilingate>
			Frame 0
			Use <if= G%id%01 0 else GoUse Print 1>
			GoUse <Exit Destination>
			Msg1 <You don't have time to go to Twilingate; you've got enough problems to deal with right here.>

			EntryPoint
			Destination 3
		end

		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_West
		inv
			STC RoadSign
			Destination 1
			Frame 2

			EntryPoint
			Destination 1
		end
	end

Content
	name <Road Blocked By Ice>
	requires <*RESOLUTION P:ALTWE C:SNOWY -FINAL>
	SingleUse
	C:FORES

	% E1 is the city for this chapter
	% E2 is the dungeon for the conclusion
	% E3 is the wilderness road
	% E4 is the next city
	% E5 is the entrance to E3
	% E6 is the way back to E1
	Element3 <NewScene Wilderness 1 1    0>
	Element4 <NewScene City 3>
	Element5 <Prefab 1>
	Element6 <Prefab 3>

	% G%id%01 = Have opened pathway

	.%id%_GoInit <if= G%id%01 0 G= %id%01 1 Print %id%01 History %id%02>
	Msg%id%01 <With that, the weather begins to warm up a little.>
	Msg%id%02 <The weather began to warm up, a little.>

	SubPlot1 <*COMPLICATION 4>

	inv
		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_South
		inv
			STC RoadSign
			mm_desc <Frozen Pass>
			Destination 3
			Frame 0
			Use <if= G%id%01 1 else GoNotYet if= V1 0 else GoUse Print 2 V= 1 1 Exit Destination>
			GoNotYet <Print 1>
			GoUse <Exit Destination>
			Msg1 <The road ahead is blocked by ice. There's no way through.>
			Msg2 <The ice has receded, allowing you to pass.>

			EntryPoint
			Destination 3
		end

		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_North
		inv
			STC RoadSign
			Destination 1
			Frame 2

			EntryPoint
			Destination 1
		end
	end


Content
	name <Boring Next City, Mk 1>
	%% Today's village is named for the author of a children's book we
	%% picked up for Sean today- Mary Morgan.
	requires <*RESOLUTION -FINAL>
	SingleUse

	% E1 is the city for this chapter
	% E2 is the dungeon for the conclusion
	% E3 is the wilderness road
	% E4 is the next city
	% E5 is the entrance to E3
	% E6 is the way back to E1
	Element3 <NewScene Wilderness 1 1    0>
	Element4 <NewScene City 3>
	Element5 <Prefab 1>
	Element6 <Prefab 3>

	% G%id%01 = Have opened pathway

	.%id%_GoInit <if= G%id%01 0 G= %id%01 1 Print %id%01 History %id%02>
	Msg%id%01 <With everything settled here, you should probably go visit the town of Morganton to see what's happening there.>
	Msg%id%02 <You decide to go to Morganton next.>

	SubPlot1 <*COMPLICATION 4>

	inv
		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_East
		inv
			STC RoadSign
			Destination 3
			mm_desc <To Morganton>
			Frame 0
			Use <if= G%id%01 0 else GoUse Print 1>
			GoUse <Exit Destination>
			Msg1 <The road to Morganton is quite dangerous. You have no reason to go there just yet.>

			EntryPoint
			Destination 3
		end

		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_West
		inv
			STC RoadSign
			Destination 1
			Frame 2

			EntryPoint
			Destination 1
		end
	end

Content
	name <Boring Next City, Mk 2>
	requires <*RESOLUTION -FINAL>
	SingleUse

	% E1 is the city for this chapter
	% E2 is the dungeon for the conclusion
	% E3 is the wilderness road
	% E4 is the next city
	% E5 is the entrance to E3
	% E6 is the way back to E1
	Element3 <NewScene Wilderness 1 1    0>
	Element4 <NewScene City 3>
	Element5 <Prefab 1>
	Element6 <Prefab 3>

	% G%id%01 = Have opened pathway

	.%id%_GoInit <if= G%id%01 0 G= %id%01 1 Print %id%01 History %id%02>
	Msg%id%01 <With everything settled here, you should probably go visit the town of Burgeo to see what's happening there.>
	Msg%id%02 <You decide to go to Burgeo next.>

	SubPlot1 <*COMPLICATION 4>

	inv
		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_West
		inv
			STC RoadSign
			Destination 3
			mm_desc <To Burgeo>
			Frame 0
			Use <if= G%id%01 0 else GoUse Print 1>
			GoUse <Exit Destination>
			Msg1 <The road to Burgeo is quite dangerous. You have no reason to go there just yet.>

			EntryPoint
			Destination 3
		end

		MapFeature
		MFWidth 8
		MFHeight 8
		MFAnchor_East
		inv
			STC RoadSign
			Destination 1
			Frame 2

			EntryPoint
			Destination 1
		end
	end


Content
	name <The Demo Resolution>
	requires <*RESOLUTION FINAL>

	% E1 is the city for this chapter
	% E2 is the dungeon for the conclusion

	.%id%_GoInit <if= v%id%01 0 v= %id%01 1 Print %id%01>
	Msg%id%01 <Congratulations, you have finished the game! I had loads of fun trying to kill you off, but apparently didn't succeed. Let me know what you think.>


