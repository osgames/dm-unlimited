unit avatar;
{
	The avatar generator. Given a PC record, generate an avatar for it.
}

{
	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}


interface

uses sdlgfx,gears,gamebook;

const
	Num_Avatar_Layers = 3;
	AL_Base = 1;
	AL_Hair = 2;
	AL_Beard = 3;

	Num_Hair_Styles = 24;
	Num_Beard_Styles = 9;

	Num_Species_Skins: Array [1..Num_Species] of integer = (
		3, 3, 3, 3, 3,
		3, 3, 6, 3
	);

Function GenerateAvatar( PC: GearPtr ): SensibleSpritePtr;

implementation

uses sdl;

var
	avatar_base,avatar_hair,avatar_beard,avatar_head,
	avatar_boot,avatar_arm,avatar_cloak,avatar_hand1,
	avatar_hand2,avatar_body,avatar_legs: SensibleSpritePtr;


Function AvatarBackend( PC: GearPtr ): SensibleSpritePtr;
	{ This is the honest-to-goodness avatar renderer behind the curtain. }
	{ The avatar layer sprites must currently be in memory. }
const
	Species_Body_Type: Array [1..Num_Species] of integer = (
		0, 0, 6, 0, 12,
		0, 18, 24, 36
	);
var
	it: SensibleSpritePtr;
	Species,Frame: Integer;
	MyDest: TSDL_Rect;
	item: GearPtr;
	IsMale: Boolean;
begin
	{ Create a blank sprite. }
	it := CreateBlankSprite( 54, 54, 54, 54 );
	MyDest.X := 0;
	MyDest.Y := 0;

	IsMale := NAttValue( PC^.NA , NAG_CharacterData , NAS_Gender ) = NAV_Male;

	{ Apply the layers to this sprite. }

	{ Start with the cloak. }
	Item := FindEquippedItem( PC , NAV_Back );
	if Item <> Nil then begin
		Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		if Frame >= 0 then DrawSprite( avatar_cloak , it^.Img , MyDest , Frame );
	end;

	{ Find the body- this will depend on species, gender, and skin color. }
	Species := NAttValue( PC^.NA , NAG_CharacterData , NAS_Species );
	if ( Species < 1 ) or ( Species > Num_Species ) then Species := 1;
	Frame := Species_Body_Type[ Species ] + NAttValue( PC^.NA , NAG_Appearance , NAS_Avatar_Skin );
	if NAttValue( PC^.NA , NAG_CharacterData , NAS_Gender ) = NAV_Male then Frame := Frame + Num_Species_Skins[ Species ];
	DrawSprite( avatar_base , it^.Img , MyDest , Frame );

	{ Add the shoes. }
	Item := FindEquippedItem( PC , NAV_Feet );
	if Item <> Nil then begin
		Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		if Frame >= 0 then DrawSprite( avatar_boot , it^.Img , MyDest , Frame );
	end;


	{ Add the clothing. }
	Item := FindEquippedItem( PC , NAV_Body );
	if Item <> Nil then begin
		{ This clothing might require pants. }
		if isMale then begin
			Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_MalePants );
			if Frame = 0 then Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarPants );
		end else Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarPants );
		if ( Frame >= 0 ) and ( Species <> SPECIES_Centaur ) then DrawSprite( avatar_legs , it^.Img , MyDest , Frame );

		if isMale then begin
			Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_MaleBody );
			if Frame = 0 then Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		end else Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		if Frame >= 0 then DrawSprite( avatar_body , it^.Img , MyDest , Frame );
	end;

	{ Add the hair. }
	Frame := NAttValue( PC^.NA , NAG_Appearance , NAS_Avatar_Hair ) - 1;
	if Frame > 0 then DrawSprite( avatar_hair , it^.Img , MyDest , Frame );

	{ Add the beard. }
	Frame := NAttValue( PC^.NA , NAG_Appearance , NAS_Avatar_Beard ) - 1;
	if Frame > 0 then DrawSprite( avatar_beard , it^.Img , MyDest , Frame );

	{ Add the gloves. }
	Item := FindEquippedItem( PC , NAV_Arm );
	if Item <> Nil then begin
		Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		if Frame >= 0 then DrawSprite( avatar_arm , it^.Img , MyDest , Frame );
	end;

	{ Add the right hand item. }
	Item := FindEquippedItem( PC , NAV_Hand1 );
	if Item <> Nil then begin
		Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		if Frame >= 0 then DrawSprite( avatar_hand1 , it^.Img , MyDest , Frame );
	end;

	{ Add the left hand item. }
	Item := FindEquippedItem( PC , NAV_Hand2 );
	if Item <> Nil then begin
		Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		if Frame >= 0 then DrawSprite( avatar_hand2 , it^.Img , MyDest , Frame );
	end;

	{ Add the hat. }
	Item := FindEquippedItem( PC , NAV_Head );
	if Item <> Nil then begin
		Frame := NAttValue( Item^.NA , NAG_ItemData , NAS_AvatarFrame );
		if Frame >= 0 then DrawSprite( avatar_head , it^.Img , MyDest , Frame );
	end;

	AvatarBackend := it;
end;

Procedure ConfirmAvatarParts;
	{ Load the avatar parts if needed, and mark them as basically unimportant. }
begin
	avatar_base := LocateBUSprite( 'avatar_base.png' , 54 , 54 );
	avatar_hair := LocateBUSprite( 'avatar_hair.png' , 54 , 54 );
	avatar_beard := LocateBUSprite( 'avatar_beard.png' , 54 , 54 );
	avatar_hand1 := LocateBUSprite( 'avatar_hand1.png' , 54 , 54 );
	avatar_hand2 := LocateBUSprite( 'avatar_hand2.png' , 54 , 54 );
	avatar_body := LocateBUSprite( 'avatar_body.png' , 54 , 54 );
	avatar_legs := LocateBUSprite( 'avatar_legs.png' , 54 , 54 );
	avatar_head := LocateBUSprite( 'avatar_head.png' , 54 , 54 );
	avatar_boot := LocateBUSprite( 'avatar_boot.png' , 54 , 54 );
	avatar_arm := LocateBUSprite( 'avatar_arm.png' , 54 , 54 );
	avatar_cloak := LocateBUSprite( 'avatar_cloak.png' , 54 , 54 );
end;

Function GenerateAvatar( PC: GearPtr ): SensibleSpritePtr;
	{ We've got a PC to deal with. Create its avatar. }
begin
	ConfirmAvatarParts;
	GenerateAvatar := AvatarBackend( PC );
end;

end.
