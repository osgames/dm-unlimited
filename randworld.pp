unit randworld;
	{ This unit is the world generator. The entire campaign is generated }
	{ at startup. }
{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface


uses gamebook;

Function RandomCampaign( MaxLevel: Integer ): CampaignPtr;
Function DebugCampaign: CampaignPtr;


implementation

uses uiconfig,gears,texutil,gearparser,monsters,equipment,sdlgfx,sdlmenus;

const
	Num_Sub_Plots = 8;

var
	MaxUID,MaxSCID: LongInt;	{ Used for assigning UIDs, Story Content IDs. }
	DifficultyRating,MaxDifficultyRating: Integer;	{ Goes up any time a non-city scene is added. }
	Select_Components_Manually: Boolean;

	Story_Component_List: GearPtr;

type
	{ I feel just like Dmitri Mendelev writing this... }
	ElementTable = Array [1..Num_Plot_Elements] of Integer;
	{ Unlike GearHead, all gear types use the same identification system. }
	{ So, we don't have to store element types, just element identities. }


{  *********************************  }
{  ***   FORWARD  DECLARATIONS   ***  }
{  *********************************  }
Function AddSubPlot( ParentPlot: GearPtr; SPReq: String; const Params: ElementTable ): GearPtr;forward;


{  ************************************  }
{  ***   KINDA  SHY  DECLARATIONS   ***  }
{  ************************************  }

Function NewUID: Integer;
	{ Return a new unique ID number. }
begin
	Inc( MaxUID );
	NewUID := MaxUID;
end;

Function NewSCID: Integer;
	{ Return a new unique story content identifier. }
begin
	Inc( MaxSCID );
	NewSCID := MaxSCID;
end;

Function ElementID( Plot: GearPtr; N: Integer ): Integer;
	{ Return the ID for the Nth element of PLOT. }
begin
	ElementID := NAttValue( Plot^.NA , NAG_ElementID , N );
end;

Function ParamContext( Plot: GearPtr; const Params: ElementTable ): String;
	{ Return the context of this set of elements. }
var
	t: Integer;
	context: String;
	Part: GearPtr;
begin
	context := '';
	for t := 1 to Num_Plot_Elements do begin
		if Params[ T ] <> 0 then begin
			Part := SeekGearByIDTag( FindRoot( Plot ) , NAG_StoryData , NAS_UniqueID , Params[ T ] );
			if Part <> Nil then begin
				Context := Context + ' ' + GearContextTags( Part , BStr( t ) + ':' );
				if Part^.G = GG_MapFeature then begin
					Part := SeekGearByIDTag( FindRoot( Plot ) , NAG_StoryData , NAS_UniqueID , NAttValue( Part^.NA , NAG_StoryData , NAS_ToBePlacedHere ) );
					if Part <> Nil then Context := Context + ' ' + BStr( t ) + ':MAP_F ' + GearContextTags( Part , BStr( t ) + ':' );
				end;
			end;
		end;
	end;
	ParamContext := context;
end;

Procedure CopyPlotContext( Source, Dest: GearPtr );
	{ Copy over the context info from SOURCE to DEST. If DEST already has }
	{ a value for the given context, don't bother to copy. }
var
	T: Integer;
begin
	if Source <> Nil then begin
		for t := 1 to Num_Context_Descriptors do begin
			if NAttValue( Dest^.NA , NAG_StoryData , CDIndex[ t ] ) = 0 then begin
				SetNAtt( Dest^.NA , NAG_StoryData , CDIndex[ t ] , NAttValue( Source^.NA , NAG_StoryData , CDIndex[ t ] ) );
			end;
		end;
	end;
end;

Procedure RecordError( msg: String );
	{ An error has happened in the world-building process. }
begin
	writeln( msg );
end;

Procedure ClearElementTable( var Params: ElementTable );
	{ Fill an element table with zeroes. }
var
	N: Integer;
begin
	for N := 1 to Num_Plot_Elements do Params[ N ] := 0;
end;

Function MakeNewScene( Plot: GearPtr; var EReq: String; ESlot: Integer ): Boolean;
	{ Attempt to generate a new scene and initialize all needed }
	{ content. Return TRUE if the scene was created successfully, }
	{ or FALSE if it wasn't. }
	Function GetNumScenes: Integer;
		{ Determine how many scenes to add from the EReq. }
	var
		hi,lo: Integer;
	begin
		lo := ExtractValue( EReq );
		hi := ExtractValue( EReq );
		if ( lo < 1 ) or ( hi < 1 ) then begin
			GetNumScenes := 1;
		end else if ( lo >= hi ) then begin
			GetNumScenes := lo;
		end else begin
			GetNumScenes := lo + Random( hi - lo + 1 );
		end;
	end;
	Function AddAutoSubPlot( ParentPlot: GearPtr; const SPReq: String; const Params: ElementTable ): GearPtr;
		{ Turn off the manual component menu for a bit, and add some }
		{ stuff automatically. }
	var
		Old_SCM: Boolean;
		it: GearPtr;
	begin
		Old_SCM := Select_Components_Manually;
		Select_Components_Manually := False;
		it := AddSubPlot( ParentPlot , SPReq , Params );
		Select_Components_Manually := Old_SCM;
		AddAutoSubPlot := it;
	end;
	Procedure AddACity( Scene: GearPtr );
		{ Attempt to add a city to this scene. }
	var
		Params: ElementTable;
	begin
		ClearElementTable( Params );
		Params[1] := Scene^.S;
		if AddAutoSubPlot( Plot , '*MNS_City' , Params ) = Nil then RecordError( 'ERROR: AddACity failed for ' + GearName( Plot) + '.' );
	end;
	Procedure AddLinkBack( Scene: GearPtr; Link_Back: Integer );
		{ Attempt to link this scene back to the previous one. }
	var
		Params: ElementTable;
	begin
		ClearElementTable( Params );
		Params[1] := Link_Back;
		Params[2] := Scene^.S;
		if AddAutoSubPlot( Plot , '*MNS_Link' , Params ) = Nil then RecordError( 'ERROR: AddLinkBack failed for ' + GearName( Plot) + '.' );
	end;
	Procedure AddEncounters( Scene: GearPtr );
		{ Try to add enough encounters so that the player can level }
		{ up without any grinding. }
	var
		Target, Tries: LongInt;
		SubPlot: GearPtr;
		Params: ElementTable;
	begin
		Target := NAttValue( Scene^.NA , NAG_StoryData , NAS_DifficultyLevel ) * 4000;
		Tries := 0;
		ClearElementTable( Params );
		Params[1] := Scene^.S;
		while ( Target > 0 ) and ( Tries < 12 ) do begin
			SubPlot := AddAutoSubPlot( Plot , '*MNS_Encounter' , Params );
			if SubPlot <> Nil then begin
				Target := Target - NAttValue( SubPlot^.NA , NAG_StoryData , NAS_PlotXP );
			end else begin
				RecordError( 'ERROR: AddEncounter failed for ' + GearName( Plot) + '.' );
			end;
			Inc( Tries );
		end;
	end;
	Procedure AddAFeature( Scene: GearPtr; const FReq: String );
		{ Attempt to add a notable feature to this scene. }
	var
		Params: ElementTable;
	begin
		ClearElementTable( Params );
		Params[1] := Scene^.S;
		if AddAutoSubPlot( Plot , FReq , Params ) = Nil then RecordError( 'ERROR: AddAFeature failed for ' + GearName( Plot) + '.' );
	end;
const
	Scene_Prototype_Header = 'SCENE_';
	Wilderness_Scene_Prototype = 'SCENE_Default';
	MODE_City = 1;
	MODE_Wilderness = 2;
	MODE_Dungeon = 3;
var
	cmd,proto_desig,special_feature: String;
	S_Mode,Num_Scenes_Requested,Link_Back,T: Integer;
	Scene: GearPtr;
begin
	{ Determine what kind of scene we'll be creating. There are three }
	{ possibilities: City, Wilderness, and Dungeon. }
	{ City gives a relatively peaceful map with a city on it. }
	{ Wilderness gives a series of outdoor maps with encounters. }
	{ Dungeon gives a series of indoor maps with encounters. }
	cmd := ExtractWord( EReq );
	if cmd = '' then Exit( False );
	case cmd[1] of
		'C':	begin
				S_Mode := MODE_City;
				proto_desig := Wilderness_Scene_Prototype;
				special_feature := '*MNS_Feature_City';
				Num_Scenes_Requested := 1;
			end;

		'W':	begin
				S_Mode := MODE_Wilderness;
				proto_desig := Wilderness_Scene_Prototype;
				Num_Scenes_Requested := GetNumScenes;
				special_feature := '*MNS_Feature_Wilderness';
			end;
		'D':	begin
				S_Mode := MODE_Dungeon;
				proto_desig := ExtractWord( EReq );
				if proto_desig = '' then proto_desig := Wilderness_Scene_Prototype
				else proto_desig := Scene_Prototype_Header + proto_desig;
				Num_Scenes_Requested := GetNumScenes;
				special_feature := '*MNS_Feature_Dungeon';
			end;

	{ If you've requested an unknown scene type, you get nothing. }
	else Exit( False );
	end;

	{ Finally, determine which scene we link from. }
	Link_Back := ElementID( Plot , ExtractValue( EReq ) );

	for t := 1 to Num_Scenes_Requested do begin
		{ Create the new scene gear, and stuff it into Plot's invcoms. }
		Scene := LoadNewSTC( proto_desig );
		if Scene = Nil then begin
			{ We failed to load a proper scene. Exit with an error. }
			RecordError( 'ERROR: Scene ' + proto_desig + ' not found.' );
			Exit( False );
		end;
		InsertInvCom( Plot , Scene );

		{ Assign a new uniqueID for it. }
		Scene^.S := NewUID;
		SetNAtt( Scene^.NA , NAG_StoryData , NAS_UniqueID , Scene^.S );
		SetNAtt( Scene^.NA , NAG_StoryData , NAS_DifficultyLevel , DifficultyRating );

		{ Copy over the context details from the Plot. Some of these }
		{ are needed by the map generator- climate, for instance. }
		CopyPlotContext( Plot, Scene );

		{ Connect this scene to the previous scene via story content. }
		if Link_Back <> 0 then AddLinkBack( Scene , Link_back );

		{ If needed, add encounters. }
		AddEncounters( Scene );

		{ Cities need a city. }
		if S_Mode = MODE_City then AddACity( Scene );

		{ Every scene needs a special feature. }
		AddAFeature( Scene , special_feature );

		{ If needed, increase the difficulty rating. }
		if S_Mode <> MODE_City then Inc( DifficultyRating );

		{ Set the LinkBack scene to this scene, so that the next scene }
		{ in the series will link back here. }
		Link_Back := Scene^.S;
	end;

	{ Record the ID of the last scene created in the element slot. }
	SetNAtt( Plot^.NA , NAG_ElementID , ESlot , Scene^.S );

	MakeNewScene := True;
end;

Function PrepNextPrefab( Plot: GearPtr; var EReq: String; ESlot: Integer ): Boolean;
	{ Prefabs are pre-fabricated components which get stored as invcoms. }
	{ Often, they need some initialization. At the very least they need }
	{ to be assigned a unique ID. }
	Procedure InitPrefab ( PFC: GearPtr; Level: Integer );
		{ Do any special initialization that this part may need. }
	begin

		Case NAttValue( PFC^.NA , NAG_StoryData , NAS_InventoryGenerator ) of
			NAV_GeneralStore:	InsertInvCom( PFC , GenerateStoreWares( Level ) );
			NAV_SmallTreasure:	GenerateTreasureDrop( PFC , Level , 100 );
			NAV_MediumTreasure:	GenerateTreasureDrop( PFC , Level , 200 );
			NAV_LargeTreasure:	GenerateTreasureDrop( PFC , Level , 400 );
		end;
	end;
	Procedure InitPFList ( PFList: GearPtr; Level: Integer );
		{ Do initialization for all gears in this list, as well as all }
		{ of their descendants. }
	begin
		while PFList <> Nil do begin
			InitPrefab( PFList , Level );
			InitPFList( PFList^.SubCom , Level );
			InitPFList( PFList^.InvCom , Level );
			PFList := PFList^.Next;
		end;
	end;
var
	PFC,Scene: GearPtr;
	Dest,Level: LongInt;
begin
	{ Step one: Locate the next prefab component. }
	{ We'll be able to tell which one it is because it won't have }
	{ an ID assigned yet. }
	{ Find the first uninitialized entry in the list. }
	{ This is gonna be our next element. }
	PFC := Plot^.InvCom;
	While ( PFC <> Nil ) and ( NAttValue( PFC^.NA , NAG_StoryData , NAS_UniqueID ) <> 0 ) do begin
		PFC := PFC^.Next;
	end;

	{ If we didn't find an element to prep, that's a bad thing. }
	if PFC = Nil then begin
		RecordError( 'ERROR: Prefab ' + BStr( ESlot ) + ' not found in ' + GearName( Plot ) + '.' );
		Exit( False );
	end;

	{ Determine the destination for this component. }
	Dest := ElementID( Plot , ExtractValue( EReq ) );
	if Dest = 0 then begin
		RecordError( 'ERROR: No dest for ' + BStr( ESlot ) + ' in ' + GearName( Plot ) + '.' );
		Exit( False );
	end;
	Scene := SeekGearByIDTag( FindRoot( Plot ) , NAG_StoryData , NAS_UniqueID , Dest );
	if Scene <> Nil then Level := NAttValue( Scene^.NA , NAG_StoryData , NAS_DifficultyLevel )
	else Level := 1;

	{ Record the destination, Unique ID, and Difficulty Level. }
	SetNAtt( PFC^.NA , NAG_StoryData , NAS_UniqueID , NewUID );
	SetNAtt( PFC^.NA , NAG_StoryData , NAS_ToBePlacedHere , Dest );
	SetNAtt( PFC^.NA , NAG_StoryData , NAS_DifficultyLevel , Level );

	{ Do some initialization, if needed. }
	InitPrefab ( PFC , Level );
	InitPFList ( PFC^.SubCom , Level );
	InitPFList ( PFC^.InvCom , Level );

	{ Also record the ID in the plot. }
	SetNAtt( Plot^.NA , NAG_ElementID , ESlot , NAttValue( PFC^.NA , NAG_StoryData , NAS_UniqueID ) );

	PrepNextPrefab := True;
end;

Function MakeEncounter( Plot: GearPtr; var EReq: String; ESlot: Integer ): Boolean;
	{ Create some monsters. Yay! }
	{ Return TRUE if everything went smoothly, or FALSE otherwise. }
var
	DestID: LongInt;	{ For locating the containers. }
	Lvl,Strength,MobID: LongInt;
	Scene: GearPtr;	{ Just to make sure they exist. }
	Monsters,M: GearPtr;
begin
	{ Step One- locate the scene and the map feature to be used. }
	DestID := ElementID( Plot , ExtractValue( EReq ) );
	Scene := SeekGearByIDTag( FindRoot( Plot ) , NAG_StoryData , NAS_UniqueID , DestID );
	DestID := ElementID( Plot , ExtractValue( EReq ) );
	Lvl := NAttValue( Scene^.NA , NAG_StoryData , NAS_DifficultyLevel );
	if Random( 15 ) = 1 then Inc( Lvl );
	Strength := ExtractValue( EReq );

	if ( Scene = Nil ) or ( DestID = 0 ) then begin
		RecordError( 'ERROR: No dest for Mob' + BStr( ESlot ) + ' in ' + GearName( Plot ) + '.' );
		Exit( False );
	end;

	{ The remainder of the EReq should be our monster request. }
	{ Add the scene's monster context to it and pass it on to the }
	{ encounter generator. }
	EReq := EReq + ' ' + MonsterContext( Scene );

	{ Generate the monster list. }
	Monsters := BuildEncounter( EReq , Lvl , Strength );
	if Monsters <> Nil then begin
		{ Let's initialize these puppies. }
		M := Monsters;
		MobID := NewUID;
		while M <> Nil do begin
			SetNAtt( M^.NA , NAG_StoryData , NAS_ToBePlacedHere , DestID );
			SetNAtt( M^.NA , NAG_StoryData , NAS_UniqueID , NewUID );
			SetNAtt( M^.NA , NAG_StoryData , NAS_MobID , MobID );

			{ Also store the XP value OF THE monsters. }
			AddNAtt( Plot^.NA , NAG_StoryData , NAS_PlotXP , ExperienceValue( M , NAttValue( Scene^.NA , NAG_StoryData , NAS_DifficultyLevel ) )  );

			M := M^.Next;
		end;
		InsertInvCom( Plot , Monsters );

		{ Record the MobID in the plot. }
		SetNAtt( Plot^.NA , NAG_ElementID , ESlot , MobID );

		MakeEncounter := True;
	end else begin
		MakeEncounter := False;
	end;
end;

Function MakeBoss( Plot: GearPtr; var EReq: String; ESlot: Integer ): Boolean;
	{ Create a monster. Yay! }
	{ Return TRUE if everything went smoothly, or FALSE otherwise. }
var
	DestID: LongInt;	{ For locating the containers. }
	Lvl,Strength,MobID: LongInt;
	Scene: GearPtr;	{ Just to make sure they exist. }
	Monsters,M: GearPtr;
begin
	{ Step One- locate the scene and the map feature to be used. }
	DestID := ElementID( Plot , ExtractValue( EReq ) );
	Scene := SeekGearByIDTag( FindRoot( Plot ) , NAG_StoryData , NAS_UniqueID , DestID );
	DestID := ElementID( Plot , ExtractValue( EReq ) );
	Strength := ExtractValue( EReq );
	Lvl := NAttValue( Scene^.NA , NAG_StoryData , NAS_DifficultyLevel ) + Strength;

	if ( Scene = Nil ) or ( DestID = 0 ) then begin
		RecordError( 'ERROR: No dest for Boss' + BStr( ESlot ) + ' in ' + GearName( Plot ) + '.' );
		Exit( False );
	end;

	{ The remainder of the EReq should be our monster request. }
	{ Add the scene's monster context to it and pass it on to the }
	{ encounter generator. }
	EReq := EReq + ' ' + MonsterContext( Scene );

	{ Generate the monster list. }
	Monsters := BuildBoss( EReq , Lvl );
	if Monsters <> Nil then begin
		{ Let's initialize these puppies. }
		M := Monsters;
		MobID := NewUID;
		while M <> Nil do begin
			SetNAtt( M^.NA , NAG_StoryData , NAS_ToBePlacedHere , DestID );
			SetNAtt( M^.NA , NAG_StoryData , NAS_UniqueID , NewUID );
			SetNAtt( M^.NA , NAG_StoryData , NAS_MobID , MobID );

			{ Also store the XP value OF THE monsters. }
			AddNAtt( Plot^.NA , NAG_StoryData , NAS_PlotXP , ExperienceValue( M , NAttValue( Scene^.NA , NAG_StoryData , NAS_DifficultyLevel ) )  );

			M := M^.Next;
		end;

		{ Record the UniqueID of the first monster in the plot. }
		SetNAtt( Plot^.NA , NAG_ElementID , ESlot , NAttValue( Monsters^.NA , NAG_StoryData , NAS_UniqueID ) );

		InsertInvCom( Plot , Monsters );

		MakeBoss := True;
	end else begin
		MakeBoss := False;
	end;
end;

Function FindPlotElements( Plot: GearPtr ): Boolean;
	{ Attempt to locate and initailize all the elements of this plot. }
	{ If an element cannot be found, return FALSE. }
	{ Plot should be installed as a subcom of its parent by this point- }
	{ should we need to locate any game resources, we're gonna go to root }
	{ and start looking from there. }
var
	EverythingOK, OKNow: Boolean;
	EReq,ECom: String;	{ The element request, element command. }
	T: Integer;
begin
	{ Begin by assuming TRUE. }
	EverythingOK := True;
	OkNow := True;

	for t := 1 to Num_Plot_Elements do begin
		{ Check all the plot elements. Some of these may have been inherited from the SLOT. }
		if ( ElementID( Plot , T ) = 0 ) and EverythingOK then begin
			EReq := SAttValue( Plot^.SA , 'ELEMENT' + BStr( T ) );
			ECom := UpCase( ExtractWord( EReq ) );

			if ECom <> '' then begin
				case ECom[1] of
					'N':	OkNow := MakeNewScene( Plot , EReq , T );
					'P':	OkNow := PrepNextPrefab( Plot , EReq , T );
					'M':	OkNow := MakeEncounter( Plot , EReq , T );
					'B':	OkNow := MakeBoss( Plot , EReq , T );
				else OkNow := False;
				end;
			end else begin
				OkNow := True;
				break;
			end;
		end;

		if not OkNow then RecordError( 'Element ' + BStr( t ) + ' not found in ' + GearName( Plot ) + '.' );
		EverythingOK := EverythingOK and OKNow;
	end;

	if not EverythingOK then RecordError( 'ERROR: Elements not found for ' + GearName( Plot ) + '.' );

	FindPlotElements := EverythingOK;
end;

Procedure ApplyDictionaryToString( var Info: String; Dictionary: SAttPtr );
	{ This dictionary contains a bunch of substitution strings. Apply them to }
	{ the string provided. }
var
	P,P2: Integer;
	SPat,SRep: String;
begin
	P := 1;
	while P < Length( Info ) do begin
		if ( Info[P] = '%' ) and ( P < ( Length( Info ) - 1 ) ) then begin
			{ We've found a hash. This could be a replacement string. See what string it is. }
			SPat := '%';
			P2 := P;
			repeat
				Inc( P2 );
				SPat := SPat + Info[P2];
			until ( P2 >= Length( Info ) ) or ( Info[P2] = '%' );

			{ We now have a string that may very well be something we want to replace. Check it. }
			SRep := SAttValue( Dictionary , SPat );
			if SRep <> '' then begin
				{ The pattern was found in the dictionary. Replace all instances of it. }
				ReplacePat( Info , SPat , SRep );
				P := P + Length( SRep ) - 1;
			end;
		end;

		Inc( P );
	end;
end;

Procedure InitPlotStrings( Plot: GearPtr; Dictionary: SAttPtr );
	{ Initialize the strings for this plot and all descendants. }
	Procedure ReplaceStrings( Part: GearPtr );
		{ Apply the dictionary to this part specifically. }
	var
		S: SAttPtr;
	begin
		S := Part^.SA;
		while S <> Nil do begin
			ApplyDictionaryToString( S^.Info , Dictionary );
			S := S^.Next;
		end;
	end;
	Procedure HandleList( LList: GearPtr );
		{ Run LList, all of its siblings and children, through the ReplaceStrings }
		{ procedure. }
	begin
		while LList <> Nil do begin
			ReplaceStrings( LList );
			HandleList( LList^.SubCom );
			HandleList( LList^.InvCom );
			LList := LList^.Next;
		end;
	end;
begin
	ReplaceStrings( Plot );
	HandleList( Plot^.SubCom );
	HandleList( Plot^.InvCom );
end;

Procedure RestoreUniqueStoryContent( MasterPlot: GearPtr );
	{ This plot has been rejected. So, any unique story content it has }
	{ should be restored. }
	Procedure RestoreThisPlot( Plot: GearPtr );
		{ Restore this plot specifically. }
	var
		SCID: LongInt;
	begin
		SCID := NAttValue( Plot^.NA , NAG_StoryData , NAS_SingleUse );
		if SCID <> 0 then begin
			{ This is a single use plot. Find its prototype and reset }
			{ its counter. }
			Plot := SeekGearByIDTag( Story_Component_List , NAG_StoryData , NAS_SingleUse , SCID );
			if Plot <> Nil then SetNAtt( Plot^.NA , NAG_StoryData , NAS_SingleUse , -1 );
		end;
	end;
	Procedure HandleList( LList: GearPtr );
		{ Restore all the plots in this linked list, as well as their }
		{ descendants. }
	begin
		while LList <> Nil do begin
			if LList^.G = GG_StoryContent then begin
				RestoreThisPlot( LList );
				HandleList( LList^.SubCom );
			end;
			LList := LList^.Next;
		end;
	end;
begin
	RestoreThisPlot( MasterPlot );
	HandleList( MasterPlot^.SubCom );
end;


Function InitSubPlot( ParentPlot,PlotToAdd: GearPtr; PlotID: LongInt; const Params: ElementTable ): GearPtr;
	{ Initialize the provided subplot, also creating any }
	{ requested scenes or subplots. }
	{ If the content cannot be added, delete it and all subcoms. }
	Procedure InitDestinations( LList: GearPtr );
		{ Check through the list for props or checkpoints with destination }
		{ defined. Replace the elemnet number with the element ID. }
	var
		DestID: LongInt;
		Dest: GearPtr;
	begin
		while LList <> Nil do begin
			if ( LList^.G = GG_CheckPoint ) or ( LList^.G = GG_Prop ) then begin
				if ( LList^.Stat[ STAT_Destination ] > 0 ) and ( LList^.Stat[ STAT_Destination ] <= Num_Plot_Elements ) then begin
					DestID := ElementID( PlotToAdd , LList^.Stat[ STAT_Destination ] );
					Dest := SeekGearByIDTag( FindRoot( ParentPlot ) , NAG_StoryData , NAS_UniqueID , DestID );
					if Dest^.G = GG_MapFeature then DestID := NAttValue( Dest^.NA , NAG_StoryData , NAS_ToBePlacedHere );
					LList^.Stat[ STAT_Destination ] := DestID;
				end;
			end;
			if LList^.G <> GG_StoryContent then begin
				InitDestinations( LList^.SubCom );
				InitDestinations( LList^.InvCom );
			end;
			LList := LList^.Next;
		end;
	end;
var
	InitOK: Boolean;
	T,E,N: Integer;		{ Three counters, spelling 10. }
	SPReq,SPCom: String;
	SPParams: ElementTable;
	SubPlot: GearPtr;
	Dictionary: SAttPtr;
begin
	{ Copy the LayerID and other values to this shard. }
	SetNAtt( PlotToAdd^.NA , NAG_StoryData , NAS_UniqueID , PlotID );

	{ Alter the context based on this subplot. }
	{ Basically, if a context element isn't defined in PlotToAdd copy over }
	{ the value from ParentPlot. }
	CopyPlotContext( ParentPlot, PlotToAdd );

	{ Copy over all provided parameters. }
	for T := 1 to Num_Plot_Elements do begin
		if Params[ t ] <> 0 then SetNAtt( PlotToAdd^.NA , NAG_ElementID , T , Params[ t ] );
	end;

	{ Attempt the basic content insertion routine: locate/initialize }
	{ all requested elements. }
	InsertSubCom( ParentPlot, PlotToAdd );
	InitOK := FindPlotElements( PlotToAdd );

	{ If the installation has gone well so far, time to initialize subplots. }
	if InitOK then begin
		{ Before we get to the subplots, initialize exits/entrances. }
		InitDestinations( PlotToAdd^.SubCom );
		InitDestinations( PlotToAdd^.InvCom );

		{ Attempt to add subplots. }
		{ If any of the needed subplots fail, installation of this shard fails }
		{ as well. }
		t := 1;

		for t := 1 to Num_Sub_Plots do begin
			SPReq := SAttValue( PlotToAdd^.SA , 'SUBPLOT' + BStr( T ) );
			if SPReq <> '' then begin
				SPCom := ExtractWord( SPReq );

				{ Create the list of parameters. }
				ClearElementTable( SPParams );
				N := 1;
				while ( N <= Num_Plot_Elements ) and ( SPReq <> '' ) do begin
					E := ExtractValue( SPReq );
					if ( E >= 1 ) and ( E <= Num_Plot_Elements ) then begin
						{ This element is being shared with the subplot. }
						SPParams[ N ] := ElementID( PlotToAdd , E );
						Inc( N );
					end;
				end;

				SubPlot := AddSubPlot( PlotToAdd, SPCom, SPParams );
				if SubPlot <> Nil then begin
					{ Store the ID of the subplot here. }
					SetNAtt( PlotToAdd^.NA , NAG_SubPlotID , T , NAttValue( SubPlot^.NA , NAG_StoryData , NAS_UniqueID ) );
				end else begin
					{ The subplot request failed, meaning that this shard fails }
					{ as well. }
					InitOK := False;
					Break;
				end;
			end;
		end; { for T }
	end;

	{ Do string substitutions now. }
	if InitOK then begin
		{ Start by creating the dictionary. }
		Dictionary := Nil;
		SetSAtt( Dictionary , '%id% <' + BStr( NAttValue( PlotToAdd^.NA , NAG_StoryData , NAS_UniqueID ) ) + '>' );
		for t := 1 to Num_Context_Descriptors do SetSAtt( Dictionary , '%' + Context_Head[ t ] + '% <' + BStr( NAttValue( PlotToAdd^.NA , NAG_StoryData , CDIndex[ t ] ) ) + '>' );
		for t := 1 to Num_Sub_Plots do SetSAtt( Dictionary , '%id' + BStr( T ) + '% <' + Bstr( NAttValue( PlotToAdd^.NA , NAG_SubPlotID , T ) ) + '>' );
		for t := 1 to Num_Plot_Elements do SetSAtt( Dictionary , '%' + BStr( T ) + '% <' + BStr( ElementID( PlotToAdd , T ) ) + '>' );

		{ Initialize the strings here and in all children. Don't need to }
		{ worry about overwriting subplots since they've all been dealt }
		{ with during their own initialization. }
		InitPlotStrings( PlotToAdd , Dictionary );

		DisposeSAtt( Dictionary );
	end;


	{ If, after all that, Init was not OK, delete this subplot and all of }
	{ its children. }
	if not InitOK then begin
		RecordError( GearName( PlotToAdd ) + ' rejected.' );
		RestoreUniqueStoryContent( PlotToAdd );
		RemoveGear( ParentPlot^.SubCom , PlotToAdd );
	end;

	InitSubPlot := PlotToAdd;
end;

Function CreateStoryContentShoppingList( const Context: String ): NAttPtr;
	{ Create a list of components to be used by SELECTCOMPONENTFROMLIST below. }
	{ The list will be of the form G:0 S:[Component Index] V:[Match Weight]. }
const
	Non_Single_Use_Multiplier = 5;
var
	C: GearPtr;	{ A component. }
	N: Integer;	{ A counter. }
	MW,SU: Integer;	{ The match-weight of the current component. }
	ShoppingList: NAttPtr;	{ The list of legal components. }
begin
	{ Initialize all the values. }
	ShoppingList := Nil;
	C := Story_Component_List;
	N := 1;

	{ Go through the list, adding everything that matches. }
	while C <> Nil do begin
		SU := NAttValue( C^.NA , NAG_StoryData , NAS_SingleUse );
		if SU <= 0 then begin
			MW := StringMatchWeight( Context , SAttValue( C^.SA , 'REQUIRES' ) );
			if MW > 0 then begin
				{ Non-Single-Use components are more likely to show up. }
				if SU = 0 then MW := MW * Non_Single_Use_Multiplier;
				SetNAtt( ShoppingList , 0 , N , MW );
			end;
		end;

		Inc( N );
		C := C^.Next;
	end;

	CreateStoryContentShoppingList := ShoppingList;
end;

Procedure ComponentMenuRedraw;
	{ The redraw for the component selector below. }
begin
	PrettyPictureDisplay;
	InfoBox( ZONE_MainMenu );
end;

Function ComponentMenu( var ShoppingList: NAttPtr ): GearPtr;
	{ Select one of the components from a menu. }
var
	RPM: RPGMenuPtr;
	C: GearPtr;
	N: Integer;
	SL: NAttPtr;
begin
	RPM := CreateRPGMenu( MenuItem, MenuSelect , ZONE_MainMenu );
	SL := ShoppingList;
	while SL <> Nil do begin
		C := RetrieveGearSib( Story_Component_List , SL^.S );
		AddRPGMenuItem( RPM , '[' + BStr( SL^.V ) + ']' + GearName( C ) , SL^.S );
		SL := SL^.Next;
	end;

	N := SelectMenu( RPM , @ComponentMenuRedraw );
	SetNAtt( ShoppingList , 0 , N , 0 );
	DisposeRPGMenu( RPM );
	if N > 0 then begin
		ComponentMenu := RetrieveGearSib( Story_Component_List , N );
	end else begin
		DisposeNAtt( ShoppingList );
		ComponentMenu := Nil;
	end;
end;



Function AddSubPlot( ParentPlot: GearPtr; SPReq: String; const Params: ElementTable ): GearPtr;
	{ Attempt to add some new plot content to the adventure. }
var
	Context: String;
	PlotID: LongInt;
	ShoppingList: NAttPtr;
	PlotToAdd: GearPtr;
	NotFoundMatch: Boolean;
begin
	{ Determine the context based on the parent plot. }
	Context := ExtractWord( SPReq ) + ' ' + PlotContext( ParentPlot ) + ' ' + ParamContext( ParentPlot , Params );

	{ If it's time to end things, add the FINAL tag. }
	if DifficultyRating >= MaxDifficultyRating then Context := Context + ' FINAL';

	{ Generate LayerID. }
	PlotID := NewUID;

	{ Generate the list of potential subplots. }
	ShoppingList := CreateStoryContentShoppingList( Context );
	if ShoppingList = Nil then RecordError( 'WARNING: No content found for ' + Context );

	{ Based on this shopping list, search for applocable subplots and attempt to }
	{ fit them into the adventure. }
	NotFoundMatch := True;
	PlotToAdd := Nil;
	while ( ShoppingList <> Nil ) and NotFoundMatch do begin
		if Select_Components_Manually then begin
			PlotToAdd := ComponentMenu( ShoppingList );
		end else begin
			PlotToAdd := SelectComponentFromList( Story_Component_List , ShoppingList );
		end;
		if PlotToAdd <> Nil then begin
			if NAttValue( PlotToAdd^.NA , NAG_StoryData , NAS_SingleUse ) <> 0 then SetNAtt( PlotToAdd^.NA , NAG_StoryData , NAS_SingleUse , NewSCID );
			PlotToAdd := InitSubPlot( ParentPlot, CloneGear( PlotToAdd ), PlotID, Params );
			NotFoundMatch := PlotToAdd = Nil;
		end;
	end;

	{ Get rid of the shopping list. }
	DisposeNAtt( ShoppingList );

	{ Return our selected subplot. }
	AddSubPlot := PlotToAdd;
end;

Procedure BuildMegalist( Dest: GearPtr; AddOn: SAttPtr );
	{ Combine the scripts listed in ADDON into LLIST. }
	{ If a script with the same label already exists in LLIST, the new }
	{ script from ADDON supercedes it, while the old script gets moved to }
	{ a new label. }
	{ The %pop% substitution string is used to jump to the superceded script. }
var
	SPop,Key,Current: String;
	SPopSA: SAttPtr;
begin
	SPop := 'na';
	while AddOn <> Nil do begin
		{ If there's currently a SAtt in the megalist with this }
		{ key, it has to be "pushed" to a new position. }
		Key := UpCase( RetrieveAPreamble( AddOn^.Info ) );
		if ( Key <> 'REQUIRES' ) and ( Key <> 'DESC' ) and ( Key <> 'DESIG' ) and ( Key <> 'SPECIAL' )
					 and not ( HeadMatchesString( 'ELEMENT' , Key ) or HeadMatchesString( 'TEAM' , Key ) or HeadMatchesString( 'CONTENT' , Key ) or HeadMatchesString( 'CONTEXT' , Key )
					 or HeadMatchesString( 'MINIMAP' , Key ) or HeadMatchesString( 'QUEST' , Key ) or HeadMatchesString( 'SCENE' , Key ) or HeadMatchesString( 'NAME' , Key )
					 or HeadMatchesString( 'SUBPLOT' , Key ) ) then begin
			Current := SAttValue( Dest^.SA , Key );

			if Current <> '' then begin
				SPopSA := AddSAtt( Dest^.SA , Key , Current );
				SPop := RetrieveAPreamble( SPopSA^.Info );
			end else begin
				SPop := 'na';
			end;

			ReplacePat( AddOn^.Info , '%pop%' , SPop );
			SetSAtt( Dest^.SA , AddOn^.Info );
		end;

		AddOn := AddOn^.Next;
	end;
end;


Function AssembleCampaign( CamPlan: GearPtr ): CampaignPtr;
	{ We've been given a campaign plan. Assemble it into a real campaign. }
	Function GetThing( Camp: CampaignPtr; LList: GearPtr; UID: LongInt ): GearPtr;
		{ Locate a thing with a unique ID. }
	var
		it: GearPtr;
	begin
		it := SeekGearByIDTag( Camp^.Source , NAG_StoryData , NAS_UniqueID , UID );
		if it = Nil then it := SeekGearByIDTag( FindRoot( LList ) , NAG_StoryData , NAS_UniqueID , UID );
		GetThing := it;
	end;
	Procedure MoveMFContents( Source, Dest: GearPtr );
		{ Move all contents from Source into Dest. }
	var
		Thing: GearPtr;
	begin
		while Source^.SubCom <> Nil do begin
			Thing := Source^.SubCom;
			DelinkGear( Source^.SubCom , Thing );
			InsertSubCom( Dest , Thing );
		end;
		while Source^.InvCom <> Nil do begin
			Thing := Source^.InvCom;
			DelinkGear( Source^.InvCom , Thing );
			InsertInvCom( Dest , Thing );
		end;
	end;
	Procedure MoveInvComs( Camp: CampaignPtr; Plot: GearPtr );
		{ Move the inventory components of this thingamajig to their }
		{ proper locations. If it's a scene, stick it in the campaign. }
		{ Otherwise locate the thing it's supposed to go in and stick }
		{ it there. }
	var
		I,I2,Dest: GearPtr;
	begin
		I := Plot^.InvCom;
		while I <> Nil do begin
			I2 := I^.Next;

			if I^.G = GG_Scene then begin
				DelinkGear( Plot^.InvCom , I );
				InsertSubCom( Camp^.Source , I );
			end else begin
				Dest := GetThing( Camp , Plot , NAttValue( I^.NA , NAG_StoryData , NAS_ToBePlacedHere ) );
				if Dest = Nil then begin
					RecordError( 'WARNING: No destination found for ' + GearName( I ) + ' in ' + GearName( Plot ) + '.' );
				end else begin
					DelinkGear( Plot^.InvCom , I );
					if ( Dest^.G = GG_Scene ) and ( I^.G = GG_MapFeature ) then InsertSubCom( Dest , I )
					else if ( Dest^.G = GG_MapFeature ) and ( I^.G = GG_MapFeature ) then begin
						MoveMFContents( I , Dest );
						DisposeGear( I );
					end else InsertInvCom( Dest , I );
				end;
			end;

			I := I2;
		end;
	end;
	Procedure MergeScripts( Camp: CampaignPtr; Plot: GearPtr );
		{ Take the scripts from this plot and merge them with whatever }
		{ they're supposed to be merged with. Scripts from the plot itself }
		{ get merged into the adventure root; other scripts may be defined }
		{ in MetaScript gears and get merged into whatever plot element is }
		{ indicated. }
	var
		MS,Dest: GearPtr;
	begin
		BuildMegalist( Camp^.Source , Plot^.SA );
		MS := Plot^.SubCom;
		while mS <> Nil do begin
			if MS^.G = GG_MetaScript then begin
				Dest := GetThing( Camp , Plot , ElementID( Plot , MS^.S ) );
				if Dest <> Nil then begin
					BuildMegalist( Dest , MS^.SA );
				end else begin
					RecordError( 'WARNING: No dest found for MetaScript ' + BStr( MS^.S ) + ' in ' + GearName( Plot ) + '.' );
				end;
			end;
			MS := MS^.Next;
		end;
	end;
	Procedure ProcessList( Camp: CampaignPtr; LList: GearPtr );
		{ Work through this list. For each story content gear, do }
		{ the following: }
		{ - Move its InvComs to their destinations. }
		{ - Apply metascripts to their destinations. }
	begin
		while LList <> Nil do begin
			if LList^.G = GG_StoryContent then begin
				MoveInvComs( Camp , LList );
				MergeScripts( Camp , LList );
			end;

			ProcessList( Camp , LList^.SubCom );
			LList := LList^.Next;
		end;
	end;
var
	Camp: CampaignPtr;
	Source: GearPtr;
begin
	Camp := NewCampaign;
	Source := NewGear( Nil );
	Camp^.Source := Source;

	{ Starting with the first component and moving through the list, }
	{ assemble everything into the list of scenes. }
	ProcessList( Camp, CamPlan );

	{ We're finished with the Campaign Plan now. Dispose of it. }
	DisposeGear( CamPlan );

	AssembleCampaign := Camp;
end;

Function RandomCampaign( MaxLevel: Integer ): CampaignPtr;
	{ Create a random campaign lasting for the requested number of levels. }
	Procedure ResetSingleUseComponents;
		{ The single use components may be marked as in use because of }
		{ a previous campaign. Mark them all as free. }
	var
		LList: GearPtr;
	begin
		LList := Story_Component_List;
		while LList <> Nil do begin
			if NAttValue( LList^.NA , NAG_StoryData , NAS_SingleUse ) > 0 then SetNAtt( LList^.NA , NAG_StoryData , NAS_SingleUse , -1 );
			LList := LList^.Next;
		end;
	end;
var
	CampSeed: GearPtr;
	DummyParams: ElementTable;
begin
	{ Initialize the global variables. }
	MaxUID := 1;
	MaxSCID := 1;
	DifficultyRating := 1;
	MaxDifficultyRating := MaxLevel;
	Select_Components_Manually := False;

	{ Reset the single use components. }
	ResetSingleUseComponents;

	{ Create a campaign seed with the default options. }
	CampSeed := NewGear( Nil );
	CampSeed^.G := GG_StoryContent;

	{ Begin the adventure with an *INTRODUCTION plot. }
	ClearElementTable( DummyParams );
	AddSubPlot( CampSeed , '*INTRODUCTION' , DummyParams );

	{ Assemble the completed plot tree into a campaign. }
	RandomCampaign := AssembleCampaign( CampSeed );
end;

Function DebugCampaign: CampaignPtr;
	{ Create a random campaign manually. }
	Procedure ResetSingleUseComponents;
		{ The single use components may be marked as in use because of }
		{ a previous campaign. Mark them all as free. }
	var
		LList: GearPtr;
	begin
		LList := Story_Component_List;
		while LList <> Nil do begin
			if NAttValue( LList^.NA , NAG_StoryData , NAS_SingleUse ) > 0 then SetNAtt( LList^.NA , NAG_StoryData , NAS_SingleUse , -1 );
			LList := LList^.Next;
		end;
	end;
var
	CampSeed: GearPtr;
	DummyParams: ElementTable;
begin
	{ Initialize the global variables. }
	MaxUID := 1;
	MaxSCID := 1;
	DifficultyRating := 1;
	MaxDifficultyRating := 1;
	Select_Components_Manually := True;

	{ Reset the single use components. }
	ResetSingleUseComponents;

	{ Create a campaign seed with the default options. }
	CampSeed := NewGear( Nil );
	CampSeed^.G := GG_StoryContent;

	{ Begin the adventure with an *INTRODUCTION plot. }
	ClearElementTable( DummyParams );
	AddSubPlot( CampSeed , '*INTRODUCTION' , DummyParams );

	{ Assemble the completed plot tree into a campaign. }
	DebugCampaign := AssembleCampaign( CampSeed );
end;


initialization
	Story_Component_List := AggregatePattern( 'advcom_*.txt' , Data_Directory );

finalization
	DisposeGear( Story_Component_List );

end.
