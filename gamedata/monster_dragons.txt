%%
%%  *******************
%%  ***   DRAGONS   ***
%%  *******************
%%
%% Unless there's a reason for it, all monsters should have a minimum Strength
%% of 10 and a maximum Luck of 1.
%%
%% King of the monsters. Or queen, depending.
%%
%% Statline: STR TGH REF INT PIE LUC

	Monster
	name <Young Forest Dragon>
	statline 14 18 15 12 12 1
	Move 14
	Class:Dragon 5
	RESIST:Acid 100
	SDL_Sprite <monster_dragons.png>
	Frame 4
	MonsterGP 150
	type <EVERY FORES JUNGL EARTH DRAGO RENFA>
	inv
		STC CLAW_d4
	end
	sub
		STC MFX_BW_ACID_2d6
	end

	Monster
	name <Young Sand Dragon>
	statline 15 18 14 12 12 1
	Move 14
	Class:Dragon 5
	RESIST:Fire 100
	SDL_Sprite <monster_dragons.png>
	Frame 2
	MonsterGP 150
	type <EVERY DESER WASTE FIRE_ DRAGO RENFA>
	inv
		STC CLAW_d4
	end
	sub
		STC MFX_BW_FIRE_2d6
	end

	Monster
	name <Young Sea Dragon>
	statline 15 18 15 12 12 1
	Move 14
	Class:Dragon 6
	RESIST:Frost 100
	SDL_Sprite <monster_dragons.png>
	Frame 1
	MonsterGP 180
	type <EVERY SWAMP WATER DRAGO RENFA>
	inv
		STC CLAW_d6
	end
	sub
		STC MFX_BW_FROST_2d6
	end

	Monster
	name <Young Sky Dragon>
	statline 12 18 18 12 12 1
	Move 14
	Class:Dragon 6
	RESIST:Lightning 100
	SDL_Sprite <monster_dragons.png>
	Frame 5
	MonsterGP 180
	type <EVERY GRASS AIR__ DRAGO RENFA>
	inv
		STC CLAW_d6
	end
	sub
		STC MFX_BW_LIGHTNING_2d10
	end

	Monster
	name <Young Cave Dragon>
	statline 16 19 12 12 12 1
	Move 14
	Class:Dragon 7
	RESIST:Acid 100
	SDL_Sprite <monster_dragons.png>
	Frame 0
	MonsterGP 210
	type <EVERY DUNGE EARTH CAVER WASTE DRAGO RENFA>
	inv
		STC CLAW_d8
	end
	sub
		STC MFX_BW_ACID_2d6
	end

	Monster
	name <Young Solar Dragon>
	statline 15 18 15 12 12 1
	Move 14
	Class:Dragon 7
	RESIST:Lightning 100
	SDL_Sprite <monster_dragons.png>
	Frame 3
	MonsterGP 210
	type <EVERY GRASS AIR__ SOLAR DRAGO RENFA>
	inv
		STC CLAW_d8
	end
	sub
		STC MFX_BW_LIGHTNING_2d10
	end

	Monster
	name <Young Ice Dragon>
	statline 16 18 14 12 12 1
	Move 14
	Class:Dragon 8
	RESIST:Frost 100
	SDL_Sprite <monster_dragons.png>
	Frame 7
	MonsterGP 240
	type <EVERY SNOWY ICE__ DRAGO RENFA>
	inv
		STC CLAW_d8
	end
	sub
		STC MFX_BW_FROST_2d6
	end

	Monster
	name <Young Fire Dragon>
	statline 14 19 16 12 12 1
	Move 14
	Class:Dragon 8
	RESIST:Fire 100
	SDL_Sprite <monster_dragons.png>
	Frame 6
	MonsterGP 240
	type <EVERY DESER FIRE_ DRAGO RENFA>
	inv
		STC CLAW_d8
	end
	sub
		STC MFX_BW_FIRE_2d6
	end

	Monster
	name <Forest Dragon>
	statline 15 19 16 13 13 1
	Move 12
	Class:Dragon 10
	RESIST:Acid 100
	SDL_Sprite <monster_dragons.png>
	Frame 12
	MonsterGP 400
	type <EVERY FORES JUNGL EARTH DRAGO RENFA>
	inv
		STC CLAW_d8
	end
	sub
		STC MFX_BW_ACID_3d6
	end

	Monster
	name <Sand Dragon>
	statline 16 19 15 13 13 1
	Move 12
	Class:Dragon 10
	RESIST:Fire 100
	SDL_Sprite <monster_dragons.png>
	Frame 10
	MonsterGP 400
	type <EVERY DESER WASTE FIRE_ DRAGO RENFA>
	inv
		STC CLAW_d8
	end
	sub
		STC MFX_BW_FIRE_3d6
	end

	Monster
	name <Sea Dragon>
	statline 16 19 16 13 13 1
	Move 12
	Class:Dragon 11
	RESIST:Frost 100
	SDL_Sprite <monster_dragons.png>
	Frame 9
	MonsterGP 500
	type <EVERY SWAMP WATER DRAGO RENFA>
	inv
		STC CLAW_d10
	end
	sub
		STC MFX_BW_FROST_3d6
	end

	Monster
	name <Sky Dragon>
	statline 13 19 19 13 13 1
	Move 12
	Class:Dragon 11
	RESIST:Lightning 100
	SDL_Sprite <monster_dragons.png>
	Frame 13
	MonsterGP 500
	type <EVERY GRASS AIR__ DRAGO RENFA>
	inv
		STC CLAW_d10
	end
	sub
		STC MFX_BW_LIGHTNING_3d10
	end

	Monster
	name <Cave Dragon>
	statline 18 20 13 14 12 1
	Move 12
	Class:Dragon 12
	RESIST:Acid 100
	SDL_Sprite <monster_dragons.png>
	Frame 8
	MonsterGP 600
	type <EVERY DUNGE EARTH CAVER WASTE DRAGO RENFA>
	inv
		STC CLAW_2d6
	end
	sub
		STC MFX_BW_ACID_3d6
	end

	Monster
	name <Solar Dragon>
	statline 16 20 16 13 13 1
	Move 12
	Class:Dragon 12
	RESIST:Lightning 100
	SDL_Sprite <monster_dragons.png>
	Frame 11
	MonsterGP 600
	type <EVERY GRASS AIR__ SOLAR DRAGO RENFA>
	inv
		STC CLAW_d12
	end
	sub
		STC MFX_BW_LIGHTNING_3d10
	end

	Monster
	name <Ice Dragon>
	statline 17 19 15 13 13 1
	Move 12
	Class:Dragon 13
	RESIST:Frost 100
	SDL_Sprite <monster_dragons.png>
	Frame 15
	MonsterGP 650
	type <EVERY SNOWY ICE__ DRAGO RENFA>
	inv
		STC CLAW_d12
	end
	sub
		STC MFX_BW_FROST_3d6
	end

	Monster
	name <Fire Dragon>
	statline 15 20 18 13 13 1
	Move 12
	Class:Dragon 13
	RESIST:Fire 100
	SDL_Sprite <monster_dragons.png>
	Frame 14
	MonsterGP 650
	type <EVERY DESER FIRE_ DRAGO RENFA>
	inv
		STC CLAW_d12
	end
	sub
		STC MFX_BW_FIRE_3d6
	end

