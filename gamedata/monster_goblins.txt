%%
%%  *******************
%%  ***   GOBLINS   ***
%%  *******************
%%
%% Unless there's a reason for it, all monsters should have a minimum Strength
%% of 10 and a maximum Luck of 1.
%%
%% Goblins, Hobgoblins, Orcs and Trolls
%%
%% Statline: STR TGH REF INT PIE LUC


	Monster
	name <Goblin>
	statline 10 7 12 8 6 1
	Move 12
	Class:Humanoid 1
	MonsterGP 5
	SDL_Sprite <monster_goblinoid.png>
	Frame 0
	type <EVERY WILDE FORES ORCS_ THIEF NORSE>
	inv
		STC BLUNT_d6
	end

	Monster
	name <Goblin Archer>
	statline 10 7 13 8 6 1
	Move 12
	Class:Humanoid 2
	MonsterGP 15
	SDL_Sprite <monster_goblinoid.png>
	Frame 26
	type <EVERY WILDE FORES ORCS_ NORSE>
	inv
		STC BLUNT_d6
	end
	sub
		STC MFX_BOW_d6
	end

	Monster
	name <Goblin Shaman>
	statline 10 5 12 10 10 1
	Move 12
	Class:Humanoid 3
	MonsterGP 150
	SDL_Sprite <monster_goblinoid.png>
	Frame 7
	type <EVERY WILDE FORES ORCS_ MAGE_ NORSE>
	inv
		STC BLUNT_d6
	end
	sub
		StdSpell Fire Arc
		StdSpell Wizard Missile
	end

	Monster
	name <Goblin Warrior>
	statline 10 8 12 8 6 1
	Move 10
	Class:Humanoid 4
	MonsterGP 50
	SDL_Sprite <monster_goblinoid.png>
	Frame 2
	type <EVERY WILDE FORES ORCS_ FIGHT NORSE>
	inv
		STC SHARP_d10
	end

	Monster
	name <Goblin Cook>
	statline 12 8 12 8 6 1
	Move 12
	Class:Humanoid 5
	MonsterGP 150
	SDL_Sprite <monster_goblinoid.png>
	Frame 39
	type <DUNGE FORES ORCS_ NORSE STEAM>
	inv
		STC POINTY_d10
	end

	Monster
	name <Goblin Champ>
	statline 11 10 13 9 7 10
	Move 10
	Class:Humanoid 6
	MonsterGP 90
	SDL_Sprite <monster_goblinoid.png>
	Frame 3
	type <EVERY WILDE FORES ORCS_ FIGHT NORSE>
	inv
		STC SHARP_d8
	end

	Monster
	name <Goblin Elite>
	statline 10 9 12 8 6 1
	Move 8
	Class:Humanoid 8
	MonsterGP 120
	SDL_Sprite <monster_goblinoid.png>
	Frame 5
	PhysDef 20
	type <EVERY WILDE FORES ORCS_ FIGHT NORSE>
	inv
		STC BLUNT_d10
	end


	Monster
	name <Hobgoblin>
	statline 12 10 12 10 10 1
	Move 10
	Class:Humanoid 2
	MonsterGP 10
	SDL_Sprite <monster_goblinoid.png>
	Frame 11
	type <EVERY SNOWY DESER ORCS_ LUNAR NORSE>
	inv
		STC SHARP_d8
	end

	Monster
	name <Hobgoblin Thief>
	statline 12 10 16 10 10 1
	Move 10
	Class:Humanoid 5
	MonsterGP 150
	SDL_Sprite <monster_goblinoid.png>
	Frame 15
	type <EVERY SNOWY DESER ORCS_ LUNAR THIEF NORSE>
	PhysDef 5
	inv
		STC SHARP_d6
	end

	Monster
	name <Hobgoblin Priest>
	statline 12 7 16 10 10 1
	Move 10
	Class:Humanoid 6
	MonsterGP 150
	SDL_Sprite <monster_goblinoid.png>
	Frame 14
	type <EVERY SNOWY DESER ORCS_ LUNAR MAGE_ NORSE>
	inv
		STC BLUNT_d8
	end
	sub
		StdSpell Freeze Foe
		StdSpell Blessing
		StdSpell Thunder Shock
	end

	Monster
	name <Orc>
	statline 14 12 10 10 8 1
	Move 10
	Class:Humanoid 3
	MonsterGP 30
	SDL_Sprite <monster_goblinoid.png>
	Frame 17
	type <EVERY DUNGE WASTE ORCS_ FIRE_>
	inv
		STC SHARP_d8
	end

	Monster
	name <Orc Archer>
	statline 14 12 10 10 8 1
	Move 12
	Class:Humanoid 4
	MonsterGP 40
	SDL_Sprite <monster_goblinoid.png>
	Frame 25
	type <EVERY DUNGE WASTE ORCS_ FIRE_>
	inv
		STC SHARP_d8
	end
	sub
		STC MFX_BOW_d8
	end

	Monster
	name <Orc Pirate>
	statline 14 14 10 10 8 1
	Move 10
	Class:Humanoid 6
	MonsterGP 180
	SDL_Sprite <monster_goblinoid.png>
	Frame 20
	type <EVERY SWAMP ORCS_ FIRE_ WATER THIEF>
	inv
		STC SHARP_d10
	end

	Monster
	name <Troll>
	statline 18 18 10 8 8 1
	Move 10
	Class:Humanoid 9
	MonsterGP 640
	SDL_Sprite <monster_goblinoid.png>
	Frame 28
	type <EVERY WASTE CAVE_ ORCS_ RENFA NORSE>
	inv
		STC BLUNT_2d6
	end
	sub
		STC MFX_REGENERATION_d10
	end




