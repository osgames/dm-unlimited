program convert32;

uses sdl,sdlgfx;

var
	MyDest: TSDL_Rect;

	MySprite: SensibleSpritePtr;
	X,Y,N: Integer;

begin
	ClrScreen;
	MyDest.X := 0;
	MyDest.Y := 0;
	MyDest.W := 800;
	MyDest.H := 600;
	SDL_FillRect( game_screen , @MyDest , SDL_MapRGBA( Game_Screen^.Format , 0 , 0 , 254 , 0 ) );
	SDL_SetColorKey( Game_Screen , SDL_SRCCOLORKEY or SDL_RLEACCEL , SDL_MapRGB( Game_Screen^.Format , 0 , 0 , 255 ) );

	MySprite := LocateSprite( 'convert.png' , 31 , 31 );
	N := 0;

	MyDest.W := 32;
	MyDest.H := 32;
	for Y := 1 to 15 do begin
		for X := 1 to 15 do begin
			MyDest.X := ( X - 1 ) * 54 + 6;
			MyDest.Y := ( Y - 1 ) * 40 + 4;

			SDL_FillRect( game_screen , @MyDest , SDL_MapRGBA( Game_Screen^.Format , 255 , 0 , 255 , 0 ) );
			DrawSprite( MySprite , MyDest , N );
			Inc( N );
		end;
	end;

	DoFlip;
	SDL_SaveBmp( Game_Screen , 'demo.bmp' );
end.
