unit joescript;
	{ This unit holds the scripting language for Dungeon Monkey Unlimited. }
	{ It's a stripped down version of GearHead's ASL originally created }
	{ for NovaCity. }

	{ Certain game events will add a trigger to the }
	{ list. In the main combat procedure, if there are any pending }
	{ triggers, they will be checked against the events list to }
	{ see if anything happens. }

	{ Both the triggers and the event scripts will be stored as }
	{ normal string attributes. }
{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses sdl,sdlmenus,texutil,gears,gamebook,sdlmap,sdlgfx,frontend;

var
	Grabbed_Gear: GearPtr;	{ This gear can be acted upon by }
				{ generic commands. }


Function ScriptValue( GB: GameBoardPtr; var Event: String; Source: GearPtr ): LongInt;

Function ScriptMessage( GB: GameBoardPtr; msg_label: String; Source: GearPtr ): String;

Procedure InvokeEvent( GB: GameBoardPtr; Event: String; Source: GearPtr; var Trigger: String );

Procedure TriggerGearScript( GB: GameBoardPtr; Source: GearPtr; var Trigger: String );
Procedure HandleTriggers( GB: GameBoardPtr );


implementation

uses grabgear,uiconfig,services,equipment,gearparser;

const
{	ZONE_JSIOMessage: TSDL_Rect = ( x: screenwidth div 2 - 200; y: screenheight - 215 ; w: 400; h: 115 );}
	ZONE_JSIOCaption: TSDL_Rect = ( x: screenwidth div 2 - 220; y: screenheight - 255 ; w: 250; h: 15 );

	ZONE_JSIOMenu: TSDL_Rect = ( x: screenwidth div 2 - 200; y: screenheight - 215 ; w: 400; h: 115 );

	Script_Macro_File = Data_Directory + 'aslmacro.txt';
	Value_Macro_File = Data_Directory + 'asvmacro.txt';

var
	script_macros,value_macros: SAttPtr;

	{ JoeScript Input/Output variables }
	JSIO_Caption: String;
	JSIO_Menu: RPGMenuPtr;
	JSIO_GB: GameBoardPtr;


Procedure InitiateMacro( var Event: String; ProtoMacro: String );
	{ Initialize the provided macro, and stick it to the head of }
	{ event. To initialize just go through it word by word, replacing }
	{ all question marks with words taken from EVENT. }
	function NeededParameters( cmd: String ): Integer;
		{ Return the number of parameters needed by this function. }
	const
		NumStandardFunctions = 5;
		StdFunName: Array [1..NumStandardFunctions] of string = (
			'-', 'GNATT', 'GSTAT','*','NU'
		);
		StdFunParam: Array [1..NumStandardFunctions] of byte = (
			1, 2, 1, 2, 1
		);
	var
		it,T: Integer;
		Mac: String;
	begin
		it := 0;
		CMD := UpCase( CMD );
		{ If a hardwired function, look up the number of parameters }
		{ from the above list. }
		for t := 1 to NumStandardFunctions do begin 
			if CMD = StdFunName[ t ] then it := StdFunParam[ t ];
		end;

		{ If a macro function, derive the number of parameters }
		{ from the number of ?s in the macro. }
		if it = 0 then begin
			Mac := SAttValue( Value_Macros , Cmd );
			if Mac <> '' then begin
				for t := 1 to Length( Mac ) do begin
					if Mac[ T ] = '?' then Inc( it );
				end;
			end;
		end;
		NeededParameters := it;
	end;
	function  GetSwapText: String;
		{ Grab the swap text from EVENT. If it's a function }
		{ that we just grabbed, better grab the parameters as }
		{ well. Oh, bother... }
	var
		it,cmd: String;
		N: Integer;
	begin
		{ Grab the beginning of the swap text. }
		it := ExtractWord( Event );

		{ Check to see if it's a function. Get rid of the - first }
		{ since it'll screw up our check. }
		cmd := it;
		if ( Length( cmd ) > 1 ) and ( cmd[1] = '-' ) then DeleteFirstChar( cmd );
		N := NeededParameters( cmd );
		While N > 0 do begin
			it := it + ' ' + GetSwapText();
			Dec( N );
		end;
		GetSwapText := it;
	end;
var
	Cmd,NewMacro,LastWord: String;
begin
	NewMacro := '';
	LastWord := '';

	while ProtoMacro <> '' do begin
		cmd := ExtractWord( ProtoMacro );
		if cmd = '?' then begin
			LastWord := GetSwapText;
			cmd := LastWord;
		end else if cmd = '!' then begin
			cmd := LastWord;
		end;
		NewMacro := NewMacro + ' ' + cmd;
	end;

	Event := NewMacro + ' ' + Event;
end;

Function NumberOfUnits( GB: GameBoardPtr; MID: LongInt ): Integer;
	{ Return the number of alright models on the gameboard belonging to }
	{ this mob. }
var
	M: GearPtr;
	N: Integer;
begin
	N := 0;
	M := GB^.Contents;
	while M <> Nil do begin
		if ( M^.G = GG_Model ) and IsAlright( M ) and ( NAttValue( M^.NA , NAG_StoryData , NAS_MobID ) = MID ) and OnTheMap( GB , M ) then Inc( N );
		M := M^.Next;
	end;
	NumberOfUnits := N;
end;

Function ScriptValue( GB: GameBoardPtr; var Event: String; Source: GearPtr ): LongInt;
	{ Normally, numerical values will be stored as constants. }
	{ Sometimes we may want to do algebra, or use the result of }
	{ scenario variables as the parameters for commands. That's }
	{ what this function is for. }
var
	Old_Grabbed_Gear: GearPtr;
	VCode,VC2: LongInt;
	SV: LongInt;
	SMsg,S2: String;
begin
	{ Save the grabbed gear, to restore it later. }
	Old_Grabbed_Gear := Grabbed_Gear;

	SMsg := ExtractWord( Event );
	SV := 0;

	{ If the first character is one of the value commands, }
	{ process the string as appropriate. }
	if SMsg = '' then begin
		{ An empty string is a bad thing. }
		SV := 0;
		Alert( GB , 'WARNING: Blank scriptvalue from ' + GearName( Source ) );

	end else if SAttValue( Value_Macros , SMsg ) <> '' then begin
		{ Install the macro, then re-call this procedure to get }
		{ the results. }
		InitiateMacro( Event , SAttValue( Value_Macros , SMsg ) );
		SV := ScriptValue( GB , Event , Source );

	end else if ( UpCase( SMsg ) = 'GNATT' ) then begin
		{ Get a Numeric Attribute from the currently grabbed gear. }
		VCode := ScriptValue( GB , Event , Source );
		VC2 := ScriptValue( GB , Event , Source );
		if Grabbed_Gear <> Nil then begin
			SV := NAttValue( Grabbed_Gear^.NA , VCode , VC2 );
		end;

	end else if ( UpCase( SMsg ) = 'GSTAT' ) then begin
		{ Get a Numeric Attribute from the currently grabbed gear. }
		VCode := ScriptValue( GB , Event , Source );
		if ( Grabbed_Gear <> Nil ) then begin
			if ( VCode >= 1 ) and ( VCode <= NumGearStats ) then begin
				SV := Grabbed_Gear^.Stat[ VCode ];
			end;
		end;

	end else if Attempt_Gear_Grab( GB , SMsg , Event , Source ) then begin
		{ The correct Grabbed_Gear was set by the above call, }
		{ so just recurse to find the value we want. Am I looming }
		{ closer to functional programming or something? }
		SV := ScriptValue( GB , Event , Source );

	end else if ( UpCase( SMsg ) = '*' ) then begin
		VCode := ScriptValue( GB , Event , Source );
		VC2 := ScriptValue( GB , Event , Source );
		SV := VCode * VC2;

	end else if ( UpCase( SMsg[1] ) = 'D' ) then begin
		{ Roll a die, return the result. }
		DeleteFirstChar( SMsg );
		Event := SMsg + ' ' + Event;
		VCode := ScriptValue( GB , Event , Source );
		if VCode < 1 then VCode := 1;
		SV := Random( VCode ) + 1;

	end else if ( UpCase( SMsg ) = 'NU' ) then begin
		VCode := ScriptValue( GB , Event , Source );
		SV := NumberOfUnits( GB , VCode );

	{ As a last resort, see if the first character shows up in the }
	{ scripts file. If so, use that. }
	end else if SAttValue( Value_Macros , SMsg[1] ) <> '' then begin
		{ Install the macro, then re-call this procedure to get }
		{ the results. }
		S2 := SMsg[ 1 ];
		DeleteFirstChar( SMsg );
		Event := SMsg + ' ' + Event;
		InitiateMacro( Event , SAttValue( Value_Macros , S2 ) );
		SV := ScriptValue( GB , Event , Source );

	end else if SMsg[1] = '-' then begin
		{ We want the negative of the value to follow. }
		DeleteFirstChar( SMsg );
		event := SMsg + ' ' + event;
		SV := -ScriptValue( GB , event , Source );

	end else begin
		{ No command was given, so this must be a constant value. }
		S2 := SMsg;
		SV := ExtractValue( SMsg );
		if ( SV = 0 ) and ( S2 <> '' ) and ( S2 <> '0' ) then begin
			Alert( GB , 'WARNING: Script value ' + S2 + ' in ' + GearName( Source ) + ' CONTEXT: ' + event );
		end;

	end;

	{ Restore the grabbed gear before exiting. }
	Grabbed_Gear := Old_Grabbed_Gear;

	ScriptValue := SV;
end;

Procedure FormatMessageString( GB: GameBoardPtr; var msg: String; Source: GearPtr );
	{ There are a number of formatting commands which may be used }
	{ in an arenascript message string. }
var
	S0,S1,w: String;
	ID: LongInt;
begin
	S0 := msg;
	S1 := '';

	while S0 <> '' do begin
		w := ExtractWord( S0 );

		if UpCase( W ) = '\VAL' then begin
			{ Insert the value of a specified variable. }
			ID := ScriptValue( GB , S0 , Source );
			W := BStr( ID );
		end else if UpCase( W ) = '\MODEL' then begin
			{ Insert the name of a specified model. }
			ID := ScriptValue( GB , S0 , Source );
			W := GearName( GG_LocateModel( GB , ID ) );
		end else if UpCase( W ) = '\SCENE' then begin
			W := GearName( GB^.Scene );
		end;

		if ( W <> '' ) and ( S1 <> '' ) and ( IsPunctuation( W[1] ) or ( S1[Length(S1)] = '$' ) or ( S1[Length(S1)] = '@' ) ) then begin
			S1 := S1 + W;
		end else begin
			S1 := S1 + ' ' + W;
		end;

	end;

	msg := S1;
end;

Function ConditionAccepted( GB: GameBoardPtr; Event: String; Source: GearPtr ): Boolean;
	{ Run a conditional script. }
	{ If it returns 'ACCEPT', this function returns true. }
var
	T: String;	{ The trigger to be used. }
begin
	{ Error check - an empty condition is always true. }
	if Event = '' then Exit( True );

	{ Generate the trigger. }
	T := 'null';

	{ Execute the provided event. }
	InvokeEvent( GB , Event , Source , T );

	{ If the trigger was changed, that counts as a success. }
	ConditionAccepted := T = 'ACCEPT';
end;

Function ScriptMessage( GB: GameBoardPtr; msg_label: String; Source: GearPtr ): String;
	{ Retrieve and format a message from the source. }
var
	N,T: LongInt;
	C,msg: String;
	MList,M: SAttPtr;
begin
	{ Create the list of possible strings. }
	MList := Nil;
	C := SAttValue( Source^.SA , 'C' + msg_label );

	{ The master condition must be accepted in order to continue. }
	if ConditionAccepted( GB , C , Source ) and ( Source <> Nil ) then begin
		msg := SAttValue( Source^.SA , msg_label );
		if msg <> '' then StoreSAtt( MList , msg );

		msg := msg_label + '_';
		N := NumHeadMatches( msg , Source^.SA );
		for t := 1 to N do begin
			M := FindHeadMatch( msg , Source^.SA , T);
			C := SAttValue( Source^.SA , 'C' + RetrieveAPreamble( M^.info ) );
			if ConditionAccepted( GB , C , Source ) then begin
				StoreSAtt( MList , RetrieveAString( M^.Info ) );
			end;
		end;
	end;

	{ If any messages were found, pick one. }
	if MList <> Nil then begin
		msg := SelectRandomSAtt( MList )^.Info;
		DisposeSAtt( MList );
		FormatMessageString( GB , msg , source );
	end else begin
		msg := '';
	end;

	ScriptMessage := Msg;
end;

Function GetTheMessage( GB: GameBoardPtr; head: String; idnum: LongInt; Scene: GearPtr ): String;
	{ Just call the SCRIPTMESSAGE with the correct label. }
begin
	GetTheMessage := ScriptMessage( GB , head + BStr( idnum ) , Scene );
end;

Procedure ProcessGSetNAtt( GB: GameBoardPtr; var Event: String; source: GearPtr );
	{ The script is going to assign a value to one of the scene }
	{ variables. }
var
	G,S: LongInt;
	V: LongInt;
begin
	{ Find the variable ID number and the value to assign. }
	G := ScriptValue( GB , event , source );
	S := ScriptValue( GB , event , source );
	V := ScriptValue( GB , event , source );

	if Grabbed_Gear <> Nil then SetNAtt( Grabbed_Gear^.NA , G , S , V );
end;

Procedure ProcessGAddNAtt( GB: GameBoardPtr; var Event: String; source: GearPtr );
	{ The script is going to add a value to one of the scene }
	{ variables. }
var
	G,S: LongInt;
	V: LongInt;
begin
	{ Find the variable ID number and the value to assign. }
	G := ScriptValue( GB , event , source );
	S := ScriptValue( GB , event , source );
	V := ScriptValue( GB , event , source );

	if Grabbed_Gear <> Nil then AddNAtt( Grabbed_Gear^.NA , G , S , V );
end;

Procedure ProcessGSetStat( GB: GameBoardPtr; var Event: String; source: GearPtr );
	{ The script is going to add a value to one of the scene }
	{ variables. }
var
	Slot,Value: LongInt;
begin
	{ Find the variable ID number and the value to assign. }
	Slot := ScriptValue( GB , event , source );
	Value := ScriptValue( GB , event , source );

	if Grabbed_Gear <> Nil then Grabbed_Gear^.Stat[ Slot ] := Value;
end;

Procedure ProcessGAddStat( GB: GameBoardPtr; var Event: String; source: GearPtr );
	{ The script is going to add a value to one of the scene }
	{ variables. }
var
	Slot,Value: LongInt;
begin
	{ Find the variable ID number and the value to assign. }
	Slot := ScriptValue( GB , event , source );
	Value := ScriptValue( GB , event , source );

	if Grabbed_Gear <> Nil then Grabbed_Gear^.Stat[ Slot ] := Grabbed_Gear^.Stat[ Slot ] + Value;
end;

Procedure ProcessGSetSAtt( var Event: String; Source: GearPtr );
	{ Store a string attribute in the grabbed gear. }
var
	Key,Info: String;
begin
	Key := ExtractWord( Event );
	Info := ExtractWord( Event );
	if Source <> Nil then Info := SAttValue( Source^.SA , Info );
	if Grabbed_Gear <> Nil then SetSAtt( Grabbed_Gear^.SA , Key + ' <' + Info + '>' );
end;

Procedure IfSuccess( var Event: String );
	{ An IF call has generated a "TRUE" result. Just get rid of }
	{ any ELSE clause that the event string might still be holding. }
var
	cmd: String;
begin
	{ Extract the next word from the script. }
	cmd := ExtractWord( Event );

	{ If the next word is ELSE, we have to also extract the label. }
	{ If the next word isn't ELSE, better re-assemble the line... }
	if UpCase( cmd ) = 'ELSE' then ExtractWord( Event )
	else Event := cmd + ' ' + Event;
end;

Procedure IfFailure( var Event: String; source: GearPtr );
	{ An IF call has generated a "FALSE" result. See if there's }
	{ a defined ELSE clause, and try to load the next line. }
var
	cmd: String;
begin
	{ Extract the next word from the script. }
	cmd := ExtractWord( Event );

	if UpCase( cmd ) = 'ELSE' then begin
		{ There's an else clause. Attempt to jump to the }
		{ specified script line. }
		cmd := ExtractWord( Event );
		Event := SAttValue( source^.SA , CMD );

	end else begin
		{ There's no ELSE clause. Just cease execution of this }
		{ line by setting it to an empty string. }
		Event := '';
	end;
end;

Procedure ProcessIfEqual( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Two values are supplied as the arguments for this procedure. }
	{ If they are equal, that's a success. }
var
	a,b: LongInt;
begin
	{ Determine the two values. }
	A := ScriptValue( GB , Event , Source );
	B := ScriptValue( GB , Event , Source );

	if A = B then IfSuccess( Event )
	else IfFailure( Event , Source );
end;

Procedure ProcessIfNotEqual( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Two values are supplied as the arguments for this procedure. }
	{ If they are not equal, that's a success. }
var
	a,b: LongInt;
begin
	{ Determine the two values. }
	A := ScriptValue( GB , Event , Source );
	B := ScriptValue( GB , Event , Source );

	if A <> B then IfSuccess( Event )
	else IfFailure( Event , Source );
end;

Procedure ProcessIfGreater( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Two values are supplied as the arguments for this procedure. }
	{ If A > B, that's a success. }
var
	a,b: LongInt;
begin
	{ Determine the two values. }
	A := ScriptValue( GB , Event , Source );
	B := ScriptValue( GB , Event , Source );

	if A > B then IfSuccess( Event )
	else IfFailure( Event , Source );
end;

Procedure ProcessIfGOK( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Check the Grabbed_Gear. If on the map and alright, count as success. }
begin
	if ( Grabbed_Gear <> Nil ) and OnTheMap( GB , Grabbed_Gear ) and IsAlright( Grabbed_Gear ) then IfSuccess( Event )
	else IfFailure( Event , Source );
end;

Procedure ProcessIfGOut( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Opposite of the above procedure. If on the map and alright, count as failure. }
begin
	if ( Grabbed_Gear <> Nil ) and OnTheMap( GB , Grabbed_Gear ) and IsAlright( Grabbed_Gear ) then IfFailure( Event , Source )
	else IfSuccess( Event );
end;

Procedure ProcessCompose( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ A new item is going to be added to the scene list. }
var
	Trigger,Ev2: String;
	P: LongInt;
begin
	if Source = Nil then exit;

	{ Extract the values we need. }
	Trigger := ExtractWord( Event );
	P := ScriptValue( GB , Event , Source );
	Ev2 := SAttValue( Source^.SA , ExtractWord( Event ) );

	StoreSAtt( Source^.SA , Trigger + BStr( P ) + ' <' + Ev2 + '>' );
end;

Procedure ProcessExit( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ The party is moving to somewhere else. }
var
	Dest: LongInt;
	Scene: GearPtr;
begin
	if Source = Nil then exit;

	{ Extract the values we need. }
	Dest := ScriptValue( GB , Event , Source );

	{ Check to make sure this is a legal destination. }
	Scene := SeekGearByIDTag( GB^.Camp^.Source , NAG_StoryData , NAS_UniqueID , Dest );
	if ( Scene <> Nil ) and ( Scene^.G = GG_Scene ) then begin
		G_Destination := Dest;
	end;
end;

Procedure ProcessGoto( var Event: String; Source: GearPtr );
	{ Attempt to jump to a different line of the script. }
	{ If no line label is provided, or if the label can't be }
	{ found, this procedure sets EVENT to an empty string. }
var
	destination: String;
begin
	{ Error check- if there's no defined source, we can't very }
	{ well jump to another line, can we? }
	if Source = Nil then begin
		Event := '';
		Exit;
	end;

	destination := ExtractWord( Event );
	if destination <> '' then begin
		{ Change the event script to the requested line. }
		Event := SAttValue( Source^.SA , destination );
	end else begin
		{ No label was provided. Just return a blank line. }
		Event := '';
	end;
end;

Procedure ProcessPrint( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Print a message in the JSIO_Message area. }
var
	msg: String;
	id: LongInt;
begin
	id := ScriptValue( GB , Event , Source );
	msg := getTheMessage( GB , 'msg', id , Source );
	if msg <> '' then Alert( GB , msg );
end;

Procedure ProcessHistory( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Print a message in the JSIO_Message area. }
var
	msg: String;
	id: LongInt;
begin
	id := ScriptValue( GB , Event , Source );
	msg := getTheMessage( GB , 'msg', id , Source );
	if ( msg <> '' ) and ( GB^.Camp <> Nil ) then AddSAtt( GB^.Camp^.Source^.SA , 'HISTORY' , msg );
end;

Procedure ProcessCaption( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Print a message in the JSIO_Caption area. }
var
	msg: String;
	id: LongInt;
begin
	id := ScriptValue( GB , Event , Source );
	msg := getTheMessage( GB , 'msg', id , Source );
	JSIO_Caption := msg;
end;


Procedure ProcessAddMenu( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Add a prompt to JSIO_Menu. }
var
	msg: String;
	id: LongInt;
begin
	id := ScriptValue( GB , Event , Source );
	msg := getTheMessage( GB , 'PROMPT', id , Source );
	AddRPGMenuItem( JSIO_Menu , msg , id );
end;

Procedure ProcessMoreText( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Load and display a text file. }
var
	FName: String;
	txt,L: SAttPtr;
begin
	{ First, find the file name of the text file to look for. }
	FName := ExtractWord( Event );
	if Source <> Nil then begin
		FName := SAttValue( Source^.SA , FName );
	end else begin
		FName := '';
	end;

	{ Secondly, load and display the file. }
	if FName <> '' then begin
		txt := LoadStringList( FName );


		if txt <> Nil then begin
			{ Process the text. }
			L := txt;
			while L <> Nil do begin
				FormatMessageString( GB , L^.Info , Source );
				L := L^.Next;
			end;

			MoreText( txt , 1 );
			DisposeSAtt( txt );
		end;
	end;

end; { ProcessMoreText }

Procedure ProcessBlock( var T: String );
	{ Erase the trigger, so as to prevent other narrative gears }
	{ from acting upon it. }
begin
	{ Do I really need to comment this line? }
	T := '';
end;

Procedure ProcessAccept( var T: String );
	{ Set the trigger to ACCEPT so the CONDITIONACCEPTED function }
	{ knows that it's been accepted. }
begin
	{ Do I really need to comment this line? }
	T := 'ACCEPT';
end;

Procedure ProcessIfSkillRoll( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ The party needs to succeed at a skill roll. Take the greatest skill }
	{ value present and compare it to the difficulty level of the scene. }
var
	skill,stat,hi,c,t: LongInt;
begin
	{ Determine the two values. }
	skill := ScriptValue( GB , Event , Source );
	stat := ScriptValue( GB , Event , Source );

	{ Determine the highest value in the party. }
	hi := 0;
	for t := 1 to Num_Party_Members do begin
		if IsAlright( G_Party[ t ] ) then begin
			C := ComScore( G_Party[ t ] , skill ) + StatBonus( G_Party[ t ] , stat );
			if C > hi then hi := C;
		end;
	end;

	{ Roll off against the local difficulty rating. }
	skill := Random( 100 ) + hi;
	T := 46 + NAttValue( GB^.Scene^.NA , NAG_StoryData , NAS_DifficultyLevel ) * 4;

	if skill > T then IfSuccess( Event )
	else IfFailure( Event , Source );
end;


Procedure ProcessMagicMap( GB: GameBoardPtr );
	{ Make every tile on the map visible, then redraw. }
var
	X,Y: LongInt;
begin
	for X := 1 to GB^.Map_Width do begin
		for Y := 1 to GB^.Map_Height do begin
			SetVisibility( GB , X , Y , True );
		end;
	end;
end;

Procedure ProcessOpenGuild( GB: GameBoardPtr );
	{ Open the advancement guild. Yay! }
begin
	AdvancementGuild( GB );
end;

Procedure ProcessOpenInn( GB: GameBoardPtr );
	{ Open the adventurer's inn. Yay! }
begin
	OpenInn( GB );
end;

Procedure ProcessOpenInv( GB: GameBoardPtr; Source: GearPtr );
	{ Open the inventory for this source gear and trade with the PC. }
var
	GP: LongInt;
begin
	{ First, see if there's any gold in the container. }
	GP := NAttValue( Source^.NA , NAG_CampaignData , NAS_Gold );
	if GP > 0 then begin
		Alert( GB , ReplaceHash( ReplaceHash( MsgString( 'OPENINV_GP' ) , BStr( GP ) ) , GearName( Source ) ) );
		SetNAtt( Source^.NA , NAG_CampaignData , NAS_Gold , 0 );
		AddNAtt( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold , GP );
	end;

	{ Next, trade the items. }
	PCTradeItems( GB , Source );
end;

Procedure ProcessOpenLibrary( GB: GameBoardPtr );
	{ Open the library. Yay! }
begin
	OpenLibrary( GB );
end;

Procedure ProcessOpenShop( GB: GameBoardPtr; Source: GearPtr );
	{ Open the shop. Yay! }
begin
	if ( Source <> Nil ) and ( Source^.InvCom <> Nil ) then OpenShop( Source^.InvCom , GB );
end;

Procedure ProcessOpenTemple( GB: GameBoardPtr );
	{ Open the temple. Yay! }
begin
	OpenTemple( GB );
end;

Procedure ProcessTrigger( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ A new trigger will be placed in the trigger queue. }
var
	BaseTrigger: String;
begin
	{ Find out the trigger's details. }
	BaseTrigger := ExtractWord( Event );

	StoreSAtt( G_Triggers , BaseTrigger );
end;


Procedure ProcessTriggerEffect( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ We want to trigger an effect. Yay! Create an effect template, then }
	{ pass everything to the frontend procedure. }
var
	fx_desig: String;
	target,X,Y: LongInt;
	FX,PC: GearPtr;
	FX_Stencil: effect_stencil;
begin
	{ Determine the effect prototype and the target. }
	fx_desig := ExtractWord( Event );
	target := ScriptValue( GB , Event , Source );

	FX := LoadNewSTC( fx_desig );
	if FX = Nil then Exit;

	Clear_Effect_Stencil( GB , FX_Stencil );
	if target = 0 then begin
		{ Select a single PC. }
		PC := RandomPC( GB );
		if PC <> Nil then begin
			X := NAttValue( PC^.NA , NAG_Location , NAS_X );
			Y := NAttValue( PC^.NA , NAG_Location , NAS_Y );
			if OnTheMap( GB , X , Y ) then FX_Stencil[ X , Y ] := True;
		end;
	end else begin
		{ Target all PCs simultaneously. }
		for target := 1 to Num_Party_Members do begin
			if G_Party[ target ] <> Nil then begin
				X := NAttValue( G_Party[ target ]^.NA , NAG_Location , NAS_X );
				Y := NAttValue( G_Party[ target ]^.NA , NAG_Location , NAS_Y );
				if OnTheMap( GB , X , Y ) then FX_Stencil[ X , Y ] := True;
			end;
		end;
	end;

	EffectFrontEnd( GB , FX , Nil , FX_Stencil );
	DisposeGear( FX );
end;

Procedure ProcessUnlockMenu();
	{ Set the JSIO_Menu so that ESC escapes. }
begin
	JSIO_Menu^.Mode := RPMNormal;
end;

Procedure ProcessXPV( GB: GameBoardPtr; var Event: String; Source: GearPtr );
	{ Give an XP award to all PCs. }
var
	XP,T: LongInt;
begin
	XP := ScriptValue( GB , Event , Source );
	for t := 1 to Num_Party_Members do if G_Party[ t ] <> Nil then AddNAtt( G_Party[ t ]^.NA , NAG_CharacterData , NAS_Experience , XP );
end;


Procedure InvokeEvent( GB: GameBoardPtr; Event: String; Source: GearPtr; var Trigger: String );
	{ Do whatever is requested by game script EVENT. }
	{ SOURCE refers to the virtual gear which is currently being }
	{ used- it may be a SCENE gear, or a CONVERSATION gear, or }
	{ whatever else I might add in the future. }
var
	cmd: String;
begin
	{ Process the event. }
	while ( Event <> '' ) do begin
		cmd := UpCase( ExtractWord( Event ) );

		if SAttValue( Script_Macros , cmd ) <> '' then begin
			{ Install the macro. }
			InitiateMacro( Event , SAttValue( Script_Macros , cmd ) );

		end else if not Attempt_Gear_Grab( GB , Cmd , Event , Source ) then begin
			{ If this is a gear-grabbing command, our work here is done. }

			if cmd = 'GADDNATT' then ProcessGAddNAtt( GB , Event , Source )
			else if cmd = 'GSETNATT' then ProcessGSetNAtt( GB , Event , Source )
			else if cmd = 'GADDSTAT' then ProcessGAddStat( GB , Event , Source )
			else if cmd = 'GSETSTAT' then ProcessGSetStat( GB , Event , Source )
			else if cmd = 'GSETSATT' then ProcessGSetSAtt( Event , Source )
			else if cmd = 'IF=' then ProcessIfEqual( GB , Event , Source )
			else if cmd = 'IF#' then ProcessIfNotEqual( GB , Event , Source )
			else if cmd = 'IFG' then ProcessIfGreater( GB , Event , Source )
			else if cmd = 'IFGOK' then ProcessIfGOK( GB , Event , Source )
			else if cmd = 'IFGOUT' then ProcessIfGOut( GB , Event , Source )
			else if cmd = 'COMPOSE' then ProcessCompose( GB , Event , Source )
			else if cmd = 'BLOCK' then ProcessBlock( Trigger )
			else if cmd = 'ACCEPT' then ProcessAccept( Trigger )
			else if cmd = 'ADDMENU' then ProcessAddMenu( GB , Event , Source )
			else if cmd = 'CAPTION' then ProcessCaption( GB , Event , Source )
			else if cmd = 'EXIT' then ProcessExit( GB , Event , Source )
			else if cmd = 'GOTO' then ProcessGoto( Event , Source )
			else if cmd = 'HISTORY' then ProcessHistory( GB , Event , Source )
			else if cmd = 'IFSKILLROLL' then ProcessIfSkillRoll( GB , Event , Source )
			else if cmd = 'MAGICMAP' then ProcessMagicMap( GB )
			else if cmd = 'MORETEXT' then ProcessMoreText( GB , Event , Source )
			else if cmd = 'OPENGUILD' then ProcessOpenGuild( GB )
			else if cmd = 'OPENINN' then ProcessOpenInn( GB )
			else if cmd = 'OPENINV' then ProcessOpenInv( GB , Source )
			else if cmd = 'OPENLIBRARY' then ProcessOpenLibrary( GB )
			else if cmd = 'OPENSHOP' then ProcessOpenShop( GB , Source )
			else if cmd = 'OPENTEMPLE' then ProcessOpenTemple( GB )
			else if cmd = 'PRINT' then ProcessPrint( GB , Event , Source )
			else if cmd = 'TRIGGER' then ProcessTrigger( GB , Event , Source )
			else if cmd = 'TRIGGEREFFECT' then ProcessTriggerEffect( GB , Event , Source )
			else if cmd = 'UNLOCKMENU' then ProcessUnlockMenu()
			else if cmd = 'XPV' then ProcessXPV( GB , Event , Source )

			else if cmd <> '' then Alert( GB , 'ERROR: Unknown JSL command ' + cmd + ' in line: ' + event )
			;
		end; { If not GrabGear }
	end;
end;

Procedure JSIO_Redraw;
	{ Prepare a redraw for the message, the menu, and the caption if appropriate. }
begin
	BasicRedraw( JSIO_GB );

	if JSIO_Menu^.NumItem > 0 then begin
		InfoBox( ZONE_JSIOMenu );
	end;
	if JSIO_Caption <> '' then begin
		InfoBox( ZONE_JSIOCaption );
		CMessage( JSIO_Caption , ZONE_JSIOCaption , TextColor );
	end;
end;

Procedure HandleJSIO( GB: GameBoardPtr; var Trigger: String );
	{ Handle Input/Output for JoeScript. This consists of printing messages, }
	{ querying the menu, and waiting for the user to acknowledge. }
var
	N: LongInt;
begin
	JSIO_GB := GB;

	if JSIO_Menu^.NumItem > 0 then begin
		RPMSortAlpha( JSIO_Menu );
		N := SelectMenu( JSIO_Menu , @JSIO_Redraw );
		Trigger := 'RESULT' + BStr( N );
		ClearOverlays;
	end else if ( JSIO_Caption <> '' ) then begin
		repeat
			JSIO_Redraw;
			DoFlip;
		until IsMoreKey( RPGKey );
		ClearOverlays;
	end;
end;

Procedure TriggerGearScript( GB: GameBoardPtr; Source: GearPtr; var Trigger: String );
	{ Attempt to trigger the requested script in this gear. If the }
	{ script cannot be found, then do nothing. }
var
	LastTrigger,E: String;
begin
	if Source <> Nil then begin
		repeat
			{ Start by initializing the IO bits. }
			JSIO_Caption := '';
			JSIO_GB := GB;

			JSIO_Menu^.Mode := RPMNoCancel;
			ClearMenu( JSIO_Menu );

			ClearOverlays;
			LastTrigger := Trigger;

			{ Process the scripts. }
			E := SAttValue( Source^.SA , Trigger );
			if E <> '' then begin
				InvokeEvent( GB , E , Source , Trigger );
			end;

			{ Handle the IO. }
			{ If there are menu items to process or messages to display, do that here. }
			if JSIO_Menu^.NumItem > 0 then LastTrigger := 'na';
			HandleJSIO( GB , Trigger );

		{ If the trigger hasn't changed, don't execute the same script twice. }
		until ( Trigger = '' ) or ( Trigger = LastTrigger );
	end;
end;

Procedure HandleTriggers( GB: GameBoardPtr );
	{ Go through the list of triggers, enacting events if any are }
	{ found. Deallocate the triggers as they are processed. }
var
	TList,TP: SAttPtr;	{ Trigger List , Trigger Pointer }
	Source: GearPtr;
begin
	{ Only try to implement triggers if this gameboard has a scenario }
	{ defined. }
	if GB^.Scene <> Nil then begin

		{ Some of the events we process might add their own }
		{ triggers to the list. So, we check all the triggers }
		{ currently set, then look at the GB^.Trig list again }
		{ to see if any more got put there. }
		while G_Triggers <> Nil do begin
			{ Copy the list pointer to TList, clear the }
			{ list pointer from GB, and set the pointer }
			{ to the first trigger. }
			TList := G_Triggers;
			G_Triggers := Nil;
			TP := TList;

			while TP <> Nil do begin
				if TP^.Info <> '' then begin
					Source := GB^.Scene;
					while Source <> Nil do begin
						TriggerGearScript( GB , Source , TP^.Info );
						Source := Source^.Parent;
					end;
				end;

				TP := TP^.Next;
			end;

			{ Get rid of the trigger list. }
			DisposeSAtt( TList );

		end;
	end;
end;

initialization
	Grabbed_Gear := Nil;
	Script_Macros := LoadStringList( Script_Macro_File );
	Value_Macros := LoadStringList( Value_Macro_File );

	JSIO_Menu := CreateRPGMenu( MenuItem , MenuSelect , ZONE_JSIOMenu );

finalization
	DisposeSAtt( Script_Macros );
	DisposeSAtt( Value_Macros );
	DisposeRPGMenu( JSIO_Menu );

end.
