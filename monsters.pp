unit monsters;
	{ This unit deals with monsters. }

{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses gears;

Function GenerateMonsterFromPrototype( Proto: GearPtr ): GearPtr;

Function ExperienceValue( M: GearPtr; PLvl: Integer ): LongInt;

Function MonsterContext( Scene: GearPtr ): String;
Function BuildEncounter( const MReq: String; Lvl,Strength: Integer ): GearPtr;
Function BuildBoss( const MReq: String; Lvl: Integer ): GearPtr;

implementation

uses gamebook,gearparser,texutil,equipment;

Function GenerateMonsterFromPrototype( Proto: GearPtr ): GearPtr;
	{ Given a prototype monster, clone and initialize it. }
var
	Mon: GearPtr;
	T,Lvl: Integer;
begin
	{ Start by cloning the prototype. We do this because we don't want to }
	{ get our monster manual all marked up with crayon. }
	Mon := CloneGear( Proto );

	{ All monsters get a default package of ComScores, because the low }
	{ level default scores suck. }
	AddNAtt( Mon^.NA , NAG_ComScoreMod , CS_PhysicalAttack , 5 );
	AddNAtt( Mon^.NA , NAG_ComScoreMod , CS_PhysicalDefense , 5 );
	AddNAtt( Mon^.NA , NAG_ComScoreMod , CS_MagicalAttack , 5 );

	{ Randomize the stats a bit. }
	for t := 1 to Num_Model_Stats do begin
		Mon^.Stat[ t ] := Mon^.Stat[ t ] + Random( 2 ) - Random( 2 );
		if Random( 20 ) = 9 then Mon^.Stat[ t ] := Mon^.Stat[ t ] + Random( 3 ) - Random( 3 );
		if Mon^.Stat[ t ] < 1 then Mon^.Stat[ t ] := 1;
	end;

	{ Apply the monster's class levels. This will give it some HP and MP }
	{ so it can do stuff and, y'know, live. }
	for t := 1 to Num_Classes do begin
		Lvl := NAttValue( Mon^.NA , NAG_ClassLevels , t );
		while Lvl > 0 do begin
			ApplyClassLevel( Mon , T );
			Dec( Lvl );
		end;
	end;

	{ Randomize the monster's GP. }
	if NAttValue( Mon^.NA , NAG_MonsterData , NAS_MonsterGP ) > 0 then begin
		SetNAtt( Mon^.NA , NAG_MonsterData , NAS_MonsterGP , Random( NAttValue( Mon^.NA , NAG_MonsterData , NAS_MonsterGP ) ) + 1 );
	end else SetNAtt( Mon^.NA , NAG_MonsterData , NAS_MonsterGP , 0 );

	{ Return the monster we've made. }
	GenerateMonsterFromPrototype := Mon;
end;

Function MonsterLevel( M: GearPtr ): Integer;
	{ Return the level at which this monster may be encountered. }
	{ Some monsters have a level adjustment; they may be technically of }
	{ a lower level, but due to special abilities or stats are suitable }
	{ for higher level PCs. }
begin
	MonsterLevel := TotalLevel( M );
end;

Function ExperienceValue( M: GearPtr; PLvl: Integer ): LongInt;
	{ Return the experience value of this monster. }
	{ Base experience value is determined by total level. This gets modifed }
	{ for the monster's classes and equipment. }
const
	Class_Experience_Mod: Array [1..Num_Classes] of Integer = (
		100, 100, 100, 100, 100,
		100, 100, 100, 200, 150,
		100, 100, 100, 100, 100,
		100
	);
var
	MLvl,it: LongInt;
	I: GearPtr;
	class_mod,t: Integer;
begin
	{ Determine the experience modifier. }
	class_mod := 0;
	for t := 1 to Num_Classes do begin
		class_mod := class_mod + Class_Experience_Mod[ t ] * NAttValue( M^.NA , NAG_ClassLevels , t );
	end;
	class_mod := class_mod div TotalLevel( M );
	if class_mod <= 0 then class_mod := 100;

	MLvl := MonsterLevel( M );
	it := ( PLvl + ( MLvl * MLvl div 5 ) )  * class_mod;
	I := M^.InvCom;
	while I <> Nil do begin
		if NAttValue( I^.NA , NAG_ItemData , NAS_EquipSlot ) <> 0 then it := it + GPValue( I ) div 2;
		I := I^.Next;
	end;
	it := it div 2;

	if MLvl < PLvl then it := it div ( PLvl - MLvl + 1 )
	else if MLvl > PLvl then it := it * ( MLvl - PLvl + 1 );

	ExperienceValue := it;
end;

Function CreateMonsterList( const Context: String; MinLvl,MaxLvl: Integer ): NAttPtr;
	{ Create a list of monsters. }
	{ The list will be of the form G:0 S:[Component Index] V:[Match Weight]. }
var
	C: GearPtr;	{ A component. }
	N: Integer;	{ A counter. }
	MW: Integer;	{ The match-weight of the current component. }
	ShoppingList: NAttPtr;	{ The list of legal components. }
begin
	{ Initialize all the values. }
	ShoppingList := Nil;
	C := Standard_Monster_List;
	N := 1;

	{ Go through the list, adding everything that matches. }
	while C <> Nil do begin
		{ In order to be added to the list, this monster must be within the proper level }
		{ range. }
		if ( MonsterLevel( C ) >= MinLvl ) and ( MonsterLevel( C ) <= MaxLvl ) then begin
			MW := StringMatchWeight( SAttValue( C^.SA , 'TYPE' ) , Context );
			if MW > 0 then begin
				SetNAtt( ShoppingList , 0 , N , MW );
			end;
		end;

		Inc( N );
		C := C^.Next;
	end;

	CreateMonsterList := ShoppingList;
end;

Function MonsterContext( Scene: GearPtr ): String;
	{ Return the monster tags used by this scene. Everything sould be }
	{ preceded by a ~ to mark it as optional. }
var
	it: String;
begin
	{ Start with the climate. }
	it := '(' + SAttValue( Context_Tags , 'C_' + BStr( NAttValue( Scene^.NA , NAG_StoryData , NAS_Climate ) ) );

	{ Next add the map generator. }
	if Scene^.V = GV_Wilderness then begin
		it := it + '|' + SAttValue( Context_Tags , 'MAPGEN_' + BStr( Scene^.V ) );
	end else begin
		it := it + '|' + SAttValue( Context_Tags , 'MAPGEN_' + BStr( Scene^.V ) ) + '|' + SAttValue( Context_Tags , 'MAPGEN_D' );
	end;

	{ Add some extra weight for the enemy faction. }
	if NAttValue( Scene^.NA , NAG_StoryData , NAS_EnemyFaction ) <> 0 then begin
		it := '~' + SAttValue( Context_Tags , 'E_' + BStr( NAttValue( Scene^.NA , NAG_StoryData , NAS_EnemyFaction ) ) ) + ' ~' + SAttValue( Context_Tags , 'E_' + BStr( NAttValue( Scene^.NA , NAG_StoryData , NAS_EnemyFaction ) ) ) + ' ' + it;
	end;

	MonsterContext := it + '|EVERY) ' + GearContextTags( Scene , '~' );
end;

Function BuildEncounter( const MReq: String; Lvl,Strength: Integer ): GearPtr;
	{ Based on the monster request, build an encounter of the requested }
	{ level and strength. }
	{ MReq is the monster request. It's written in the standard REQUIRES }
	{ format... in addition, it's got the details for the scenario added }
	{ as optional tags (~) at the end. }
	{ Strength is a percent value telling roughly how many monsters to add. }
	{ It should take ten 100% Strength encounters to gain a level. }
var
	ShoppingList: NAttPtr;
	Monsters,M: GearPtr;
	Pts,MinLvl,MaxLvl,T,N: Integer;
	XPV: LongInt;
begin
	Monsters := Nil;

	{ Create the shopping list. }
	MinLvl := ( Lvl * 8 ) div 10;
	if MinLvl > 11 then MinLvl := 11;
	MaxLvl := Lvl + 3;
	ShoppingList := CreateMonsterList( MReq , MinLvl , MaxLvl );

	{ Calculate how many points we have to spend. }
	Pts := ( Lvl * 200 * Strength ) div 100;
	if Pts < Lvl then Pts := Lvl;

	while ( Pts > 0 ) and ( ShoppingList <> Nil ) do begin
		M := SelectComponentFromList( Standard_Monster_List , ShoppingList );
		XPV := ExperienceValue( M , Lvl );

		N := Pts div XPV;
		if N < 1 then N := 1
		else if ( N * XPV ) < Pts then begin
			{ We don't have quite enough points for a full monster- }
			{ roll randomly to see if we get one extra. }
			if Random( XPV ) < ( Pts - N * XPV ) then Inc( N )
			else if Random( 3 ) <> 1 then Pts := 0;
		end;

		for T := 1 to N do begin
			AppendGear( Monsters , GenerateMonsterFromPrototype( M ) );
			Pts := Pts - XPV;
		end;
	end;

	DisposeNAtt( ShoppingList );

	BuildEncounter := Monsters;
end;

Function BuildBoss( const MReq: String; Lvl: Integer ): GearPtr;
	{ Kinda like BuildEncounter, but we only want one monster. }
	{ MReq is the monster request. It's written in the standard REQUIRES }
	{ format... in addition, it's got the details for the scenario added }
	{ as optional tags (~) at the end. }
	Function SelectRandomClass( M: GearPtr ): Integer;
		{ Select a random class held by the monster in question. }
	var
		C_List: NAttPtr;
		T: Integer;
	begin
		C_List := Nil;
		for t := 1 to Num_Classes do if NAttValue( M^.NA , NAG_ClassLevels , T ) > 0 then SetNAtt( C_List , t , t , t );
		if C_List <> Nil then begin
			T := SelectRandomNAtt( C_List )^.G;
			DisposeNAtt( C_List );
		end else T := CLASS_M_Beast;
		SelectRandomClass := T;
	end;
var
	ShoppingList: NAttPtr;
	Monsters,M: GearPtr;
	N,LevDif: Integer;
begin
	Monsters := Nil;

	{ Create the shopping list. }
	ShoppingList := CreateMonsterList( MReq , Lvl - 3 , Lvl );
	if ShoppingList = Nil then ShoppingList := CreateMonsterList( MReq , 1 , Lvl );

	if ShoppingList <> Nil then begin
		{ Select one of the monsters from the list. }
		M := CloneGear( SelectComponentFromList( Standard_Monster_List , ShoppingList ) );

		{ If our monster isn't up to snuff, up its levels. }
		LevDif := Lvl - TotalLevel( M );
		if LevDif > 0 then begin
			for N := 1 to ( LevDif + 1 ) do Inc( M^.Stat[ Random( Num_Model_Stats ) + 1 ] );
			N := SelectRandomClass( M );
			AddNAtt( M^.NA , NAG_ClassLevels , N , LevDif );
		end;

		AppendGear( Monsters , GenerateMonsterFromPrototype( M ) );
		DisposeGear( M );
	end;

	DisposeNAtt( ShoppingList );

	BuildBoss := Monsters;
end;


end.
