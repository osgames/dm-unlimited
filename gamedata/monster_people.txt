%%
%%  ******************
%%  ***   PEOPLE   ***
%%  ******************
%%
%% Unless there's a reason for it, all monsters should have a minimum Strength
%% of 10 and a maximum Luck of 1.
%%
%% Humans, elves, and whatnots.
%%
%% Statline: STR TGH REF INT PIE LUC


	Monster
	name <Novice Thief>
	statline 10 10 14 10 10 1
	Move 12
	Class:Humanoid 1
	MonsterGP 25
	PhysDef 5
	SDL_Sprite <monster_people.png>
	Frame 2
	type <EVERY THIEF RENFA GREEK HUMAN>
	inv
		STC SHARP_d6
	end

	Monster
	name <Hurthling>
	statline 10 8 14 10 10 10
	Move 10
	Class:Humanoid 1
	MonsterGP 25
	PhysDef 10
	SDL_Sprite <monster_people.png>
	Frame 5
	type <EVERY THIEF>
	inv
		STC SHARP_d4
	end

	Monster
	name <Novice Warrior>
	statline 14 10 10 10 10 1
	Move 10
	Class:Humanoid 2
	MonsterGP 20
	PhysDef 5
	SDL_Sprite <monster_people.png>
	Frame 0
	type <EVERY FIGHT RENFA GREEK HUMAN>
	inv
		STC SHARP_d8
	end

	Monster
	name <Novice Mage>
	statline 10 6 10 12 12 1
	Move 12
	PhysDef -15
	Class:Humanoid 2
	MonsterGP 50
	SDL_Sprite <monster_people.png>
	Frame 1
	type <EVERY CHAOS MAGE_ GREEK HUMAN>
	inv
		STC BLUNT_d4
	end
	sub
		StdSpell Fire Arc
		StdSpell Wizard Missile
	end

	Monster
	name <Dwarf>
	statline 12 10 14 10 10 1
	Move 8
	Class:Humanoid 3
	MonsterGP 20
	SDL_Sprite <monster_people.png>
	Frame 8
	type <EVERY DUNGE EARTH NORSE>
	inv
		STC SHARP_d8
	end

	Monster
	name <Highwayman>
	statline 12 10 12 10 10 1
	Move 10
	Class:Humanoid 3
	MonsterGP 20
	SDL_Sprite <monster_people.png>
	Frame 4
	type <EVERY THIEF RENFA STEAM HUMAN>
	inv
		STC SHARP_d8
	end

	Monster
	name <Elf Thief>
	statline 10 10 14 14 10 1
	Move 10
	Class:Humanoid 4
	MonsterGP 80
	SDL_Sprite <monster_people.png>
	Frame 7
	PhysAtt 10
	PhysDef 10
	type <EVERY THIEF>
	inv
		STC SHARP_d6
	end

	Monster
	name <Bushwhacker>
	statline 12 10 10 10 10 1
	Move 10
	Class:Humanoid 5
	MonsterGP 80
	SDL_Sprite <monster_people.png>
	Frame 6
	type <EVERY THIEF RENFA HUMAN>
	inv
		STC SHARP_d10
	end

	Monster
	name <Magician>
	statline 10 7 10 14 14 1
	Move 12
	PhysDef -15
	Class:Humanoid 5
	MonsterGP 125
	SDL_Sprite <monster_people.png>
	Frame 11
	type <EVERY CHAOS MAGE_ GREEK NORSE HUMAN>
	inv
		STC BLUNT_d6
	end
	sub
		StdSpell Acid Cloud
		StdSpell Blinding Flash
	end

	Monster
	name <Conjurer>
	statline 10 7 10 15 15 1
	Move 12
	PhysDef -15
	Class:Humanoid 7
	MonsterGP 175
	SDL_Sprite <monster_people.png>
	Frame 12
	type <EVERY MAGE_ GREEK NORSE HUMAN>
	inv
		STC BLUNT_d8
	end
	sub
		StdSpell Explosion
		StdSpell Ice Ball
	end

	Monster
	name <Necromancer>
	statline 10 7 10 16 16 1
	Move 12
	PhysDef -15
	Class:Humanoid 9
	MonsterGP 225
	SDL_Sprite <monster_people.png>
	Frame 16
	type <EVERY CHAOS UNDEA MAGE_ GREEK NORSE HUMAN>
	inv
		STC BLUNT_d6
		SUB
			STC MFX_XDMG_LUNAR_d8
		END
	end
	sub
		StdSpell Frost Storm
		StdSpell Death Ray
	end


	Monster
	name <Thief>
	statline 12 10 16 10 10 1
	Move 12
	Class:Humanoid 6
	MonsterGP 150
	SDL_Sprite <monster_people.png>
	Frame 13
	type <EVERY THIEF RENFA GREEK HUMAN>
	inv
		STC SHARP_d8
	end
	sub
		STC MFX_SHURIKEN_d8
	end

	Monster
	name <Mercenary>
	statline 11 11 11 10 10 1
	Move 12
	Class:Humanoid 6
	MonsterGP 120
	SDL_Sprite <monster_people.png>
	Frame 17
	type <EVERY FIGHT RENFA NORSE STEAM SCIFA HUMAN WAR__>
	inv
		STC SHARP_d12
	end

	Monster
	name <Warrior>
	statline 16 12 10 10 10 1
	Move 10
	Class:Humanoid 7
	MonsterGP 175
	PhysAtt 5
	PhysDef 5
	SDL_Sprite <monster_people.png>
	Frame 9
	type <EVERY FIGHT RENFA GREEK NORSE HUMAN>
	inv
		STC SHARP_d10
	end

	Monster
	name <Master Warrior>
	statline 18 15 10 10 10 1
	Move 10
	Class:Humanoid 12
	MonsterGP 600
	PhysAtt 10
	PhysDef 10
	SDL_Sprite <monster_people.png>
	Frame 10
	type <EVERY FIGHT RENFA GREEK NORSE HUMAN>
	inv
		STC SHARP_2d6
	end


