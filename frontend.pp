unit frontend;
	{ Other units may sometimes want to do things that will cause graphics to change. }
	{ This unit is the frontend; anything that gets reported to the screen comes }
	{ through here first. }
{
	Dungeon Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses gears,sdl,sdl_ttf,sdlgfx,sdlmenus,texutil,gamebook,effects,sdlmap;

const
	Context_Menu_Width = 200;
	Context_Menu_Height = 150;
	Model_Status_Width = 200;
	Model_Status_Height = 40;

	Char_Sheet_Width = 250;
	Char_Sheet_Height = 450;

	L_Column_X = ScreenWidth div 2 - Char_Sheet_Width - 30;
	R_Column_X = ScreenWidth div 2 + 30;


	ZONE_CharSheet: TSDL_Rect = ( x:L_Column_X; y: 100; w: Char_Sheet_Width; h: Char_Sheet_Height );
		ZONE_2CR_LeftText: TSDL_Rect = ( X: L_Column_X; Y: 100; w: Char_Sheet_Width; h: 150 );
		ZONE_2CR_LeftMenu: TSDL_Rect = ( X: L_Column_X; Y: 266; w: Char_Sheet_Width; h: Char_Sheet_Height - 166 );
	ZONE_2CR_RightTotal: TSDL_Rect = ( X: R_Column_X; Y: 100; w: Char_Sheet_Width; h: Char_Sheet_Height );
		ZONE_2CR_RightText: TSDL_Rect = ( X: R_Column_X; Y: 100; w: Char_Sheet_Width; h: 150 );
		ZONE_2CR_RightMenu: TSDL_Rect = ( X: R_Column_X; Y: 266; w: Char_Sheet_Width; h: Char_Sheet_Height - 166 );
	ZONE_2CR_Caption: TSDL_Rect = ( X: screenwidth div 2 - 150; Y: 30; W: 300; H: 15 );

	ZONE_1CR_Total: TSDL_Rect = ( X: screenwidth div 2 - Char_Sheet_Width div 2; Y: 100; w: Char_Sheet_Width; h: Char_Sheet_Height );
		ZONE_1CR_Text: TSDL_Rect = ( X: screenwidth div 2 - Char_Sheet_Width div 2; Y: 100; w: Char_Sheet_Width; h: 150 );
		ZONE_1CR_Menu: TSDL_Rect = ( X: screenwidth div 2 - Char_Sheet_Width div 2; Y: 266; w: Char_Sheet_Width; h: Char_Sheet_Height - 166 );

	Mini_Map_Max = 102;
	Mini_Map_Width = Mini_Map_Max * 4;
	ZONE_MiniMap: TSDL_Rect = ( X: 64; Y: ( ScreenHeight - Mini_Map_Width ) div 2; W: Mini_Map_Width; H: Mini_Map_Width );
	ZONE_MiniMapMenu: TSDL_Rect = ( X: screenwidth - 250; Y: 100; W: 200; H: 400 );

Procedure DrawCharacterSheet( GB: GameBoardPtr; PC: GearPtr; IsProbe: Boolean );

Procedure TacticsRedraw( GB: GameBoardPtr; ActiveModel: GearPtr; APtoSpend: Integer );
Procedure BasicRedraw( GB: GameBoardPtr );

Function OpenPopupMenu: RPGMenuPtr;
Function QueryPopupMenu( GB: GameBoardPtr; RPM: RPGMenuPtr ): Integer;

Procedure OneColumnRedraw;
Procedure SetupOneColumnRedraw( GB: GameBoardPtr; const Caption,Message: String );

Procedure TwoColumnRedraw;
Procedure SetupTwoColumnRedraw( GB: GameBoardPtr; const Caption: String; PC: GearPtr );
Procedure AttachTwoColumnInfo( TCMenu: RPGMenuPtr; GearList: GearPtr );
Procedure SetTwoColumnInfoGear( Item: GearPtr );
Procedure TwoColumnAlert( msg: String );

Procedure TradeItemsRedraw;
Procedure SetupTradeItemsRedraw( GB: GameBoardPtr; ActiveMenu,PassiveMenu: RPGMenuPtr; PC,Container,LList: GearPtr );

Procedure Alert( GB: GameBoardPtr; msg: String );

Procedure Display_Effect_History( GB: GameBoardPtr );

Procedure EffectFrontEnd( GB: GameBoardPtr; FX,Originator: GearPtr; FX_Stencil: effect_stencil );
Procedure ApplySTCEffectToModel( GB: GameBoardPtr; const FX_Desig: String; Target: GearPtr );

Procedure OpenMiniMap( GB: GameBoardPtr );


implementation

uses uiconfig,gearparser;

var
	FE_GB: GameBoardPtr;
	FE_Caption,FE_Text: String;
	FE_PC: GearPtr;
	FE_Menu,FE_Menu2: RPGMenuPtr;
	FE_GearList: GearPtr;

	Gem_Counters,Mini_Map_Sprite,Quill_Sprite: SensibleSpritePtr;

	Mini_Map_Frame: Array [1..Mini_Map_Max,1..Mini_Map_Max] of Integer;


Function OpenPopupMenu: RPGMenuPtr;
	{ Open the context menu over the mouse position. }
var
	FE_MenuZone: TSDL_Rect;
begin
	FE_MenuZone.X := Mouse_X + 8;
	FE_MenuZone.Y := Mouse_Y + 8;
	FE_MenuZone.W := Context_Menu_Width;
	FE_MenuZone.H := Context_Menu_Height;

	if ( FE_MenuZone.X + FE_MenuZone.W ) > ( ScreenWidth - 10 ) then FE_MenuZone.X := FE_MenuZone.X - FE_MenuZone.W - 32;
	if ( FE_MenuZone.Y + FE_MenuZone.H ) > ( ScreenHeight - 10 ) then FE_MenuZone.Y := FE_MenuZone.Y - FE_MenuZone.H - 32;

	OpenPopupMenu := CreateRPGMenu( MenuItem , MenuSelect , FE_MenuZone );
end;

Procedure PopupMenuRedraw;
	{ Redraw the screen for the popup menu. }
begin
	if FE_GB <> Nil then begin
		BasicRedraw( FE_GB );
	end else begin
		PrettyPictureDisplay;
	end;
	if FE_Menu <> Nil then InfoBox( FE_Menu^.Menu_Zone );
end;

Function QueryPopupMenu( GB: GameBoardPtr; RPM: RPGMenuPtr ): Integer;
	{ Query the popup menu. }
begin
	FE_GB := GB;
	FE_Menu := RPM;
	QueryPopupMenu := SelectMenu( RPM , @PopupMenuRedraw );
end;

Function MassStr( mass: Integer ): String;
	{ Mass is measured in half pounds, so provide a string scaled to full }
	{ pounds. }
begin
	if ( mass mod 2 ) = 1 then MassStr := BStr( mass div 2 ) + '.5'
	else MassStr := BStr( mass div 2 );
end;

Procedure BriefGearStats( Item: GearPtr; MyDest: TSDL_Rect );
	{ We need to display some info about this gear. Do so in the space }
	{ provided. }
var
	Z: TSDL_Rect;
	T,N: Integer;
	Gems: Array [1..Num_Spell_Colors] of Byte;
begin
	{ Print the name. }
	QuickTextC( GearName( Item ) , MyDest , StdWhite , Game_Font );
	MyDest.Y := MyDest.Y + TTF_FontLineSkip( game_font ) + 4;
	MyDest.H := MyDest.H - TTF_FontLineSkip( game_font ) - 4;

	{ Show the special details- for items, cost + mass. }
	{ For spells, MP + gems. }
	if Item^.G = GG_Invocation then begin
		Z.Y := MyDest.Y;

		{ Fill out the array with gem requirements first. }
		for t := 1 to Num_Spell_Colors do begin
			N := SpellGemsRequired( Item , T );
			if t > 1 then N := N + Gems[ t - 1 ];
			Gems[ t ] := N;
		end;

		N := 1;
		for T := 1 to SpellGemsRequired( Item , 0 ) do begin
			Z.X := MyDest.X + t * 10;
			while ( N <= Num_Spell_Colors ) and ( Gems[ N ] < t ) do inc( N );
			DrawSprite( Gem_Counters , Z , N mod ( Num_Spell_Colors + 1 ) );
		end;
		Z.X := MyDest.X + MyDest.W - 10;
		QuickTextRJ( BStr( MPRequired( Item ) ) + 'MP' , Z , TextColor , Game_Font );
		MyDest.Y := MyDest.Y + 20;
		MyDest.H := MyDest.H - 20;
	end else if Item^.G = GG_Item then begin
		Z.Y := MyDest.Y;
		Z.X := MyDest.X + 10;
		if IsIdentified( Item ) then begin
			QuickText( BStr( GPValue( Item ) ) + 'GP' , Z , TextColor , Game_Font );
		end else begin
			QuickText( '?GP' , Z , TextColor , Game_Font );
		end;

		Z.X := MyDest.X + MyDest.W - 10;
		QuickTextRJ( MassStr( ItemMass( Item ) ) + 'lb' , Z , TextColor , Game_Font );

		MyDest.Y := MyDest.Y + TTF_FontLineSkip( game_font ) + 4;
		MyDest.H := MyDest.H - TTF_FontLineSkip( game_font ) - 4;
	end;

	{ Print the description in the space remaining. }
	GameMsg( GearDesc( Item ) , MyDest , TextColor );
end;

Procedure OneColumnRedraw;
	{ The one column redraw has no info- just one message/menu area }
	{ in the center of the screen. }
begin
	if FE_GB <> Nil then begin
		BasicRedraw( FE_GB );
	end else begin
		PrettyPictureDisplay;
	end;
	if FE_Caption <> '' then begin
		InfoBox( ZONE_2CR_Caption );
		CMessage( FE_Caption , ZONE_2CR_Caption , TextColor );
	end;
	InfoBox( ZONE_1CR_Total );
	if FE_Text <> '' then begin
		CMessage( FE_Text , ZONE_1CR_Text , TextColor );
	end;
end;

Procedure SetupOneColumnRedraw( GB: GameBoardPtr; const Caption,Message: String );
	{ Set up the one column redraw. }
begin
	FE_GB := GB;
	FE_Caption := Caption;
	FE_Text := Message;
end;

Procedure TwoColumnRedraw;
	{ Set up the display for the 2-column format. The left column will }
	{ contain a character sheet, the right column will contain a menu plus }
	{ other content, and there will be a caption at the top. }
var
	N: Integer;
	Item: GearPtr;
begin
	if FE_GB <> Nil then begin
		BasicRedraw( FE_GB );
	end else begin
		PrettyPictureDisplay;
	end;
	InfoBox( ZONE_2CR_Caption );
	CMessage( FE_Caption , ZONE_2CR_Caption , TextColor );
	InfoBox( ZONE_2CR_RightTotal );
	if ( FE_PC <> Nil ) then begin
		DrawCharacterSheet( FE_GB , FE_PC , False );
	end;
	if ( FE_Menu <> Nil ) and ( FE_GearList <> Nil ) then begin
		N := CurrentMenuItemValue( FE_Menu );
		if N > 0 then begin
			Item := RetrieveGearSib( FE_GearList , N );
			if Item <> Nil then BriefGearStats( Item , ZONE_2CR_RightText );
		end;
	end else if ( FE_GearList <> Nil ) then begin
		BriefGearStats( FE_GearList , ZONE_2CR_RightText );
	end;
end;

Procedure TwoColumnAlert( msg: String );
	{ Display this message in the ZONE_2CR_RightMenu area. }
begin
	repeat
		TwoColumnRedraw;
		GameMsg( msg , ZONE_2CR_RightMenu , TextColor );
		DoFlip;
	until IsMoreKey( RPGKey );
end;

Procedure SetupTwoColumnRedraw( GB: GameBoardPtr; const Caption: String; PC: GearPtr );
	{ Prepare the variables for the two column display. }
begin
	FE_GB := GB;
	FE_Caption := Caption;
	FE_PC := PC;
	FE_Menu := Nil;
	FE_GearList := Nil;
end;

Procedure AttachTwoColumnInfo( TCMenu: RPGMenuPtr; GearList: GearPtr );
	{ This menu corresponds to a list of gears. Display the info for it }
	{ in the upper box of the right hand column. }
begin
	FE_Menu := TCMenu;
	FE_GearList := GearList;
end;

Procedure SetTwoColumnInfoGear( Item: GearPtr );
	{ We want to show the info of one specific gear along with this menu. }
begin
	FE_Menu := Nil;
	FE_GearList := Item;
end;

Procedure DisplayEncumberanceStats( GB: GameBoardPtr; PC: GearPtr );
	{ Display the character name and encumberance level. }
var
	T: LongInt;
	Z: TSDL_Rect;
begin
	Z := ZONE_CharSheet;
	QuickTextC( GearName( PC ) , Z , TextColor , Game_Font );
	GotoNextLine( Z );
	QuickTextC( 'L' + BStr( TotalLevel( PC ) ) + ' ' + MsgString( 'GENDER_' + BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_Gender ) ) ) + ' ' + MsgString( 'SPECIES_' + BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_Species ) ) )  + ' ' + MsgString( 'CLASS_' + BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_CurrentClass ) ) ), Z , TextColor , Game_Font );
	GotoNextLine( Z );


	Z.Y := Z.Y + TTF_FontLineSkip( game_font ) * 2;
	GotoNextLine( Z );
	Z.X := ZONE_CharSheet.X;
	QuickText( MsgSTring( 'GOLDPIECES' ) , Z , TextColor , Game_Font );
	Z.X := ZONE_CharSheet.X + 205;
	if GB <> Nil then begin
		QuickTextRJ( BStr( NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold )  ) , Z , TextColor , Game_Font );
	end else begin
		QuickTextRJ( '0' , Z , TextColor , Game_Font );
	end;
	GotoNextLine( Z );
	Z.X := ZONE_CharSheet.X;
	QuickText( MsgSTring( 'ENCUMBERANCELEVEL' ) , Z , TextColor , Game_Font );
	Z.X := ZONE_CharSheet.X + 205;
	T := EncumberanceLevel( PC );
	QuickTextRJ( MassStr( InventoryMass( PC ) ) + '/' + MassStr( EncumberanceCeiling( PC , T ) ) , Z , TextColor , Game_Font );
	Z.X := ZONE_CharSheet.X + 210;
	QuickText( MsgString( 'ENCUMBERANCE_' + BStr( T ) ) , Z , TextColor , Game_Font );
end;

Procedure TradeItemsRedraw;
	{ Redraw for the trade items procedure. }
var
	N: Integer;
	Item: GearPtr;
begin
	if FE_GB <> Nil then begin
		BasicRedraw( FE_GB );
	end else begin
		PrettyPictureDisplay;
	end;
	InfoBox( ZONE_2CR_Caption );
	CMessage( FE_Caption , ZONE_2CR_Caption , TextColor );
	InfoBox( ZONE_2CR_RightTotal );
	InfoBox( ZONE_CharSheet );
	if ( FE_PC <> Nil ) then begin
		DisplayEncumberanceStats( FE_GB , FE_PC );
	end;
	if ( FE_Menu <> Nil ) and ( FE_GearList <> Nil ) then begin
		N := CurrentMenuItemValue( FE_Menu );
		if N > 0 then begin
			Item := RetrieveGearSib( FE_GearList , N );
			if Item <> Nil then BriefGearStats( Item , ZONE_2CR_RightText );
		end;
	end;
	DisplayMenu( FE_Menu2 , Nil );
end;

Procedure SetupTradeItemsRedraw( GB: GameBoardPtr; ActiveMenu,PassiveMenu: RPGMenuPtr; PC,Container,LList: GearPtr );
	{ Set up the above redraw procedure. }
begin
	FE_GB := GB;
	FE_Caption := ReplaceHash( MsgSTring( 'OpenContainer' ) , GearName( Container ) );
	FE_Menu := ActiveMenu;
	FE_Menu2 := PassiveMenu;
	FE_GearList := LList;
	FE_PC := PC;
end;

Procedure DrawCharacterSheet( GB: GameBoardPtr; PC: GearPtr; IsProbe: Boolean );
	{ Display the character sheet for this character. We're going to use a }
	{ highly realistic simulation for this. }
const
	Num_CharSheet_Comscores = 4;
	CSCS_Stat: Array [1..Num_CharSheet_Comscores] of Byte = (
		0, STAT_Reflexes, STAT_Intelligence, STAT_Piety
	);
var
	T,CS: LongInt;
	Z: TSDL_Rect;
	msg: String;
begin
	InfoBox( ZONE_CharSheet );
	Z := ZONE_CharSheet;

	QuickTextC( GearName( PC ) , Z , TextColor , Game_Font );
	GotoNextLine( Z );
	if IsProbe then begin
		QuickTextC( 'L' + BStr( TotalLevel( PC ) ) + ' ' + MsgString( 'CLASS_' + BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_CurrentClass ) ) ), Z , TextColor , Game_Font );
	end else begin
		QuickTextC( 'L' + BStr( TotalLevel( PC ) ) + ' ' + MsgString( 'GENDER_' + BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_Gender ) ) ) + ' ' + MsgString( 'SPECIES_' + BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_Species ) ) )  + ' ' + MsgString( 'CLASS_' + BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_CurrentClass ) ) ), Z , TextColor , Game_Font );
	end;
	GotoNextLine( Z );

	for t := 1 to Num_Model_Stats do begin
		GotoNextLine( Z );
		Z.X := ZONE_CharSheet.X;
		QuickText( MsgString( 'STAT_' + BStr( T ) ) + ': ' , Z , TextColor , Game_Font );
		Z.X := ZONE_CharSheet.X + 115;
		CS := CStat( PC, T );
		if CS < PC^.Stat[ t ] then QuickTextRJ( BStr( CS ) , Z , BadRed , Game_Font )
		else if CS > PC^.Stat[ t ] then QuickTextRJ( BStr( CS ) , Z , GoodGreen , Game_Font )
		else QuickTextRJ( BStr( CS ) , Z , TextColor , Game_Font );

		if t <= Num_CharSheet_Comscores then begin
			Z.X := ZONE_CharSheet.X + 130;
			QuickText( MsgString( 'COMSCORE_' + BStr( T ) ) , Z , TextColor , Game_Font );
			Z.X := ZONE_CharSheet.X + Char_Sheet_Width;
			if T = CS_PhysicalDefense then begin
				CS := BestPhysicalDefense( PC );
			end else begin
				CS := ComScore( PC, T );
			end;
			if CSCS_Stat[ t ] <> 0 then CS := CS + StatBonus( PC , CSCS_Stat[ t ] );
			QuickTextRJ( SgnStr( CS ) + '%' , Z , TextColor , Game_Font );
		end;
	end;

	Z.Y := Z.Y + TTF_FontLineSkip( game_font ) * 2;
	Z.X := ZONE_CharSheet.X;
	QuickText( MsgSTring( 'HEALTHPOINTS' ) , Z , TextColor , Game_Font );
	Z.X := ZONE_CharSheet.X + 150;
	T := MaxHP( PC );
	QuickTextRJ( BStr( T - NAttValue( PC^.NA , NAG_Damage , NAS_HPDmg )  ) + '/' + BStr( T ) , Z , TextColor , Game_Font );
	GotoNextLine( Z );
	Z.X := ZONE_CharSheet.X;
	QuickText( MsgSTring( 'MAGICPOINTS' ) , Z , TextColor , Game_Font );
	Z.X := ZONE_CharSheet.X + 150;
	T := MaxMP( PC );
	QuickTextRJ( BStr( T - NAttValue( PC^.NA , NAG_Damage , NAS_MPDmg )  ) + '/' + BStr( T ) , Z , TextColor , Game_Font );
	GotoNextLine( Z );
	if not IsProbe then begin
		{ If this is not a probing, display the PC stats. }
		Z.X := ZONE_CharSheet.X;
		QuickText( MsgSTring( 'EXPERIENCEPOINTS' ) , Z , TextColor , Game_Font );
		Z.X := ZONE_CharSheet.X + 150;
		QuickTextRJ( BStr( NAttValue( PC^.NA , NAG_CharacterData , NAS_Experience )  ) + '/' + BStr( XPNeededForNextLevel( PC ) ) , Z , TextColor , Game_Font );
		GotoNextLine( Z );
		Z.X := ZONE_CharSheet.X;
		QuickText( MsgSTring( 'GOLDPIECES' ) , Z , TextColor , Game_Font );
		Z.X := ZONE_CharSheet.X + 205;
		if GB <> Nil then begin
			QuickTextRJ( BStr( NAttValue( GB^.Camp^.Source^.NA , NAG_CampaignData , NAS_Gold )  ) , Z , TextColor , Game_Font );
		end else begin
			QuickTextRJ( '0' , Z , TextColor , Game_Font );
		end;
		GotoNextLine( Z );
		Z.X := ZONE_CharSheet.X;
		QuickText( MsgSTring( 'ENCUMBERANCELEVEL' ) , Z , TextColor , Game_Font );
		Z.X := ZONE_CharSheet.X + 205;
		T := EncumberanceLevel( PC );
		QuickTextRJ( MassStr( InventoryMass( PC ) ) + '/' + MassStr( EncumberanceCeiling( PC , T ) ) , Z , TextColor , Game_Font );
		Z.X := ZONE_CharSheet.X + 210;
		QuickText( MsgString( 'ENCUMBERANCE_' + BStr( T ) ) , Z , TextColor , Game_Font );

		Z.Y := Z.Y + TTF_FontLineSkip( game_font ) * 2;
		Z.X := ZONE_CharSheet.X;
		QuickText( MsgString( 'SPELLCOLOR_ALL' ) + ': ' , Z , TextColor , Game_Font );
		Z.X := ZONE_CharSheet.X + 175;
		QuickTextRJ( BStr( TotalFreeSpellGems( PC ) ) + '/' + BStr( TotalSpellGems( PC ) ) , Z , TextColor , Game_Font );
		for t := 1 to Num_Spell_Colors do begin
			GotoNextLine( Z );
			Z.X := ZONE_CharSheet.X + 9;
			DrawSprite( Gem_Counters , Z , t );
			Z.X := ZONE_CharSheet.X + 20;
			QuickText( MsgString( 'SPELLCOLOR_' + BStr( T ) ) + ': ' , Z , TextColor , Game_Font );
			Z.X := ZONE_CharSheet.X + 175;
			QuickTextRJ( BStr( FreeSpellGems( PC , T ) ) + '/' + BStr( SpellGemsOfColor( PC, T ) ) , Z , TextColor , Game_Font );
		end;
	end else begin
		{ If this is a probing, display the elemental resistances. }
		Z.X := ZONE_CharSheet.X + 9;
		GotoNextLine( Z );
		for t := 0 to ( Num_Elements - 1 ) do begin
			CS := ComScore( PC , Resist_X_ComScore[ t ] );
			if CS < -50 then msg := MsgSTring( 'RESIST_VeryVulnerable' )
			else if CS < 0 then msg := MsgSTring( 'RESIST_Vulnerable' )
			else if CS > 150 then msg := MsgSTring( 'RESIST_Immune' )
			else if CS > 50 then msg := MsgSTring( 'RESIST_VeryResistant' )
			else if CS > 0 then msg := MsgSTring( 'RESIST_Resistant' )
			else msg := '';
			if msg <> '' then begin
				msg := ReplaceHash( msg , MsgString( 'ELEMENT_' + BStr( t ) ) );
				QuickText( msg , Z , TextColor , Game_Font );
				GotoNextLine( Z );
			end;
		end;
	end;
end;


Procedure BasicRedraw( GB: GameBoardPtr );
	{ Redraw the screen for combat. }
begin
	RenderMap( GB );
end;

Procedure TacticsRedraw( GB: GameBoardPtr; ActiveModel: GearPtr; APtoSpend: Integer );
	{ Redraw the screen for combat. This means you have to indicate the }
	{ active model and show how many action+attack points it has left. }
const
	ZONE_AMStatus: TSDL_Rect = ( X: 24; Y: 24; W: 300; H: 15 );
var
	MyDest: TSDL_Rect;
	T,AP,APR: Integer;
begin
	BasicRedraw( GB );
	if ActiveModel <> Nil then begin
		InfoBox( ZONE_AMStatus );
		QuickText( GearName( ActiveModel ) , ZONE_AMStatus , TextColor , Game_Font );

		{ Draw the AP indicator. }
		AP := ActionPoints( ActiveModel ) - NAttValue( ActiveModel^.NA , NAG_FightingStat , NAS_ActionPointsSpent );
		APR := AP - APtoSpend;
		if AP > 12 then AP := 12;
		if AP > 0 then begin
			MyDest := ZONE_AMStatus;
			MyDest.X := ZONE_AMStatus.X + 180;
			QuickText( 'AP:' , MyDest , TextColor , Small_Font );
			for t := 1 to ( ( AP + 1 ) div 2 ) do begin
				MyDest.X := ZONE_AMStatus.X + 190 + T * 8;
				if  T <= ( ( APR + 1 ) div 2 ) then begin
					if APR >= ( T * 2 ) then DrawSprite( Counter_Sprite , MyDest , 1 )
					else DrawSprite( Counter_Sprite , MyDest , 6 );
				end else DrawSprite( Counter_Sprite , MyDest , 5 );
			end;
		end;
	end;
end;

Procedure Alert( GB: GameBoardPtr; msg: String );
	{ Display an alert until the player presses a button. }
const
	ZONE_AlertMessage: TSDL_Rect = ( x: screenwidth div 2 - 200; y: screenheight - 215 ; w: 400; h: 115 );
begin
	repeat
		BasicRedraw( GB );
		InfoBox( ZONE_AlertMessage );
		GameMsg( msg , ZONE_AlertMessage , TextColor );
		DoFlip;
	until IsMoreKey( RPGKey );
end;

Procedure ProbeModel( GB: GameBoardPtr; M: GearPtr );
	{ PROBE is a spell which shows a model's stats. Go through the game }
	{ board and display the stats of anyone who's been PROBEd. }
begin
	repeat
		BasicRedraw( GB );
		DrawCharacterSheet( GB , M , True );
		DoFlip;
	until IsMoreKey( RPGKey );

	SetNAtt( M^.NA , NAG_FightingStat , NAS_Probed , 0 );
end;

Procedure MitoseModel( GB: GameBoardPtr; M: GearPtr );
	{ This ooze has been struck with a bladed weapon- time to split. }
var
	P: Point;
	NewM: GearPtr;
	TargetHP: Integer;
begin
	{ Start by clearing the mitose flag- whether mitosis succeeds or fails }
	{ we aren't going to worry about it any more. }
	SetNAtt( M^.NA , NAG_FightingStat , NAS_ShouldMitose , 0 );
	P := FindPointNearModel( GB , M );
	if ( not TileBlocksMovement( GB , P.X , P.Y ) ) and ( FindModelXY( GB , P.X , P.Y ) = Nil ) then begin
		{ We have a nice spot for a new monster. Go to work. }
		NewM := CloneGear( M );
		AppendGear( GB^.Contents , NewM );
		SetNAtt( NewM^.NA , NAG_Location , NAS_X , P.X );
		SetNAtt( NewM^.NA , NAG_Location , NAS_Y , P.Y );

		TargetHP := ( CurrentHP( M ) + 3 ) div 2;
		SetNAtt( NewM^.NA , NAG_Damage , NAS_HPDmg , MaxHP( NewM ) - TargetHP );
		SetNAtt( M^.NA , NAG_Damage , NAS_HPDmg , MaxHP( M ) - TargetHP );
	end;
end;

Procedure DoDelayedResults( GB: GameBoardPtr );
	{ Some effects get delayed until the end of processing. This may be }
	{ for practical or aesthetic reasons. }
var
	M: GearPtr;
begin
	M := GB^.Contents;
	while M <> Nil do begin
		if ( M^.G = GG_Model ) and IsAlright( M ) then begin
			if ( NAttValue( M^.NA , NAG_FightingStat , NAS_Probed ) <> 0 ) then begin
				ProbeModel( GB , M );
			end;
			if ( NAttValue( M^.NA , NAG_FightingStat , NAS_ShouldMitose ) <> 0 ) then begin
				MitoseModel( GB , M );
			end;
		end;
		M := M^.Next;
	end;
end;

Procedure Display_Effect_History( GB: GameBoardPtr );
	{ Display all the queued animations, deleting them as we go along. }
var
	N: Integer;
	AnimOb,A2: GearPtr;
	DelayThisFrame: Boolean;
begin
	N := 0;
	AnimDelay;

	{ Keep processing until we run out of animation objects. }
	while ( Effect_History <> Nil ) and ( N < 100 ) do begin
		{ Erase all current image overlays. }
		ClearOverlays;

		AnimOb := Effect_History;

		{ Assume there'll be no animation delay, unless }
		{ otherwise requested. }
		DelayThisFrame := False;

		while AnimOb <> Nil do begin
			A2 := AnimOb^.Next;

			{ Call a routine based upon the type of }
			{ animation requested. }
			if AnimOb^.G <= N then begin
				case AnimOb^.S of

				GS_AOShot: ProcessShotAnimation( GB , Effect_History , AnimOb );
				GS_AOPoint: ProcessPointAnimation( GB , Effect_History , AnimOb );

				{ If no routine was found to deal with the animation }
				{ requested, just delete the gear. }
				else RemoveGear( Effect_History , AnimOb );
				end;

				DelayThisFrame := True;
			end;

			{ Move to the next animation. }
			AnimOb := A2;
		end;

		{ Delay the animations, if appropriate. }
		if DelayThisFrame then begin
			BasicRedraw( GB );
			DoFlip;
			AnimDelay;
		end else begin
			Inc( N );
		end;
	end;

	if Effect_History <> Nil then DisposeGear( Effect_History );

	ClearOverlays;

	{ Once the regular attack history has been dealt with, deal with the }
	{ delayed results. }
	DoDelayedResults( GB );
end;

Procedure TidyTheMap( GB: GameBoardPtr );
	{ Take the disabled models and move them off the map. }
var
	M: GearPtr;
begin
	M := GB^.Contents;
	while M <> Nil do begin
		if ( M^.G = GG_Model ) and OnTheMap( GB , M ) and not IsAlright( M ) then begin
			SetNAtt( M^.NA , NAG_Location , NAS_X , 0 );
			if ( NAttValue( M^.NA , NAG_StoryData , NAS_UniqueID ) <> 0 ) then begin
				AddTrigger( TRIGGER_ModelEliminated , NAttValue( M^.NA , NAG_StoryData , NAS_UniqueID ) );
			end;
			if ( NAttValue( M^.NA , NAG_StoryData , NAS_MobID ) <> 0 ) then begin
				AddTrigger( TRIGGER_MobEliminated , NAttValue( M^.NA , NAG_StoryData , NAS_MobID ) );
			end;
		end;
		M := M^.Next;
	end;
end;


Procedure EffectFrontEnd( GB: GameBoardPtr; FX,Originator: GearPtr; FX_Stencil: effect_stencil );
	{ This procedure will: }
	{ - Call the effect procedure. }
	{ - Display pending animations. }
	{ - Remove disabled models from the board. }
	{ This procedure will not: }
	{ - Add an "initial shot" animation if needed. }
	{ - Handle triggers. }
	{      ^- You know, these original comments from NovaCity were }
	{         oddly specific in certain ways. It's good to have clearly }
	{         delineated responsibilities, of course, it's just the }
	{         "this proc WILL NOT" declarations which seem a bit off }
	{         to me in retrospect. }
begin
	{ Prep the effect_event_order. If an initial shot was added by }
	{ anyone else, it got stuck in at effect_event_order=0. }
	effect_event_order := 1;

	{ Call the effect procedure. }
	HandleEffect( GB , FX , Originator , FX_Stencil );

	{ Display pending animations. }
	Display_Effect_History( GB );

	{ After processing an event, tidy the map by moving disabled models off }
	{ the gameboard. They're still there, just no longer on the map. }
	TidyTheMap( GB );
end;

Procedure ApplySTCEffectToModel( GB: GameBoardPtr; const FX_Desig: String; Target: GearPtr );
	{ Quickly apply an effect to a target. }
var
	FX: GearPtr;
	FX_Stencil: effect_stencil;
	X,Y: Integer;
begin
	FX := LoadNewSTC( fx_desig );
	if FX <> Nil then begin
		Clear_Effect_Stencil( GB , FX_Stencil );
		X := NAttValue( Target^.NA , NAG_Location , NAS_X );
		Y := NAttValue( Target^.NA , NAG_Location , NAS_Y );
		if OnTheMap( GB , X , Y ) then FX_Stencil[ X , Y ] := True;
		EffectFrontEnd( GB , FX , Nil , FX_Stencil );
		DisposeGear( FX );
	end;
end;

Procedure MiniMapRedraw;
	{ Redraw the screen for the minimap display. }
var
	X,Y,XMax,YMax,N,XF,YF: Integer;
	MyDest: TSDL_Rect;
	Item: GearPtr;
begin
	if FE_GB <> Nil then begin
		BasicRedraw( FE_GB );
	end else begin
		PrettyPictureDisplay;
		Exit;
	end;
	InfoBox( ZONE_MiniMapMenu );
	MapBox( ZONE_MiniMap );

	XF := -1;
	if ( FE_Menu <> Nil ) then begin
		N := CurrentMenuItemValue( FE_Menu );
		if N > 0 then begin
			Item := RetrieveGearSib( FE_GB^.Contents , N );
			if Item <> Nil then begin
				XF := NAttValue( Item^.NA , NAG_Location , NAS_X );
				YF := NAttValue( Item^.NA , NAG_Location , NAS_Y );
			end;
		end;
	end;


	XMax := FE_GB^.Map_Width;
	if XMax > Mini_Map_Max then XMax := Mini_Map_Max;
	YMax := FE_GB^.Map_Height;
	if YMax > Mini_Map_Max then YMax := Mini_Map_Max;

	for x := 1 to XMax do begin
		for y := 1 to YMax do begin
			MyDest.X := ZONE_MiniMap.X + ( X - 1 ) * 4;
			MyDest.Y := ZONE_MiniMap.Y + ( Y - 1 ) * 4;
			if Mini_Map_Frame[ X , Y ] >= 0 then begin
				DrawSprite( Mini_Map_Sprite , MyDest , Mini_Map_Frame[ X , Y ] );
			end;
		end;
	end;

	if ( XF >= 1 ) and ( XF <= XMax ) and ( YF >= 1 ) and ( YF <= YMax ) and ( ( ( Animation_Phase div 4 ) mod 2 ) = 1 ) then begin
		MyDest.X := ZONE_MiniMap.X + ( XF - 1 ) * 4 + 3;
		MyDest.Y := ZONE_MiniMap.Y + ( YF - 1 ) * 4 - 18;
		DrawSprite( Quill_Sprite , MyDest , 0 );
	end;
end;

Procedure OpenMiniMap( GB: GameBoardPtr );
	{ Display a miniature display of this level. }
var
	X,Y,XMax,YMax,N,Set_Menu_To_This: Integer;
	Thing,PC: GearPtr;
	RPM: RPGMenuPtr;
begin
	FE_GB := GB;

	{ Begin by filling the Mini_Map_Frame with terrain info. }
	XMax := GB^.Map_Width;
	if XMax > Mini_Map_Max then XMax := Mini_Map_Max;
	YMax := GB^.Map_Height;
	if YMax > Mini_Map_Max then YMax := Mini_Map_Max;
	for x := 1 to XMax do begin
		for y := 1 to YMax do begin
			if TileVisible( GB , X , Y ) then begin
				N := 0;
				if TileWall( GB , X , Y ) <> 0 then N := 2;
				if TileBlocksMovement( GB , X , Y ) then N := N + 1;
				Mini_Map_Frame[ X , Y ] := N;
			end else begin
				Mini_Map_Frame[ X , Y ] := -1;
			end;		
		end;
	end;

	{ Create the menu. }
	RPM := CreateRPGMenu( MenuItem , MenuSelect , ZONE_MiniMapMenu );

	{ Add in the extra info }
	Thing := GB^.Contents;
	PC := FirstActivePC( GB );
	Set_Menu_To_This := 0;
	N := 1;
	while Thing <> Nil do begin
		X := NAttValue( Thing^.NA , NAG_Location , NAS_X );
		Y := NAttValue( Thing^.NA , NAG_Location , NAS_Y );
		if ( X >= 1 ) and ( Y >= 1 ) and ( X <= XMax ) and ( Y <= YMax ) and TileVisible( GB , X , Y ) then begin
			if Thing^.G = GG_Prop then Mini_Map_Frame[ X , Y ] := 4
			else if Thing^.G = GG_Model then begin
				if ( Thing^.S = GS_PCTeam ) or ( Thing^.S = GS_AllyTeam ) then Mini_Map_Frame[ X , Y ] := 5
				else Mini_Map_Frame[ X , Y ] := 6;
			end;

			if Thing = PC then begin
				AddRPGMenuItem( RPM , MsgString( 'MINIMAP_PARTY' ) , N );
				Set_Menu_To_This := N;
			end else if SAttValue( Thing^.SA , 'MM_DESC' ) <> '' then begin
				AddRPGMenuItem( RPM , SAttValue( Thing^.SA , 'MM_DESC' ) , N , SAttValue( Thing^.SA , 'DESC' ) );
			end;
		end;
		Thing := Thing^.Next;
		Inc( N );
	end;

	RPMSortAlpha( RPM );
	AlphaKeyMenu( RPM );
	AddRPGMenuItem( RPM , MsgString( 'CANCEL' ) , -1 );
	if Set_Menu_To_This <> 0 then SetItemByValue( RPM , Set_Menu_To_This );

	FE_Menu := RPM;

	N := SelectMenu( RPM , @MiniMapRedraw );
	DisposeRPGMenu( RPM );

	if N > 0 then begin
		Thing := RetrieveGearSib( FE_GB^.Contents , N );
		if Thing <> Nil then begin
			X := NAttValue( Thing^.NA , NAG_Location , NAS_X );
			Y := NAttValue( Thing^.NA , NAG_Location , NAS_Y );
			FocusOnTile( GB , X , Y );
		end;
	end;
end;

initialization
	Gem_Counters := LocateSprite( 'basic_gems.png' , 10 , 16 );
	Mini_Map_Sprite := LocateSprite( 'basic_minimap.png' , 4 , 4 );
	Quill_Sprite  := LocateSprite( 'basic_quill.png' , 21 , 18 );

end.
