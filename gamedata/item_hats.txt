%%
%%  **************************************
%%  ***   DUNGEON  MONKEY  UNLIMITED   ***
%%  ***           HATS  FILE           ***
%%  **************************************
%%
%% Oh, Ivan's not going to be very happy with this file...
%%
%% Most hats provide no armor protection at all. Helmets can provide up to
%% five points.
%%

Hat
	name <Bandana>
	desc <>
	AvatarFrame 0

Hat
	name <White Headband>
	desc <>
	AvatarFrame 1

Hat
	name <Red Headband>
	desc <>
	AvatarFrame 2

Hat
	name <Blue Headband>
	desc <>
	AvatarFrame 3

Hat
	name <Tricorne Hat>
	desc <>
	AvatarFrame 4

Hat
	name <Coxcomb Hat>
	desc <>
	AvatarFrame 5

Hat
	name <Pajama Hat>
	desc <>
	AvatarFrame 6

Hat
	name <Phrygian Cap>
	desc <>
	AvatarFrame 7

Hat
	name <Blue Hat>
	desc <>
	AvatarFrame 8

Hat
	name <Green Hat>
	desc <>
	AvatarFrame 9

Hat
	name <Red Hat>
	desc <>
	AvatarFrame 10

Hat
	name <White Hat>
	desc <>
	AvatarFrame 11

Hat
	name <Yellow Hat>
	desc <>
	AvatarFrame 12

Hat
	name <Blue Hood>
	desc <>
	AvatarFrame 13

Hat
	name <Black Hood>
	desc <>
	AvatarFrame 14

Hat
	name <Green Hood>
	desc <>
	AvatarFrame 15

Hat
	name <Orange Hood>
	desc <>
	AvatarFrame 16

Hat
	name <Red Hood>
	desc <>
	AvatarFrame 17

Hat
	name <Black Hat>
	desc <>
	AvatarFrame 18

Hat
	name <Grey Wizard Hat>
	desc <>
	AvatarFrame 19

Hat
	name <Ninja Mask>
	desc <>
	AvatarFrame 20

Hat
	name <Straw Hat>
	desc <>
	AvatarFrame 21

Hat
	name <Turban>
	desc <>
	AvatarFrame 22

Hat
	name <Black Wizard Hat>
	desc <>
	AvatarFrame 23

Hat
	name <Blue Wizard Hat>
	desc <>
	AvatarFrame 24

Hat
	name <Cosmic Hat>
	desc <>
	AvatarFrame 25

Hat
	name <Traveler's Hat>
	desc <>
	AvatarFrame 26

Hat
	name <Green Wizard Hat>
	desc <>
	AvatarFrame 27

Hat
	name <Purple Wizard Hat>
	desc <>
	AvatarFrame 28

Hat
	name <Red Wizard Hat>
	desc <>
	AvatarFrame 29

Hat
	name <White Wizard Hat>
	desc <>
	AvatarFrame 30

Helm
	name <Steel Helmet>
	desc <>
	AvatarFrame 31
	PhysDef 3
	mass 5
	GPFudge 15

Helm
	name <Horned Helmet>
	desc <>
	AvatarFrame 32
	PhysDef 3
	mass 6

Helm
	name <Knight Helm>
	desc <>
	AvatarFrame 33
	PhysDef 5
	mass 9
	GPFudge 5

Helm
	name <Steel Helm>
	desc <>
	AvatarFrame 34
	PhysDef 5
	mass 9

Helm
	name <Winged Helm>
	desc <>
	AvatarFrame 35
	PhysDef 5
	mass 10

Helm
	name <Horned Helm>
	desc <>
	AvatarFrame 36
	PhysDef 5
	mass 10

Helm
	name <Iron Helm>
	desc <>
	AvatarFrame 37
	PhysDef 5
	mass 11
	GPFudge -5



