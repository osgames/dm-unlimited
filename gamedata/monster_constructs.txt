%%
%%  **********************
%%  ***   CONSTRUCTS   ***
%%  **********************
%%
%% Unless there's a reason for it, all monsters should have a minimum Strength
%% of 10 and a maximum Luck of 1.
%%
%% Golems and clockwork horrors.
%%
%% Statline: STR TGH REF INT PIE LUC

	Monster
	name <Pewter Golem>
	desc <Although the larger golems made of rock or iron are more useful, the wizards who specialize in creating these diminutive constructs are known to obsess over expanding their collections.>
	statline 10 12 12 3 3 1
	Move 10
	Class:Defender 3
	Template:Construct
	SDL_Sprite <monster_constructs.png>
	Frame 10
	type <DUNGE SILLY GOLEM>
	inv
		STC SHARP_d6
	end

	Monster
	name <Clockwork Soldier>
	desc <>
	statline 12 12 10 5 8 1
	Move 10
	Class:Humanoid 5
	Template:Construct
	SDL_Sprite <monster_constructs.png>
	Frame 11
	type <DUNGE STEAM GOLEM>
	inv
		STC SHARP_d8
	end

	Monster
	name <Living Statue>
	desc <>
	statline 14 13 8 9 9 1
	Move 10
	Class:Defender 7
	RESIST:Fire 155
	Template:Construct
	SDL_Sprite <monster_constructs.png>
	Frame 13
	type <DUNGE FIRE_ SOLAR TOMB_ CRYPT GOLEM>
	inv
		STC SHARP_d6
	end
	sub
		STC MFX_BW_FIRE_2d6
	end

	Monster
	name <Clay Golem>
	desc <>
	statline 22 16 8 5 12 1
	Move 10
	Class:Defender 11
	Template:Construct
	SDL_Sprite <monster_constructs.png>
	Frame 1
	type <DUNGE EARTH FORT_ CRYPT GOLEM>
	inv
		STC BLUNT_2d6
	end


