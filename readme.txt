
  **************************************
  ***   DUNGEON  MONKEY  UNLIMITED   ***
  **************************************

Welcome to Dungeon Monkey Unlimited. This is a combat oriented tactics style
fantasy role playing game. You guide a party of up to four characters through
a randomly generated world. Mostly you will be thwacking monsters and taking
their stuff, which we all know is the true essence of roleplaying.

If you've ever played D&D or any other fantasy RPG then you should be able to
get into this game fairly easily. If not, then you've had a deprived childhood
but it's never too late to make up for it.

  *******************************
  ***   QUICK  START  GUIDE   ***
  *******************************

Create some characters, give them some equipment, and start a campaign.
A good starting party is a fighter, a bard, a mage, and a priest.

The first thing you'll want to do is go into town and learn some spells from
the library. You may also want to check out the store, in case they have any
good equipment there.


  *********************
  ***   COMPILING   ***
  *********************

Dungeon Monkey Unlimited is compiled using FreePascal. It uses SDL, SDLText,
and SDLImage.



