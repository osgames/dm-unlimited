unit gearparser;
	{ This unit reads a text file and converts it into game }
	{ data for GearHead. Wait a minute... this isn't Younge Street! }
	{ This isn't Younge Street, man! See the GearHead docs for info }
	{ on how this unit works. }

{	Dungeone Monkey Unlimited, a tactics combat CRPG
	Copyright (C) 2010 Joseph Hewitt

	This library is free software; you can redistribute it and/or modify it
	under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 2.1 of the License, or (at
	your option) any later version.

	The full text of the LGPL can be found in license.txt.

	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
	General Public License for more details. 

	You should have received a copy of the GNU Lesser General Public License
	along with this library; if not, write to the Free Software Foundation,
	Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
}
{$LONGSTRINGS ON}

interface

uses dos,texutil,gears,uiconfig;

var
	Parser_Macros: SAttPtr;
	Standard_Equipment_List, Standard_Monster_List, Standard_Spell_List, STC_Item_List,
	Standard_Enhancement_List: GearPtr;

Function ReadGear( var F: Text; const ZZZFName: String ): GearPtr;

Function LoadNewSTC( Desig: String ): GearPtr;
Function LoadNewMonster( Name: String ): GearPtr;
Function LoadNewSpell( Name: String ): GearPtr;
Function LoadNewItem( Name: String ): GearPtr;

function SeekSibByName( LList: GearPtr; Name: String ): GearPtr;

Function LoadFile( FName,DName: String ): GearPtr;
Function LoadGearPattern( FName,DName: String ): GearPtr;
Function AggregatePattern( FName,DName: String ): GearPtr;

implementation

uses gamebook,monsters;

Const
	Recursion_Level: Integer = 0;

	SLOT_Next = 0;
	SLOT_Sub  = 1;
	SLOT_Inv  = 2;

	PARSER_MACRO_FILE = Data_Directory + 'gparser.txt';

Function ReadGear( var F: Text; const ZZZFName: String ): GearPtr;
	{F is an open file of type F.}
	{Start reading information from the file, stopping}
	{whenever all the info is read.}
	{ Note that the current implementation of this procedure contracts }
	{ out all validity checking to the CheckGearRange procedure }
	{ in gearutil.pp. }
const
	NDum: GearPtr = Nil;
var
	TheLine,cmd: String;
	it,C: GearPtr;	{IT is the total list which is returned.}
			{C is the current GEAR being worked on.}
	dest: Byte;	{DESTination of the next GEAR to be added.}
			{ 0 = Sibling; 1 = SubCom; 2 = InvCom }

{*** LOCAL PROCEDURES FOR GHPARSER ***}

Procedure InstallGear(IG_G,IG_S,IG_V: Integer);
	{Install a GEAR of the specified type in the currently}
	{selected installation location. }
begin
	{Determine where the GEAR is to be installed, and check to}
	{see if this is a legal place to stick it.}

	{Do the installing}
	{if C = Nil, we're installing as a sibling at the root level.}
	{ASSERT: If IT = Nil, then C = Nil as well.}
	if (dest = 0) or (C = Nil) then begin
		{NEXT COMPONENT}
		if C = Nil then C := AddGear(it,NDum)
		else C := AddGear(C,C^.Parent);

	end else if dest = 1 then begin
		{SUB COMPONENT}
		C := AddGear(C^.SubCom,C);
		dest := SLOT_Next;
	end else if dest = 2 then begin
		{INV COMPONENT}
		C := AddGear(C^.InvCom,C);
		dest := SLOT_Next;
	end;

	C^.G := IG_G;
	C^.S := IG_S;
	C^.V := IG_V;
end;

Procedure AssignStat( AS_Slot, AS_Val: Integer );
	{ Set stat SLOT of the current gear to value VAL. }
	{ Do nothing if there's some reason why this is impossible. }
begin
	{ Can only assign a stat if the current gear is defined. }
	if C <> Nil then begin
		{ Make sure NUM is in the allowable range. }
		if ( AS_Slot >= 1 ) and ( AS_Slot <= NumGearStats ) then begin
			C^.Stat[ AS_Slot ] := AS_Val;
		end;
	end;
end;

Procedure AssignNAtt( ANA_G, ANA_S, ANA_V: LongInt );
	{ Store the described numeric attribute in the current gear. }
	{ Do nothing if there's some reason why this is impossible. }
begin
	{ Can only store info if the current gear is defined. }
	if C <> Nil then begin
		SetNAtt( C^.NA , ANA_G , ANA_S , ANA_V );
	end;
end;

Procedure CheckMacros( CM_CMD: String );
	{ This command wasn't found in the list of basic commands. }
	{ Maybe it's part of the macro file? Check and see. }
	Function GetMacroValue( var GMV_FX: String ): LongInt;
		{ Extract a value from the macro string, or read it }
		{ from the current line. }
	var
		GMV_MacDat,GMV_LineDat: String;
	begin
		{ First, read the data definition. }
		GMV_MacDat := ExtractWord( GMV_FX );

		{ If the first character is a ?, this means that we should }
		{ read the value from the regular line. }
		if ( GMV_MacDat <> '' ) and ( GMV_MacDat[1] = '?' ) then begin
			{ If the data is found, return it. }
			{ Otherwise return the macro default value. }
			GMV_LineDat := ExtractWord( TheLine );
			if GMV_LineDat <> '' then begin
				GetMacroValue := ExtractValue( GMV_LineDat );
			end else begin
				DeleteFirstChar( GMV_MacDat );
				GetMacroValue := ExtractValue( GMV_MacDat );
			end;
		end else begin
			GetMacroValue := ExtractValue( GMV_MacDat );
		end;
	end;
var
	CM_FX: String;
	CM_A, CM_B, CM_C: LongInt;
begin
	CM_FX := SAttValue( Parser_Macros , CM_CMD );

	{ If a macro matching this command was found, process it. }
	if CM_FX <> '' then begin
		CM_CMD := UpCase( ExtractWord( CM_FX ) );

		if CM_CMD[1] = 'G' then begin
			CM_A := GetMacroValue( CM_FX );
			CM_B := GetMacroValue( CM_FX );
			CM_C := GetMacroValue( CM_FX );
			InstallGear( CM_A , CM_B , CM_C );

		end else if CM_CMD[1] = 'S' then begin
			CM_A := GetMacroValue( CM_FX );
			CM_B := GetMacroValue( CM_FX );
			AssignStat( CM_A , CM_B );

		end else if CM_CMD[1] = 'N' then begin
			CM_A := GetMacroValue( CM_FX );
			CM_B := GetMacroValue( CM_FX );
			CM_C := GetMacroValue( CM_FX );
			AssignNAtt( CM_A , CM_B , CM_C );

		end;

	end else if CM_CMD <> '' then begin
		WriteLn( 'ERROR: Command ' + UpCase( CM_CMD ) + ' not found in ' + ZZZFName );

	end;
end;

Procedure CMD_Sub;
	{Set the destination to SUBCOMPONENT}
begin
	dest := SLOT_Sub;
end;

Procedure CMD_Inv;
	{Set the destination to INVCOMPONENT}
begin
	dest := SLOT_Inv;
end;

Procedure CMD_End;
	{Finish off a range of subcomponents.}
	{If Dest = 0, move to the parent gear.}
	{If Dest <> 0, set Dest to 0.}
begin
	if (dest = 0) and (C <> Nil) then C := C^.Parent
	else dest := 0;
end;


Procedure CMD_Size;
	{Set the V field of the current module to the supplied value.}
var
	CS_Size: Integer;
begin
	CS_Size := ExtractValue(TheLine);

	if C <> Nil then C^.V := CS_Size;
end;

Procedure CMD_DamageDice;
	{We've got some damage to explain.}
var
	CD_P,CD_DieN,CD_DieD: Integer;
	CD_Exp,CD_NStr,CD_DStr: String;
begin
	{ This is gonna involve cutting this string into bits, I'm afraid. }
	CD_Exp := ExtractWord( TheLine );
	CD_P := Pos( 'D', CD_Exp );
	if CD_P > 1 then begin
		CD_NStr := Copy( CD_Exp , 1 , CD_P - 1 );
		CD_DStr := Copy( CD_Exp , CD_P + 1 , Length( CD_Exp ) );
		CD_DieN := ExtractValue( CD_NStr );
		CD_DieD := ExtractValue( CD_DStr );
	end else begin
		CD_DieN := 1;
		if ( CD_Exp <> '' ) and ( CD_Exp[1] = 'D' ) then DeleteFirstChar( CD_Exp );
		CD_DieD := ExtractValue( CD_Exp );
	end;

	if CD_DieN < 1 then CD_DieN := 1;
	if CD_DieD < 2 then CD_DieD := 2;
	if C <> Nil then begin
		C^.Stat[ STAT_WeaponN ] := CD_DieN;
		C^.Stat[ STAT_WeaponD ] := CD_DieD;
	end;
end;

Procedure CMD_StatLine;
	{ Read all the stats for this gear from a single line. }
var
	CSL_N,CSL_V: Integer;
begin
	{ Error Check! }
	if C = Nil then Exit;

	CSL_N := 1;
	while ( TheLine <> '' ) and ( CSL_N <= NumGearStats ) do begin
		CSL_V := ExtractValue( TheLine );
		C^.Stat[CSL_N] := CSL_V;
		Inc( CSL_N );
	end;
end;

Procedure META_InsertPartIntoIt( IPII_Part: GEarPtr );
	{ Insert a part into the current gear-being-loaded by the }
	{ method specified in the dest variable. Afterwards, set C }
	{ to equal this new part. }
begin
	if IPII_Part <> Nil then begin
		{ Stick the part somewhere appropriate. }
		if (dest = 0) or (C = Nil) then begin
			{NEXT COMPONENT}
			{ If there is no currently defined C component, }
			{ stick the NPC as the next component at root level. }
			if C = Nil then LastGear( it )^.Next := IPII_Part
			else begin
				LastGear( C )^.Next := IPII_Part;
				IPII_Part^.Parent := C^.Parent;
			end;
		end else if dest = 1 then begin
			{SUB COMPONENT}
			InsertSubCom( C , IPII_Part );
		end else if dest = 2 then begin
			{INV COMPONENT}
			InsertInvCom( C , IPII_Part );
		end;
		dest := SLOT_Next;

		{ Set the current gear to the item's base record. }
		{ Any further modifications will be done there. }
		C := IPII_Part;
	end;
end;


Procedure CMD_STC;
	{ Search for & then duplicate one of the standard item }
	{ archetypes, inserting it in the gear-under-construction at }
	{ the standard insertion point. }
var
	STC_Part: GearPtr;
begin
	{ Item cannot be the first gear in a list!!! }
	if it = Nil then Exit;

	{ Clone the item record first. }
	{ Exit the procedure if no appropriate item can be found. }
	DeleteWhiteSpace( TheLine );
	STC_Part := LoadNewSTC( TheLine );
	if STC_Part = Nil then begin
		WriteLn( 'ERROR: STC Pattern ' + UpCase( TheLine ) + ' not found in ' + ZZZFName );
		TheLine := '';
		Exit;
	end;

	{ Get rid of the item name, so the parser doesn't try }
	{ interpreting it as a series of commands. }
	TheLine := '';

	{ Stick the item somewhere appropriate. }
	META_InsertPartIntoIt( STC_Part );
end;

Procedure CMD_STDSPELL;
	{ Search for & then duplicate one of the standard spells, }
	{ inserting it in the gear-under-construction at }
	{ the standard insertion point. }
var
	STC_Part: GearPtr;
begin
	{ Item cannot be the first gear in a list!!! }
	if it = Nil then Exit;

	{ Clone the item record first. }
	{ Exit the procedure if no appropriate item can be found. }
	DeleteWhiteSpace( TheLine );
	STC_Part := LoadNewSpell( TheLine );
	if STC_Part = Nil then begin
		Exit;
	end;

	{ Get rid of the item name, so the parser doesn't try }
	{ interpreting it as a series of commands. }
	TheLine := '';

	{ Stick the item somewhere appropriate. }
	META_InsertPartIntoIt( STC_Part );
end;

Procedure CMD_MOBMONSTER;
	{ Search for & then duplicate one of the standard monster }
	{ archetypes, inserting it in the gear-under-construction at }
	{ the standard insertion point. }
var
	STC_Part: GearPtr;
begin
	{ Item cannot be the first gear in a list!!! }
	if it = Nil then Exit;

	{ Clone the item record first. }
	{ Exit the procedure if no appropriate item can be found. }
	DeleteWhiteSpace( TheLine );
	STC_Part := LoadNewMonster( TheLine );
	if STC_Part = Nil then begin
		Exit;
	end;

	{ Get rid of the item name, so the parser doesn't try }
	{ interpreting it as a series of commands. }
	TheLine := '';

	{ Stick the item somewhere appropriate. }
	META_InsertPartIntoIt( STC_Part );
end;
begin
	{ Initialize variables. }
	it := Nil;
	C := Nil;
	dest := SLOT_Next;

	{ Increase the recursion level; the NPC command uses recursion }
	{ and could get stuck in an endless loop. }
	Inc( Recursion_Level );

	while not EoF(F) do begin
		{Read the line from disk, and delete leading whitespace.}
		readln(F,TheLine);
		DeleteWhiteSpace(TheLine);

		if ( TheLine = '' ) or ( TheLine[1] = '%' ) then begin
			{ *** COMMENT *** }
			TheLine := '';

		end else if Pos('<',TheLine) > 0 then begin
			{ *** STRING ATTRIBUTE *** }
			if C <> Nil then SetSAtt(C^.SA,TheLine);

		end else begin
			{ *** COMMAND LINE *** }

			{To make things easier upon us, just set the whole}
			{line to uppercase now.}
			TheLine := UpCase(TheLine);

			{Keep processing the line until the end is reached.}
			while TheLine <> '' do begin
				CMD := ExtractWord(TheLine);

				{Branch depending upon what the command is.}
					if CMD = 'SUB' then CMD_Sub
				else	if CMD = 'INV' then CMD_Inv
				else	if CMD = 'END' then CMD_End
				else	if CMD = 'SIZE' then CMD_Size
				else	if CMD = 'STATLINE' then CMD_StatLine
				else	if CMD = 'DAMAGE' then CMD_DamageDice
				else	if CMD = 'STC' then CMD_STC
				else	if CMD = 'STDSPELL' then CMD_STDSPELL
				else	if CMD = 'MOBMONSTER' then CMD_MOBMONSTER
				else	CheckMacros( CmD );
			end;
		end;
	end;

	{ Decrement the recursion level. }
	Dec( Recursion_Level );

	ReadGear := it;
end;

Function LoadFile( FName,DName: String ): GearPtr;
	{ Open and load a text file. }
var
	F: Text;
	it: GearPtr;
begin
	{ Use FSEARCH to confirm the file name. }
	FName := FSearch( FName , DName );
	it := Nil;
	if FName <> '' then begin
		{ The filename has been found and confirmed. }
		{ Actually load the file. }
		Assign( F , FName );
		Reset( F );
		it := ReadGear( F , FName );
		Close( F );
	end;
	LoadFile := it;
end;

Function LoadGearPattern( FName,DName: String ): GearPtr;
	{ Attempt to load a gear file from disk. Search for }
	{ pattern matches. }
var
	FList: SAttPtr;
	it: GearPtr;
begin
	it := Nil;
	if FName <> '' then begin
		{ Build search list for files that match the source. }
		FList := CreateFileList( DName + FName );

		if FList <> Nil then begin
			FName := SelectRandomSAtt( FList )^.Info;
			DisposeSAtt( FList );

			it := LoadFile( FName , DName );
		end;
	end;

	{ Return the selected & loaded gears. }
	LoadGearPattern := it;
end;

Function AggregatePattern( FName,DName: String ): GearPtr;
	{ Search for the given pattern. Then, load all files that match }
	{ the pattern and concatenate them together. Did I spell that }
	{ right? Probably not. }
var
	FList,F: SAttPtr;
	part,it: GearPtr;
begin
	it := Nil;
	if FName <> '' then begin
		{ Build search list for files that match the pattern. }
		FList := CreateFileList( DName + FName );
		F := FList;

		while F <> Nil do begin
			part := LoadFile( F^.Info , DName );
			AppendGear( it , part );
			F := F^.Next;
		end;
		DisposeSAtt( FList );
	end;

	{ Return the selected & loaded gears. }
	AggregatePattern := it;
end;

Function LoadNewSTC( Desig: String ): GearPtr;
	{ This function will load the STC parts list and }
	{ return an item of the requested type. }
var
	Item: GearPtr;
begin
	{ Attempt to load the item from the standard items file. }
	if STC_Item_List = Nil then Exit( Nil );

	Item := CloneGear( SeekGearByDesig( STC_Item_List , Desig ) );

	{ Return the finished product. }
	LoadNewSTC := Item;
end;

function SeekSibByName( LList: GearPtr; Name: String ): GearPtr;
	{ Seek a gear with the provided full name. If no such gear is }
	{ found, return NIL. }
var
	it: GearPtr;
begin
	it := Nil;
	Name := UpCase( Name );
	while ( LList <> Nil ) and ( it = Nil ) do begin
		if UpCase( GearName( LList ) ) = Name then it := LList;
		LList := LList^.Next;
	end;
	SeekSibByName := it;
end;


Function LoadNewMonster( Name: String ): GearPtr;
	{ This function will check the standard items list }
	{ for a gear with the provided full name. }
var
	Proto,Monster: GearPtr;
begin
	{ Attempt to load the item from the standard items file. }
	if Standard_Monster_List = Nil then Exit( Nil );

	Proto := SeekSibByName( Standard_Monster_List , Name );
	if Proto <> Nil then Monster := GenerateMonsterFromPrototype( Proto )
	else Monster := Nil;

	{ Return the finished product. }
	LoadNewMonster := Monster;
end;

Function LoadNewSpell( Name: String ): GearPtr;
	{ This function will check the standard spells list }
	{ for a gear with the provided full name. }
var
	Proto,Item: GearPtr;
begin
	{ Attempt to load the item from the standard items file. }
	if Standard_Spell_List = Nil then Exit( Nil );

	Proto := SeekSibByName( Standard_Spell_List , Name );
	if Proto <> Nil then Item := CloneGear( Proto )
	else Item := Nil;

	{ Return the finished product. }
	LoadNewSpell := Item;
end;

Function LoadNewItem( Name: String ): GearPtr;
	{ This function will check the standard items list }
	{ for a gear with the provided full name. }
var
	Proto,Item: GearPtr;
begin
	{ Attempt to load the item from the standard items file. }
	if Standard_Equipment_List = Nil then Exit( Nil );

	Proto := SeekSibByName( Standard_Equipment_List , Name );
	if Proto <> Nil then Item := CloneGear( Proto )
	else Item := Nil;

	{ Return the finished product. }
	LoadNewItem := Item;
end;

Procedure CreateStoryContextMacros;
	{ Y'know there's a bunch of story context tags, right? Well, we want }
	{ to convert these to macros. }
var
	CT: SAttPtr;
	m_value: String;
	m_label: Char;
	T: Integer;
begin
	{ Go through the context tags. The format for the macro will be: }
	{     [label]:[tag] <NAtt NAG_StoryData [NAS_ContextWhatever] [value]>     }
	{ So, to set the climate to Snowy in a subplot, you'll be able to }
	{ just say C:SNOWY and away to go. }
	CT := Context_Tags;
	while CT <> Nil do begin
		{ Not all context tags are things we wanna use! Stick to the }
		{ ones with a one-character label. }
		if ( Length( CT^.Info ) > 2 ) and ( CT^.Info[ 2 ] = '_' ) then begin
			m_value := Copy( RetrieveAPreamble( CT^.Info ) , 3 , Length( CT^.Info ) );
			m_label := UpCase( CT^.Info[ 1 ] );

			for t := 1 to Num_Context_Descriptors do if m_label = Context_Head[ t ] then begin
				m_value := 'NAtt ' + BStr( NAG_StoryData ) + ' ' + BStr( CDIndex[ t ] ) + ' ' + m_value;
				break;
			end;

			SetSAtt( Parser_Macros , m_label + ':' + RetrieveAString( CT^.Info ) + ' <' + m_value + '>' );
		end;
		CT := CT^.Next;
	end;
end;


initialization
	Parser_Macros := LoadStringList( Parser_Macro_File );

	STC_Item_List := AggregatePattern( 'stc_*.txt' , Data_Directory );
	Standard_Spell_List := AggregatePattern( 'spell_*.txt' , Data_Directory );
	Standard_Equipment_List := AggregatePattern( 'item_*.txt' , Data_Directory );
	Standard_Monster_List := AggregatePattern( 'monster_*.txt' , Data_Directory );
	Standard_Enhancement_List := AggregatePattern( 'magic_*.txt' , Data_Directory );

	CreateStoryContextMacros;

finalization
	DisposeSAtt( Parser_Macros );

	DisposeGear( STC_Item_List );
	DisposeGear( Standard_Equipment_List );
	DisposeGear( Standard_Monster_List );
	DisposeGear( Standard_Spell_List );
	DisposeGear( Standard_Enhancement_List );

end.
